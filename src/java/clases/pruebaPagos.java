/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import Conexion.conexion;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Pochii
 */
public class pruebaPagos {
   /**Propiedades de los pagos**/
    int    idPago;
    double monto;
    double monto_auxiliar;                  //En un momento, tendra el valor del monto sobrante (en caso que pague mas)
    
    String codigoPrestamo;                  //Codigo de prestamo BD
    
    double intereses;                       //Intereses a pagar BD
    double interes_moratorio;               //Mora a pagar BD
    double capital;                         //Capital a pagar BD
    
    double pago_interes;                    //Abono de intereses BD
    double pago_interes_moratorio;          //Abono a mora BD
    double pago_capital;                    //Abono a capital BD
    
    double mpago_interes;                   //Parte del monto que va a intereses
    double mpago_interes_moratorio;         //Parte del monto que va a intereses moratorios
    double mpago_capital;                   //Parte del monto que va a capital
    
    /**Propiedades de base de datos**/
    conexion con;
    ResultSet rs;
    CachedRowSetImpl crPagos;
    
    public pruebaPagos(){
        rs = null;
        con = null;
    }
    /**
     * @param pidPago id del pago a procesar
     * @param pmonto  Monto a abonar
     */
    public pruebaPagos(int pidPago, double pmonto){
        this.monto  = pmonto;
        this.idPago = pidPago;
        crPagos = null;
    }
    /**
     * Funcion para llenar las propiedades de la clase con los datos de la base de datos
     * @return Verdadero en caso que la seleccion se hiciera correctamente, falso en caso contrario
     * @throws java.sql.SQLException
     */
    public boolean llenarDatos(String idPago) throws SQLException{
        try{
            this.crPagos = getPagos("WHERE idPago = " + idPago + " AND estado != 'Pagado'");
            while(crPagos.next()){   
                this.codigoPrestamo         = crPagos.getString("codigoPrestamo");
                this.capital                = crPagos.getDouble("capital");
                this.intereses              = crPagos.getDouble("intereses");
                this.capital                = crPagos.getDouble("capital");
                this.interes_moratorio      = crPagos.getDouble("interes_moratorio");
                this.pago_interes           = crPagos.getDouble("pago_interes");
                this.pago_interes_moratorio = crPagos.getDouble("pago_interes_moratorio");
                this.pago_capital           = crPagos.getDouble("pago_capital");    
            }
            return true;
        }
        catch(SQLException e){
            System.out.println("Hubo un error: " + e);
            return false;
        } //fin catch
    }//fin llenar Datos
    /**
     * @param condicion filtrado de la query de pagos, puede ser un where, order by,group by
     * @return Resultset con los datos consultados
     * @throws java.sql.SQLException
     */
    public CachedRowSetImpl getPagos(String condicion) throws SQLException{
        CachedRowSetImpl crs = new CachedRowSetImpl();
        String query = "SELECT (@rownum:=@rownum+1) as fila, idPago, codigoPrestamo, comprobante, monto, fechaPago, fechaEstablecida," +
                       "intereses, capital, estado, pago_interes, pago_capital, interes_moratorio, pago_interes_moratorio, saldo, mora " +
                       "FROM (SELECT @rownum:=-1)r,pagos ";
        /**Agredando condicion a la query**/
        query += condicion;
        try{
            con  = new conexion();
            con.setRS(query);
            rs = con.getRs();
            crs.populate(rs);
            return crs;
        }
        catch(SQLException e){
            System.out.println("query: " + query + "\n");
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        finally{
            con.cerrarConexion();
        }
        
    }//fin getPagos()
    
    /**
     * Imprimir
     */
    public void imprimir(){
        System.out.println("Monto: $" + this.monto);
        System.out.println("Parte del monto a capital: " + this.mpago_capital);
        System.out.println("Parte del monto a intereses: " + this.mpago_interes);
        System.out.println("Parte del monto a interes_moratorio: " + this.mpago_interes_moratorio);
    }
    /**
     * Funcion para separar el monto obtenido
     */
    public void desglosarMonto(){
        monto_auxiliar = this.monto;
        if(monto_auxiliar >= (interes_moratorio - pago_interes_moratorio))
        {
            mpago_interes_moratorio = (interes_moratorio - pago_interes_moratorio);
            monto_auxiliar -= mpago_interes_moratorio;
            if(monto_auxiliar >= (intereses - pago_interes))
            {
                mpago_interes = (intereses - pago_interes);
                monto_auxiliar -= mpago_interes;
                if(monto_auxiliar >= (capital - pago_capital))
                {
                    mpago_capital = (capital - pago_capital);
                    monto_auxiliar -= mpago_capital;
                }
                else{ //Si no cubre el capital
                    mpago_capital = monto_auxiliar;
                    monto_auxiliar = 0;
                }//fin if capital
            }
            else{ //Si no cubre intereses normales
                mpago_interes = monto_auxiliar;
                monto_auxiliar = 0;
            } //fin else interes
        }
        else{ //Si no cubre intereses moratorios
            mpago_interes_moratorio = monto_auxiliar;
            monto_auxiliar = 0;
        } //fin if interes moratorio
    }
    
    public String generarComprobanteAbono(String idPago) throws SQLException
    {
        int correlativo=0;
        String comprobanteAnt="";
        String comprobantePago="";
       // obtener los abonos anteriores a dicho pago
        String query = "select p.comprobante as Cpago,a.comprobante  as Cabono from pagos as p\n" +
                        "inner join abonos as a on p.idPago = a.idPago where\n" +
                        "p.idPago=" + idPago+" order by Cabono DESC limit 1;";
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        if (rs.next())
        {
            comprobanteAnt = rs.getString("Cabono");
            comprobantePago = rs.getString("Cpago");
        }
        else
        {
            query ="select comprobante from pagos where idPago=" + idPago;
            con.setRS(query);
            rs = con.getRs();
            if (rs.next())
                comprobanteAnt = rs.getString("comprobante") + "00";
                comprobantePago = rs.getString("comprobante");
        }
        con.cerrarConexion();
        // obtener correlativo del abono
        correlativo = Integer.parseInt(comprobanteAnt.substring(6,8));
        correlativo++;
        if  (correlativo < 10)
            return (comprobantePago + "0" + correlativo);
        else
            return (comprobantePago + correlativo);
    }
    
    public boolean insertarAbono(String idPago, Double monto) throws SQLException
    {
        String comprobante = generarComprobanteAbono(idPago);
       // JOptionPane.showMessageDialog(null, comprobante);
        String query = "";
        this.monto = monto;
        desglosarMonto();
        diferenciaDias dd = new diferenciaDias();
        String diaActual = dd.fechaActual();
        query = "insert into abonos(idPago,monto,fechaPago,pago_interes,pago_capital,pago_interes_moratorio,comprobante) "
                + "values("+idPago+","+monto+",'"+diaActual+"',"+mpago_interes+","+mpago_capital+","+mpago_interes_moratorio+",'"+comprobante+"');";
        conexion con = new conexion();
       // JOptionPane.showMessageDialog(null, query);
        boolean estado = con.actualizar(query);
        con.cerrarConexion();
        return estado;
    }
    
    public List<Double> sumaAbono(String idPago) throws SQLException
    {
        List<Double> SUMAbono = new ArrayList<Double>();
        String query ="select sum(pago_interes) as interes, sum(pago_interes_moratorio) as moratorio"
                + " ,sum(pago_capital) as capital from abonos where idPago=" + idPago;
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        SUMAbono.add(0, rs.getDouble("moratorio"));
        SUMAbono.add(1, rs.getDouble("interes"));
        SUMAbono.add(2, rs.getDouble("capital"));
        con.cerrarConexion();
        return SUMAbono;
    }
    
    private String getPagoSiguiente(String codPrestamo) throws SQLException{
        try{
            String id = "";
            con = new conexion();
            con.setRS("SELECT idPago FROM pagos WHERE estado!='Pagado' AND codigoPrestamo='" + codigoPrestamo + "' ORDER BY idPago ASC LIMIT 1");
            rs = con.getRs();
            if(rs.next())
                id =  rs.getString("idPago");
            else
                id = "0";
            return id;
        }
        catch(SQLException e){
            return "0";
        }
        finally{
            rs.close();
            con.cerrarConexion();   
        }
    } //fin getPagoSiguiente
    /**
     * 
     * @param tope            Numero de pagos a ingresar
     * @param fechaInicial    Fecha a partir de la cual se tomaran los pagos
     * @param nuevoSaldo      Saldo a insertar en el pago
     * @param codigoP         Codigo del prestamo 
     * @param fechaPago       Fecha que se tomara como pago del prestamo
     * @throws SQLException 
     */
    public static void nuevoPago(int tope, String fechaInicial, double nuevoSaldo, String codigoP, String fechaPago) throws SQLException{
        int ultimoPago = 0;
        String fechaE = ""; //Fecha establecida para el pago siguiente
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String[] split = fechaInicial.split("-");
        String comprobante = "0";         //Dato que almacenara el comprobante
        Calendar c = Calendar.getInstance();
        conexion Con = new conexion();
        c.set(Integer.parseInt(split[0]), Integer.parseInt(split[1])-1, Integer.parseInt(split[2]));
         
        Con.setRS("SELECT comprobante FROM pagos ORDER BY comprobante DESC LIMIT 1");
        ResultSet Rs = Con.getRs();

        if(Rs.next())
            ultimoPago = Rs.getInt("comprobante");
        /**
         * Llenando la tabla pagos
         */
        for(int i = 1; i <= tope; i++)
        {
            ultimoPago++; //Sumando al comprobante
            c.add(Calendar.MONTH, 1);
            fechaE = formato.format(c.getTime()); 
            comprobante = String.format("%06d", ultimoPago); 
            if (i == 1)
               Con.actualizar("INSERT INTO pagos(codigoPrestamo,comprobante,fechaEstablecida,fechaPago, estado,saldo) VALUES('" + codigoP + "','"
                            + comprobante + "','" + fechaE +"','" + fechaPago + "','Pendiente', " + nuevoSaldo+  ");");
            else 
            {
                Con.actualizar("INSERT INTO pagos(codigoPrestamo,comprobante,fechaEstablecida,fechaPago, estado) VALUES('" + codigoP + "','"
                            + comprobante + "','" + fechaE +"','" + fechaPago + "','Pendiente');");
            }
            
        }//fin for
        Con.cerrarConexion();
    }//fin nuevo Pago
    
    private boolean finalizarPrestamo() throws SQLException{
        try{
            con = new conexion();
            con.actualizar("UPDATE prestamos SET estado = 'Pagado' WHERE codigoPrestamo =  '" + this.codigoPrestamo + "'");
            
            return true;
        }
        catch(SQLException e){
            return false;
        }
        finally{
            con.cerrarConexion();   
        }
    } //fin finalizar prestamo
    /**
    * FUNCIONES NECESARIAS
    * 1. Generar comprobante de abono
    * 2. Generar abonos
    * 3. comprobar pago (Verificar si el abono que se acaba de realizar, cubre la cuota)
    * 4. registrarPago (Donde se cambiarían los datos de la tabla padre: Pagos)
    * 5. imprimirAbono 
    **/
   
    /***POR HACER HOY**
     * 1. Crear funcion para crear nueva fila de pagos (Editando cambiarEstado.jsp)    - Ale
     * 2. Crear funcion que actualice el pago a Abonado o Pagado                       - Jorge   
     *    2.1 Se debe llamar a getPagoSiguiente()                       
     *    2.2 Se debe llamar a llenarDatos()
     *    2.3 llamar a ingresarAbono();         
     * 3. Crear funcion para actualizar saldo del prestamo                              - Jorge
     * 4. Crear funcion que verifique si la actualizacion anterior cancela el prestamo  - Ale
     *    4.1 Verificar si saldo EN PRESTAMO es igual a CERO
     *    4.2 Redireccion a pagina para impresion de documento
     *    
     * i. Crear funcion para eliminarAbono (Maniana)
    **/

}
