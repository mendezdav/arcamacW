/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "ReportePrestamosPorPlazo", urlPatterns = {"/ReportePrestamosPorPlazo"})
public class ReportePrestamosPorPlazo extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, JRException {
         ServletOutputStream out = response.getOutputStream();
            
     try {
         JasperReport reporte = (JasperReport)
         JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/prestamos_por_plazo.jasper"));
         //Cargamos parametros del reporte (si tiene).
         Map parameters = new HashMap();
         List<Object> reports = new LinkedList<Object>();
         /**
          * Obteniendo parametros
          */
         String no_meses =request.getParameter("meses");
         
         String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");  
         /**
          * Estableciendo query
          */
         String query  = "SELECT nombreSolicitante, estadoPrestamo, codigoPrestamo, cantidad, plazo,cuotas, saldo, fechaAprobacion, fechaVencimiento, "
                        + " fechaEmision, fechaFinalizacion, tipoPago, destinoPrestamo, tasaInteres FROM viewdetalleprestamo WHERE plazo = '" + no_meses + "'";
         conexion con = new conexion();
         con.setRS(query);
         ResultSet rs = con.getRs();
         while(rs.next()){
             BeanDetallePrestamo detalle = new BeanDetallePrestamo();
             detalle.setCantidad(rs.getInt("cantidad"));
             detalle.setNombreSolicitante(rs.getString("nombreSolicitante"));
             detalle.setCodigoPrestamo(rs.getString("codigoPrestamo"));
             detalle.setEstadoPrestamo(rs.getString("estadoPrestamo"));
             detalle.setPlazo(rs.getInt("plazo"));
             detalle.setCuota(rs.getDouble("cuotas"));
             detalle.setSaldoPendiente(rs.getDouble("saldo"));
             detalle.setFechaAprobacion(rs.getString("fechaAprobacion"));
             detalle.setFechaSolicitud(rs.getString("fechaEmision"));
             detalle.setFechaFinalizacion(rs.getString("fechaFinalizacion"));
             detalle.setFechaVencimiento(rs.getString("fechaVencimiento"));
             detalle.setTipoPago(rs.getString("tipoPago"));
             detalle.setTasaInteres(rs.getDouble("tasaInteres"));
             detalle.setImg_url(rutaImg);
             
             reports.add((Object) detalle);
         }
         
         String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
         if(reports.isEmpty())
         {
              // redireccion al panel principal
           response.sendRedirect(ruta + "/reportes/panelReportes.jsp?erno=4");
         }
         else{
            //fill the ready report with data and parameter
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
            JRExporter exporter = null;
            response.setContentType("application/pdf");
               //Nombre del reporte
               response.setHeader("Content-Disposition","inline; filename=\"Prestamos.pdf\";");
               //Exportar a pdf
               exporter = new JRPdfExporter();
               exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
               exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
               //view the report using JasperViewer
               exporter.exportReport();
            }
          
         } catch (JRException e) {
            e.printStackTrace();
        }
     }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReportePrestamosPorPlazo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportePrestamosPorPlazo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReportePrestamosPorPlazo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportePrestamosPorPlazo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
