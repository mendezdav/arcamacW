/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;
/**
 *
 * @author jorge
 */
@WebServlet(name = "reporteSolicitud_fiadores", urlPatterns = {"/reporteSolicitud_fiadores"})
public class reporteSolicitud_fiadores extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    ServletOutputStream out = response.getOutputStream();
    try {
        // obteniendo parametro enviado
        HttpSession SessionActual = request.getSession();
        String codigoPrestamo = SessionActual.getAttribute("codigoReporte").toString();
        //Obtenemos la conexion del pool de conexiones
        Context init = new InitialContext();
        Context context = (Context) init.lookup("java:comp/env");
        DataSource dataSource =(DataSource)context.lookup("jdbc/mysql");
        Connection conexion = dataSource.getConnection();
        //Nombre del archivo .jasper
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/SolicitudPrestamoPrincipal.jasper"));

        // ESTABLECIENDO PARAMETROS NECESARIOS PARA EL REPORTE
        Map parameters = new HashMap();
        parameters.put("codigoPrestamo", codigoPrestamo);
        String rutaPadre = getServletContext().getRealPath("reportes_jrxml/"); 
        rutaPadre+= "/";
        parameters.put("rutaPadre", rutaPadre);
        String logo = getServletContext().getRealPath("images/logoFactura.jpg"); 
        parameters.put("logo", logo);

        //Enviamos la ruta del reporte los parametros y la conexion
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, conexion);
        JRExporter exporter = null;

        //Tipo de contenido a regresar al cliente
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"SolicitudPrestamo.pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        exporter.exportReport();
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
        finally {
        out.close();
    }
   } // processRequest

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
