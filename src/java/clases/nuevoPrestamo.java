
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;
import Conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;
import javax.swing.JOptionPane;
/**
 *
 * @author Pochii
 */
public class nuevoPrestamo {
    /**
     * Propiedades de la clase
     * plazo,           numero de meses de plazo
     * cuota,           cuota mensual del prestamo
     * capital,         valor del prestamo que se abonara a capital 
     * iva,             valor de intereses mas iva
     * cantidad,        valor del monto solicitado
     * intereses_pago,  valor de la cuota dedicada a intereses
     * interes   ,      tasa de interes a aplicar
     */
    
    /**
     * Propiedades para nuevo prestamo
     */
    private String codigoPrestamo;
    private double cantidad;
    private double saldo;
    private int    plazo;
    private double cuotas;
    private String destinoPrestamos;
    private double interes;
    private int    idSolicitante;
    private String fechaVencimiento;
    private String fechaEmision;
    private String estado;
    private String tipo_pago;
    //Propiedad a modificarse luego
    private String fechaAprobacion;
    //Propiedades para el calculo
    private double intereses_pago;
    private double capital;
    private double iva;
    private double interes_mora;
    private double interes_diario;
    private double total_interes;
    private double iva_mora;
    private double iva_interes;
    private double interes_mora_total;
    private diferenciaDias diferencia;
    private String refinanciamiento;
    
    public nuevoPrestamo()
	{
		interes_mora_total=0.0;
		cantidad=0.0;
		saldo = 0.0;
		plazo =0;
		cuotas =0.0;
		interes=0.0;
		intereses_pago=0.0;
		capital=0.0;
		iva = 0.0;
		interes_mora=0.0;
		interes_diario=0.0;
		total_interes=0.0;
		interes_mora_total=0.0;
                iva_mora = 0.0;
                iva_interes = 0.0;
                diferencia = new diferenciaDias();
	}
    /**
     * @return the interes
     */
    public double getInteres() {
        return interes/100;
    }
    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }
    /**
     * @return the intereses_pago
     */
    public double getIntereses_pago() {
        this.intereses_pago = this.getCantidad() * this.getInteres();
        return Math.rint(intereses_pago*100)/100;
    }
    /**
     * 
     * @param fechaEstablecidaAnterior
     * @param fechaUltimoPago
     * @param fechaActual
     * @param estado
     * @return 
     */
    
    public double getIntereses_pago(String fechaEstablecidaAnterior, String fechaUltimoPago, String fechaActual, String estado) throws SQLException{
        int diasInteres = 0; 
        if(fechaUltimoPago.equals("1000-01-01"))
             diasInteres = (int) diferencia.obtenerDiasRetraso(fechaActual, fechaEstablecidaAnterior);
        else
            diasInteres = (int) diferencia.obtenerDiasRetraso(fechaActual, fechaUltimoPago);
        
        this.setInteres_diario();
        
        if(diasInteres <= 0)
            diasInteres = 0;
        this.intereses_pago = this.getInteres_diario() * diasInteres; 
       
        return this.intereses_pago;
    }
    /**
     * 
     * @param fechaEstablecidaAnterior
     * @param fechaUltimoPago
     * @param fechaActual
     * @param estado
     * @return diasInteres dias de interes normal desde la ultima cuota pendiente
     * @throws SQLException 
     */
    public int getDias_Interes(String fechaEstablecidaAnterior, String fechaUltimoPago, String fechaActual, String estado) throws SQLException{
        int diasInteres = 0; 
        if(fechaUltimoPago.equals("1000-01-01"))
             diasInteres = (int) diferencia.obtenerDiasRetraso(fechaActual, fechaEstablecidaAnterior);
        else
            diasInteres = (int) diferencia.obtenerDiasRetraso(fechaActual, fechaUltimoPago);
       
        if(diasInteres <= 0)
            diasInteres = 0;
        return  diasInteres; 
    }
    /**
     * @return the cantidad
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }
   
    /**
     * @return the cuotas
     */
    public double getCuotas() {
        double cuota_no_iva = this.getIntereses_pago() / (1- Math.pow((1+this.getInteres()), -1*this.getPlazo())); //Almacenando cuota sin tomar el iva
        //JOptionPane.showMessageDialog(null,"cuota sin iva: " + cuota_no_iva);
        this.cuotas = cuota_no_iva + this.getIva();
        //JOptionPane.showMessageDialog(null,"cuota get iva: " + cuotas);
        this.cuotas = Math.rint((cuota_no_iva + this.getIva())*100)/100;
       // JOptionPane.showMessageDialog(null,"cuota get iva: " + cuotas);
        return cuotas;
    }
    
    /**
     * @return the capital
     */
    public double getCapital() {
        this.capital = this.getCuotas() - (this.getIntereses_pago_iva()) - this.interes_mora_total; 
        this.capital= Math.rint(this.capital*100)/100;
        return capital;
    }
   

    /**
     * @return the plazo
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the iva
     */
    // analisis de prestamos
    public void setIva()
    {
        this.iva = this.getIntereses_pago() * 0.13;
    }
    
    public void setIva(double interes)
    {
        this.iva = interes * 0.13;
    }
    
    /**
     * @return the iva
     */
  
    
    public double getIva() {
      //  JOptionPane.showMessageDialog(null, "IVA: " + iva);
        return Math.rint(iva*100)/100;
    } 
    /**
     * @return the interes_mora
     */
    
    // analisis de prestamos
    public void setInteres_mora()
    {
        //El 1 al final, simboliza el atraso de un dia
        this.interes_mora = ((this.getCuotas()*this.getInteres())/30)*1;
    }
   
     /**
     * @return the interes_mora
     */
    public double getInteres_mora() {
       // JOptionPane.showMessageDialog(null, "Interes mora: " + interes_mora);
        return this.interes_mora;
    }
  
    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the destinoPrestamo
     */
    public String getDestinoPrestamos() {
        return destinoPrestamos;
    }

    /**
     * @param destinoPrestamo the destinoPrestamo to set
     */
    public void setDestinoPrestamos(String destinoPrestamos) {
        this.destinoPrestamos = destinoPrestamos;
    }

    /**
     * @return the tipo_pago
     */
    public String getTipo_pago() {
        return tipo_pago;
    }

    /**
     * @param tipo_pago the tipo_pago to set
     */
    public void setTipo_pago(String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }
    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }
    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
       /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the idSolicitante
     */
    public int getIdSolicitante() {
        return idSolicitante;
    }

    /**
     * @param idSolicitante the idSolicitante to set
     */
    public void setIdSolicitante(int idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    /**
     * @return the fechaEmision
     */
    public String getFechaEmision() {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        fechaEmision = formato.format(new Date());
        return fechaEmision;
    }

    /**
     * @param fechaEmision the fechaEmision to set
     */
    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the fechaAprobacion
     */
    public String getFechaAprobacion() {
        return fechaAprobacion;
    }

    /**
     * @param fechaAprobacion the fechaAprobacion to set
     */
    public void setFechaAprobacion(String fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }
    /**
     * @return the interes_diario
     */
    public double getInteres_diario() {
        return interes_diario;
    }

    /**
     * 
     * @param num_dias_totales numero de dias a aplicar el interes normal 
     */
 

    public TreeMap<String, String> mapa()
    {
        TreeMap<String, String> prestamosPost = new TreeMap<String, String>();
        prestamosPost.put("codigoPrestamo", this.getCodigoPrestamo());
        prestamosPost.put("cantidad", Double.toString(this.getCantidad()));
        prestamosPost.put("saldo", Double.toString(this.getSaldo()));
        prestamosPost.put("plazo", Integer.toString(this.getPlazo()));
        prestamosPost.put("cuotas", Double.toString(this.getCuotas()));
        prestamosPost.put("destinoPrestamos", this.getDestinoPrestamos());
        prestamosPost.put("interes", Double.toString(this.getIntereses_pago()));
        prestamosPost.put("tasa_interes", Double.toString(this.getInteres()*100));
        prestamosPost.put("idSolicitante", Integer.toString(this.getIdSolicitante()));
        prestamosPost.put("fechaEmision", this.getFechaEmision());
        prestamosPost.put("estado", this.getEstado());
        prestamosPost.put("tipo_pago",  this.getTipo_pago());
        prestamosPost.put("refinanciado",   this.getRefinanciamiento() );
        return prestamosPost;
    }
   
    ///////////////////////////////////////////////////
    ///// FUNCIONES UTILIZADAS POR SERVLET PAGOS //////
    ///////////////////////////////////////////////////
    public double getTotal_interes() {
        this.total_interes = this.getIntereses_pago() + this.getInteres_mora() + this.getIva();
        return Math.rint(total_interes*100)/100;
    }
    
    // OBTENER LOS INTERESES TOTALES DEL PAGO, SIN INTE MORATORIO
    public double getIntereses_pago_iva() {
        this.intereses_pago = this.intereses_pago + this.getIva(); 
        return Math.rint(intereses_pago*100)/100; 
    }

   
    // funcion utilizada al momento de pagar se le cantidad que es el valor de la cuota
    public double getCapital_cantidad(double cantidad) {
     
        this.capital = cantidad - (this.getIntereses_pago_iva()); 
        this.capital= Math.rint(this.capital*100)/100;
        return capital;
    }
    
    // ETABLECER EL INTERES MORATORIA SEGUN DIAS DE RETRASO
    public void setInteres_moras(double cuota,double dias_retraso)
    {
        if (dias_retraso > 0)
         this.interes_mora = ((cuota*this.getInteres())/30)*dias_retraso;
        else
            this.interes_mora = 0.0;
    }
    
    // ESTABLECE EL INTERES DE X DIAS, CREO Q NO FUNCIONA, NO RECUERDO HABERLO HECHO
       public void setInteres_diario() {
        this.interes_diario = (this.cantidad * this.getInteres())/30;
        //this.interes_diario = (this.interes_diario) * num_dias_totales;
    }
       
    /***Funcion para obtener datos de todos los prestamos
     * @return ArrayList<nuevoPrestamo> contiene todos los prestamos **/
    public static ArrayList<nuevoPrestamo> getPrestamos(String fechaSeleccion) throws SQLException{ 
        conexion con = new conexion();
        ArrayList<nuevoPrestamo> prestamos = new ArrayList<nuevoPrestamo>();
        try{
             
             String query = "SELECT codigoPrestamo, cantidad,plazo, cuotas, tasa_interes,date_format(fechaAprobacion, '%Y%m%d') as fechaAprobacion, "
                           + "date_format(fechaVencimiento, '%Y%m%d') as fechaVencimiento FROM prestamos"
                           + " WHERE date_format(fechaAprobacion, '%m%Y') = date_format('" + fechaSeleccion + "', '%m%Y')";
             con.setRS(query);
             System.out.println("Query: " + query);
             ResultSet rs = con.getRs();
             while(rs.next()){ 
                 nuevoPrestamo n = new nuevoPrestamo();
                 n.setCantidad(rs.getDouble("cantidad"));
                 n.setCodigoPrestamo(rs.getString("codigoPrestamo"));
                 n.setInteres(rs.getDouble("tasa_interes"));
                 n.setFechaAprobacion(rs.getString("fechaAprobacion"));
                 n.setFechaVencimiento(rs.getString("fechaVencimiento"));
                 n.setPlazo(rs.getInt("plazo"));
                 prestamos.add(n);
             }
             rs.close();
        }
        catch(Exception c){
            System.out.println("No se pudo seleccionar debido a " + c.getMessage());
        }
        finally{con.cerrarConexion();}
        return prestamos;
    } //fin getPrestamos

    /**
     * @return the refinanciamiento
     */
    public String getRefinanciamiento() {
        return refinanciamiento;
    }

    /**
     * @param refinanciamiento the refinanciamiento to set
     */
    public void setRefinanciamiento(String refinanciamiento) {
        this.refinanciamiento = refinanciamiento;
    }
      
     public double getCuotaDiaria() {
        return (cuotas/30);
    }
}
