/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jorge Luis
 */
public class beanPagoEfectuados {
    
    private String fechaInicio;
    private String fechaFin;
    private String comprobante;
    private String fechaEstablecida;
    private String fechaPago;
    private String codigoPrestamo;
    private String solicitante;// otorgado a
    private Double saldoActual;
    private Double abono; // valor cancelado
    private Double interesMoratorio;
    private Double interesNormal;
    private Double capital;
    private Double saldo;
    private String logo;
    // datos para reporte_por_prestamos
    private String Fiador;
    private int plazo;
    private Double cantidad;
    private String fecha_aprobacion;
    private Double tasa_interes;
    private Double cuota;
    private String destino_prestamo;
    private String estado;
    private String tipo_pago;
    private String refinanciado;
    private String estadoPrestamo;
    
     /**
     * @return the fechaInicio
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * @return the fechaFin
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the comprobante
     */
    public String getComprobante() {
        return comprobante;
    }

    /**
     * @param comprobante the comprobante to set
     */
    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    /**
     * @return the fechaPago
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the saldoActual
     */
    public Double getSaldoActual() {
        return saldoActual;
    }

    /**
     * @param saldoActual the saldoActual to set
     */
    public void setSaldoActual(Double saldoActual) {
        this.saldoActual = saldoActual;
    }

    /**
     * @return the abono
     */
    public Double getAbono() {
        return abono;
    }

    /**
     * @param abono the abono to set
     */
    public void setAbono(Double abono) {
        this.abono = abono;
    }

    /**
     * @return the interesMoratorio
     */
    public Double getInteresMoratorio() {
        return interesMoratorio;
    }

    /**
     * @param interesMoratorio the interesMoratorio to set
     */
    public void setInteresMoratorio(Double interesMoratorio) {
        this.interesMoratorio = interesMoratorio;
    }

    /**
     * @return the interesNormal
     */
    public Double getInteresNormal() {
        return interesNormal;
    }

    /**
     * @param interesNormal the interesNormal to set
     */
    public void setInteresNormal(Double interesNormal) {
        this.interesNormal = interesNormal;
    }

    /**
     * @return the capital
     */
    public Double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(Double capital) {
        this.capital = capital;
    }

    /**
     * @return the saldo
     */
    public Double getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the fechaEstablecida
     */
    public String getFechaEstablecida() {
        return fechaEstablecida;
    }

    /**
     * @param fechaEstablecida the fechaEstablecida to set
     */
    public void setFechaEstablecida(String fechaEstablecida) {
        this.fechaEstablecida = fechaEstablecida;
    }

    /**
     * @return the Fiador
     */
    public String getFiador() {
        return Fiador;
    }

    /**
     * @param Fiador the Fiador to set
     */
    public void setFiador(String Fiador) {
        this.Fiador = Fiador;
    }

    /**
     * @return the plazo
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the cantidad
     */
    public Double getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the fecha_aprobacion
     */
    public String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    /**
     * @param fecha_aprobacion the fecha_aprobacion to set
     */
    public void setFecha_aprobacion(String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    /**
     * @return the tasa_interes
     */
    public Double getTasa_interes() {
        return tasa_interes;
    }

    /**
     * @param tasa_interes the tasa_interes to set
     */
    public void setTasa_interes(Double tasa_interes) {
        this.tasa_interes = tasa_interes;
    }

    /**
     * @return the cuota
     */
    public Double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(Double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the destino_prestamo
     */
    public String getDestino_prestamo() {
        return destino_prestamo;
    }

    /**
     * @param destino_prestamo the destino_prestamo to set
     */
    public void setDestino_prestamo(String destino_prestamo) {
        this.destino_prestamo = destino_prestamo;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the tipo_pago
     */
    public String getTipo_pago() {
        return tipo_pago;
    }

    /**
     * @param tipo_pago the tipo_pago to set
     */
    public void setTipo_pago(String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    /**
     * @return the refinanciado
     */
    public String getRefinanciado() {
        return refinanciado;
    }

    /**
     * @param refinanciado the refinanciado to set
     */
    public void setRefinanciado(String refinanciado) {
        this.refinanciado = refinanciado;
    }

    /**
     * @return the estadoPrestamo
     */
    public String getEstadoPrestamo() {
        return estadoPrestamo;
    }

    /**
     * @param estadoPrestamo the estadoPrestamo to set
     */
    public void setEstadoPrestamo(String estadoPrestamo) {
        this.estadoPrestamo = estadoPrestamo;
    }
}
