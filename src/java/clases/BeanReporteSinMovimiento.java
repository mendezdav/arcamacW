/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 */
public class BeanReporteSinMovimiento {
    private int numeroMeses;
    private String fechaActual;
    private String logo;
    private String codigo;
    private String solicitante;
    private double monto;
    private double cuota;
    private double interes;
    private double SaldoActual;
    private int diasRetraso;
    
    public BeanReporteSinMovimiento(){}

    /**
     * @return the numeroMeses
     */
    public int getNumeroMeses() {
        return numeroMeses;
    }

    /**
     * @param numeroMeses the numeroMeses to set
     */
    public void setNumeroMeses(int numeroMeses) {
        this.numeroMeses = numeroMeses;
    }

    /**
     * @return the fechaActual
     */
    public String getFechaActual() {
        return fechaActual;
    }

    /**
     * @param fechaActual the fechaActual to set
     */
    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the monto
     */
    public double getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the SaldoActual
     */
    public double getSaldoActual() {
        return SaldoActual;
    }

    /**
     * @param SaldoActual the SaldoActual to set
     */
    public void setSaldoActual(double SaldoActual) {
        this.SaldoActual = SaldoActual;
    }

    /**
     * @return the diasRetraso
     */
    public int getDiasRetraso() {
        return diasRetraso;
    }

    /**
     * @param diasRetraso the diasRetraso to set
     */
    public void setDiasRetraso(int diasRetraso) {
        this.diasRetraso = diasRetraso;
    }
    
    
    
    
    
}
