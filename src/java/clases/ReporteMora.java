/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author jorge
 */
@WebServlet(name = "ReporteMora", urlPatterns = {"/ReporteMora"})
public class ReporteMora extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
       ServletOutputStream out = response.getOutputStream();
            
     try {
        String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        // obteniendo id del pago!
      
        String no_meses = request.getParameter("no_meses");
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/reporteMora.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        //parameters.put("parametro", "parametro");
              
        List<Object> reports = new LinkedList<Object>();
        // creando instancia conexion
       conexion con = new conexion();
       String qr="";
       String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");  
        //instancia al javaBean
       
       //Verificar si hay registros
       con.setRS("SELECT count(*) as cuenta1 FROM reportemora WHERE cuenta >=" + no_meses);
       ResultSet res = (ResultSet) con.getRs();
       res.next();
       if(res.getInt("cuenta1") <= 0)
            response.sendRedirect(ruta + "/reportes/panelReportes.jsp?erno=4");
       else
       {
           // realizacion de consulta
           qr = "SELECT * FROM reportemora WHERE cuenta >= " + no_meses;
           con.setRS(qr);
           res = (ResultSet) con.getRs();
           while (res.next())
           {
               BeanMora mora = new BeanMora();
                mora.setNo_meses(Integer.parseInt(no_meses));
                mora.setSolicitante(res.getString("solicitante"));
                mora.setCodigoPrestamo(res.getString("codigoPrestamo"));
                mora.setDiferencia(res.getString("cuenta"));
                mora.setTotal_pagar(res.getDouble("total"));
                mora.setCuotas(res.getDouble("cuotas"));
                mora.setCelular(res.getString("celular"));
                mora.setFecha(res.getString("fecha"));
                mora.setImg(rutaImg);
                // creando reporte
                reports.add((Object) mora);     
           }//fin while
           res.close();
           con.cerrarConexion();
           //fill the ready report with data and parameter
                JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
                JRExporter exporter = null;
                response.setContentType("application/pdf");

                //Nombre del reporte
                response.setHeader("Content-Disposition","inline; filename=\"ReporteMora.pdf\";");
                //Exportar a pdf
                exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                //view the report using JasperViewer
                exporter.exportReport();
            } 
         } catch (JRException e) {
                e.printStackTrace();
  }
}

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteMora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteMora.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
