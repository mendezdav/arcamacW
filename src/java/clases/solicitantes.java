    /*
 * To change this template; choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.TreeMap;
/**
 *
 * @author Pochii
 */
public class solicitantes {
    private double ingresos;
    private double liquidez;
    private double descuentos;
    private String idSolicitante;
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private String DUI ;
    private String NIT;
    private String domicilio;
    private String profesion;
    private String lugarTrabajo;
    private String foto;
    private String edad;
    private String tiempo_servicio;
    private String cargo;
    private String tel_residencia;
    private String celular;
    private String tel_trabajo;
    private int ext;
    private String nombre_conyuge;
    private String apellidos_conyuge;
    private int    edad_conyuge;
    private String profesion_conyuge;
    private String lugar_direccion_trabajo_conyuge;
    private String fecha_nacimiento;
    private String estado_sistema;
    private String img_url;
    private String correo;
    private String estado_civil;
    private int id_departamento;
    private int id_municipio;
    private String fecha_expedicion_DUI;
    private String lugar_expedicion_DUI;
    private String lugar_nacimiento;
    /**
     * @return the sueldo
     */
    
    public solicitantes()
    {
        idSolicitante = "";
        nombre1= "";
        nombre2= "";
        apellido1= "";
        apellido2= "";
        DUI = "";
        NIT= "";
        domicilio= "";
        profesion= "";
        edad_conyuge = 0;
        profesion_conyuge= "";
        lugar_direccion_trabajo_conyuge= "";
        fecha_nacimiento= "";
        estado_sistema= "";
        
        lugarTrabajo = "";
        foto= "";
        edad= "";
        tiempo_servicio= "";
        cargo= "";
        tel_residencia = "";
        celular = "";
        tel_trabajo= "";
        ext= 0;
        nombre_conyuge= "";
        apellidos_conyuge= "";
        img_url = "";
        estado_civil = "";
        
    }
    public double getIngresos() {
        return ingresos;
    }

    public void setIngresos(double ingresos) {
        this.ingresos = ingresos;
    }

    public double getLiquidez() {
        this.liquidez =  this.getIngresos() - this.getDescuentos();
        return Math.rint(liquidez*100)/100;
    }

    public void setLiquidez(double liquidez) {
        this.liquidez = liquidez;
    }
    
    /**
     * @return the descuentos
     */
    public double getDescuentos() {
        return descuentos;
    }

    /**
     * @param descuentos the descuentos to set
     */
    public void setDescuentos(double descuentos) {
        this.descuentos = descuentos;
    }
    /**
     * @return the nombre1
     */
    public String getNombre1() {
        return nombre1;
    }

    /**
     * @param nombre1 the nombre1 to set
     */
    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    /**
     * @return the nombre2
     */
    public String getNombre2() {
        return nombre2;
    }

    /**
     * @param nombre2 the nombre2 to set
     */
    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    /**
     * @return the apellido1
     */
    public String getApellido1() {
        return apellido1;
    }

        /**
     * @return the idSolicitante
     */
    public String getIdSolicitante() {
        return idSolicitante;
    }

    /**
     * @param idSolicitante the idSolicitante to set
     */
    public void setIdSolicitante(String idSolicitante) {
        this.idSolicitante = idSolicitante;
    }
    /**
     * @param apellido1 the apellido1 to set
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    /**
     * @return the apellido2
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * @param apellido2 the apellido2 to set
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /**
     * @return the DUI
     */
    public String getDUI() {
        return DUI;
    }

    /**
     * @param DUI the DUI to set
     */
    public void setDUI(String DUI) {
        this.DUI = DUI;
    }

    /**
     * @return the NIT
     */
    public String getNIT() {
        return NIT;
    }

    /**
     * @param NIT the NIT to set
     */
    public void setNIT(String NIT) {
        this.NIT = NIT;
    }

    /**
     * @return the domicilio
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * @param domicilio the domicilio to set
     */
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * @return the profesion
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * @param profesion the profesion to set
     */
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    /**
     * @return the lugarTrabajo
     */
    public String getLugarTrabajo() {
        return lugarTrabajo;
    }

    /**
     * @param lugarTrabajo the lugarTrabajo to set
     */
    public void setLugarTrabajo(String lugarTrabajo) {
        this.lugarTrabajo = lugarTrabajo;
    }

    /**
     * @return the foto
     */
    public String getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     * @return the edad
     */
    public String getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(String edad) {
        this.edad = edad;
    }

    /**
     * @return the tiempo_servicio
     */
    public String getTiempo_servicio() {
        return tiempo_servicio;
    }

    /**
     * @param tiempo_servicio the tiempo_servicio to set
     */
    public void setTiempo_servicio(String tiempo_servicio) {
        this.tiempo_servicio = tiempo_servicio;
    }

    /**
     * @return the cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * @return the tel_residencia
     */
    public String getTel_residencia() {
        return tel_residencia;
    }

    /**
     * @param tel_residencia the tel_residencia to set
     */
    public void setTel_residencia(String tel_residencia) {
        this.tel_residencia = tel_residencia;
    }

    /**
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * @param celular the celular to set
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * @return the tel_trabajo
     */
    public String getTel_trabajo() {
        return tel_trabajo;
    }

    /**
     * @param tel_trabajo the tel_trabajo to set
     */
    public void setTel_trabajo(String tel_trabajo) {
        this.tel_trabajo = tel_trabajo;
    }

    /**
     * @return the ext
     */
    public int getExt() {
        return ext;
    }

    /**
     * @param ext the ext to set
     */
    public void setExt(int ext) {
        this.ext = ext;
    }

    /**
     * @return the nombre_conyuge
     */
    public String getNombre_conyuge() {
        return nombre_conyuge;
    }

    /**
     * @param nombre_conyuge the nombre_conyuge to set
     */
    public void setNombre_conyuge(String nombre_conyuge) {
        this.nombre_conyuge = nombre_conyuge;
    }

    /**
     * @return the apellidos_conyuge
     */
    public String getApellidos_conyuge() {
        return apellidos_conyuge;
    }

    /**
     * @param apellidos_conyuge the apellidos_conyuge to set
     */
    public void setApellidos_conyuge(String apellidos_conyuge) {
        this.apellidos_conyuge = apellidos_conyuge;
    }

    /**
     * @return the edad_conyuge
     */
    public int getEdad_conyuge() {
        return edad_conyuge;
    }

    /**
     * @param edad_conyuge the edad_conyuge to set
     */
    public void setEdad_conyuge(int edad_conyuge) {
        this.edad_conyuge = edad_conyuge;
    }

    /**
     * @return the profesion_conyuge
     */
    public String getProfesion_conyuge() {
        return profesion_conyuge;
    }

    /**
     * @param profesion_conyuge the profesion_conyuge to set
     */
    public void setProfesion_conyuge(String profesion_conyuge) {
        this.profesion_conyuge = profesion_conyuge;
    }

    /**
     * @return the lugar_direccion_trabajo_conyuge
     */
    public String getLugar_direccion_trabajo_conyuge() {
        return lugar_direccion_trabajo_conyuge;
    }

    /**
     * @param lugar_direccion_trabajo_conyuge the lugar_direccion_trabajo_conyuge to set
     */
    public void setLugar_direccion_trabajo_conyuge(String lugar_direccion_trabajo_conyuge) {
        this.lugar_direccion_trabajo_conyuge = lugar_direccion_trabajo_conyuge;
    }

    /**
     * @return the fecha_nacimiento
     */
    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    /**
     * @param fecha_nacimiento the fecha_nacimiento to set
     */
    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    /**
     * @return the estado_sistema
     */
    public String getEstado_sistema() {
        return estado_sistema;
    }

    /**
     * @param estado_sistema the estado_sistema to set
     */
    public void setEstado_sistema(String estado_sistema) {
        this.estado_sistema = estado_sistema;
    }

    /**
     * @return the estado_civil
     */
    public String getEstado_civil() {
        return estado_civil;
    }

    /**
     * @param estado_civil the estado_civil to set
     */
    public void setEstado_civil(String estado_civil) {
        this.estado_civil = estado_civil;
    }
    
    public void reiniciar()
    {
        this.setIngresos(0.0);
        liquidez         = 0.0;
        descuentos       = 0.0;
        idSolicitante    = "";
        nombre1          = "";
        nombre2          = "";
        apellido1        = "";
        apellido2        = "";
        DUI              = "";
        NIT              = "";
        domicilio        = "";
        profesion        = "";
        lugarTrabajo     = "";
        foto             = "";
        edad             = "";
        tiempo_servicio  = "";
        cargo            = "";
        tel_residencia   = "";
        celular          = "";
        tel_trabajo      = "";
        ext              = 0;
        nombre_conyuge   = "";
        apellidos_conyuge    = "";
        edad_conyuge         = 0;
        profesion_conyuge    = "";
        lugar_direccion_trabajo_conyuge  = "";
        fecha_nacimiento = "";
        img_url = "";
        estado_sistema   = "";
        correo = "";
        id_municipio = 1;
        id_departamento = 1;
        fecha_expedicion_DUI = "";
        lugar_expedicion_DUI = "";
        lugar_nacimiento = "";
    }
    /**
     * Funcion para retornar el mapa de valores
     */
    public TreeMap<String, String> mapa()
    {
       TreeMap<String, String> solicitantesPost = new TreeMap<String, String>();
       solicitantesPost.put("nombre1",                              this.getNombre1());
       solicitantesPost.put("nombre2",                              this.getNombre2());
       solicitantesPost.put("apellido1",                            this.getApellido1());
       solicitantesPost.put("apellido2",                            this.getApellido2());
       solicitantesPost.put("DUI" ,                                 this.getDUI());
       solicitantesPost.put("NIT" ,                                 this.getNIT());
       solicitantesPost.put("estado_sistema",                       this.getEstado_sistema());
       solicitantesPost.put("domicilio" ,                           this.getDomicilio());
       solicitantesPost.put("edad" ,                                this.getEdad());
       solicitantesPost.put("tel_residencia" ,                      this.getTel_residencia());
       solicitantesPost.put("celular" ,                             this.getCelular());
       solicitantesPost.put("fecha_nacimiento" ,                    this.getFecha_nacimiento());
       solicitantesPost.put("profesion" ,                           this.getProfesion ());
       solicitantesPost.put("lugarTrabajo" ,                        this.getLugarTrabajo());
       solicitantesPost.put("tiempo_servicio" ,                     this.getTiempo_servicio());
       solicitantesPost.put("cargo" ,                               this.getCargo());
       solicitantesPost.put("tel_trabajo" ,                         this.getTel_trabajo());
       solicitantesPost.put("ext" ,                                 Integer.toString(this.getExt()));  
       solicitantesPost.put("ingresos",                             Double.toString(this.getIngresos()));
       solicitantesPost.put("descuentos",                           Double.toString(this.getDescuentos()));
       solicitantesPost.put("liquidez",                             Double.toString(this.getLiquidez()));
       solicitantesPost.put("descuentos" ,                          Double.toString(this.getDescuentos()));
       solicitantesPost.put("nombre_conyuge" ,                      this.getNombre_conyuge());
       solicitantesPost.put("apellidos_conyuge" ,                   this.getApellidos_conyuge());
       solicitantesPost.put("edad_conyuge" ,                        Integer.toString(this.getEdad_conyuge()));
       solicitantesPost.put("profesion_conyuge" ,                   this.getProfesion_conyuge());
       solicitantesPost.put("lugar_expedicion_DUI" ,                this.getLugar_expedicion_DUI());
       solicitantesPost.put("lugar_nacimiento" ,                    this.getLugar_nacimiento());
       solicitantesPost.put("fecha_expedicion_DUI" ,                this.getFecha_expedicion_DUI());
       solicitantesPost.put("lugar_direccion_trabajo_conyuge" ,     this.getLugar_direccion_trabajo_conyuge());
       solicitantesPost.put("correo" ,                              this.getCorreo());
       solicitantesPost.put("id_departamento" ,                     Integer.toString(this.getId_departamento()));
       solicitantesPost.put("id_municipio" ,                        Integer.toString(this.getId_municipio()));       
       
       return solicitantesPost;
    }

    /**
     * @return the img_url
     */
    public String getImg_url() {
        return img_url;
    }

    /**
     * @param img_url the img_url to set
     */
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * @return the id_departamento
     */
    public int getId_departamento() {
        return id_departamento;
    }

    /**
     * @param id_departamento the id_departamento to set
     */
    public void setId_departamento(int id_departamento) {
        this.id_departamento = id_departamento;
    }

    /**
     * @return the id_municipio
     */
    public int getId_municipio() {
        return id_municipio;
    }

    /**
     * @param id_municipio the id_municipio to set
     */
    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    /**
     * @return the fecha_expedicion_DUI
     */
    public String getFecha_expedicion_DUI() {
        return fecha_expedicion_DUI;
    }

    /**
     * @param fecha_expedicion_DUI the fecha_expedicion_DUI to set
     */
    public void setFecha_expedicion_DUI(String fecha_expedicion_DUI) {
        this.fecha_expedicion_DUI = fecha_expedicion_DUI;
    }

    /**
     * @return the lugar_expedicion_DUI
     */
    public String getLugar_expedicion_DUI() {
        return lugar_expedicion_DUI;
    }

    /**
     * @param lugar_expedicion_DUI the lugar_expedicion_DUI to set
     */
    public void setLugar_expedicion_DUI(String lugar_expedicion_DUI) {
        this.lugar_expedicion_DUI = lugar_expedicion_DUI;
    }

    /**
     * @return the lugar_nacimiento
     */
    public String getLugar_nacimiento() {
        return lugar_nacimiento;
    }

    /**
     * @param lugar_nacimiento the lugar_nacimiento to set
     */
    public void setLugar_nacimiento(String lugar_nacimiento) {
        this.lugar_nacimiento = lugar_nacimiento;
    }


}
