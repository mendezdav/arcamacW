/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Jorge Luis
 */
   
@WebServlet(name = "ReporteEstadoCuenta", urlPatterns = {"/ReporteEstadoCuenta"})
public class ReporteEstadoCuenta extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, JRException {
            ServletOutputStream out = response.getOutputStream();
        try {
            String codigoPrestamo = "";
            String idSolicitante  = "";
            HttpSession SesionActual = request.getSession(false);
           // JOptionPane.showMessageDialog(null, r);
            String id="";
            if ( request.getParameter("idSolicitante") != null 
                    || request.getParameter("idSolicitante") != "" )
            {
                id = request.getParameter("idSolicitante");
            }
            conexion con = new conexion();
            con.setRS("select codigoPrestamo from prestamos where idSolicitante="+ id + " and estado='Activo'");
            ResultSet rs = con.getRs();
            if (rs.next()){
                codigoPrestamo = rs.getString("codigoPrestamo");
            }
            else
                response.sendRedirect("ldf.jps=sdsd");
            con.cerrarConexion();
            rs.close();
            con = new conexion();
                String qr = "select concat(nombre1, ' ', nombre2, ' ', apellido1, ' ',apellido2) as nombre, "
                        + "s.domicilio as direccion_socio, s.tel_residencia as tel_residencia_socio, "
                        + "s.celular as celular_socio, concat(f.nombreFiador,' ', apellidoFiador) as codeudor,"
                        + " f.telResidenciaF as telefono_residencia_fiador,celularF as celular_fiador, p.cantidad,"
                        + " p.tasa_interes,p.idSolicitante,p.saldo,p.plazo,p.fechaAprobacion as apertura, p.fechaVencimiento as vencimiento,"
                        + " p.cuotas,t.tipoPago from prestamos as p inner join solicitante as s on p.idSolicitante = "
                        + "s.idSolicitante  inner join prestamo_fiador as p_f on p_f.id_prestamo = p.codigoPrestamo inner "
                        + "join fiadores as f on f.idfiadores = p_f.id_fiador inner join tipopago as t "
                        + "on t.idtipoPago = p.tipo_pago  where p.codigoPrestamo = '" + codigoPrestamo + "'";
               // conexion con = new conexion();
                con.setRS(qr);
                rs = con.getRs();
                if (rs.next())
                {
                    idSolicitante = rs.getString("idSolicitante");
                    JasperReport reporte = (JasperReport)JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/estadoCuenta.jasper"));
                    //Cargamos parametros del reporte (si tiene).
                    Map parameters = new HashMap();
                    String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg"); 
                    // llenando datos
                    List<beanEstadoCuenta> datos = new ArrayList<beanEstadoCuenta>();
                    beanEstadoCuenta bean = new beanEstadoCuenta();
                    bean.setLogo(rutaImg);
                    bean.setCodigoPrestamo(codigoPrestamo);
                        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                        //Calendar c1 = Calendar.getInstance();
                        Date c1 = new Date();
                    bean.setFecha(formato.format(c1));
                    bean.setMonto_otorgado(rs.getDouble("cantidad"));
                    bean.setTasa(rs.getDouble("tasa_interes") / 100);
                    bean.setPlazo_meses(rs.getInt("plazo"));
                        // obteniendo fecha de apertura
                        String aperturaPartes[] = rs.getString("apertura").split(" ");
                        String aperturaParteDias[] = aperturaPartes[0].split("-");
                    bean.setApertura(aperturaParteDias[2] + "/" + aperturaParteDias[1] + "/" + aperturaParteDias[0]);
                        String fechaPartes[] = rs.getString("vencimiento").split("-");
                    bean.setVencimiento(fechaPartes[2]  + "/" + fechaPartes[1] + "/" + fechaPartes[0]);
                    bean.setFecha_pago(aperturaParteDias[2] + " DE CADA MES");
                    bean.setCuota(rs.getDouble("cuotas"));
                    bean.setForma_pago(rs.getString("tipoPago"));
                    bean.setAsociado(rs.getString("nombre").toUpperCase() + " ");
                    bean.setDireccion(rs.getString("direccion_socio").toUpperCase() + " ");
                    bean.setTel_residencia(rs.getString("tel_residencia_socio"));
                    bean.setCelular(rs.getString("celular_socio"));
                    bean.setCodeudor(rs.getString("codeudor").toUpperCase() + " ");
                    bean.setTel_residencia_codeudor(rs.getString("telefono_residencia_fiador"));
                    bean.setCelular_codeudor(rs.getString("celular_fiador"));
                        double saldo_actual = rs.getDouble("saldo");
                    bean.setSaldo_prestamo(saldo_actual);
                        // OBTENER SALDO IDEAL
                        generarTablaAmortizacion tabla = new generarTablaAmortizacion(codigoPrestamo);
                        double saldo_ideal = tabla.cargarDatosPagosExtraordinarios("");
                        
                    bean.setSaldo_ideal(saldo_ideal);
                        String signo_debe_ser = "";
                        boolean adelanto_estado = false;
                        if (saldo_ideal > saldo_actual)
                        {
                            signo_debe_ser = "(-) ";
                            adelanto_estado = true; 
                            // es positivo porque el saldo debe ser 
                            // es mayor al actual, por lo que es falso
                         }
                        else  if (saldo_ideal < saldo_actual){
                            signo_debe_ser = "(+) ";
                            adelanto_estado = false;
                        }
                   bean.setSigno_debe_ser(signo_debe_ser);
                   if (saldo_ideal == 0.00)
                   {
                        bean.setSigno_debe_ser("");
                   }
                  
                        double adelanto = Math.abs(saldo_ideal - saldo_actual);
                    bean.setCapital_adelantado(adelanto);
                        PagosExtraordinarios pex = new PagosExtraordinarios(idSolicitante, "dia");
                   bean.setInteres_normales(pex.getTotal_interes());
                   bean.setInteres_mora(pex.getTotal_interes_moratorio());
                   bean.setIva_interes_normales(pex.getIva_total_interes());
                   bean.setIva_interes_mora(pex.getIva_total_interes_moratorio());
                   // debo capital??
                        double total_a_fecha = 0.00;
                        if (!adelanto_estado) // debo capital!!
                            total_a_fecha = adelanto +  pex.getTotal_interes() + pex.getTotal_interes_moratorio() + 
                                    pex.getIva_total_interes() + pex.getIva_total_interes_moratorio();
                        else
                            total_a_fecha = pex.getTotal_interes() + pex.getTotal_interes_moratorio() + 
                                    pex.getIva_total_interes() + pex.getIva_total_interes_moratorio();
                        if (saldo_ideal == 0.00)
                        {
                            total_a_fecha=0.0;
                        }
                        
                   bean.setTotal_a_fecha(total_a_fecha);
                   if (!adelanto_estado)
                   {
                       bean.setPara_cancelar(total_a_fecha + saldo_ideal);
                   }
                   else
                   {
                       bean.setPara_cancelar(total_a_fecha + saldo_actual);
                   }
                   if (saldo_ideal == 0.00)
                   {
                       bean.setPara_cancelar(saldo_actual);
                   }
                   // temporales no se que son!
                   bean.setAportaciones(0);
                   bean.setSimultaneos(0);
                   bean.setFecha_pago("21/09/1997");
                   bean.setFecha_ultima_pago("24/06/1999");

                   
            datos.add(bean);
        
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(datos));
        JRExporter exporter = null;
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"estadoCuenta_" + codigoPrestamo+ ".pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        //view the report using JasperViewer
        exporter.exportReport();
       // redireccion al panel principal
       response.sendRedirect("/arcamacW/panelAdmin.jsp");
                }
 
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteEstadoCuenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReporteEstadoCuenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteEstadoCuenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReporteEstadoCuenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
