/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 * clase para ver/generar reporte de los solicitantes que cumplen 
 * años en el mes actual
 */
import Conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;


public class cumpleMes {
    // miembros de la clase
    ResultSet resultado;
    String qr;
    Date fecha_nacimiento,Factual;
    int anio;
    SimpleDateFormat formatoDelTexto;
    String[] actual;

    public cumpleMes(){ 
        resultado = null;
        qr = "";
        anio = 0;
        Factual = new Date();
        formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        actual = formatoDelTexto.format(Factual).split("-");
    } // constructor sin parametros
    
    // se devolveran los solicitantes que se mostran en el menu
    
    public ResultSet obtenerMensaje() throws SQLException // para obtener los cumpleañers del mes
    {
              
        qr ="select concat(nombre1,' ', nombre2,' ', apellido1, ' ', apellido2 ) as nombre,"
                + "edad,fecha_nacimiento,tel_residencia,idSolicitante from solicitante where MONTH(fecha_nacimiento)= " + actual[1];
       
        //establecimiento conexion con la base de datos
        conexion con = new conexion();
        con.setRS(qr);
        resultado = (ResultSet) con.getRs();
        resultado.next();
        
        // pasando de sting a date
       
        String strFecha = resultado.getString("fecha_nacimiento");
        fecha_nacimiento = null;
        try {
           fecha_nacimiento = formatoDelTexto.parse(strFecha);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        //sacando edad segun fecha nacimiento
        // fecha de nacimiento
        String[] nacimiento = formatoDelTexto.format(fecha_nacimiento).split("-");
        
        //obteniendo edad
        anio = Integer.parseInt(actual[0])- Integer.parseInt(nacimiento[0]);
        resultado.previous();
        
        if (resultado != null)
            return resultado;
        else
            return null;
        
    }
    
    public ResultSet obtenerDias() throws SQLException
    {
        qr = "select concat(nombre1,' ', nombre2,' ', apellido1, ' ', apellido2 ) as nombre,edad,fecha_nacimiento,tel_residencia"
                + " from solicitante where MONTH(fecha_nacimiento)= "+actual[1]+" and DAY(fecha_nacimiento) = "+actual[2]+" limit 3;";
        
        //establecimiento conexion con la base de datos
        conexion con = new conexion();
        con.setRS(qr);
        resultado = (ResultSet) con.getRs();
        resultado.next();
        
        // pasando de sting a date
       
        String strFecha = resultado.getString("fecha_nacimiento");
        fecha_nacimiento = null;
        try {
           fecha_nacimiento = formatoDelTexto.parse(strFecha);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        //sacando edad segun fecha nacimiento
        // fecha de nacimiento
        String[] nacimiento = formatoDelTexto.format(fecha_nacimiento).split("-");
        
        //obteniendo edad
        anio = Integer.parseInt(actual[0])- Integer.parseInt(nacimiento[0]);
        resultado.previous();
        
        if (resultado != null)
            return resultado;
        else
            return null;
    }
    
    public int getEdad()
    {
        return anio;
    }
    
}
