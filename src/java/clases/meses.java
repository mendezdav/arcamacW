/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jorge Luis
 */
public class meses {
    private int correlativo;
    private String anio;
    private String mes;
    private double sumaOtorgado;
    private double sumaCuota;
    private double sumaSaldo;
    private double capital;
    private double mora;
    private double normal;
    private double iva;
    String[] mesString = {"", "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
    
    meses(int correlativo,String anio, String mes, double sumaOtorgado, double sumaCuota, double sumaSaldo, 
            double capital, double mora, double normal, double iva)
    {
        this.correlativo = correlativo;
        this.anio = anio;
        this.mes = mes;
        this.sumaOtorgado = sumaOtorgado;
        this.sumaCuota = sumaCuota;
        this.sumaSaldo = sumaSaldo;
        this.capital = capital;
        this.mora = mora;
        this.normal = normal;
        this.iva  = iva;
    }

    public String getMesString()
    {
        return this.mes;
    }
    /**
     * @return the correlativo
     */
    public int getCorrelativo() {
        return correlativo;
    }

    /**
     * @return the anio
     */
    public String getAnio() {
        return anio;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @return the sumaOtorgado
     */
    public double getSumaOtorgado() {
        return sumaOtorgado;
    }

    /**
     * @return the sumaCuota
     */
    public double getSumaCuota() {
        return sumaCuota;
    }

    /**
     * @return the sumaSaldo
     */
    public double getSumaSaldo() {
        return sumaSaldo;
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @return the mora
     */
    public double getMora() {
        return mora;
    }

    /**
     * @return the normal
     */
    public double getNormal() {
        return normal;
    }

    /**
     * @return the iva
     */
    public double getIva() {
        return iva;
    }
}
