/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import javax.swing.JOptionPane;

/**
 *
 * @author Pochii
 */
public class BeanResolucionPrestamo {
    private String idSolicitante = "";
    private String nombres = "";
    private String apellidos = "";
    private double sueldo = 0.0;
    private double descuentos = 0.0; 
    private double liquidez = 0.0;
    private double cantidad = 0.0;
    private String codigoPrestamo = "";
    private String nombreFiador1 = "";
    private String apellidoFiador1 = "";
    private double salarioF1 = 0.0;
    private double deduccionesF1 = 0.0;
    private double liquidezF1 = 0.0;
    private String nombreFiador2 = "No aplica";
    private String apellidoFiador2 = "No aplica";
    private double salarioF2 = 0.0;
    private double deduccionesF2 = 0.0;
    private double liquidezF2 = 0.0;
    private String img_url;
    private String correlativo;
    
    private double capital = 0.0;
    private double intereses = 0.0;
    private double interesMoratorio = 0.0;
    private double totalSaldo = 0.0;
    private double papeleria = 0.0;
    
    private double cuotas = 0.0;
    private int plazo = 0;
    private double tasaInteres = 0.0;
    private double tasaInteresMoratorio = 0.0;
    
    public double getPapeleria(){
        return papeleria;
    }
    
    public void setPapeleria(double papeleria){
        this.papeleria = papeleria;
    }
    
    public double getCuotas(){
        return cuotas;
    }
    
    public void setCuotas(double cuotas){
        this.cuotas = cuotas;
    }
    
    public int getPlazo(){
        return plazo;
    }
    
    public void setPlazo(int plazo){
        this.plazo = plazo;
    }
    
    public double getTasaInteres(){
        return tasaInteres;
    }
    
    public void setTasaInteres(double tasaInteres){
        this.tasaInteres = tasaInteres;
    }
    
    public double getTasaInteresMoratorio(){
        return tasaInteresMoratorio;
    }
    
    public void setTasaInteresMoratorio(double tasaInteresMoratorio){
        this.tasaInteresMoratorio = tasaInteresMoratorio;
    }
    
    public double getCapital(){
        return capital;
    }
    
    public void setCapital(double capital){
        this.capital = capital;
    }
    
    public double getIntereses(){
        return intereses;
    }
    
    public void setIntereses(double intereses){
        this.intereses = intereses;
    }
    
    public double getInteresMoratorio(){
        return interesMoratorio;
    }
    
    public void setInteresMoratorio(double interesMoratorio){
        this.interesMoratorio = interesMoratorio;
    }
    
    public double getTotalSaldo(){
        return totalSaldo;
    } 
    
    public void setTotalSaldo(double totalSaldo){
        this.totalSaldo = totalSaldo;
    }
    /**
     * @return the idSolicitante
     */
    public String getIdSolicitante() {
        return idSolicitante;
    }

    /**
     * @param idSolicitante the idSolicitante to set
     */
    public void setIdSolicitante(String idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the sueldo
     */
    public double getSueldo() {
        return sueldo;
    }

    /**
     * @param sueldo the sueldo to set
     */
    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    /**
     * @return the descuentos
     */
    public double getDescuentos() {
        return descuentos;
    }

    /**
     * @param descuentos the descuentos to set
     */
    public void setDescuentos(double descuentos) {
        this.descuentos = descuentos;
    }

    /**
     * @return the liquidez
     */
    public double getLiquidez() {
        return liquidez;
    }

    /**
     * @param liquidez the liquidez to set
     */
    public void setLiquidez(double liquidez) {
        this.liquidez = liquidez;
    }

    /**
     * @return the cantidad
     */
    public double getCantidad() {
        return diferenciaDias.redondear(cantidad);
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the nombreFiador1
     */
    public String getNombreFiador1() {
        return nombreFiador1;
    }

    /**
     * @param nombreFiador1 the nombreFiador1 to set
     */
    public void setNombreFiador1(String nombreFiador1) {
        this.nombreFiador1 = nombreFiador1;
    }

    /**
     * @return the apellidoFiador1
     */
    public String getApellidoFiador1() {
        return apellidoFiador1;
    }

    /**
     * @param apellidoFiador1 the apellidoFiador1 to set
     */
    public void setApellidoFiador1(String apellidoFiador1) {
        this.apellidoFiador1 = apellidoFiador1;
    }

    /**
     * @return the salarioF1
     */
    public double getSalarioF1() {
        return salarioF1;
    }

    /**
     * @param salarioF1 the salarioF1 to set
     */
    public void setSalarioF1(double salarioF1) {
        this.salarioF1 = salarioF1;
    }

    /**
     * @return the deduccionesF1
     */
    public double getDeduccionesF1() {
        return deduccionesF1;
    }

    /**
     * @param deduccionesF1 the deduccionesF1 to set
     */
    public void setDeduccionesF1(double deduccionesF1) {
        this.deduccionesF1 = deduccionesF1;
    }

    /**
     * @return the liquidezF1
     */
    public double getLiquidezF1() {
        return liquidezF1;
    }

    /**
     * @param liquidezF1 the liquidezF1 to set
     */
    public void setLiquidezF1(double liquidezF1) {
        this.liquidezF1 = liquidezF1;
    }

    /**
     * @return the nombreFiador2
     */
    public String getNombreFiador2() {
        return nombreFiador2;
    }

    /**
     * @param nombreFiador2 the nombreFiador2 to set
     */
    public void setNombreFiador2(String nombreFiador2) {
        this.nombreFiador2 = nombreFiador2;
    }

    /**
     * @return the apellidoFiador2
     */
    public String getApellidoFiador2() {
        return apellidoFiador2;
    }

    /**
     * @param apellidoFiador2 the apellidoFiador2 to set
     */
    public void setApellidoFiador2(String apellidoFiador2) {
        this.apellidoFiador2 = apellidoFiador2;
    }

    /**
     * @return the salarioF2
     */
    public double getSalarioF2() {
        return salarioF2;
    }

    /**
     * @param salarioF2 the salarioF2 to set
     */
    public void setSalarioF2(double salarioF2) {
        this.salarioF2 = salarioF2;
    }

    /**
     * @return the deduccionesF2
     */
    public double getDeduccionesF2() {
        return deduccionesF2;
    }

    /**
     * @param deduccionesF2 the deduccionesF2 to set
     */
    public void setDeduccionesF2(double deduccionesF2) {
        this.deduccionesF2 = deduccionesF2;
    }

    /**
     * @return the liquidezF2
     */
    public double getLiquidezF2() {
        return liquidezF2;
    }

    /**
     * @param liquidezF2 the liquidezF2 to set
     */
    public void setLiquidezF2(double liquidezF2) {
        this.liquidezF2 = liquidezF2;
    }

    /**
     * @return the img_url
     */
    public String getImg_url() {
        return img_url;
    }

    /**
     * @param img_url the img_url to set
     */
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    /**
     * @return the correlativo
     */
    public String getCorrelativo() {
        return correlativo;
    }

    /**
     * @param correlativo the correlativo to set
     */
    public void setCorrelativo(String correlativo) {
        int aux = Integer.parseInt(correlativo);
        this.correlativo = String.format("%06d", aux);
    }

}
