/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Jorge Luis
 */
public class saldoCierre {
    static List<meses> meses = new ArrayList<meses>();
    private String[] mes = {"","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
    String[] ff = {"",""};
    String[] fechaPartes;
    int mesInicial;
    ///
    private String fechaInicio;
    private String fechaFin;
    private conexion con;
    private ResultSet rs;
    private int contador;
    private int anio;
    // sumas 
    private double sumaOtorgado;
    private double sumaCuota;
    private double sumaSaldo;
    private boolean banderaPagoPrestamo;
    public saldoCierre(String fechaInicio, String fechaFin)
    {
        contador=0;
        banderaPagoPrestamo = false;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        fechaPartes = fechaInicio.split("-");
        mesInicial = Integer.parseInt(fechaPartes[1]);
        this.anio = Integer.parseInt(fechaPartes[0]);
        sumaOtorgado = 0.0;
        sumaCuota = 0.0;
        sumaSaldo = 0.0;
    }
    
    public double sumaOtorgadoInicial() throws SQLException
    {
        double capital =0.0, mora =0.0, normal=0.0, iva=0.0;
        DiasMes diaMes = new DiasMes(this.mesInicial, this.anio);
        List<String> fechasLimites = diaMes.getFecha(); // retorna fecha inicio y fecha fin , ej: 2015-03-01 , 2015-03-31
        con = new conexion();
        String qr = "select ifnull(sum(cantidad),0) as total_otorgado,ifnull(sum(cuotas),0) as cuota,ifnull(sum(saldo),0) as saldo from prestamos "
                + "where (estado='Activo' and fechaAprobacion <= '" +fechasLimites.get(1)+ "') or "
                + "(estado='Pagado' and fechaFinalizacion >= '"+ fechasLimites.get(1) +"')";
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
            sumaOtorgado = rs.getDouble("total_otorgado");
            sumaCuota = rs.getDouble("cuota");
            sumaSaldo = rs.getDouble("saldo");
        }
        contador++;
        /// obtener parte individual del mes en cuestion
        qr =   "select ifnull(sum(pago_capital),0) as capital, ifnull(sum(pago_interes_moratorio),0) as mora,"
                + "ifnull(sum(pago_interes),0) as normal from abonos where month(fechaPago)=" + fechaPartes[1]+
                " and year(fechaPago)=" + anio;
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
            capital = rs.getDouble("capital");
            mora = rs.getDouble("mora");
            normal = rs.getDouble("normal");
            iva = ((rs.getDouble("mora") * 0.13)) +((rs.getDouble("normal") * 0.13));
        }
        /*
         *  COMPROBAR SI EL SALDO ACTUAL OBTENIDO CORRESPONDE AL SALDO QUE SE TENIA 
         *  PARA ESE MES Y AÑO, YA QUE EL OBTENIDO ES EL ACTUAL, DE TODOS LOS MES
         * 
         */
        double SaldoAdicional = abonoMes(Integer.parseInt(fechaPartes[1]),anio);
        sumaSaldo+= SaldoAdicional;
        rs.close();
        con.cerrarConexion();
        meses.add(new meses(contador,String.valueOf(anio), mes[Integer.parseInt(fechaPartes[1])], sumaOtorgado, sumaCuota, sumaSaldo, 
            capital, mora, normal,iva));
        return sumaOtorgado;
    }
    
    public int mesesCalcular() throws SQLException
    {
        String qr = "select timestampdiff(month,'" +fechaInicio+ "','" + fechaFin+ "') as meses;";
        con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        int totalMeses = rs.getInt("meses");
        return totalMeses;
    }
    
    public void recorridoMensual() throws SQLException
    {
        contador++;
        int contador_aux = mesesCalcular()+1;
        int x = 1;
        while (x != contador_aux)       
        {
            if ( mesInicial + x > 12)
            {
                contador = 1;
                mesInicial = (-x+1);
                anio++;
            }
            saldoMes(String.valueOf(anio), (mesInicial+x));
            System.out.print(String.valueOf(anio) + "->" + (mesInicial+x) +  "\n");
            contador++;
            x++;
        }
    }
    
    public void saldoMes(String anio, int mes) throws SQLException
    {
        double capital = 0.0, mora=0.0, normal=0.0, iva=0.0;
        con = new conexion();
        // debo considerar q pueden haber prestamos q fue cancelado en dicho mes!, pero necesito el mes inicial
         /*
         *  LLAMANDO DIASMES PARA CONCOER FECHA INICIO/FIN DEL MES EN DICHO AÑO
         */
        DiasMes diaMes = new DiasMes(mes, Integer.parseInt(anio));
        List<String> fechasLimites = diaMes.getFecha(); // retorna fecha inicio y fecha fin , ej: 2015-03-01 , 2015-03-31
        String qr = "select ifnull(sum(cantidad),0) as total_otorgado,ifnull(sum(cuotas),0) as cuota,"
                + " ifnull(sum(saldo),0) as saldo  from prestamos where (estado='Activo' and "
                + "month(fechaAprobacion)=" + mes + " and year(fechaAprobacion)=" + anio + ") or "
                + "(estado='Pagado' and fechaFinalizacion>='" +fechasLimites.get(0)+ "' and "
                + "fechaFinalizacion<='" + fechasLimites.get(1) + "');";
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
            sumaOtorgado += rs.getDouble("total_otorgado");
            sumaCuota += rs.getDouble("cuota");
            sumaSaldo += rs.getDouble("saldo");
        }
        // sumas individual
        qr =   "select ifnull(sum(pago_capital),0) as capital, ifnull(sum(pago_interes_moratorio),0) as mora,"
                + "ifnull(sum(pago_interes),0) as normal from abonos where month(fechaPago)=" + mes +
                " and year(fechaPago)=" + anio;
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
            capital = rs.getDouble("capital");
            mora = rs.getDouble("mora");
            normal = rs.getDouble("normal");
            iva = ((rs.getDouble("mora") * 0.13)) +((rs.getDouble("normal") * 0.13));
        }
        /*
         *  VERIFICAR SI EN ESE MES SE FINALIZO UN PRESTAMO, SI ES ASI
         *  RESTAR LA CUOTA, EL MONTO DEL MISMO DE LOS DATOS HISTORICOS DEL
         *  REPORTE
         */
        
        qr = "select ifnull(sum(cantidad),0) as total_otorgado,ifnull(sum(cuotas),0) as cuota,"
                + "ifnull(sum(saldo),0) as saldo from prestamos where estado='Pagado'"
                + " and month(fechaFinalizacion)=" +mes+ "  and year(fechaFinalizacion)=" + anio;
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
            banderaPagoPrestamo = true;
        }
        if (banderaPagoPrestamo)
        {
            sumaOtorgado -= rs.getDouble("total_otorgado");
            sumaCuota -= rs.getDouble("cuota")*2;
            sumaSaldo -= rs.getDouble("saldo")*2;
        }
        /*
         * VERIFICAR SI EN DICHA MES AUN HAY MOVIMIENTO DE EFECTIVO
         */
         double SaldoAdicional = 0.0;
         if (capital != 0.0 || mora != 0.0 || normal != 0.0 || iva != 0.0 )
         {
                SaldoAdicional = abonoMes(Integer.parseInt(fechaPartes[1]),Integer.parseInt(anio));
                sumaSaldo-= SaldoAdicional;
         }
         rs.close();
         con.cerrarConexion();
         meses.add(new meses(contador,anio, this.mes[mes], sumaOtorgado, sumaCuota, sumaSaldo, capital, mora, normal,iva));
    }
    
    public double abonoMes(int mes, int anio) throws SQLException
    {
        DiasMes diaMes = new DiasMes(mes, anio);
        List<String> fechasLimites = diaMes.getFecha(); // retorna fecha inicio y fecha fin , ej: 2015-03-01 , 2015-03-31
        String qr = "  select group_concat(DISTINCT codigoPrestamo ORDER BY codigoPrestamo DESC SEPARATOR ';') as codigos"
                + " from prestamos  where (estado='Activo' and month(fechaAprobacion)="+mes+" and year(fechaAprobacion)="+ anio+ ") or "
                + "(estado='Pagado' and fechaFinalizacion>='" +fechasLimites.get(0)+ "' and fechaFinalizacion<='" + fechasLimites.get(1)+ "')";
        conexion con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        boolean existencia = false;
        String codigos = "";
        if (rs.next())
        {
            // obteniendo codigos
            if (rs.getString("codigos") != null)
            {
                codigos = rs.getString("codigos");
                existencia = true;
            }
            else
                existencia = false;
            
        }
        else
            existencia = false;
        
        if (existencia)
        {
            String[] codigosPartes = codigos.split(";");
            // obtener todos del saldos por cada codigo prestamo
            double sumaAbonos=0.00;
            double resultado =0.00;
            for (int x = 0 ; x < codigosPartes.length; x++)
            {
                // hacer para todos los prestamos en dicho mes
                // obtener el total de abnos realizado historicamente
                qr = "select sum(pago_capital) as suma from pagos where codigoPrestamo='" + codigosPartes[x] + "'";
                con.setRS(qr);
                rs = con.getRs();
                rs.next();
                sumaAbonos = rs.getDouble("suma");
                qr = "select sum(pago_capital) as suma from pagos where codigoPrestamo='" + codigosPartes[x]+ "' "
                        + "and (fechaPago >= '"  + this.mesInicial + "' and fechaPago <= '" + fechasLimites.get(1) + "')";
                con.setRS(qr);
                rs = con.getRs();
                rs.next();
                resultado = rs.getDouble("suma");
                resultado = sumaAbonos - resultado;
            }
            // solo sirve para uno!!
            return resultado;
        }
        else
            return 0.00;
    }
}
