package clases;

import Conexion.conexion;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ActualizacionPagos {
    static  int no_pagos;
    private String codigoPrestamo;
    private ResultSet prestamos;
    private CachedRowSetImpl pagos;
    private CachedRowSetImpl rs;
    private conexion con; 
    private conexion conAux;
    private classPagos classPagos;
    private String qr;
    private double saldo;
    private double tasa_interes;
    private int plazos;
    private String estado;
    private String fechaEstablecida;
    private String fechaActual;
    private diferenciaDias diasDif;
    private double diasRetraso;
    private double cuota;
    private int dias_interes_normal;
    private int dias_interes_moratorio;
    private double iva;  //El iva es sobre la sumatoria de interes normal e interes moratorio
    /**Variables auxiliares***/
    private int dias_interes_normal_anterior;
    private int dias_interes_moratorio_anterior;
 
    private double interesApagar;
    private double interesAnterior;
    private double pagoCapitalAnt;
    private double saldoAnt;
    
    private String fechaEstablecidaAnterior;
    private String fechaUltimoPago;
    private String fechaAprobacion;
    private String id;
    private String fechaPago;
    /**Propiedades de pago abonado***/
    private double pago_interes=0.0;
    private double pago_capital=0.0;
    private double pago_interes_moratorio=0.0;
    private double a_pago_interes=0.0;
    private double a_pago_capital=0.0;
    private double a_pago_interes_moratorio=0.0;
    private double mora =0.0;
    private double capital = 0.0;
    private double a_capital = 0.0;
    private double interes_moratorio;
    private double a_interes_moratorio = 0.0;
    private double interes = 0.0;
    private double fila;
    private double a_interes = 0.0;
    private double iva_interes = 0.0;
    private double iva_mora = 0.0;
    private double capital_vencido;
    private int numero_pago;
    
    private boolean PRIMERA_VEZ = false;
    
    public ActualizacionPagos(String codigoPrestamo){ 
        this.codigoPrestamo = codigoPrestamo;
        prestamos = null;
        pagos = null;
        con = null;
        conAux = null;
        qr ="";
        diasDif = new diferenciaDias();
        id="";
        capital = 0.0;
        interes_moratorio = 0.0; 
    }
    
    public double getCapital(){
        return this.a_capital;
    }
    
    public double getPagoCapital(){
        return this.a_pago_capital;
    }
    
    public double getInteres(){
        return this.a_interes;
    }
    
    public double getPagoInteres(){
        return this.a_pago_interes;
    }
    
    public double getInteresMoratorio(){
        return this.a_interes_moratorio;
    }
    
    public double getPagoInteresMoratorio(){
        return this.a_pago_interes_moratorio;
    }
    
    public double getSaldo(){
        return this.saldo;
    }

    public void actualizar() throws SQLException
    {
        if (this.codigoPrestamo.equals(""))
        {
            /*
             * No se ha enviado codigo de Prestamos, por lo que
             * la actualizacion se realizara en todos los prestamos activos
             */
            qr = "select * from prestamos where estado='Activo'";
        }
        else
            qr = "SELECT * from prestamos where estado='Activo' and codigoPrestamo = '" + codigoPrestamo + "'";
        recorrido(qr, "", 1);
    }
    
    public void actualizar(boolean pv) throws SQLException
    {
        if (this.codigoPrestamo.equals(""))
        {
            /*
             * No se ha enviado codigo de Prestamos, por lo que
             * la actualizacion se realizara en todos los prestamos activos
             */
            qr = "select * from prestamos where estado='Activo'";
        }
        else
            qr = "SELECT * from prestamos where estado='Activo' and codigoPrestamo = '" + codigoPrestamo + "'";
        PRIMERA_VEZ = pv;
        recorrido(qr, "", 1);
    }
    /**
     * recorrido, funcion que calcula el desglose de los pagos
     * @param qr  Query de los prestamos
     * @param fechaInicio fecha hasta la cual se calcularan los pagos
     * @param actualizar  bandera 1 actualizara la base, 0 no hara la actualizacion
     * @throws SQLException 
     */
    public void recorrido(String qr, String fechaInicio, int actualizar) throws SQLException{
            /**
             * Instanciando la clase de pagos
             */
            classPagos = new classPagos(); 
            con = new conexion();
            con.setRS(qr);
            prestamos = (ResultSet) con.getRs();
            while (prestamos.next())
            {
                // sacar datos necesario de los prestamos!
                this.codigoPrestamo = prestamos.getString("codigoPrestamo");
                this.saldo = prestamos.getDouble("saldo");
                this.plazos = prestamos.getInt("plazo");
                this.tasa_interes = prestamos.getDouble("tasa_interes");
                this.cuota = prestamos.getDouble("cuotas");
                this.fechaAprobacion = prestamos.getString("fechaAprobacion");
                //this.saldo = prestamos.getDouble("saldo");
                // instanciar e inicializar el beanNuevoPrestamo
                nuevoPrestamo bean = new nuevoPrestamo();
                bean.setCantidad(saldo);
                bean.setPlazo(plazos);
                bean.setInteres(tasa_interes);
                if(fechaInicio.equals(""))
                    fechaActual = bean.getFechaEmision();
                else
                    fechaActual = fechaInicio;
                
                nuevoPrestamo bean_aux = new nuevoPrestamo();
                bean_aux.setPlazo(plazos);
                bean_aux.setInteres(tasa_interes);
                /*
                 * sacar los pagos del prestamo actual que no esten Pagados
                 */
                if(PRIMERA_VEZ){
                    qr ="where codigoPrestamo='"+ prestamos.getString("codigoPrestamo") + "'";
                }else{
                    qr ="where codigoPrestamo='"+ prestamos.getString("codigoPrestamo") + "' AND estado != 'Pagado' LIMIT 1";
                }
                    
                pagos = classPagos.getPagos(qr);
                /**
                * OBTENER EL PRIMER PAGO
                **/
                String idPrimerPago = "";  //ID del primer pago
                rs = classPagos.getPagos("WHERE codigoPrestamo = '" + codigoPrestamo + "' ORDER BY idPago ASC LIMIT 1");
                if(rs.next())
                {
                    idPrimerPago = rs.getString("idPago"); 
                    fechaEstablecidaAnterior = fechaAprobacion;
                    fechaUltimoPago = fechaAprobacion;
                }
                rs.close();
                System.out.println("id del primer pago: " + idPrimerPago);
                /*
                 * recorriendo los pagos de los prestamos
                 */
                while(pagos.next())
                {
                   /*sacar dato de pagos*/
                   this.estado = pagos.getString("estado");
                   this.fila = pagos.getDouble("fila");
                   this.id = pagos.getString("idPago");
                   this.fechaEstablecida = pagos.getString("fechaEstablecida");
                   this.fechaPago = (String)pagos.getDate("fechaPago").toString(); 
                   this.capital = pagos.getDouble("capital"); 
                   this.mora = pagos.getDouble("interes_moratorio"); 
                   this.interes = pagos.getDouble("intereses");
                   this.pago_capital = pagos.getDouble("pago_capital");
                   this.numero_pago = pagos.getInt("Npago");
                   
                   System.out.println("\tEvaluando pago: " + id + "\n\n");
                   /**Tomando datos para calculo de intereses normales**/
                   
                   if(!id.equals(idPrimerPago)){ /***Si no se trata del primer pago***/
                        String c = "";
                        c = "WHERE codigoPrestamo = '" + codigoPrestamo + "' HAVING fila= (" + fila + "-1)";
                        rs = classPagos.getPagos(c);
                        if(rs.next()){
                              pagoCapitalAnt = rs.getDouble("pago_capital");
                              saldoAnt = rs.getDouble("saldo");
                              fechaEstablecidaAnterior = rs.getString("fechaEstablecida"); 
                              fechaUltimoPago = rs.getString("fechaPago");
                         }
                         rs.close();
                         saldoAnt = saldoAnt + pagoCapitalAnt;
                   }
                   
                   /*Pagos pendientes**/
                    if (estado.equals("Pendiente"))
                    {
                        
                        diasRetraso =  diasDif.obtenerDiasRetraso(fechaActual,fechaEstablecida); //Dias de retraso de mora
                        diasRetraso = (int) diasRetraso;
                        int dias_mora = (int) diasRetraso;
                        System.out.println("Fila: " + (int)fila);
                        
                        this.capital_vencido = this.getCapital_vencido_sd(dias_mora, this.numero_pago);
                        
                        System.out.println("capital vencido:  " + capital_vencido);
                        bean.setInteres_moras(this.capital_vencido,diasRetraso);
                        
                        /**Estableciendo los dias de interes moratorio***/
                        this.dias_interes_moratorio = (int) diasRetraso;
                        interes_moratorio = bean.getInteres_mora();
                        System.out.println("interes_mora:  " + interes_moratorio);
                        System.out.println(fechaUltimoPago);
                        if(fechaUltimoPago.equals("1000-01-01") || id.equals(idPrimerPago)){
                           System.out.println("ACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA :V");
                           interesApagar = bean.getIntereses_pago(fechaEstablecidaAnterior, fechaUltimoPago, fechaActual, estado);
                           this.dias_interes_normal = bean.getDias_Interes(fechaEstablecidaAnterior, fechaUltimoPago, fechaActual, estado);
                        }
                        else
                        {
                           System.out.println("OLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE :V"); 
                           bean_aux.setCantidad(saldoAnt);                           
                           
                           interesAnterior = bean_aux.getIntereses_pago(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaUltimoPago, estado);
                           this.dias_interes_normal_anterior = bean_aux.getDias_Interes(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaUltimoPago, estado);
                           System.out.println("ANTERIOR:" + interesAnterior);
                           interesApagar = bean.getIntereses_pago(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaActual, estado);
                           System.out.println("ACTUAL:" + interesApagar);
                           this.dias_interes_normal = bean.getDias_Interes(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaActual, estado);
                        }
                        Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "ActualizacionPagos: Estado de pago: Pendiente,CodigoPrestamo:{0}", codigoPrestamo);
                        Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "Dias de interes MORATORIO: Pendiente,{0}", this.dias_interes_moratorio);
                        Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "Dias de interes normal: Pendiente, {0}", this.dias_interes_normal);
                        //this.a_interes += this.interes;
                    }
                    else if(estado.equals("Abonado")){
                        /**
                         * Calculos de mora
                         * 1. Calculamos la mora desde fechaEstablecida hasta fechaPago
                         * 2. Sumamos a esa mora, la mora desde la fecha de Pago hasta la fechaActual
                         **/
                        double dias_aux = diasDif.obtenerDiasRetraso(fechaPago, fechaEstablecida);
                        if(dias_aux <= 0.0){
                            mora = 0;
                            dias_aux = 0.0;
                        }
                        // dias_aux = dias mora
                        this.capital_vencido = this.getCapital_vencido_sd((int)dias_aux, this.numero_pago);
                        bean.setInteres_moras(this.capital_vencido,dias_aux);
                        /**Estableciendo los dias de interes moratorio***/
                        this.dias_interes_moratorio = (int) diasRetraso;
                        interes_moratorio = bean.getInteres_mora();
                        // cuota = capi vencido
                        //bean.setInteres_moras((int) dias_aux, dias_aux); //SE DEBE CAMBIAR CUOTA POR GETCAPITALVENCIDO
                        //interes_moratorio = bean.getInteres_mora();
                        /**Estableciendo los dias de interes moratorio anterior**/
                        this.dias_interes_moratorio_anterior = (int) dias_aux;
                        

                        dias_aux = diferenciaDias.retrasoAbono(fechaPago, fechaEstablecida, fechaActual); 
                        if(dias_aux == -0.0001){
                            mora = 0;
                            dias_aux = 0.0;
                        }
                        bean.setInteres_moras(capital- pago_capital, dias_aux); 
                        interes_moratorio += bean.getInteres_mora();
                        this.dias_interes_moratorio = (int)dias_aux;
                        /**Sumando los dias de interes moratorio anterior y actual***/
                        this.dias_interes_moratorio += this.dias_interes_moratorio_anterior;
                        /**
                         * Calculos de interes
                         * 1. Calculamos el interes normal desde la fecha de pago hasta la fecha actual
                         * 2. Sumamos a esa cantidad, los intereses que ya estaban generados hasta esa fecha
                         */
                        saldoAnt = saldo + pago_capital;
                        bean_aux.setCantidad(saldoAnt);
                        interesAnterior = bean_aux.getIntereses_pago(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaPago, estado);
                        this.dias_interes_normal = bean.getDias_Interes(fechaEstablecidaAnterior, fechaPago, fechaActual, estado);
                        interesApagar = bean.getIntereses_pago(fechaEstablecidaAnterior, fechaPago, fechaActual, "Abonado");
                        System.out.println("ABONOOOOOO: "+interesApagar);
                    } //fin if
                    /**
                     * OPERACIONES GENERALES
                     *
                     */
                    if (diasRetraso <= 0)
                            diasRetraso = 0;
                    if(estado.equals("Abonado") || (estado.equals("Pendiente")&& !fechaUltimoPago.equals("1000-01-01"))){
                            //interesApagar += interesAnterior;
                    }
                    /**
                     * Estableciendo IVA
                     */
                    // CLAVE
                    this.iva = diferenciaDias.redondear(interes_moratorio*0.13) + diferenciaDias.redondear(interesApagar*0.13);
                    this.iva_mora = diferenciaDias.redondear(interes_moratorio*0.13);
                    this.iva_interes = diferenciaDias.redondear(interesApagar*0.13);
                    //interes_moratorio = interes_moratorio + diferenciaDias.redondear(interes_moratorio*0.13);
                    bean.setIva(interesApagar);
                    //interesApagar += diferenciaDias.redondear(interesApagar * 0.13);
                    /**If de los dias**/
                    if(this.dias_interes_moratorio <0.0)
                        this.dias_interes_moratorio = 0;
                    if(this.dias_interes_normal < 0.0)
                        this.dias_interes_normal = 0;
                    /**
                     * Sumando al nuevo interes, el interes anterior
                     */  
                    /**Verificando si el saldo es mayor que la cuota,
                     * de caso contrario, se deben restar los intereses al saldo
                     */
                    
                    
                    capital = this.getCapital_pago(this.numero_pago);
                    cuota = capital + (interesApagar +  iva_interes);
                    
                    /**
                     * Validando si el capital es menor a cero
                     */
                    
                    /**Aproximando todas las cantidades**/
                    interes_moratorio   = diferenciaDias.redondear(interes_moratorio);
                    interesApagar       = diferenciaDias.redondear(interesApagar);
                    capital             = diferenciaDias.redondear(capital);
                    this.a_interes += (interesApagar +  iva_interes);
                    this.a_interes_moratorio += (interes_moratorio + iva_mora);
                    
        
                   /* verificar si el saldo es menor a la cuota  */
                   if (( this.saldo - capital) < cuota)
                   {
                     double tmp = saldo - capital;
                     if (capital + tmp < cuota)
                     {
                         capital = capital + tmp;
                     }  
                   }//fin if
                   
                   if(actualizar == 1)
                       this.actualizar_db(); //Se actualizan los campos en la BD
                } // fin recurrido de los pagos 
            }// fin while, fin recorrido de los prestamos activos
            con.cerrarConexion();
    }
    
    /**Metodo para hacer la actualizacion de los campos calculados en la base de datos**/
    public void actualizar_db(){
        try {
            /*****VERIFICAMOS SI EL PAGO YA TENIA ESTADO DE MORA*/
            int moraant = 0;
            conexion conmora =  new conexion();
            conmora.setRS("SELECT mora FROM pagos WHERE idPago = " + this.id);
            ResultSet rsmora = conmora.getRs();
            if(rsmora.next())
                moraant = rsmora.getInt("mora");
            rsmora.close();
            conmora.cerrarConexion();
            /**Estableciendo la consulta**/
            qr ="update pagos set diasInteres = " + this.dias_interes_normal
                    + ", diasInteresMora = " + this.dias_interes_moratorio
                    + ",iva = " + this.iva
                    + ",iva_interes = " + this.iva_interes
                    + ",iva_mora = " + this.iva_mora
                    + ",intereses="+ this.interesApagar+",interes_moratorio="+ this.interes_moratorio+","
                    + "capital=" + this.capital + ",mora=";
            
            if (diasRetraso <= 0){
                if(moraant == 0)
                    qr+="0";       //Si el pago no estaba en mora antes de la actualizacion
                else
                    qr +="1";      //Si el pago ya estaba en mora antes de la actualizacion se deja el estado
            }
            else
                qr+="1";
            
            qr+=" where idPago="+ this.id;
            Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "Query: , {0}", qr);
            /*
            * realizar actualizacion
            */
            conAux = new conexion();
            if(!estado.equals("Pagado")){
                conAux.actualizar(qr);
            }
            conAux.cerrarConexion();
        } //fin actualizar_db
        catch (SQLException ex) {
            Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    } //fin actualizar_db
    /***Metodo para calcular interes normal para un dia especifico***/
    private double getInteres_sd(){
        return this.interesApagar;
    }
    /**Metodo para calcular interes moratorio para un dia especifico***/
    private double getInteres_mora_sd(String codigoPrestamo, String fechaPago){
        double interes_moratorio = 0.0;
        return interes_moratorio;
    }
    /**Metodo para calcular abono a capital para un dia especifico***/
    private double getCapital_sd(String codigoPrestamo){
        double capital = 0.0;
        return capital;
    }
    /**Metodo para calcular cargo por IVA para un dia especifico**/
    private double getIva_sd(){
        return this.iva;
    } //fin getIva_setdate
        /**Metodo para calcular el capital vencido de un pago a una fecha establecida***/
    private double getCapital_vencido_sd(int dias_mora, int no_pago){
        System.out.println("Dias Mora: " + dias_mora + "----- NPago: " + no_pago);
        float capital_vencido = 0.0F;
        int npago_aux = 0;
        int diapago_aux = 0;
        double saldoIdeal = 0;
        if(dias_mora > 0){ 
            try { //Si hay mas de un dia de mora existe capital vencido
                conexion con_ideal = new conexion();
                String query_ideal = "SELECT saldoFinal FROM tabla_amortizacion WHERE codigoPrestamo='" + this.codigoPrestamo + "' "
                        + "and Npago = " + no_pago;
                //System.out.println("Query de capital vencido: " + query_ideal);
                con_ideal.setRS(query_ideal);
                ResultSet rs_ideal = con_ideal.getRs();
                if(rs_ideal.next()){ //Si la condicion coincide
                    saldoIdeal = rs_ideal.getDouble("saldoFinal");
                } //fin if
                else{ //condicion
                    
                }
                capital_vencido = (float)((float)this.saldo - (float)saldoIdeal);
                System.out.println("Saldo ideal: " + saldoIdeal + "   Saldo: " + this.saldo 
                        +"CapVencido: " + capital_vencido);
                
                if(capital_vencido < 0)
                    capital_vencido = 0;
            } catch (SQLException ex) {
                capital_vencido = 0;
                Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return capital_vencido;
    }
    
    private double getCapital_pago(int no_pago){
        float capital_pago = 0.0F;
        int npago_aux = 0;
        int diapago_aux = 0;
        double cap = 0;
        try { //Si hay mas de un dia de mora existe capital vencido
            conexion con_ideal = new conexion();
            String query_ideal = "SELECT capital FROM tabla_amortizacion WHERE codigoPrestamo='" + this.codigoPrestamo + "' "
                        + "and Npago = " + no_pago;
                //System.out.println("Query de capital vencido: " + query_ideal);
            con_ideal.setRS(query_ideal);
            ResultSet rs_ideal = con_ideal.getRs();
            if(rs_ideal.next()){ //Si la condicion coincide
                cap = rs_ideal.getDouble("capital");
            }
            
            capital_pago = (float)(cap);
            
            
        } catch (SQLException ex) {
            capital_pago = 0;
            Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        return capital_pago;
    }
    
} //fin de clase