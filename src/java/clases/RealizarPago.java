/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import clases.diferenciaDias;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.TreeMap;
import javax.swing.JOptionPane;
/**
 *
 * @author Jorge Luis
 */
public class RealizarPago  {
    conexion con = null;
    ResultSet rs = null;
    Statement s = null;
    private double id;
    private String query;
    private double monto; 
    private String fechaPago;
    private String fechaEstablecida;
    private double intereses;
    private double capital;
    private String estado;
    private double pago_interes;
    private double pago_capital;
    private double interes_moratorio;
    private double pago_interes_moratorio;
    private double saldo;
    private String fechaActual;
    private double cuota;
    private nuevoPrestamo prestamo;
    private TreeMap<String, Double> mapa;
    private double diasRetraso;
    private double montoAbonar; //Monto de cada pago
    private double saldo_prestamo;
    private double iva;
    private double iva_interes;
    private double iva_mora;
    private double pago_iva_interes;
    private double pago_iva_mora;
    private String codigoPrestamo;
    private double tmp_montoAbonar;
    private double saldoIdeal;
    private double nuevoSaldo;
    
    
    public RealizarPago(TreeMap<String,Double> mapa,String codigoPrestamo, double montoAbonar,
            String estado,String fechaActual) throws SQLException
    {
        this.mapa = mapa;
        this.id   = this.mapa.get("id");
        this.codigoPrestamo = codigoPrestamo;
        this.saldo = this.mapa.get("saldo");
        this.cuota = this.mapa.get("cuota");
        this.saldoIdeal = this.mapa.get("saldoIdeal");
        this.nuevoSaldo = 0.0;
        diasRetraso = 0; 
        prestamo = null;
        this.montoAbonar = montoAbonar;
        
        this.estado = estado;
     
        // llamar a funcion procesar pago
        this.fechaActual = fechaActual;
       procesar();
    }
    
    public void calcular()
    {
        tmp_montoAbonar = this.montoAbonar;
       
           if(tmp_montoAbonar >= (mapa.get("ideal_interes_moratorio")-  mapa.get("pay_interes_moratorio")))
           {
            tmp_montoAbonar -= (mapa.get("ideal_interes_moratorio")-  mapa.get("pay_interes_moratorio"));
            pago_interes_moratorio = (mapa.get("ideal_interes_moratorio")-  mapa.get("pay_interes_moratorio"));
            
             if(tmp_montoAbonar >= (mapa.get("ideal_interes_pagoIVA")-mapa.get("pay_interes")))
             {
                
                tmp_montoAbonar -= (mapa.get("ideal_interes_pagoIVA")-mapa.get("pay_interes"));
                pago_interes = (mapa.get("ideal_interes_pagoIVA")-mapa.get("pay_interes"));
                // hacer update
                //Pagando capital
              
                if(tmp_montoAbonar >= (mapa.get("ideal_capital_cantidad")- mapa.get("pay_capital")))
                {
                    tmp_montoAbonar -= (mapa.get("ideal_capital_cantidad")- mapa.get("pay_capital"));
                    pago_capital = (mapa.get("ideal_capital_cantidad")- mapa.get("pay_capital"));
                }
                else
                {
                    pago_capital = tmp_montoAbonar;
                    tmp_montoAbonar =0;
                }
                    
             }
             else
             {
                 pago_interes = tmp_montoAbonar;
                 tmp_montoAbonar =0;
             }
                 
        }
           else 
           {
               pago_interes_moratorio = tmp_montoAbonar;
               tmp_montoAbonar = 0;
           }
    }
       
    
    public boolean pagar(double montoAbonar) throws SQLException
    {
        /**Reestableciendo el mapa de datos***/
        this.mapa = new TreeMap<String, Double>();
        this.montoAbonar = montoAbonar;
        con = new conexion();
        query ="SELECT * FROM pagos WHERE codigoPrestamo = '" + codigoPrestamo + "' and (estado='Pendiente' or estado='Abonado') limit 1";
        con.setRS(query);
        rs = con.getRs();
       // if(rs.wasNull())
         //   return false;
        while(rs.next()){
            /***MODIFICACIONES*/
            /*this.id  = rs.getDouble("idPago");
            this.capital = rs.getDouble("capital");
            this.estado  = rs.getString("estado");
            this.fechaEstablecida  = rs.getString("fechaEstablecida");
            this.fechaPago         = rs.getString("fechaPago");
            this.interes_moratorio = rs.getDouble("interes_moratorio");
            this.intereses         = rs.getDouble("intereses");
            this.monto        = rs.getDouble("monto");
            this.pago_capital = rs.getDouble("pago_capital");
            this.pago_interes = rs.getDouble("pago_interes");
            this.pago_interes_moratorio = rs.getDouble("pago_interes_moratorio");
            this.saldo = rs.getDouble("saldo");*/
            
            this.id  = rs.getDouble("idPago");
            this.capital = rs.getDouble("capital");
            this.estado  = rs.getString("estado");
            this.fechaEstablecida  = rs.getString("fechaEstablecida");
            this.fechaPago         = rs.getString("fechaPago");
            this.interes_moratorio = rs.getDouble("interes_moratorio");
            this.intereses         = rs.getDouble("intereses");
            this.monto        = rs.getDouble("monto");
            this.pago_capital = rs.getDouble("pago_capital");
            this.pago_interes = rs.getDouble("pago_interes");
            this.pago_iva_mora = rs.getDouble("pago_iva_mora");
            this.pago_iva_interes = rs.getDouble("pago_iva_interes");
            this.pago_interes_moratorio = rs.getDouble("pago_interes_moratorio");
            this.saldo = rs.getDouble("saldo");
            
        }//fin while
        /**Agregando datos al mapa**/
         mapa.put("id",  id);                                           // id del pago a abonar
         mapa.put("pay_interes_moratorio", pago_interes_moratorio);     //Cuanto se ha abonado de interes moratorio
         mapa.put("pay_interes", pago_interes);                         //Cuanto se ha abonado de interes normal
         mapa.put("pay_capital", pago_capital);                         //Cuanto se ha abonado de capital
         mapa.put("ideal_interes_pagoIVA", intereses);                  //Cuanto se debe pagar de intereses
         mapa.put("ideal_capital_cantidad", capital);                   //Cuanto se debe pagar de capital
         mapa.put("ideal_interes_moratorio", interes_moratorio);        //Cuanto se debe pagar de mora
         mapa.put("saldo", saldo);    
         mapa.put("saldoIdeal", saldoIdeal);    
         mapa.put("cuota", cuota); 
        // sacando cuota del prestamo;
        query="select cantidad,interes,plazo,saldo,tasa_interes from prestamos where codigoPrestamo='"+codigoPrestamo+"'";
        // obtencion interes normal
        con.setRS(query);
        rs = con.getRs();
        if (rs.next())
        {
            prestamo = new nuevoPrestamo();
            prestamo.setCantidad(rs.getDouble("saldo"));
            prestamo.setPlazo(rs.getInt("plazo"));
            prestamo.setInteres(rs.getDouble("tasa_interes"));
            
            /**Cargando la mora**/
            this.fechaActual = prestamo.getFechaEmision();
            diferenciaDias diasF = new diferenciaDias();
            diasRetraso = diasF.obtenerDiasRetraso(fechaActual,fechaEstablecida);
          
           // prestamo.setInteres_moras(diasRetraso);

            /**Estableciendo IVA**/
            prestamo.setIva(prestamo.getIntereses_pago() + prestamo.getInteres_mora());
            saldo_prestamo = rs.getDouble("saldo");
            this.iva = prestamo.getIva();
            
            // agregado
            this.saldo = saldo_prestamo;
                   
           
        }
         con.cerrarConexion();
         procesar();
        return true;
    }
    
    // funcion pago con mora
    public void procesar() throws SQLException
    {
        String qr ="";
        double abonar=0.0;
        calcular();
        nuevoSaldo = saldo - pago_capital;
        // lo pagado en capital era lo esperado?
        con = new conexion();
      
        if (pago_capital ==  mapa.get("ideal_capital_cantidad") || pago_capital > mapa.get("ideal_capital_cantidad")) 
        { 
             
              // actualizar tabla pagos
                qr = "UPDATE pagos SET pago_interes_moratorio = " + pago_interes_moratorio + ", pago_interes= " + 
                        (pago_interes) + " ,monto="+this.montoAbonar +" ,pago_capital=" + pago_capital 
                        + ", saldo = " + nuevoSaldo +",fechaPago='"+this.fechaActual+"' where codigoPrestamo='"+this.codigoPrestamo+"'"
                        + " and idPago=" + this.id;
              
              con.actualizar(qr);  
       }
        else 
        {
            if (pago_capital < mapa.get("ideal_capital_cantidad")) 
            {
                 // actualizar tabla pagos
                   qr = "UPDATE pagos SET pago_interes_moratorio = " + pago_interes_moratorio + ", pago_interes=(pago_interes + " + 
                        (pago_interes) + ") ,monto=(monto+"+this.montoAbonar+"),"
                        + "pago_capital=(pago_capital+" + pago_capital 
                        + "), estado='Abonado', saldo = " + nuevoSaldo + " , fechaPago='"+this.fechaActual+"' where codigoPrestamo='"+this.codigoPrestamo+"'"
                        + " and idPago=" + this.id;
                  con.actualizar(qr); // realizar actualizacion
                  // verificar si el pago anteriormente abonado fue cancelado
            }
        }
        
        double diffCapital=0.0,diffInteresMora=0.0,diffInteres =0.0, diffSaldo = 0.0;
                  
        qr = "select pago_interes,pago_capital,pago_interes_moratorio, capital,intereses, interes_moratorio, saldo, saldoIdeal from \n" +
                 "pagos where idPago=" + this.id;
        con.setRS(qr);
        rs = con.getRs();
        if(rs.next())
        {
            diffCapital = rs.getDouble("capital") - rs.getDouble("pago_capital");
            diffInteresMora = rs.getDouble("interes_moratorio") - rs.getDouble("pago_interes_moratorio");
            diffInteres = rs.getDouble("intereses") - rs.getDouble("pago_interes");
            diffSaldo =  rs.getDouble("saldoIdeal") - rs.getDouble("saldo");
            saldoIdeal = rs.getDouble("saldoIdeal");
            saldo = rs.getDouble("saldo");
        }
        
        
        qr = "UPDATE prestamos SET saldo = " + nuevoSaldo + " where codigoPrestamo='"+this.codigoPrestamo+"'";
    
        con.actualizar(qr);
        con.cerrarConexion();
        
        if (diffCapital == 0.0 && diffInteresMora == 0.0 && diffInteres == 0.0)
        {
            /**
             * Si la diferencia de los saldos es cero, el pago pasa a 'Pagado'
             * Si la diferencia es menor que cero, hay un mayor abono a capital y se deben igualar los saldos y pasar al pago siguiente
             * Si la diferencia es mayor a cero, hay un saldo pendiente y el pago queda abonado
             */
        
            if(diffSaldo == 0)
                qr = "UPDATE pagos set estado='Pagado' WHERE idPago=" + this.id;
            if(diffSaldo > 0.0){
              //  abonar = saldo - saldoIdeal;
                qr = "UPDATE pagos set estado='Pagado', saldo = " + saldoIdeal + " WHERE idPago= " + this.id;
                con = new conexion();
                con.actualizar(qr);
                con.cerrarConexion();
            }
            if(diffSaldo < 0.0)
               qr = "update pagos set estado='Abonado' where idPago=" + this.id;
            con = new conexion();
            con.actualizar(qr);
            con.cerrarConexion();

            if (pago_capital > saldo - saldoIdeal)
            { 
                // proximo pago
                //abonar = saldoIdeal- saldo;
                { // JOptionPane.showMessageDialog(null, "abono: " + abonar);
               // qr = "update pagos set estado='Pagado',saldo=(saldo+"+ abonar+") where idPago=" + this.id;
                //con = new conexion();
                //con.actualizar(qr);
            }
            
            if (pago_capital < diffSaldo)
            {
               // JOptionPane.showMessageDialog(null,"diffSaldo ME: " + diffSaldo);
                qr = "update pagos set estado='Abonado' where idPago=" + this.id;
                con = new conexion();
                con.actualizar(qr);
            }
           
        }
        
        //update pagos set pago_interes_moratorio = pago_interes_moratorio
         //tmp_montoAbonar = Math.rint(tmp_montoAbonar * 100) /100;
         if(abonar > 0.0)
         {
               //JOptionPane.showMessageDialog(null, "repagarra");
         }
            pagar(abonar); 
         }
    }
    /**
     * Metodo para verificar si el monto a pagar cubre la cuota y sus intereses
     * @return false si no la cubre
     *         true si la cubre
     */
    public boolean cubreCuota()
    {
        if((this.monto - prestamo.getTotal_interes() - prestamo.getCapital()) >= 0.0)
            return true;
        else
            return false;
    }
}
