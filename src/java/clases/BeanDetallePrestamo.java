/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

/**
 *
 * @author Pochii
 */
public class BeanDetallePrestamo {
    private String nombreSolicitante;
    private String estadoPrestamo;
    private String codigoPrestamo;
    private double cantidad;
    private double cuota;
    private double saldoPendiente;
    private int    plazo;
    private double montoUltimoPago;
    private String tipoPago;
    private String destinoPrestamo;
    private double tasaInteres;
    
    private String fechaSolicitud;
    private String fechaFinalizacion;
    private String fechaAprobacion;
    private String fechaVencimiento;
    private String fechaUltimoPago;
    
    private String nombreFiador_1;
    private String telefonoFiador_1;
    private int    edadFiador_1;
    private String nombreFiador_2;
    private String telefonoFiador_2;
    private int    edadFiador_2;
    
    
    private String img_url;
    /**
     * Constructor
     */
    public BeanDetallePrestamo(){
        this.nombreFiador_2   = "No aplica";
        this.telefonoFiador_2 = "No aplica";
        this.edadFiador_2 = 0;
        this.montoUltimoPago = 0.00;
        
        this.fechaAprobacion   = "No establecida";
        this.fechaVencimiento  = "No establecida";
        this.fechaFinalizacion = "No establecida";
    }

    /**
     * @return the nombreSolicitante
     */
    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    /**
     * @param nombreSolicitante the nombreSolicitante to set
     */
    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    /**
     * @return the estadoPrestamo
     */
    public String getEstadoPrestamo() {
        return estadoPrestamo;
    }

    /**
     * @param estadoPrestamo the estadoPrestamo to set
     */
    public void setEstadoPrestamo(String estadoPrestamo) {
        this.estadoPrestamo = estadoPrestamo;
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the cantidad
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the saldoPendiente
     */
    public double getSaldoPendiente() {
        return saldoPendiente;
    }

    /**
     * @param saldoPendiente the saldoPendiente to set
     */
    public void setSaldoPendiente(double saldoPendiente) {
        this.saldoPendiente = saldoPendiente;
    }

    /**
     * @return the plazo
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the montoUltimoPago
     */
    public double getMontoUltimoPago() {
        return montoUltimoPago;
    }

    /**
     * @param montoUltimoPago the montoUltimoPago to set
     */
    public void setMontoUltimoPago(double montoUltimoPago) {
        this.montoUltimoPago = montoUltimoPago;
    }

    /**
     * @return the fechaSolicitud
     */
    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    /**
     * @param fechaSolicitud the fechaSolicitud to set
     */
    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    /**
     * @return the fechaFinalizacion
     */
    public String getFechaFinalizacion() {
        return fechaFinalizacion;
    }

    /**
     * @param fechaFinalizacion the fechaFinalizacion to set
     */
    public void setFechaFinalizacion(String fechaFinalizacion) {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    /**
     * @return the fechaAprobacion
     */
    public String getFechaAprobacion() {
        return fechaAprobacion;
    }

    /**
     * @param fechaAprobacion the fechaAprobacion to set
     */
    public void setFechaAprobacion(String fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the fechaUltimoPago
     */
    public String getFechaUltimoPago() {
        return fechaUltimoPago;
    }

    /**
     * @param fechaUltimoPago the fechaUltimoPago to set
     */
    public void setFechaUltimoPago(String fechaUltimoPago) {
        this.fechaUltimoPago = fechaUltimoPago;
    }

    /**
     * @return the nombreFiador_1
     */
    public String getNombreFiador_1() {
        return nombreFiador_1;
    }

    /**
     * @param nombreFiador_1 the nombreFiador_1 to set
     */
    public void setNombreFiador_1(String nombreFiador_1) {
        this.nombreFiador_1 = nombreFiador_1;
    }

    /**
     * @return the telefonoFiador_1
     */
    public String getTelefonoFiador_1() {
        return telefonoFiador_1;
    }

    /**
     * @param telefonoFiador_1 the telefonoFiador_1 to set
     */
    public void setTelefonoFiador_1(String telefonoFiador_1) {
        this.telefonoFiador_1 = telefonoFiador_1;
    }

    /**
     * @return the edadFiador_1
     */
    public int getEdadFiador_1() {
        return edadFiador_1;
    }

    /**
     * @param edadFiador_1 the edadFiador_1 to set
     */
    public void setEdadFiador_1(int edadFiador_1) {
        this.edadFiador_1 = edadFiador_1;
    }

    /**
     * @return the nombreFiador_2
     */
    public String getNombreFiador_2() {
        return nombreFiador_2;
    }

    /**
     * @param nombreFiador_2 the nombreFiador_2 to set
     */
    public void setNombreFiador_2(String nombreFiador_2) {
        this.nombreFiador_2 = nombreFiador_2;
    }

    /**
     * @return the telefonoFiador_2
     */
    public String getTelefonoFiador_2() {
        return telefonoFiador_2;
    }

    /**
     * @param telefonoFiador_2 the telefonoFiador_2 to set
     */
    public void setTelefonoFiador_2(String telefonoFiador_2) {
        this.telefonoFiador_2 = telefonoFiador_2;
    }

    /**
     * @return the edadFiador_2
     */
    public int getEdadFiador_2() {
        return edadFiador_2;
    }

    /**
     * @param edadFiador_2 the edadFiador_2 to set
     */
    public void setEdadFiador_2(int edadFiador_2) {
        this.edadFiador_2 = edadFiador_2;
    }

    /**
     * @return the tipoPago
     */
    public String getTipoPago() {
        return tipoPago;
    }

    /**
     * @param tipoPago the tipoPago to set
     */
    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    /**
     * @return the destinoPrestamo
     */
    public String getDestinoPrestamo() {
        return destinoPrestamo;
    }

    /**
     * @param destinoPrestamo the destinoPrestamo to set
     */
    public void setDestinoPrestamo(String destinoPrestamo) {
        this.destinoPrestamo = destinoPrestamo;
    }

    /**
     * @return the tasaInteres
     */
    public double getTasaInteres() {
        return tasaInteres;
    }

    /**
     * @param tasaInteres the tasaInteres to set
     */
    public void setTasaInteres(double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }
    /**
     * @return the img_url
     */
    public String getImg_url() {
        return img_url;
    }

    /**
     * @param img_url the img_url to set
     */
    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }
}
