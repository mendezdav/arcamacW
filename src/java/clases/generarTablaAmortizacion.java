package clases;
import Conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
/**
 *
 * @author Jorge Luis
 */
public class generarTablaAmortizacion {
    String codigoPrestamo = "";
    conexion con = null;
    ResultSet rs = null;
    List<beanTablarAmortizacionIdeal> datos = null;
    // variables para datos necesarios
    String fechaAprobacion = "";
    String fechaVencimiento = "";
    String solicitante = "";
    double cantidad = 0.00;
    double tasa_interes = 0.00;
    int plazo = 0;
    boolean estado;
    String fechaPagoAnt;
    String tipo;
    double saldoFinal;
    // constuctor para ser llamado en el proceso de cambiar estado de "En Proceso" a
    // "Activo"
    
    public generarTablaAmortizacion(String codigoPrestamo) throws SQLException
    {
        saldoFinal=0.0;
        this.tipo = "s"; // solicitud
        this.codigoPrestamo = codigoPrestamo;
        estado = true;
        String qr = "select fechaAprobacion,fechaVencimiento,cantidad,tasa_interes,plazo, "
                + "concat(s.nombre1,' ' , s.nombre2,' ', s.apellido1,' ', apellido2) "
                + "as solicitante,estado,fechaEmision from prestamos as p inner join solicitante as s "
                + "on p.idSolicitante = s.idSolicitante  where  p.codigoPrestamo='"+
                this.codigoPrestamo+"'";
        con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
            if (rs.getString("estado").equals("En proceso"))
                this.fechaAprobacion = rs.getString("fechaEmision");
            else
                this.fechaAprobacion = rs.getString("fechaAprobacion");
            
            this.fechaVencimiento = rs.getString("fechaVencimiento");
            cantidad = rs.getDouble("cantidad");
            tasa_interes = rs.getDouble("tasa_interes");
            plazo = rs.getInt("plazo");
            this.solicitante = rs.getString("solicitante");
        }
        else 
            estado = false;
    }
    
    // constructor para ser llamado desde "analisis nuevo prestamo"
    public generarTablaAmortizacion(String fecha, double cantidad, double tasa_interes, int plazo)
    {
            this.tipo = "a"; // analisis
            this.codigoPrestamo  = "ANALISIS";
            this.solicitante = "- - - ";
            fechaAprobacion = fecha;
            this.cantidad = cantidad;
            this.tasa_interes = tasa_interes * 100;
            this.plazo = plazo;
            saldoFinal=0.0;
    }
    
    public List<beanTablarAmortizacionIdeal> cargarDatos (String rutaImg) throws SQLException
    {
        String tipo="2"; // tipo = 1 ideal, 2 = real
        datos = new ArrayList<beanTablarAmortizacionIdeal>();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        nuevoPrestamo nuevo = new nuevoPrestamo();
        nuevo.setCantidad(cantidad);
        nuevo.setPlazo(plazo);
        nuevo.setInteres(tasa_interes);
        nuevo.setIva();
        nuevo.setInteres_mora();
        int plazo = nuevo.getPlazo();
        double interes = nuevo.getInteres();
        double cuotaInicial = 0.00;
        // llenar datos
        for (int x = 1; x <= plazo; x++)
        {
            beanTablarAmortizacionIdeal tabla = new beanTablarAmortizacionIdeal();
            // estableciendo parametros generales
            tabla.setCantidad(nuevo.getCantidad());
            tabla.setPlazo(String.valueOf(nuevo.getPlazo()));
            tabla.setTasa_interes(String.valueOf(nuevo.getInteres() * 100));
            tabla.setSaldoInicial(cantidad);
            tabla.setNpago(String.valueOf(x));
            tabla.setSolicitante(this.solicitante);
            // demas datos
            tabla.setLogo(rutaImg);
            tabla.setCodigoPrestamo(this.codigoPrestamo);
            tabla.setFecha(diferenciaDias.fechaActual("dd/MM/yyyy")); // actual
            //String fechaInicial = diferenciaDias.fechaActual();
            String fechaInicial = this.fechaAprobacion;
            String fechaE = ""; //Fecha establecida para el pago siguiente
            String[] split = fechaInicial.split("-");
           // String comprobante = "0";         //Dato que almacenara el comprobante
            Calendar c = Calendar.getInstance();
            Calendar original = Calendar.getInstance();               //Calendario con la fecha original
            // filtrando valor de horas en la ultima parte de la fecha, "dia hh:mm:ss" 
            if (this.tipo.equals("s"))
            {
                String tmp[] = split[2].split(" ");
                c.set(Integer.parseInt(split[0]), Integer.parseInt(split[1])-1, Integer.parseInt(tmp[0]));
            }
            else if (this.tipo.equals("a"))
            {
                split = fechaInicial.split("/");
                c.set(Integer.parseInt(split[2]), Integer.parseInt(split[1])-1, Integer.parseInt(split[0]));
            }
            original.setTime(c.getTime()); //Inicializando el calendario original 
            c.add(Calendar.MONTH, x);   //Agregando al calendario el salto.  
            fechaE = formato.format(c.getTime()); 
            /* MODIFICACION PARA CONTAR CON LOS DIAS EXACTOS
            */
            double interesTmp = 0.0;
            diferenciaDias d = new diferenciaDias();
            c.setTime(original.getTime()); //Reiniciando calendario
            int diasTranscurridos = 0;
            diferenciaDias dd = new diferenciaDias();
            if (tipo.equals("1"))
            {
                interesTmp = (cantidad * interes);   
            }
            else if (tipo.equals("2"))// interes real
            {
                if (x==1) // primer pago
                {
                    //dias = aprobacion - primerPago
                    // cortar fechas debido al formato dd/mm/yyyy mysql necesita yyyy-mm-dd
                    String fechaFin="";
                    String f1[] = fechaE.split("/");
                    String fechaInicio = f1[2] + "-" + f1[1] + "-" +f1[0];
                    switch (this.tipo) {
                        case "s":
                            {
                                String f2[] = fechaAprobacion.split("-");
                                fechaFin    = f2[0] + "-" + f2[1] + "-" +f2[2];
                                break;
                            }
                        case "a":
                            {
                                String f2[] = fechaAprobacion.split("/");
                                fechaFin    = f2[2] + "-" + f2[1] + "-" +f2[0];
                                break;
                            }
                    }
                    diasTranscurridos =  (int)(dd.obtenerDiasRetraso(fechaInicio,fechaFin));
                    fechaPagoAnt = fechaE;
                }
                else if (x>1)
                {
                    String f1[] = fechaE.split("/");
                    String f2[] = fechaPagoAnt.split("/");
                    diasTranscurridos =  (int)(dd.obtenerDiasRetraso(
                            f1[2] + "-" + f1[1] + "-" +f1[0],f2[2] + "-" + f2[1] + "-" +f2[0]));
                    fechaPagoAnt = fechaE;
                }
                interesTmp = ((cantidad * interes)/30) * diasTranscurridos;
            }
            //estableciendo interes del reporte
            tabla.setInteres(interesTmp);
            // iva del interes
            double ivaInteres = interesTmp * 0.13;
            tabla.setIVAinteres(ivaInteres);
            double capitalTmp = 0.0;
            if (cantidad < nuevo.getCuotas() + ivaInteres)
            {
                capitalTmp = cantidad + ivaInteres;
                tabla.setCuota(cantidad + interesTmp+ivaInteres); 
                tabla.setCuotaSinva(cantidad + interesTmp);
            }
            else
            {
                capitalTmp = nuevo.getCuotas()-interesTmp-ivaInteres;
                tabla.setCuota(nuevo.getCuotas());
                if ( x == 1 )
                {
                    cuotaInicial = nuevo.getCuotas()-ivaInteres;
                    tabla.setCuotaSinva(cuotaInicial);
                }else tabla.setCuotaSinva(cuotaInicial);
            }
            tabla.setCapital(capitalTmp);
            cantidad = cantidad - capitalTmp;
            if (cantidad < 0.00)
            {
                // es + por que es negativo!
                tabla.setCapital(capitalTmp + cantidad);
                tabla.setSaldoFinal(0.00);
            }
            else 
                tabla.setSaldoFinal(cantidad);
            
            tabla.setFechaPago(fechaE);
            datos.add(tabla);
        /*
         * VERIFICAR SI NO SE DEBE REALIZAR UN PAGO MES
         * AUNQUE NO SE REALIZARAN TODOS
         */ 
            if (cantidad < 0.00)
            { // se detiene debido a que el capital pendiente ya es 0.00
                break;
            }
        }
        return datos;
    }

    
    public double cargarDatosPagosExtraordinarios (String rutaImg) throws SQLException
    {
        String tipo="2"; // tipo = 1 ideal, 2 = real
        datos = new ArrayList<beanTablarAmortizacionIdeal>();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        nuevoPrestamo nuevo = new nuevoPrestamo();
        nuevo.setCantidad(cantidad);
        nuevo.setPlazo(plazo);
        nuevo.setInteres(tasa_interes);
        nuevo.setIva();
        nuevo.setInteres_mora();
        int plazo = nuevo.getPlazo();
        double interes = nuevo.getInteres();
        double cuotaInicial = 0.00;
        // cuantos dias en el prestamo
        diferenciaDias dd = new diferenciaDias();
        int dias = (int) dd.obtenerDiasRetraso(this.fechaVencimiento,this.fechaAprobacion);
        // llenar datos
        for (int x = 1; x <= dias; x++)
        {
            beanTablarAmortizacionIdeal tabla = new beanTablarAmortizacionIdeal();
            // estableciendo parametros generales
            tabla.setCantidad(nuevo.getCantidad());
            tabla.setPlazo(String.valueOf(nuevo.getPlazo()));
            tabla.setTasa_interes(String.valueOf(nuevo.getInteres() * 100));
           // JOptionPane.showMessageDialog(null,"tasa_interes" +  String.valueOf(nuevo.getInteres() * 100));
            tabla.setSaldoInicial(cantidad);
            tabla.setNpago(String.valueOf(x));
            tabla.setSolicitante(this.solicitante);
            // demas datos
            tabla.setLogo(rutaImg);
            tabla.setCodigoPrestamo(this.codigoPrestamo);
            ///tabla.setFecha(diferenciaDias.fechaActual("dd/MM/yyyy")); // actual
            tabla.setFecha(diferenciaDias.fechaActual("yyyy-MM-dd")); // actual
            //String fechaInicial = diferenciaDias.fechaActual();
            String fechaInicial = this.fechaAprobacion;
            // es de restarle uno!! porque sino no contempla casos de que vengan el mismo dia a cancelar
            
            String fechaE = ""; //Fecha establecida para el pago siguiente
            String[] split = fechaInicial.split("-");
           // String comprobante = "0";         //Dato que almacenara el comprobante
            Calendar c = Calendar.getInstance();
            Calendar original = Calendar.getInstance();               //Calendario con la fecha original
            // filtrando valor de horas en la ultima parte de la fecha, "dia hh:mm:ss" 
            if (this.tipo.equals("s"))
            {
                String tmp[] = split[2].split(" ");
                c.set(Integer.parseInt(split[0]), Integer.parseInt(split[1])-1, Integer.parseInt(tmp[0]));
            }
            else if (this.tipo.equals("a"))
            {
                split = fechaInicial.split("/");
                c.set(Integer.parseInt(split[2]), Integer.parseInt(split[1])-1, Integer.parseInt(split[0]));
            }
            original.setTime(c.getTime()); //Inicializando el calendario original 
            c.add(Calendar.DAY_OF_MONTH, x);   //Agregando al calendario el salto.  
            fechaE = formato.format(c.getTime()); 
            /* MODIFICACION PARA CONTAR CON LOS DIAS EXACTOS
            */
            double interesTmp = 0.0;
            diferenciaDias d = new diferenciaDias();
            c.setTime(original.getTime()); //Reiniciando calendario
            int diasTranscurridos = 0;
            //diferenciaDias dd = new diferenciaDias();
            if (tipo.equals("1"))
            {
                interesTmp = (cantidad * interes);   
            }
            else if (tipo.equals("2"))// interes real
            {
                if (x==1) // primer pago
                {
                    //dias = aprobacion - primerPago
                    // cortar fechas debido al formato dd/mm/yyyy mysql necesita yyyy-mm-dd
                    String fechaFin="";
                    String f1[] = fechaE.split("/");
                    String fechaInicio = f1[2] + "-" + f1[1] + "-" +f1[0];
                    switch (this.tipo) {
                        case "s":
                            {
                                String f2[] = fechaAprobacion.split("-");
                                fechaFin    = f2[0] + "-" + f2[1] + "-" +f2[2];
                                break;
                            }
                        case "a":
                            {
                                String f2[] = fechaAprobacion.split("/");
                                fechaFin    = f2[2] + "-" + f2[1] + "-" +f2[0];
                                break;
                            }
                    }
                    diasTranscurridos =  (int)(dd.obtenerDiasRetraso(fechaInicio,fechaFin));
                    fechaPagoAnt = fechaE;
                }
                else if (x>1)
                {
                    String f1[] = fechaE.split("/");
                    String f2[] = fechaPagoAnt.split("/");
                    diasTranscurridos =  (int)(dd.obtenerDiasRetraso(
                            f1[2] + "-" + f1[1] + "-" +f1[0],f2[2] + "-" + f2[1] + "-" +f2[0]));
                    fechaPagoAnt = fechaE;
                }
                interesTmp = ((cantidad * interes)/30) * diasTranscurridos;
            }
            //estableciendo interes del reporte
            tabla.setInteres(interesTmp);
            // iva del interes
            double ivaInteres = interesTmp * 0.13;
            tabla.setIVAinteres(ivaInteres);
            double capitalTmp = 0.0;
            // para este tipo se debe de complentar cuota diaria!
            nuevo.getCuotas();
            if (cantidad < nuevo.getCuotaDiaria() + ivaInteres)
            {
                capitalTmp = cantidad + ivaInteres;
                tabla.setCuota(cantidad + interesTmp+ivaInteres); 
                tabla.setCuotaSinva(cantidad + interesTmp);
            }
            else
            {
                capitalTmp = nuevo.getCuotaDiaria()-interesTmp-ivaInteres;
                tabla.setCuota(nuevo.getCuotaDiaria());
                if ( x == 1 )
                {
                    cuotaInicial = nuevo.getCuotaDiaria()-ivaInteres;
                    tabla.setCuotaSinva(cuotaInicial);
                }else tabla.setCuotaSinva(cuotaInicial);
            }
            tabla.setCapital(capitalTmp);
            cantidad = cantidad - capitalTmp;
            if (cantidad < 0.00)
            {
                // es + por que es negativo!
                tabla.setCapital(capitalTmp + cantidad);
                tabla.setSaldoFinal(0.00);
            }
            else 
                tabla.setSaldoFinal(cantidad);
            // tranformando la fecha
            // 24/09/2993
            String fechaEpartes[] = fechaE.split("/");
            String fechaEx = fechaEpartes[2] + "-" + fechaEpartes[1] + "-" + fechaEpartes[0];
            //JOptionPane.showMessageDialog(null, "fecha ex: " + fechaEx);
            tabla.setFechaPago(fechaEx);
            datos.add(tabla);
        /*
         * VERIFICAR SI NO SE DEBE REALIZAR UN PAGO MES
         * AUNQUE NO SE REALIZARAN TODOS
         */ 
            if (cantidad < 0.00)
            { // se detiene debido a que el capital pendiente ya es 0.00
                break;
            }
            String fechaActual = diferenciaDias.fechaActual("yyyy-MM-dd");
            if (fechaEx.equals(fechaActual))
            {
                saldoFinal = datos.get(x-1).getSaldoFinal();
                break;
            }
        }
        //JOptionPane.showMessageDialog(null,"SALDO IDEAL: " + saldoFinal);
        return saldoFinal;
    }

    
    
    public boolean ingresarTabla() throws SQLException
    {
        boolean ingresado = true;
        con = new conexion();
        String qr ="";
        String fechasPartes[] = null;
        String fechaPago ="";
        String fecha="";
        for (int x = 0 ; x < datos.size(); x++)
        {
            System.out.print(datos.get(x).getCodigoPrestamo());
            System.out.print(datos.get(x).getFechaPago());
            System.out.print(datos.get(x).getFecha());
            if (this.tipo.equals("s"))
            {
                fechasPartes = datos.get(x).getFechaPago().split("/");
                fechaPago = fechasPartes[2] + "-" + fechasPartes[1] + "-" + fechasPartes[0];
                fechasPartes = datos.get(x).getFecha().split("/");
                fecha = fechasPartes[2] + "-" + fechasPartes[1] + "-" + fechasPartes[0];
            }
            
            qr = "insert into tabla_amortizacion(codigoPrestamo,cuotaSIVA,"
                 + "cuotaIVA,monto,plazo, fechaPago, fechaPrestamo,Npago,saldoInicial, capital, interes,"
                 + "interesIVA,interesMora,interesMoraIVA,saldoFinal,sumaCapital, sumaInteres, sumaInteresMora)"
                 + " values ('" + datos.get(x).getCodigoPrestamo() + "'," + datos.get(x).getCuotaSinva() + ","
                 +  datos.get(x).getCuota() + "," + datos.get(x).getCantidad() + "," + datos.get(x).getPlazo()
                 + ",'" + fechaPago + "','" + fecha + "'," +datos.get(x).getNpago() 
                 + "," + datos.get(x).getSaldoInicial() + "," + datos.get(x).getCapital() + "," + datos.get(x).getInteres()
                 + "," + datos.get(x).getIVAinteres()+ ",0.00,0.00," + datos.get(x).getSaldoFinal() + ",0.00,0.00,0.00);";
            ingresado = con.actualizar(qr);
            if (ingresado == false)
                break;
        }
        return ingresado;
    }
    
    public static void main(String args[]) throws SQLException
    {
        generarTablaAmortizacion z = new generarTablaAmortizacion("MM0101");
        z.cargarDatos("null");
        z.ingresarTabla();
    }
}