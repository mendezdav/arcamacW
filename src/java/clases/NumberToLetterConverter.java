/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 * Esta clase provee la funcionalidad de convertir un numero representado en
 * digitos a una representacion en letras. Mejorado para leer centavos
 * 
 * @author Camilo Nova
 * descargable de https://github.com/AxiaCore/numero-a-letras/blob/master/java/NumberToLetterConverter.java
 */
public class NumberToLetterConverter {

    private static final String[] UNIDADES = { "", "UN ", "DOS ", "TRES ",
            "CUATRO ", "CINCO ", "SEIS ", "SIETE ", "OCHO ", "NUEVE ", "DIEZ ",
            "ONCE ", "DOCE ", "TRECE ", "CATORCE ", "QUINCE ", "DIECISEIS ",
            "DIECISIETE ", "DIECIOCHO ", "DIECINUEVE ", "VEINTE " };

    private static final String[] UNIDADES_UNO = { "", "UNO ", "DOS ", "TRES ",
            "CUATRO ", "CINCO ", "SEIS ", "SIETE ", "OCHO ", "NUEVE ", "DIEZ ",
            "ONCE ", "DOCE ", "TRECE ", "CATORCE ", "QUINCE ", "DIECISEIS",
            "DIECISIETE ", "DIECIOCHO ", "DIECINUEVE ", "VEINTE " };

    private static final String[] DECENAS = { "VEINTI ", "TREINTA ", "CUARENTA ",
            "CINCUENTA ", "SESENTA ", "SETENTA ", "OCHENTA ", "NOVENTA ",
            "CIEN " };

    private static final String[] CENTENAS = { "CIENTO ", "DOSCIENTOS ",
            "TRESCIENTOS ", "CUATROCIENTOS ", "QUINIENTOS ", "SEISCIENTOS ",
            "SETECIENTOS ", "OCHOCIENTOS ", "NOVECIENTOS " };

    private static final String[] MESES = {"","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO",
            "AGOSTO","SEPTIEMBRE","OCTUMBRE","NOVIEMBRE","DICIEMBRE"};
    /**
     * Convierte a letras un numero de la forma $123,456.32
     * 
     * @param number
     *            Numero en representacion texto
     * @throws NumberFormatException
     *             Si valor del numero no es valido (fuera de rango o )
     * @return Numero en letras
     */
    public static String convertNumberToLetter(String number)
            throws NumberFormatException {
        return convertNumberToLetter(Double.parseDouble(number));
    }

    /**
     * Convierte un numero en representacion numerica a uno en representacion de
     * texto. El numero es valido si esta entre 0 y 999'999.999
     * 
     * @param number
     *            Numero a convertir
     * @return Numero convertido a texto
     * @throws NumberFormatException
     *             Si el numero esta fuera del rango
     */
    public static String convertNumberToLetter(double doubleNumber)
            throws NumberFormatException {

        StringBuilder converted = new StringBuilder();
        String patternThreeDecimalPoints = "#.###";
        DecimalFormat format = new DecimalFormat(patternThreeDecimalPoints);
        format.setRoundingMode(RoundingMode.DOWN);
        // formateamos el numero, para ajustarlo a el formato de tres puntos
        // decimales
        String formatedDouble = format.format(doubleNumber);
        doubleNumber = Double.parseDouble(formatedDouble);

        // Validamos que sea un numero legal
        if (doubleNumber > 999999999)
            throw new NumberFormatException(
                    "El numero es mayor de 999'999.999, "
                            + "no es posible convertirlo");

        if (doubleNumber < 0)
            throw new NumberFormatException("El numero debe ser positivo");

        String splitNumber[] = String.valueOf(doubleNumber).replace('.', '#').split("#");
        if (splitNumber[1].length() == 1)
            splitNumber[1]+="0";
        
        // Descompone el trio de millones
        int millon = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
                8))
                + String.valueOf(getDigitAt(splitNumber[0], 7))
                + String.valueOf(getDigitAt(splitNumber[0], 6)));
        if (millon == 1)
            converted.append("UN MILLON ");
        else if (millon > 1)
            converted.append(convertNumber(String.valueOf(millon))
                    + "MILLONES ");

        // Descompone el trio de miles
        int miles = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0], 5))
                + String.valueOf(getDigitAt(splitNumber[0], 4))
                + String.valueOf(getDigitAt(splitNumber[0], 3)));
        if (miles == 1)
            converted.append("MIL ");
        else if (miles > 1)
            converted.append(convertNumber(String.valueOf(miles)) + "MIL ");

        // Descompone el ultimo trio de unidades
        int cientos = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0], 2))
                + String.valueOf(getDigitAt(splitNumber[0], 1))
                + String.valueOf(getDigitAt(splitNumber[0], 0)));
        if (cientos == 1)
            converted.append("UNO ");

        if (millon + miles + cientos == 0)
            converted.append("CERO");
        if (cientos > 1)
            converted.append(convertNumber(String.valueOf(cientos)));
        converted.append("DOLARES");
        // Descompone los centavos
        int centavos = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[1], 2))
                + String.valueOf(getDigitAt(splitNumber[1], 1))
                + String.valueOf(getDigitAt(splitNumber[1], 0)));
        
        boolean exacto = true;
        if (centavos == 1)
        {
            converted.append(" CON UN CENTAVO");
            exacto = false;
        }
        else if (centavos > 1)
        {
            converted.append(" CON " + convertNumber(String.valueOf(centavos)) + "CENTAVOS");
            exacto = false;
        }
        if (exacto)
            converted.append(" EXACTOS");
        
        // MODIFICANCION SISTEMA ARCAMAC 2014

        return converted.toString();
    }

    /**
     * Convierte los trios de numeros que componen las unidades, las decenas y
     * las centenas del numero.
     * 
     * @param number
     *            Numero a convetir en digitos
     * @return Numero convertido en letras
     */
    private static String convertNumber(String number) {

        if (number.length() > 3)
            throw new NumberFormatException(
                    "La longitud maxima debe ser 3 digitos");

        // Caso especial con el 100
        if (number.equals("100")) {
            return "CIEN";
        }

        StringBuilder output = new StringBuilder();
        if (getDigitAt(number, 2) != 0)
            output.append(CENTENAS[getDigitAt(number, 2) - 1]);

        int k = Integer.parseInt(String.valueOf(getDigitAt(number, 1))
                + String.valueOf(getDigitAt(number, 0)));

        if (k <= 20)
            output.append(UNIDADES[k]);
        else if (k > 30 && getDigitAt(number, 0) != 0)
            output.append(DECENAS[getDigitAt(number, 1) - 2] + "Y "
                    + UNIDADES[getDigitAt(number, 0)]);
        else
            output.append(DECENAS[getDigitAt(number, 1) - 2]
                    + UNIDADES[getDigitAt(number, 0)]);

        return output.toString();
    }

    /**
     * Retorna el digito numerico en la posicion indicada de derecha a izquierda
     * 
     * @param origin
     *            Cadena en la cual se busca el digito
     * @param position
     *            Posicion de derecha a izquierda a retornar
     * @return Digito ubicado en la posicion indicada
     */
    private static int getDigitAt(String origin, int position) {
        if (origin.length() > position && position >= 0)
            return origin.charAt(origin.length() - position - 1) - 48;
        return 0;
    }
    
    /*
     * MODIFICACIONES PRESTAMOS MUTUO
     */
    public static String NIT_DUI(String nit)
    {
        StringBuilder converted = new StringBuilder();
        // conversion del NIT
        int contador = 0, num = 0;
        nit = nit.trim();
        while (contador < nit.length())
        {
            String tmp = nit.substring(contador, contador+1);
            if (tmp.equals("-"))
             converted.append(" - ");
            else
            { 
                // es un valor numerico
                try 
                {
                    num = Integer.parseInt(tmp);
                    if (num == 0)
                        converted.append(" CERO ");
                    else
                        if (num  == 1)
                        converted.append(" UNO ");
                    else
                    converted.append(UNIDADES[num]);
                }
                catch (Exception e)
                {
                    System.out.printf("Error conversion " + tmp + "a entero, Error: " + e.getMessage());
                }
            }
            
            contador++;
        }
        
        return converted.toString().trim();
    }
    
    public static String convertNumeroEnteroLetras(double doubleNumber)
            throws NumberFormatException {

        StringBuilder converted = new StringBuilder();
        String patternThreeDecimalPoints = "#.###";
        DecimalFormat format = new DecimalFormat(patternThreeDecimalPoints);
        format.setRoundingMode(RoundingMode.DOWN);
        // formateamos el numero, para ajustarlo a el formato de tres puntos
        // decimales
        String formatedDouble = format.format(doubleNumber);
        doubleNumber = Double.parseDouble(formatedDouble);

        // Validamos que sea un numero legal
        if (doubleNumber > 999999999)
            throw new NumberFormatException("El numero es mayor de 999'999.999,no es posible convertirlo");

        if (doubleNumber < 0)
            throw new NumberFormatException("El numero debe ser positivo");
        String splitNumber[] = String.valueOf(doubleNumber).replace('.', '#').split("#");
         if (splitNumber[1].length() == 1)
            splitNumber[1]+="0";
        
        // Descompone el trio de millones
        int millon = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
                8))
                + String.valueOf(getDigitAt(splitNumber[0], 7))
                + String.valueOf(getDigitAt(splitNumber[0], 6)));
        if (millon == 1)
            converted.append("UN MILLON ");
        else if (millon > 1)
            converted.append(convertNumber(String.valueOf(millon))
                    + "MILLONES ");
        // Descompone el trio de miles
        int miles = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],5))
                + String.valueOf(getDigitAt(splitNumber[0], 4))
                + String.valueOf(getDigitAt(splitNumber[0], 3)));
        if (miles == 1)
            converted.append("MIL ");
        else if (miles > 1)
            converted.append(convertNumber(String.valueOf(miles)) + "MIL ");

        // Descompone el ultimo trio de unidades
        int cientos = Integer.parseInt(String.valueOf(getDigitAt(
                splitNumber[0], 2))
                + String.valueOf(getDigitAt(splitNumber[0], 1))
                + String.valueOf(getDigitAt(splitNumber[0], 0)));
        if (cientos == 1)
            converted.append("UNO");

        if (millon + miles + cientos == 0)
            converted.append("CERO");
        if (cientos > 1)
            converted.append(convertirDecimalANumero(String.valueOf(cientos)));
        return converted.toString().trim();
    }
    
    
    public static String convertInteresAletra(double doubleNumber)
            throws NumberFormatException {

        StringBuilder converted = new StringBuilder();
        String patternThreeDecimalPoints = "#.###";
        DecimalFormat format = new DecimalFormat(patternThreeDecimalPoints);
        format.setRoundingMode(RoundingMode.DOWN);
        // formateamos el numero, para ajustarlo a el formato de tres puntos
        // decimales
        String formatedDouble = format.format(doubleNumber);
        doubleNumber = Double.parseDouble(formatedDouble);

        // Validamos que sea un numero legal
        if (doubleNumber > 999999999)
            throw new NumberFormatException(
                    "El numero es mayor de 999'999.999, "
                            + "no es posible convertirlo");

        if (doubleNumber < 0)
            throw new NumberFormatException("El numero debe ser positivo");

        String splitNumber[] = String.valueOf(doubleNumber).replace('.', '#').split("#");
         if (splitNumber[1].length() == 1)
            splitNumber[1]+="0";
        
        // Descompone el trio de millones
        int millon = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
                8))
                + String.valueOf(getDigitAt(splitNumber[0], 7))
                + String.valueOf(getDigitAt(splitNumber[0], 6)));
        if (millon == 1)
            converted.append("UN MILLON ");
        else if (millon > 1)
            converted.append(convertNumber(String.valueOf(millon))+ "MILLONES ");

        // Descompone el trio de miles
        int miles = Integer.parseInt(String.valueOf(getDigitAt(splitNumber[0],
                5))
                + String.valueOf(getDigitAt(splitNumber[0], 4))
                + String.valueOf(getDigitAt(splitNumber[0], 3)));
        if (miles == 1)
            converted.append("MIL ");
        else if (miles > 1)
            converted.append(convertNumber(String.valueOf(miles)) + "MIL ");

        // Descompone el ultimo trio de unidades
        int cientos = Integer.parseInt(String.valueOf(getDigitAt(
                splitNumber[0], 2))
                + String.valueOf(getDigitAt(splitNumber[0], 1))
                + String.valueOf(getDigitAt(splitNumber[0], 0)));
        if (cientos == 1)
            converted.append("UNO");

        if (millon + miles + cientos == 0)
            converted.append("CERO");
        if (cientos > 1)
            converted.append(convertNumber(String.valueOf(cientos)));
        //converted.append("DOLARES");
        
        // Descompone los centavos o parte fraccionaria del numero decimal
        int centavos = Integer.parseInt(String.valueOf(getDigitAt(
                splitNumber[1], 2))
                + String.valueOf(getDigitAt(splitNumber[1], 1))
                + String.valueOf(getDigitAt(splitNumber[1], 0)));
        if (centavos == 1){
            converted.append("PUNTO UNO");}
        else if (centavos > 1)
            converted.append("PUNTO " + convertirDecimalANumero(String.valueOf(centavos)));
        return converted.toString().trim();
    }

     private static String convertirDecimalANumero(String number) {

        if (number.length() > 3)
            throw new NumberFormatException(
                    "La longitud maxima debe ser 3 digitos");

        // Caso especial con el 100
        if (number.equals("100")) {
            return "CIEN";
        }

        StringBuilder output = new StringBuilder();
        if (getDigitAt(number, 2) != 0)
            output.append(CENTENAS[getDigitAt(number, 2) - 1]);

        int k = Integer.parseInt(String.valueOf(getDigitAt(number, 1))
                + String.valueOf(getDigitAt(number, 0)));

        if (k <= 20)
        {
            output.append(UNIDADES_UNO[k]);
        }
        else if (k > 30 && getDigitAt(number, 0) != 0)
            output.append(DECENAS[getDigitAt(number, 1) - 2] + "Y "
                    + UNIDADES_UNO[getDigitAt(number, 0)]);
        else
            output.append(DECENAS[getDigitAt(number, 1) - 2]
                    + UNIDADES_UNO[getDigitAt(number, 0)]);

        return output.toString().trim();
    }
 
    public static String fechaAletra(String fecha)
    {
        StringBuilder resultado = new StringBuilder();
        String partes[] = fecha.split("-");
        resultado.append(partes[2] + " DE ");
        resultado.append(MESES[Integer.parseInt(partes[1])] + " DE ");
        resultado.append(convertNumeroEnteroLetras( Double.valueOf(partes[0])));
        return resultado.toString().trim();
    }
     
    public static void main(String args[])
    {
     NumberToLetterConverter n = new NumberToLetterConverter();
     String resultado = n.NIT_DUI("0614-030991-120-0");
     String resultado6 = n.NIT_DUI("00073141-5");
     String resultado2 = n.convertNumeroEnteroLetras(1);
     String resultado3 = n.convertInteresAletra(2.01);
     String resultado4 = n.fechaAletra("2011-06-14");
     String resultado5 = n.convertNumberToLetter(2);
   // CERO CERO CERO SIETE TRES UNO CUATRO UNO  - CINCO
      n.convertNumeroEnteroLetras(5);
     
     System.out.println(resultado);
     System.out.println(resultado2);
     System.out.println(resultado3);
     System.out.println(resultado4);
     System.out.println(resultado6);
     
     String presertatutetis = "<p>Yo, <strong><u>CARLOS HUMBERTO CAMPOS CAMPOS</u>,</strong> mayor de edad, <strong><u>MECANICO</u>,</strong> del domicilio  de <strong><u>SAN PEDRO PERULAPAN</u></strong>, jurisdicción del departamento de <strong><u>CUSCATLAN</u></strong><strong>, </strong>que me  denominare el deudor, OTORGO: <strong><u>I)-  Monto.</u></strong> Que en esta fecha, he recibido de la SOCIEDAD AYUDA Y RESERVA  COLECTIVA PARA AEROTECNICOS DEL ALA MAC, SOCIEDAD COOPERATIVA DE  RESPONSABILIDAD LIMITADA DE CAPITAL VARIABLE, del domicilio de Ilopango, la cual  en adelante se denominará &ldquo;LA   ACREDORA&rdquo;, la suma de <strong><u>DOS  MIL DOLARES EXACTOS</u></strong>, a título de MUTUO <strong><u>II)INTERESES.</u></strong>Que reconoceré sobre la suma mutuada, el  interés del <strong><u>DOS PUNTO SETENTA Y SIETE</u></strong> por ciento mensual sobre saldos; <strong><u>III)-  PLAZO, FORMA Y LUGAR DE PAGO.</u></strong> Que pagaré la suma mutuada en un plazo de <strong><u>TREINTA Y SEIS</u></strong>meses contados  a partir del día <strong><u>CATORCE DE JUNIO</u></strong>del año <strong><u>DOS MIL CATORCE</u></strong> por medio de <strong><u>TREINTA Y SEIS</u></strong> cuotas mensuales, fijas y sucesivas de <strong><u>NOVENTA  Y CINCO DOLARES CON SESENTA Y NUEVE CENTAVOS DE DOLAR</u></strong> cada una que  comprenden abono a capital, intereses e IVA, siendo exigible la primera cuota  el día <strong><u>CATORCE DE JULIO</u></strong>del  año <strong><u>DOS MIL CATORCE</u></strong>, y así  sucesivamente las restantes cuotas el día <strong><u>CATORCE</u></strong> de cada mes del plazo estipulado hasta la total cancelación de la deuda,  obligándome a hacer todo pago en las oficinas de la acreedora. <strong><u>IV)- MORA</u></strong><u>.</u> Que la mora en  el pago de una de las cuotas de abono a capital e intereses en la forma  estipulada, hará caducar el plazo y será exigible de inmediato la totalidad de  la presente obligación, como si fuere de plazo vencido, y el interés de la suma  mutuada se elevará en <strong><u>DOS PUNTO SETENTA Y SIETE</u></strong> por ciento de interés mensual, adicional al interés pactado, desde la fecha de  la mora sin que ello signifique prórroga del plazo estipulado <strong><u>V)-GARANTÍA PERSONAL SOLIDARIA</u>. </strong>Presentes  los señores <strong><u>ALONSO RIVELINO RAMIREZ  VASQUEZ</u>,</strong> mayor de edad, <strong><u>TECNICO EN AVIACION</u></strong>, del domicilio de <strong><u>ILOPANGO</u></strong>, jurisdicción del departamento de <strong><u>SAN SALVADOR</u></strong><strong>,</strong> Y <strong><u>ENMANUEL HERNANDEZ AREVALO</u>,</strong> mayor de edad, <strong><u>EMPLEADO</u></strong>, del domicilio de <strong><u>SAN MARTIN</u></strong>, jurisdicción del  departamento de <strong><u>SAN  SALVADOR</u></strong><strong>,</strong> y enterados de los conceptos vertidos en el  presente documento, se constituyen fiadores y codeudores solidarios con  renuncia al beneficio de excusión de bienes, para responder de las obligaciones  que el deudor contrae a favor de la acreedora. <strong><u>VI).-DOMICILIO ESPECIAL.</u></strong> Que el deudor y los fiadores y  codeudores solidarios señalamos ésta ciudad como domicilio especial para los  efectos legales de la presente obligación; renunciamos al derecho de apelar del  decreto de embargo, sentencia  de remate y  demás providencias del juicio ejecutivo e incidentes; y no exigiremos cuentas  ni fianza al depositario que se designare; y los gastos que ocasionare el cobro  de la deuda serán por nuestra cuenta, inclusive los llamados personales aún  cuando conforme a las reglas generales no fuéremos condenados en costas. Así  nos expresamos, leemos lo escrito, lo ratificamos y firmamos, en la ciudad de Ilopango,  el día <strong><u>CATORCE DE JUNIO DE DOS MIL CATORCE.</u></strong></p>\n" +
"<p>&nbsp;</p>\n" +
"<p>En  la ciudad de Ilopango, a las <strong><u>NUEVE HORAS</u>, </strong>del día <strong><u>CATORCE DE JUNIO</u></strong>del  año <strong><u>DOS MIL CATORCE</u></strong>. Ante mí  VICTOR RENE GUZMAN, Notario, de este domicilio y de la ciudad de SAN SALVADOR, COMPARECEN:  señor <strong><u>CARLOS HUMBERTO CAMPOS CAMPOS</u></strong>,  de <strong><u>CUARENTA Y DOS</u></strong>años, <strong><u>MECANICO</u>,</strong> del domicilio de <strong><u>SAN PEDRO PERULAPAN</u></strong>, jurisdicción del departamento de <strong><u>CUSCATLAN</u></strong><strong>,</strong> con  Tarjeta de Identificación Tributaria <strong><u>CERO  SIETE UNO CERO – UNO UNO UNO DOS SIETE UNO – UNO CERO UNO - UNO</u></strong>y los señores <strong><u>ALONSO RIVELINO RAMIREZ VASQUEZ</u></strong>,  de <strong><u>CUARENTA</u></strong>años, <strong><u>TECNICO EN AVIACION</u></strong>, del  domicilio de <strong><u>ILOPANGO</u></strong>, jurisdicción del departamento de <strong><u>SAN SALVADOR</u>,</strong> con Tarjeta de  Identificación Tributaria <strong><u>CERO SEIS  UNO CUATRO – DOS NUEVE CERO CINCO SIETE CUATRO – UNO UNO CERO - DOS</u></strong> Y <strong><u>ENMANUEL HERNANDEZ AREVALO</u></strong>, de <strong><u>CUARENTA Y UN</u></strong>años, <strong><u>EMPLEADO</u></strong>, del domicilio de <strong><u>SAN MARTIN</u></strong>, jurisdicción del departamento de <strong><u>SAN SALVADOR</u></strong><strong>,</strong> con Tarjeta  de Identificación Tributaria <strong><u>CERO  SEIS UNO CUATRO – DOS UNO CERO UNO SIETE TRES – UNO UNO OCHO - CERO</u></strong>, a  quienes no conozco pero identifico con su Documento Único de Identidad número: <strong><u>CERO CERO CERO SIETE TRES UNO CUATRO UNO  - CINCO</u></strong>, <strong><u>CERO DOS CERO CUATRO  OCHO SEIS NUEVE DOS - CINCO</u></strong>Y <strong><u>CERO  CERO UNO CINCO SEIS OCHO UNO OCHO - DOS</u></strong>respectivamente y DICEN: Que  son suyas las firmas, conceptos y obligaciones que aparecen en el documento que  antecede, fechando en esta ciudad este mismo día, y el cual declara el primer  compareciente: Que en esta fecha, ha recibido de la sociedad AYUDA Y RESERVA  COLECTIVA PARA AEROTECNICOS DEL ALA MAC, SOCIEDAD COOPERATIVA DE  RESPONSABILIDAD LIMITADA DE CAPITAL VARIABLE, LA    SUMA DE <strong><u>DOS MIL DOLARES EXACTOS</u></strong> a título de mutuo, al interés del <strong><u>DOS PUNTO  SETENTA Y SIETE</u></strong> por ciento mensual sobre saldos, para el plazo  de <strong><u>TREINTA Y SEIS</u></strong>meses,  pagaderos por medio de <strong><u>TREINTA Y SEIS</u></strong>cuotas mensuales de <strong><u>NOVENTA Y  CINCO DOLARES CON SESENTA Y NUEVE CENTAVOS DE DOLAR</u></strong>, cada una a partir  del día <strong><u>CATORCE DE JULIO</u></strong> del  año <strong><u>DOS MIL CATORCE</u>,</strong> y bajo  las demás condiciones que en el indicado documento se consignan; y el segundo y  tercer comparecientes: Que se constituyen fiadores y codeudores solidarios para  responder de las obligaciones ya indicadas, renunciando al beneficio de  excusión de bienes. DOY FE: De ser auténticas la expresadas firmas, por haber  sido puestas a mi presencia por los comparecientes al pié del referido  documento, así como de que reconocieron como suyos todos los conceptos vertidos  en el mismo; que les expliqué los efectos legales de esta acta notarial que  consta en dos hojas; y leído que les hube todo lo escrito en un solo acto,  ratificaron su contenido y firmamos, DOY FE.</p>";
     
    }
}
