/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import Conexion.conexion;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Pochii
 */
public class classPagosAnt {
    public int mensaje;                  //Mensaje que es devuelto a jsp
   /**Propiedades de los pagos**/
    String idPago;
    double monto;
    double monto_auxiliar;                  //En un momento, tendra el valor del monto sobrante (en caso que pague mas)
    int banderaMontoAux;                    // cero si monto aux == 0.00 , 1 si es mayor a 0.00
    
    String codigoPrestamo;                  //Codigo de prestamo BD
    String estadoMora;
    String fechaEstablecida;                //Fecha establecida del pago actual
    String comprobante;
    double intereses;                       //Intereses a pagar BD
    double interes_moratorio;               //Mora a pagar BD
    double capital;                         //Capital a pagar BD
    
    double pago_interes;                    //Abono de intereses BD
    double pago_interes_moratorio;          //Abono a mora BD
    double pago_capital;                    //Abono a capital BD
    
    double mpago_interes;                   //Parte del monto que va a intereses
    double mpago_interes_moratorio;         //Parte del monto que va a intereses moratorios
    double mpago_capital;                   //Parte del monto que va a capital
    String diaActual;
    
    /**Propiedades de base de datos**/
    conexion con;
    ResultSet rs;
    CachedRowSetImpl crPagos;
    
    public classPagosAnt(){
        rs = null;
        con = null;
    }
    /**
     * @param pidPago id del pago a procesar
     * @param pmonto  Monto a abonar
     */
    public classPagosAnt(String pidPago, double pmonto, String diaActual){
        this.monto  = pmonto;
        this.idPago = pidPago;
        this.diaActual = diaActual;
        crPagos = null;
    }
    /**
     * Funcion para llenar las propiedades de la clase con los datos de la base de datos
     * @return Verdadero en caso que la seleccion se hiciera correctamente, falso en caso contrario
     * @throws java.sql.SQLException
     */
    public boolean llenarDatos(String idPago) throws SQLException{
        try{
            this.crPagos = getPagos("WHERE idPago = " + idPago + " AND estado != 'Pagado'");
            while(crPagos.next()){   
                this.codigoPrestamo         = crPagos.getString("codigoPrestamo");
                this.capital                = crPagos.getDouble("capital");
                this.intereses              = crPagos.getDouble("intereses");
                this.capital                = crPagos.getDouble("capital");
                this.interes_moratorio      = crPagos.getDouble("interes_moratorio");
                this.pago_interes           = crPagos.getDouble("pago_interes");
                this.pago_interes_moratorio = crPagos.getDouble("pago_interes_moratorio");
                this.pago_capital           = crPagos.getDouble("pago_capital");   
                this.fechaEstablecida       = crPagos.getString("fechaEstablecida");
            }
            return true;
        }
        catch(SQLException e){
            System.out.println("Hubo un error: " + e);
            return false;
        } //fin catch
    }//fin llenar Datos
    /**
     * @param condicion filtrado de la query de pagos, puede ser un where, order by,group by
     * @return Resultset con los datos consultados
     * @throws java.sql.SQLException
     */
    public CachedRowSetImpl getPagos(String condicion) throws SQLException{
        CachedRowSetImpl crs = new CachedRowSetImpl();
        String query = "SELECT (@rownum:=@rownum+1) as fila, idPago, codigoPrestamo, comprobante, monto, fechaPago, fechaEstablecida," +
                       "intereses, capital, estado, pago_interes, pago_capital, interes_moratorio, pago_interes_moratorio, saldo, mora " +
                       "FROM (SELECT @rownum:=-1)r,pagos ";
        /**Agredando condicion a la query**/
        query += condicion;
        try{
            con  = new conexion();
            con.setRS(query);
            rs = con.getRs();
            crs.populate(rs);
            return crs;
        }
        catch(SQLException e){
            System.out.println("query: " + query + "\n");
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        finally{
            con.cerrarConexion();
        }
        
    }//fin getPagos()
    
    /**
     * Imprimir
     */
    public void imprimir(){
        System.out.println("Monto: $" + this.monto);
        System.out.println("Parte del monto a capital: " + this.mpago_capital);
        System.out.println("Parte del monto a intereses: " + this.mpago_interes);
        System.out.println("Parte del monto a interes_moratorio: " + this.mpago_interes_moratorio);
    }
    /**
     * Esta funcion es llamada desde insertarAbono
     * Funcion para separar el monto obtenido
     */
    public void desglosarMonto(){
        monto_auxiliar = this.monto;
        if(monto_auxiliar >= (interes_moratorio - pago_interes_moratorio))
        {
            mpago_interes_moratorio = (interes_moratorio - pago_interes_moratorio);
            monto_auxiliar -= mpago_interes_moratorio;
            if(monto_auxiliar >= (intereses - pago_interes))
            {
                mpago_interes = (intereses - pago_interes);
                monto_auxiliar -= mpago_interes;
                if(monto_auxiliar >= (capital - pago_capital))
                {
                    mpago_capital = (capital - pago_capital);
                    monto_auxiliar -= mpago_capital;
                }
                else{ //Si no cubre el capital
                    mpago_capital = monto_auxiliar;
                    monto_auxiliar = 0;
                }//fin if capital
            }
            else{ //Si no cubre intereses normales
                mpago_interes = monto_auxiliar;
                monto_auxiliar = 0;
            } //fin else interes
        }
        else{ //Si no cubre intereses moratorios
            mpago_interes_moratorio = monto_auxiliar;
            monto_auxiliar = 0;
        } //fin if interes moratorio
        monto_auxiliar = diferenciaDias.redondear(monto_auxiliar);
        mpago_interes_moratorio = diferenciaDias.redondear(mpago_interes_moratorio);
        mpago_interes = diferenciaDias.redondear(mpago_interes);
        mpago_capital = diferenciaDias.redondear(mpago_capital);
    }
    
    /**
     * Esta funcion se llama desde insertarAbono
     * @param idPago
     * @return
     * @throws SQLException 
     */
    private String generarComprobanteAbono(String idPago) throws SQLException
    {
        int correlativo=0;
        String comprobanteAnt="";
        String comprobantePago="";
       // obtener los abonos anteriores a dicho pago
        String query = "select p.comprobante as Cpago,a.comprobante  as Cabono from pagos as p\n" +
                        "inner join abonos as a on p.idPago = a.idPago where\n" +
                        "p.idPago=" + idPago+" order by Cabono DESC limit 1;";
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        if (rs.next())
        {
            comprobanteAnt = rs.getString("Cabono");
            comprobantePago = rs.getString("Cpago");
        }
        else
        {
            query ="select comprobante from pagos where idPago=" + idPago;
            con.setRS(query);
            rs = con.getRs();
            if (rs.next()){
                comprobanteAnt = rs.getString("comprobante") + "00";
                comprobantePago = rs.getString("comprobante");
            }//fin if
        }
        con.cerrarConexion();
        // obtener correlativo del abono
        correlativo = Integer.parseInt(comprobanteAnt.substring(6,8));
        correlativo++;
        if  (correlativo < 10)
            return (comprobantePago + "0" + correlativo);
        else
            return (comprobantePago + correlativo);
    }
    /**
     * Metodo principal de la clase
     * @param idPago
     * @param monto
     * @return
     * @throws SQLException 
     */
    
    public String getComprobateAbono()
    {
        return this.comprobante;
    }
    public boolean insertarAbono(String idPago, Double monto) throws SQLException
    {
        //JOptionPane.showMessageDialog(null, "idPago: " + idPago + "\n Monto: " + monto);
        llenarDatos(idPago);
        String qr = "select fechaPago from pagos where estado != 'Pendiente' order by comprobante ASC limit 1"; 
        //JOptionPane.showMessageDialog(null, "qr: " + qr);
        conexion con = new conexion();
        String fechapagoAnt = "";
        try{
            con.setRS(qr);
            rs = con.getRs();
            rs.next();
            fechapagoAnt = rs.getString("fechaPago");
        }
        catch (Exception e)
        { fechapagoAnt = "1000-01-01"; }
        this.comprobante = generarComprobanteAbono(idPago);
        boolean estado = false;
        String query = "";
        this.monto = monto;
        desglosarMonto();
        // obtener saldo anterior del prestamo
        query ="select p.saldo from prestamos as p inner join pagos on pagos.codigoPrestamo = p.codigoPrestamo where pagos.idPago=" + idPago;
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        double saldoant = rs.getDouble("saldo");
        query = "select mora from pagos where idPago=" + idPago;
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        this.monto = monto;
        this.estadoMora = rs.getString("mora");
        double monto_aux_temp = monto_auxiliar;
        if (monto_auxiliar > 0.00)
        {
            mpago_capital += monto_auxiliar;
            monto_auxiliar = 0.00;
            this.banderaMontoAux = 1;
        }
       if (estadoMora.equals("1")) // esta en mora
        {

            query = "insert into abonos(idPago,monto,fechaPago,pago_interes,pago_capital,pago_interes_moratorio,comprobante,fechaPagoAnt,saldoAnt,SaldoActual) "
                    + "values("+idPago+","+(monto-this.monto_auxiliar)+",'"+diaActual+"',"+mpago_interes+","
                    + ""+mpago_capital+","+mpago_interes_moratorio+",'"+comprobante+"','"+ fechapagoAnt+"'," + saldoant + "," + (saldoant - mpago_capital) + ");";
        }
        else  // no esta en mora
        {

            query = "insert into abonos(idPago,monto,fechaPago,pago_interes,pago_capital,pago_interes_moratorio,comprobante,fechaPagoAnt,saldoAnt,SaldoActual) "
                    + "values("+idPago+","+monto+",'"+diaActual+"',"+mpago_interes+","+(mpago_capital+this.monto_auxiliar)+","
                    + ""+mpago_interes_moratorio+",'"+comprobante+"','" + fechapagoAnt + "'," + saldoant + "," + (saldoant - mpago_capital) + ");";
        }
        this.monto_auxiliar = monto_aux_temp;
        estado = con.actualizar(query);
        con.cerrarConexion();
        actualizarPago();
        return estado;
    }
    /**
     * Esta funcion es llamada desde ActualizarPagos
     * @param valores
     * @return
     * @throws SQLException 
     */
    public int compararAbonoPagos(List<Double> valores) throws SQLException
    {
        String query ="select intereses,capital,interes_moratorio from pagos where idPago=" + this.idPago;
        int estado = -1;
        try 
        {
            con = new conexion();
            con.setRS(query);
            rs = con.getRs();
            if (rs.next())
            {
                // obtener datos a cancelar en tabla pagos
                double interes=0.0,interes_moratorio=0.0,capital=0.0;
                interes_moratorio = rs.getDouble("interes_moratorio");
                interes = rs.getDouble("intereses");
                capital = rs.getDouble("capital");
                double capital_temp = capital;
                // realizando restas
                interes_moratorio = diferenciaDias.redondear(valores.get(0) - interes_moratorio);
                interes = diferenciaDias.redondear(valores.get(1) - interes);
                capital = diferenciaDias.redondear(valores.get(2) - capital);
                if (this.monto_auxiliar > 0.00)
                {
                    capital = valores.get(2) -  (this.monto_auxiliar +  capital_temp);
                }
               
                if ( interes_moratorio == 0.0)
                {   estado =1;
                    if (interes == 0.0)
                    {   estado =2;
                        if (capital == 0.0)
                            estado = 3;
                    }
                }
             }
            return estado;
        }
        catch(SQLException e){
            System.out.println("query: " + query + "\n");
            System.out.println("Error: " + e.getMessage());
            return -1;
        }
        finally
        {
            con.cerrarConexion();
        }
 
    }
    /**
     * Esta funcion es llamada desde actualizarPago
     * @param idPago
     * @return
     * @throws SQLException 
     */
    public List<Double> sumaAbono(String idPago) throws SQLException
    {
        List<Double> SUMAbono = new ArrayList<Double>();
        String query ="select sum(pago_interes) as interes, sum(pago_interes_moratorio) as moratorio,"
                + "sum(pago_capital) as capital from abonos where idPago=" + idPago;
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        SUMAbono.add(0, rs.getDouble("moratorio"));
        SUMAbono.add(1, rs.getDouble("interes"));
        SUMAbono.add(2, rs.getDouble("capital"));
        con.cerrarConexion();
        return SUMAbono;
    }
    /**
     * Esta funcion es llamada desde ActualizarPago
     * @param codPrestamo
     * @return
     * @throws SQLException 
     */
    private String getPagoSiguiente(String codPrestamo) throws SQLException{
        try{
            String id = "";
            con = new conexion();
            con.setRS("SELECT idPago FROM pagos WHERE estado!='Pagado' AND codigoPrestamo='" + codigoPrestamo + "' ORDER BY idPago ASC LIMIT 1");
            rs = con.getRs();
            if(rs.next())
                id =  rs.getString("idPago");
            else
                id = "0";
            return id;
        }
        catch(SQLException e){
            return "0";
        }
        finally{
            rs.close();
            con.cerrarConexion();   
        }
    } //fin getPagoSiguiente
    
    /**
     * 
     * @param tope            Numero de pagos a ingresar
     * @param fechaInicial    Fecha a partir de la cual se tomaran los pagos
     * @param nuevoSaldo      Saldo a insertar en el pago
     * @param codigoP         Codigo del prestamo 
     * @param fechaPago       Fecha que se tomara como pago del prestamo
     * @throws SQLException 
     */
    public static void nuevoPago(int tope, String fechaInicial, double nuevoSaldo, String codigoP, String fechaPago) throws SQLException{
        int ultimoPago = 0;
        String fechaE = ""; //Fecha establecida para el pago siguiente
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String[] split = fechaInicial.split("-");
        String comprobante = "0";         //Dato que almacenara el comprobante
        Calendar c = Calendar.getInstance();
        conexion Con = new conexion();
        c.set(Integer.parseInt(split[0]), Integer.parseInt(split[1])-1, Integer.parseInt(split[2]));
         
        Con.setRS("SELECT comprobante FROM pagos ORDER BY comprobante DESC LIMIT 1");
        ResultSet Rs = Con.getRs();

        if(Rs.next())
            ultimoPago = Rs.getInt("comprobante");
        /**
         * Llenando la tabla pagos
         */
        for(int i = 1; i <= tope; i++)
        {
            ultimoPago++; //Sumando al comprobante
            c.add(Calendar.MONTH, 1);
            fechaE = formato.format(c.getTime()); 
            comprobante = String.format("%06d", ultimoPago); 
            if (i == 1)
               Con.actualizar("INSERT INTO pagos(codigoPrestamo,comprobante,fechaEstablecida,fechaPago, estado,saldo) VALUES('" + codigoP + "','"
                            + comprobante + "','" + fechaE +"','" + fechaPago + "','Pendiente', " + nuevoSaldo+  ");");
            else 
            {
                Con.actualizar("INSERT INTO pagos(codigoPrestamo,comprobante,fechaEstablecida,fechaPago, estado) VALUES('" + codigoP + "','"
                            + comprobante + "','" + fechaE +"','" + fechaPago + "','Pendiente');");
            }
            /***Actualizamos tabla de pagos***/
            ActualizacionPagos p = new ActualizacionPagos(codigoP);
            p.actualizar();
            
        }//fin for
        Con.cerrarConexion();
    }//fin nuevo Pago
    
    /**
     * @return true si el prestamo se finalizo
     *         false si hubo un error
     * @throws SQLException 
     */
    private boolean finalizarPrestamo() throws SQLException{
        try{
            con = new conexion();
            con.actualizar("UPDATE prestamos SET estado = 'Pagado', fechaFinalizacion = '" + this.diaActual +"' WHERE codigoPrestamo =  '" + this.codigoPrestamo + "'");
            
            return true;
        }
        catch(SQLException e){
            return false;
        }
        finally{
            con.cerrarConexion();   
        }
    } 
    /**
     * Funcion para determinar si el idPago es el ultimo
     * @param idPago 
     * @return 0 si no es el ultimo pago
     *         1 Si es el ultimo pago
     *        -1 Si es un error
     * @throws SQLException 
     */
    public int ultimoPago(String idPago) throws SQLException{
        String id_aux ="";
        int ret = 0;
        con = new conexion();
        
        con.setRS("SELECT idPago FROM pagos WHERE codigoPrestamo = '" + this.codigoPrestamo + "' ORDER BY idPago DESC limit 1");
        rs = con.getRs();
        if(rs.next()){
            id_aux = rs.getString("idPago");
            if(id_aux.equals(idPago))
                ret = 1;
            else
                ret = 0;
        }
        else
            ret = -1;
        con.cerrarConexion();
        return ret;
    }    
    private void actualizarPago() throws SQLException
    {

        String query = "update pagos set monto=(monto+" + this.monto+  "),pago_interes_moratorio=(pago_interes_moratorio+"+mpago_interes_moratorio+"),"
                + "pago_interes=(pago_interes+ "+ mpago_interes +"),pago_capital=(pago_capital + " +mpago_capital+ "), "
                + "fechaPago='" + this.diaActual + "' where idPago=" + this.idPago;
        con = new conexion();
        con.actualizar(query);
        con.cerrarConexion();
        
        List<Double> resultadoAbono = new ArrayList<Double>();
        resultadoAbono = sumaAbono(this.idPago);
        /**Comparando abonos**/    
        int estadoAbono = compararAbonoPagos(resultadoAbono);
        if  (estadoAbono == 3)
            query = "update pagos set estado='Pagado' where idPago=" + this.idPago;
        else
            query = "update pagos set estado='Abonado' where idPago=" + this.idPago;
        con = new conexion();
        con.actualizar(query);
        con.cerrarConexion();
        
        ActualizarSaldoPrestamo();
        query = "select saldo from prestamos where codigoPrestamo='" + this.codigoPrestamo + "'";
        conexion con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        double saldoActualPrestamo = 0.0;
        saldoActualPrestamo = rs.getDouble("saldo");
        
        if(ultimoPago(idPago) == 1){ /**Verificando si es el ultimo pago**/
            query = "select pre.cuotas as cuota, p.monto from pagos as p\n" +
                    "inner join prestamos as pre on p.codigoPrestamo = pre.codigoPrestamo\n" +
                    "where p.idPago=" + idPago;
            con.setRS(query);
            rs = con.getRs();
            rs.next();
            //if ( rs.getDouble("monto") >= rs.getDouble("cuota") )
            //{   // si son iguales y el saldo aun es mayor que cero, generar nueva cuota
                
                if (saldoActualPrestamo > 0.0)
                {
                nuevoPago(1, this.fechaEstablecida, 0.0, this.codigoPrestamo, "1000-01-01");
                }
            //}
        }
        
        if (this.monto_auxiliar > 0.00 && this.banderaMontoAux == 0)
        {  
            String nuevoIdPago = getPagoSiguiente(this.codigoPrestamo);
            if (nuevoIdPago != "0")
            {
                this.idPago = nuevoIdPago;
                this.monto  = this.monto_auxiliar;            //Estableciendo el monto
                mpago_interes_moratorio =0.0;
                mpago_interes = 0.0;
                mpago_capital = 0.0;
                llenarDatos(nuevoIdPago);
                insertarAbono(nuevoIdPago, this.monto_auxiliar);
            }
        }
        else{
            if(this.prestamoPagado(this.codigoPrestamo) == 1){//Si el prestamo ya esta pagado
                this.finalizarPrestamo();                     //Cambiar estado del prestamo
                mensaje = 2;
            }
            else
                mensaje = 1;
        }
    }
    /**
     * Esta funcion es llamada desde actualizarPago
     * @throws SQLException 
     */
    public void ActualizarSaldoPrestamo() throws SQLException
    {
        String query ="select saldo from prestamos where codigoPrestamo='" + this.codigoPrestamo+ "'";
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        double saldo = rs.getDouble("saldo") - mpago_capital;
        if(saldo < 0.0)
            saldo = 0.0;
        query ="update prestamos set saldo=" + saldo + " where codigoPrestamo='" + this.codigoPrestamo+ "'";
        con.actualizar(query);
        query ="update pagos set saldo=" + saldo+" where codigoPrestamo='" + this.codigoPrestamo+ "' and idPago=" + this.idPago;
        con.actualizar(query);
        con.cerrarConexion();
    }
    /**
     * 
     * @param codigoPrestamo
     * @return 1 Si el prestamo esta pagado
     *         0 Si el prestamo aun tiene saldo pendiente
     *        -1 Si el codigo no existe o hubo algun error
     * @throws SQLException 
     */
    public int prestamoPagado(String codigoPrestamo) throws SQLException{
        int ret = 0;
        try{
            double saldo_aux = 0.0; //Variable auxiliar del saldo del prestamo
            
            con = new conexion();
            con.setRS("SELECT saldo FROM prestamos WHERE codigoPrestamo='" + codigoPrestamo + "'");
            rs = con.getRs();
            if(rs.next())
                saldo_aux =  rs.getDouble("saldo");
            else
                saldo_aux = -1;
            
            if(saldo_aux == 0.0) //Verificando si el saldo esta pagado
                ret =  1;       
            if(saldo_aux > 0.0)
                ret =  0;
            if(saldo_aux < 0.0)
                ret = -1;
        }
        catch(SQLException e){
            return -1;
        }
        finally{
            rs.close();
            con.cerrarConexion();   
        }
        return ret;
    }
    
    public void eliminarAbono(String idAbono) throws SQLException
    {
        double pago_interes=0.0, pago_capital=0.0, pago_interes_moratorio = 0.0,monto=0.0;
        String fechaAnt= "",idPago="",codigoPrestamo="";
        String qr = "select monto,idPago,pago_interes,pago_capital,pago_interes_moratorio, fechaPagoAnt from abonos where idabono=" + idAbono;
        conexion con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        // sacando datos}
        idPago = rs.getString("idPago");
        pago_interes = rs.getDouble("pago_interes");
        pago_interes_moratorio = rs.getDouble("pago_interes_moratorio");
        pago_capital = rs.getDouble("pago_capital");
        fechaAnt = rs.getString("fechaPagoAnt");
        monto = rs.getDouble("monto");
        
        qr = "update pagos set fechaPago='" + fechaAnt+ "', pago_interes=(pago_interes-" + pago_interes + "),"
                + " pago_capital=(pago_capital- " + pago_capital + "),pago_interes_moratorio=(pago_interes_moratorio-" + pago_interes_moratorio + "),"
                + " saldo=(saldo+" + pago_capital+ "), monto=(monto-" + monto + ") where idPago=" + idPago;
        con.actualizar(qr);
        // obtener codigo prestamo
        qr = "select codigoPrestamo from pagos where idPago=" + idPago;
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        codigoPrestamo = rs.getString("codigoPrestamo");
        qr = "update prestamos set saldo=(saldo+ " + pago_capital + ") where codigoPrestamo='" + codigoPrestamo+ "'";
        con.actualizar(qr);
        EPactualizarPago(idPago);
        EPactualizarPrestamo(codigoPrestamo);
        qr ="delete from abonos where idabono=" + idAbono;
        con.actualizar(qr);
        con.cerrarConexion();
        
    }

    public void EPactualizarPago(String idPago) throws SQLException  // actualizar esta del pago
    {
        //JOptionPane.showMessageDialog(null, "1");
        String qr = "select count(a.idabono) as total, p.estado from abonos as a inner join pagos as p on \n" +
                    "p.idPago = a.idPago where a.idPago=" + idPago;
        conexion con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        int registrosAbonos = rs.getInt("total");
        String estado = rs.getString("estado");
        String query = "";
        if (estado.equals("Pagado") && registrosAbonos == 1)
            query ="update pagos set estado='Pendiente' where idPago=" + idPago;
        else if (registrosAbonos > 1)
            query ="update pagos set estado='Abonado' where idPago=" + idPago;
        else
            query ="update pagos set estado='Pendiente' where idPago=" + idPago;
        con.actualizar(query);
        con.cerrarConexion();
    }
    
    public void EPactualizarPrestamo(String codigoPrestamo) throws SQLException 
    {
        //JOptionPane.showMessageDialog(null, "2");
        double saldoActualPrestamo = 0.0;
        String query = "select saldo from prestamos where codigoPrestamo='" + codigoPrestamo + "'";
        conexion con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        saldoActualPrestamo = rs.getDouble("saldo");
        if (saldoActualPrestamo > 0.0)
            query = "update prestamos set estado='Activo' where codigoPrestamo='" + codigoPrestamo + "'";
        con.actualizar(query);
        con.cerrarConexion();
   }
    
    public static boolean CumplePorcentajeRefinanciamiento(String codigoPrestamo) throws SQLException
    {
        boolean estado = false;
        double porcentaje;
        int pagosRealizados = 0 ;
        int pagosTotales = 0;
        String qr = "select porcentaje from porcentaje_refinanciamiento";
        conexion con = new conexion();
        con.setRS(qr);
        ResultSet rs = con.getRs();
        rs.next();
        porcentaje = rs.getDouble("porcentaje");
       
        // obtener numero de pagos cancelados
        qr = "select count(*) as total, pre.plazo  from pagos as p inner join prestamos as pre\n" +
             "on p.codigoPrestamo = pre.codigoPrestamo where pre.codigoPrestamo='"+codigoPrestamo+"' and p.estado='Pagado'";
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        pagosRealizados = rs.getInt("total");
        pagosTotales = rs.getInt("plazo");
        // comparar!
        double totalPorcentajeGanado = (pagosRealizados*100)/pagosTotales;
        
        if (totalPorcentajeGanado >= porcentaje)
            estado = true;
        rs.close();
        con.cerrarConexion();
        return estado;
    }
    
    public static double total_deuda(String codigoPrestamo, double saldo) throws SQLException{
        conexion con = new conexion();
        String qr = "SELECT SUM((intereses - pago_interes)) as interes, SUM(interes_moratorio - pago_interes_moratorio) AS interes_mora from pagos "
                + " WHERE estado!='Pagado' AND codigoPrestamo ='" + codigoPrestamo + "'";
        con.setRS(qr);
        ResultSet rs = con.getRs();
        
        double intereses = 0.0;
        double mora = 0.0;
        if(rs.next()){
            intereses = rs.getDouble("interes");
            mora = rs.getDouble("interes_mora");
        } 
        rs.close();
        con.cerrarConexion();
        return intereses + mora + saldo;
    }
    public static int cuotas_canceladas(String codigoPrestamo) throws SQLException{
        conexion con = new conexion();
        try {
            con.setRS("SELECT count(*)as total from pagos as p inner join prestamos as pre\n" +
             "on p.codigoPrestamo = pre.codigoPrestamo where pre.codigoPrestamo='"+codigoPrestamo+"' and p.estado='Pagado'");
            ResultSet rs = con.getRs();
            int cuotas = 0;
            if (rs.next())
                cuotas = rs.getInt("total");
            return cuotas;
        } catch (SQLException ex) {
            Logger.getLogger(classPagosAnt.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        finally{
            con.cerrarConexion();
        }
    }
    /**
     * @param monto
     * @return 
     */
    public static boolean verificar_monto_refinanciamiento(double monto, double total_deuda){
        if(monto >= total_deuda)
            return true;
        else
            return false;
    }//fin verificar monto
    
    /**
    * FUNCIONES NECESARIAS
    * 1. Generar comprobante de abono ----
    * 2. Generar abonos -----
    * 3. comprobar pago (Verificar si el abono que se acaba de realizar, cubre la cuota) ---
    * 4. registrarPago (Donde se cambiarían los datos de la tabla padre: Pagos) ---
    * 5. imprimirAbono 
    **/
   
    /***POR HACER HOY**
     * 1. Crear funcion para crear nueva fila de pagos (Editando cambiarEstado.jsp)    - Ale
     * 4. Crear funcion que verifique si la actualizacion anterior cancela el prestamo  - Ale ---
     *    4.1 Verificar si saldo EN PRESTAMO es igual a CERO---
     *    4.2 Redireccion a pagina para impresion de documento
     *    
     * i.   Crear funcion para eliminarAbono - Jorge HOY
     * ii.  Validar en realizarPago.jsp si la cuota es mas de lo que se debe pagar - Ale HOY
     * iii. Arreglar NuevoPago - Ale
     * iv.  Arreglar reporte de resolucion prestamo -  Ale
     * v.   Arreglar comprobante de pago - Jorge
     * vi.  
    **/

}
