/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;
import java.sql.*;
import Conexion.conexion;
/**
 *
 * @author William
 */
public class Pago {
    
    private int idPago = 0;
    
    private double pago_capital = 0.0;
    private double pago_intereses = 0.0;
    private double pago_interes_moratorio = 0.0;
    private double pago_iva_interes = 0.0;
    private double pago_iva_mora = 0.0;
    
    private double capital = 0.0;
    private double intereses = 0.0;
    private double interes_moratorio = 0.0;
    private double iva_interes = 0.0;
    private double iva_mora = 0.0;
    private double monto_abonado = 0.0;
    
    public Pago(int idPago){
        this.idPago = idPago;
        String query = "SELECT capital, pago_capital, intereses, pago_interes, interes_moratorio, pago_interes_moratorio, iva_interes, pago_iva_interes, iva_mora, pago_iva_mora FROM pagos WHERE idPago="+idPago;
        
        try{
            conexion con = new conexion();
            con.setRS(query);
            ResultSet rs = con.getRs();
            int lim = 0;
            while(rs.next()){
                capital = rs.getDouble("capital");
                pago_capital = rs.getDouble("pago_capital");
                intereses = rs.getDouble("intereses");
                pago_intereses = rs.getDouble("pago_interes");
                interes_moratorio = rs.getDouble("interes_moratorio");
                pago_interes_moratorio = rs.getDouble("pago_interes_moratorio");
                iva_interes = rs.getDouble("iva_interes");
                pago_iva_interes = rs.getDouble("pago_iva_interes");
                iva_mora = rs.getDouble("iva_mora");
                pago_iva_mora = rs.getDouble("pago_iva_mora");
            }
        }catch(Exception e){
            
        }
    }
    
    public int getIdPago(){
        return idPago;
    }
    
    public double getAbonoRealizado(){
        return monto_abonado;
    }
    
    public double getCapitalPendiente(){
        return capital - pago_capital;
    }
    
    public double getInteresPendiente(){
        return intereses - pago_intereses;
    }
    
    public double getInteresMoratorioPendiente(){
        return interes_moratorio - pago_interes_moratorio;
    }
    
    public double getIvaPendiente(){
        return iva_mora + iva_interes - pago_iva_mora - pago_iva_interes;
    }
    
    public void abonarCapital(double monto){
        pago_capital += monto;
        monto_abonado += monto;
    }
    
    public void abonarInteres(double monto){
        pago_intereses += monto;
        monto_abonado += monto;
    }
    
    public void abonarInteresMoratorio(double monto){
        pago_interes_moratorio += monto;
        monto_abonado += monto;
    }
    
    public void abonarIva(double monto){
        if(monto >= (iva_mora - pago_iva_mora)){
            monto -= (iva_mora - pago_iva_mora);
            pago_iva_mora += (iva_mora - pago_iva_mora);
        }else{
            pago_iva_mora += monto;
            monto = 0;
        }
        
        if(monto >= (iva_interes - pago_iva_interes)){
            monto -= (iva_interes - pago_iva_interes);
            pago_iva_interes += (iva_interes - pago_iva_interes);
        }else{
            pago_iva_interes += monto;
            monto = 0;
        }
        
        monto_abonado += monto;
    }
    
    public void actualizar(){
        double totalPagado = pago_capital + pago_intereses + pago_interes_moratorio + pago_iva_interes + pago_iva_mora;
        String query = "UPDATE pagos SET monto = "+totalPagado+", pago_capital = "+pago_capital+", pago_interes = "+pago_intereses+", pago_interes_moratorio = "+pago_interes_moratorio+", pago_iva_interes = "+pago_iva_interes+", pago_iva_mora = "+pago_iva_mora+" WHERE idPago="+idPago;
        
        try{
            conexion con = new conexion();
            con.actualizar(query);
       
            if(getCapitalPendiente() == 0){
                query = "UPDATE pagos SET estado='Pagado',fechaPago = CURDATE() WHERE idPago="+idPago;
            }else{
                if(pago_capital !=0 || pago_intereses != 0 || pago_interes_moratorio != 0 || pago_iva_interes != 0 || pago_iva_mora != 0) 
                    query = "UPDATE pagos SET estado='Abonado',fechaPago = CURDATE() WHERE idPago="+idPago;
            }
        
            con.actualizar(query);
            con.cerrarConexion();
        }catch(Exception e){
            
        }
    }
}
