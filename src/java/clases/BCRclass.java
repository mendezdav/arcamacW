/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Pochii
 */
public class BCRclass {
    public static String anio_;
    public static String mes_;
    
    public static void formatFecha(String fecha){
        try {
            Date date;
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM");
            date = sd.parse(fecha);
            fecha = sd.format(date);
            String []data = fecha.split("-");
            anio_ = data[0];
            mes_  = data[1];
        } catch (Exception ex) {
            System.out.println("Hubo un error " + ex.getMessage());
            Logger.getLogger(BCRclass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static String generarReporte(ArrayList<nuevoPrestamo> prestamos){       
        try {
            String valores = "";
            /**Recorriendo arrayList***/
            for(nuevoPrestamo p : prestamos ){
                valores+="J1306151144NS;11;";
                valores+=p.getCodigoPrestamo() + ";";
                valores+="1;";
                valores+=p.getCantidad() + ";" + p.getCantidad() + ";";
                valores+=";;" + (p.getInteres()*100) + ";"; //Multiplicamos la tasa de interes * 100
                valores+="5;"+p.getFechaAprobacion() + ";" + p.getFechaVencimiento() + ";5;";
                valores+=p.getCuotas() + ";" + p.getPlazo() + ";;;;;;;;1";
                valores+="\r\n";
            }
            return valores;
        } catch (Exception ex) {
            System.out.println("Hubo un error en class BCR " + ex.getMessage());
            return "";
        }
    }
}
