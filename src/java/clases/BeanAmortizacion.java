/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jorge Luis
 */
public class BeanAmortizacion {
    private String comprobante;
    private String fechaPago;
    private double cuotaNIVA;
    private double saldoInicial;
    private double capital;
    private double interes;
    private double saldoFinal;
    private double iva_interes;
    private double cuotaCIVA;
    private String codigoPrestamo;
    private String fechaImpresion;
    private double totalCapital;
    private double totalInteres;
    private double totalIva;
    
    public BeanAmortizacion()
    {
      comprobante = "";
      fechaPago = "";
      cuotaNIVA = 0.00;
      saldoInicial = 0.00;
      capital = 0.00;
      interes = 0.00;
      saldoFinal = 0.00;
      iva_interes = 0.00;
      cuotaCIVA = 0.00;
      codigoPrestamo = "";
      fechaImpresion = "";
    }

    /**
     * @return the comprobante
     */
    public String getComprobante() {
        return comprobante;
    }

    /**
     * @param comprobante the comprobante to set
     */
    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    /**
     * @return the fechaPago
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the cuotaNIVA
     */
    public double getCuotaNIVA() {
        return cuotaNIVA;
    }

    /**
     * @param cuotaNIVA the cuotaNIVA to set
     */
    public void setCuotaNIVA(double cuotaNIVA) {
        this.cuotaNIVA = cuotaNIVA;
    }

    /**
     * @return the saldoInicial
     */
    public double getSaldoInicial() {
        return saldoInicial;
    }

    /**
     * @param saldoInicial the saldoInicial to set
     */
    public void setSaldoInicial(double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the saldoFinal
     */
    public double getSaldoFinal() {
        return saldoFinal;
    }

    /**
     * @param saldoFinal the saldoFinal to set
     */
    public void setSaldoFinal(double saldoFinal) {
        this.saldoFinal = saldoFinal;
    }

    /**
     * @return the iva_interes
     */
    public double getIva_interes() {
        return iva_interes;
    }

    /**
     * @param iva_interes the iva_interes to set
     */
    public void setIva_interes(double iva_interes) {
        this.iva_interes = iva_interes;
    }

    /**
     * @return the cuotaCIVA
     */
    public double getCuotaCIVA() {
        return cuotaCIVA;
    }

    /**
     * @param cuotaCIVA the cuotaCIVA to set
     */
    public void setCuotaCIVA(double cuotaCIVA) {
        this.cuotaCIVA = cuotaCIVA;
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the fechaImpresion
     */
    public String getFechaImpresion() {
        return fechaImpresion;
    }

    /**
     * @param fechaImpresion the fechaImpresion to set
     */
    public void setFechaImpresion(String fechaImpresion) {
        this.fechaImpresion = fechaImpresion;
    }

    /**
     * @return the totalCapital
     */
    public double getTotalCapital() {
        return totalCapital;
    }

    /**
     * @param totalCapital the totalCapital to set
     */
    public void setTotalCapital(double totalCapital) {
        this.totalCapital = totalCapital;
    }

    /**
     * @return the totalInteres
     */
    public double getTotalInteres() {
        return totalInteres;
    }

    /**
     * @param totalInteres the totalInteres to set
     */
    public void setTotalInteres(double totalInteres) {
        this.totalInteres = totalInteres;
    }

    /**
     * @return the totalIva
     */
    public double getTotalIva() {
        return totalIva;
    }

    /**
     * @param totalIva the totalIva to set
     */
    public void setTotalIva(double totalIva) {
        this.totalIva = totalIva;
    }
    
}
