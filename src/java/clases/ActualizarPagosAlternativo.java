/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import com.sun.rowset.CachedRowSetImpl;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge Luis
 */
public class ActualizarPagosAlternativo {
    // ATRIBUTOS DE LA CLASE
    // DATOS DEL PRESTAMO
    private String codigoPrestamo; 
    private double saldo;
    private int plazo;
    private double tasa_interes;
    private double cuota;        
    private String fechaAprobacion;

    // DATOS DE LOS PAGOS
    private int idPrimerPago;
    private String fechaEstablecidaAnterior;
    private String fechaUltimoPago;
    private String estado;
    private double fila;
    private int idPago;
    private String fechaEstablecida;
    private String fechaPago;
    private double capital;
    private double mora;
    private double interes;
    private double pago_capital;
    private int numero_pago;
    private double interes_mora;
    private double iva_mora;
    private double ivaMoraTotal; 
    private double capital_vencido;
    private double interes_diario;
    private double intereses_pago;
                    
    private ResultSet prestamosActivos;
    private ResultSet pagos;
    private ResultSet rs_aux;
    private conexion con;
    private String qr;
    private diferenciaDias diasDif;
    private classPagos classPagos;
    private double saldoAnt;
    private SimpleDateFormat formato;
   
   
    public ActualizarPagosAlternativo(String codigoPrestamo){ 
        this.codigoPrestamo = codigoPrestamo;
        prestamosActivos = null;
        pagos = null;
        con = null;
        classPagos = null;
        rs_aux = null;
        qr ="";
        diasDif = new diferenciaDias();
        formato = new SimpleDateFormat("yyyy-MM-dd");
        interes_mora = -1.0;
        iva_mora = -1.0;
        saldoAnt = -1.0;
    }
    
    public void actualizar() throws SQLException
    {
        if (this.codigoPrestamo.equals(""))
            qr = "select * from prestamos where estado='Activo'";
        else
            qr = "SELECT * from prestamos where estado='Activo' and codigoPrestamo = '" + codigoPrestamo + "'";
        recorrido(qr, "", 1);
    }
    
     /**
     * recorrido, funcion que calcula el desglose de los pagos
     * @param qr  Query de los prestamos
     * @param fechaInicio fecha hasta la cual se calcularan los pagos
     * @param actualizar  bandera 1 actualizara la base, 0 no hara la actualizacion
     * @throws SQLException 
     */
    public void recorrido(String qr, String fechaInicio, int actualizar) throws SQLException
    {
        fechaInicio = "2013-11-12";
        // obteniendo fecha Actual
        Date fecha = new Date();
        String fechaActual = formato.format(fecha);
        if (!fechaInicio.equals(""))
            fechaActual = fechaInicio;
        // creando instancia de classPagos
        classPagos = new classPagos();
        con = new conexion(); // estableciendo conexion
        con.setRS(qr); // obteniendo los prestamos activos en el sistema
        prestamosActivos = (ResultSet) con.getRs();
        // recorriendo los prestamos si los hay
        
        while (prestamosActivos.next())
        {
            // obtener datos necesario del prestamo
            this.codigoPrestamo = prestamosActivos.getString("codigoPrestamo");
            this.saldo = prestamosActivos.getDouble("saldo");
            this.plazo = prestamosActivos.getInt("plazo");
            this.tasa_interes = prestamosActivos.getDouble("tasa_interes");
            this.cuota = prestamosActivos.getDouble("cuotas");
            this.fechaAprobacion = prestamosActivos.getString("fechaAprobacion");
            
            //obtener los pagos que no esten pagados en dicho prestamo
            qr = " where codigoPrestamo='" + this.codigoPrestamo+ "' order by idpago ASC";
            // pagos = resultset, toma todos los pagos sin importar su estado
            pagos = classPagos.getPagos(qr);
            // obtener datos del primer pago
            if (pagos.next())
            {
                this.idPrimerPago = pagos.getInt("idPago");
                this.fechaEstablecidaAnterior = this.fechaAprobacion;
                this.fechaUltimoPago = this.fechaAprobacion;
            }
            pagos.previous();
            /*
             * RECORRIENDO LOS PAGOS DEL PRESTAMO
             */
            int c = 0;
            while (pagos.next())
            {
                 /*  obteniendo datos del pago*/
                this.estado = pagos.getString("estado");
                this.fila = pagos.getDouble("fila");
                this.idPago = pagos.getInt("idPago");
                this.fechaEstablecida = pagos.getString("fechaEstablecida");
                this.fechaPago = pagos.getDate("fechaPago").toString();
                this.capital = pagos.getDouble("capital");
                this.mora = pagos.getDouble("interes_moratorio");
                this.interes = pagos.getDouble("intereses");
                this.pago_capital = pagos.getDouble("pago_capital");
                this.numero_pago = pagos.getInt("Npago");
                
                /**   VERIFICANDO SI NO ES EL PRIMER PAGO**/
                if (idPrimerPago != idPago)
                {
                    qr = " where codigoPrestamo='" + this.codigoPrestamo+ "' having fila=(" + this.fila+ " -1)";
                    rs_aux = classPagos.getPagos(qr);
                    if (rs_aux.next())
                    {
                        this.saldoAnt = rs_aux.getDouble("saldo");
                        this.fechaEstablecidaAnterior = rs_aux.getString("fechaEstablecida");
                        this.fechaUltimoPago = rs_aux.getString("fechaPago");
                    }
                    
                } // si no es el primer pago
                
                /** SI LOS PAGOS TIENEN ESTADO PAGADO NO HACER NADA **/
                /** PAGOS PENDIENTES **/
                if (this.estado.equals("Pendiente"))
                {
                    int diasRetraso = (int) diasDif.obtenerDiasRetraso(fechaActual, fechaEstablecida);
                    /* SI DIASRETRASO ES NEGATIVO NO HAY DIAS DE MORA*/
                    if (diasRetraso <= 0) // no hay mora
                    {
                        this.interes_mora = 0.00;
                        this.iva_mora = 0.00;
                        this.ivaMoraTotal = 0.00;
                    }
                    else 
                    { // verificar si tiene capital vencido
                        this.capital_vencido = this.getCapital_vencido_sd(diasRetraso, this.numero_pago);
                        if (capital_vencido > 0.00)
                        {
                            this.interes_mora = ((this.capital_vencido * (this.tasa_interes/100)) / 30) * diasRetraso; 
                            this.iva_mora = this.interes_mora * 0.13;
                            /* redondear con bigdecimal*/
                            this.ivaMoraTotal = diferenciaDias.redondearBig((this.interes_mora + this.iva_mora));
                        }
                        else{
                            this.interes_mora = 0.00;
                            this.iva_mora = 0.00;
                            this.ivaMoraTotal = 0.00;
                        }
                    }
                    /*  CALCULANDO INTERES NORMAL */
                    int diasInteres=0;
                   
                    if ((this.fechaUltimoPago.equals("1000-01-01") ||  this.idPago == idPrimerPago) && c == 0 )
                    {
                           if(fechaUltimoPago.equals("1000-01-01"))
                               diasInteres = (int)diasDif.obtenerDiasRetraso(fechaActual, fechaEstablecidaAnterior);
                           else
                               diasInteres = (int)diasDif.obtenerDiasRetraso(fechaActual, fechaUltimoPago);
                           /* establecer interes*/
                           
                           //this.setInteres_diario();
                           this.interes_diario = (this.saldo * (this.tasa_interes/100))/30;
                           if(diasInteres <= 0)
                               diasInteres = 0;
                           this.intereses_pago = this.interes_diario * diasInteres;
                           this.intereses_pago = diferenciaDias.redondearBig(this.intereses_pago);
                                   //this.getInteres_diario() * diasInteres; 
                    }
                    else // si no es el primer pago!
                    {
                    
                         fechaUltimoPago = "2013-10-11";
                         // ver si la fecha establecida es menor a la fecha actual
                         // si es asi hay interes dias normales, sino no
                         int d = (int) diasDif.obtenerDiasRetraso(fechaActual,fechaEstablecida);
                        
                       //  if (fechaEstablecida > fechaActual) // hay interes normal
                        // {
                             diasInteres = (int) diasDif.obtenerDiasRetraso(fechaActual, fechaUltimoPago);
                            this.interes_diario = (this.saldo * (this.tasa_interes/100))/30;
                            if(diasInteres <= 0)
                                diasInteres = 0;
                            this.intereses_pago = this.interes_diario * diasInteres;
                            this.intereses_pago = diferenciaDias.redondearBig(this.intereses_pago);
                        // }
                        // else
                         //{
                             this.intereses_pago = 0.00;
                         //}
                    }
                    // impresion de datos resumida
                   
                } // fin si estado = pendiente
                c++;
            } // fin while recorrido de los pagos de los prestamos
        } // fin while prestamos activos en el sistema
    }
    
    /**Metodo para calcular el capital vencido de un pago a una fecha establecida***/
    private double getCapital_vencido_sd(int dias_mora, int no_pago){
        float capital_vencido = 0.0F;
        int npago_aux = 0;
        int diapago_aux = 0;
        double saldoIdeal = 0;
        if(dias_mora > 0){ 
            try { //Si hay mas de un dia de mora existe capital vencido
                conexion con_ideal = new conexion();
                String query_ideal = "SELECT saldoFinal FROM tabla_amortizacion WHERE codigoPrestamo='" + this.codigoPrestamo + "' "
                        + "and Npago = " + no_pago;
                //System.out.println("Query de capital vencido: " + query_ideal);
                con_ideal.setRS(query_ideal);
                ResultSet rs_ideal = con_ideal.getRs();
                if(rs_ideal.next()){ //Si la condicion coincide
                    saldoIdeal = rs_ideal.getDouble("saldoFinal");
                } //fin if
                else{ //condicion
                    
                }
                capital_vencido = (float)((float)this.saldo - (float)saldoIdeal);
                if(capital_vencido < 0)
                    capital_vencido = 0;
            } catch (SQLException ex) {
                capital_vencido = 0;
                Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return Math.rint(capital_vencido*1000)/1000;
    }
} // ES EL FIN DEL CAMINO, ES FINISTERRA!!
