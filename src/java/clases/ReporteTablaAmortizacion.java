/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import Conexion.conexion;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "ReporteTablaAmortizacion", urlPatterns = {"/ReporteTablaAmortizacion"})
public class ReporteTablaAmortizacion extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
             ServletOutputStream out = response.getOutputStream();
            FileInputStream inputStream = null;
            
        try {
        
        String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/TablaAmortizacion.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        //parameters.put("parametro", "parametro");
              
        List<Object> reports = new LinkedList<Object>();
       
        // creando instancia al bean cumpleanios
        
        // creando instancia conexion
        conexion con = new conexion();
        HttpSession Session = request.getSession();
        String codigoPrestamo = Session.getAttribute("codigoPrestamo").toString();
        String qr="select pre.cuotas, p.comprobante,p.fechaPago,p.pago_capital,p.pago_interes,p.saldo,p.codigoPrestamo,p.capital\n" +
                    "from pagos as p inner join prestamos as pre "
                + "  on p.codigoPrestamo = pre.codigoPrestamo where p.codigoPrestamo ='" + codigoPrestamo+"';";
        
        con.setRS(qr);
        ResultSet tmp = con.getRs();
        double cuotas = 0.00;
        
        // obteniendo fecha de impresion
        Date c1 = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        
        
        while (tmp.next()){
        cuotas = tmp.getDouble("cuotas");
        cuotas = Math.rint( cuotas * 100) / 100;
        
        BeanAmortizacion amortizacion = new BeanAmortizacion();
        amortizacion.setComprobante(tmp.getString("comprobante"));
        amortizacion.setFechaPago(tmp.getString("fechaPago"));
        // estableciendo fecha de impresion
        amortizacion.setFechaImpresion(formato.format(c1));
        // redondeando valores
        double pago_capital = tmp.getDouble("pago_capital");
        double pago_interes = tmp.getDouble("pago_interes");
        double iva_interes = pago_interes * (0.13);
        
        pago_capital = Math.rint(pago_capital * 100)/100;
        pago_interes = Math.rint(pago_interes * 100) /100;
        
        
        amortizacion.setCapital(pago_capital);
        amortizacion.setInteres(pago_interes);
        
        // ver si saldo es positivo! 
        double saldoFinal = tmp.getDouble("saldo");
        saldoFinal = Math.rint(saldoFinal*100)/100;
        if (saldoFinal > 0.0)
            amortizacion.setSaldoFinal(saldoFinal);
        else
            amortizacion.setSaldoFinal(0.00);
        
        iva_interes = Math.rint(iva_interes*100)/100;
        // creando reporte
        amortizacion.setCodigoPrestamo(tmp.getString("codigoPrestamo").toUpperCase());
        amortizacion.setSaldoInicial(tmp.getDouble("capital"));
        amortizacion.setIva_interes(iva_interes);
        
        double cuotaCIVA =  cuotas + iva_interes;
        cuotaCIVA = Math.rint(cuotaCIVA * 100) / 100;
        
        // agregando dato al reporte
        amortizacion.setCuotaCIVA(cuotaCIVA);
        amortizacion.setCuotaNIVA(cuotas);
       
        reports.add((Object) amortizacion);
        }
       
        //fin while
        
        //fill the ready report with data and parameter
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
        JRExporter exporter = null;
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"Otorgamiento.pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        //view the report using JasperViewer
        exporter.exportReport();
          
        
       // redireccion al panel principal
       response.sendRedirect(ruta + "/panelAdmin.jsp");
        
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteTablaAmortizacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteTablaAmortizacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
