/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Pochii
 */
/**
 * Importando librerias
 */

import java.io.*;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import org.apache.commons.io.*;
public class subirFoto {
    /*Propiedades de clase */
    FileItemFactory file_factory; //Interfaz para crear FileItem
    ServletFileUpload servlet_up; //Convierte los input file a FileItem
    List items;                   //Almacenando los FileItem en una lista
    
    public subirFoto(){
        file_factory = new DiskFileItemFactory();
        servlet_up   = new ServletFileUpload(file_factory);
    }
    
    public String upload(HttpServletRequest Request, String nombre) throws FileUploadException, Exception
    {
        System.out.println(nombre);
        String msj  = "";
        String ruta = Request.getServletContext().getRealPath("/");
        /**Borramos (si existe) el archivo tmp**/
        String archivotmp;
        File f = new File(ruta + "\\" + "tmp" + ".jpg");
        if(f.exists())
           archivotmp = ruta + "/solicitantes/images/" + "tmp" + ".jpg";
        else
           archivotmp = ruta+ "/solicitantes/images/" + "tmp" + ".png";
        items = servlet_up.parseRequest(Request);
        for(int i = 0; i < items.size(); i++){
            FileItem item  = (FileItem) items.get(i);
            if(! item.isFormField()){
                item.setFieldName(nombre); //Establecemos el nuevo nombre del archivo
                File archivo_server = new File(ruta + "images\\", item.getFieldName());
                item.write(archivo_server);
                msj = "Exitoso<br>Nombre: " + item.getFieldName() + "<br />";
                msj += "La ruta se guardo en: " + ruta + "images\\" + item.getFieldName();
                msj += "<br />Mostrando la imagen: " + "<img src='images/" + item.getFieldName() + "' />"; 
            }
            else 
                msj = "Hubo un error";
        }//fin for
        return msj;
    }

}
