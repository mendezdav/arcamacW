/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge Luis
 */
public class DiasMes {
    private int dia;
    private int mes;
    private int anio;
    
    DiasMes(int mes, int anio)
    {
       this.mes = mes;
       this.anio = anio;
        switch(mes){
            case 1:  // Enero
            case 3:  // Marzo
            case 5:  // Mayo
            case 7:  // Julio
            case 8:  // Agosto
            case 10:  // Octubre
            case 12: // Diciembre
                dia = 31;
            break;
            case 4:  // Abril
            case 6:  // Junio
            case 9:  // Septiembre
            case 11: // Noviembre
                dia = 30;
            break;
            case 2:  // Febrero
                if ( ((anio%100 == 0) && (anio%400 == 0)) || ((anio%100 != 0) && (anio%  4 == 0))   )
                    dia=29;  // Año Bisiesto
                else
                    dia = 28;
            break;
            default:
                throw new java.lang.IllegalArgumentException("El mes debe estar entre 1 y 12");
        }
    }
    public List<String> getFecha()
    {
        List<String> lista = new ArrayList<String>();
        lista.add(anio + "-" + mes + "-01");
        lista.add(anio + "-" + mes + "-" + dia);
        return lista;
    }
    
}
