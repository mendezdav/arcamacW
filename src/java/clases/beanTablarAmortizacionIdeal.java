package clases;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorge
 */
public class beanTablarAmortizacionIdeal {
    private String fecha;
    private String codigoPrestamo;
    private String npago;
    private String fechaPago;
    private double saldoInicial;
    private double cuota;
    private double interes;
    private double capital;
    private double saldoFinal;
    private String logo;
    private String plazo;
    private double cantidad;
    private String tasa_interes;
    private double cuotaSinva;
    private double IVAinteres;
    private String solicitante;


    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
        
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the npago
     */
    public String getNpago() {
        return npago;
    }

    /**
     * @param npago the npago to set
     */
    public void setNpago(String npago) {
        this.npago = npago;
    }

    /**
     * @return the fechaPago
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the saldoInicial
     */
    public double getSaldoInicial() {
        return saldoInicial;
    }

    /**
     * @param saldoInicial the saldoInicial to set
     */
    public void setSaldoInicial(double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the saldoFinal
     */
    public double getSaldoFinal() {
        return saldoFinal;
    }

    /**
     * @param saldoFinal the saldoFinal to set
     */
    public void setSaldoFinal(double saldoFinal) {
        this.saldoFinal = saldoFinal;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the plazo
     */
    public String getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the cantidad
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the tasa_interes
     */
    public String getTasa_interes() {
        return tasa_interes;
    }

    /**
     * @param tasa_interes the tasa_interes to set
     */
    public void setTasa_interes(String tasa_interes) {
        this.tasa_interes = tasa_interes;
    }

    /**
     * @return the cuotaSinva
     */
    public double getCuotaSinva() {
        return cuotaSinva;
    }

    /**
     * @param cuotaSinva the cuotaSinva to set
     */
    public void setCuotaSinva(double cuotaSinva) {
        this.cuotaSinva = cuotaSinva;
    }

    /**
     * @return the IVAinteres
     */
    public double getIVAinteres() {
        return IVAinteres;
    }

    /**
     * @param IVAinteres the IVAinteres to set
     */
    public void setIVAinteres(double IVAinteres) {
        this.IVAinteres = IVAinteres;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }
}
