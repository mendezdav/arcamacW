package clases;

import static clases.saldoCierre.meses;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author ASUS
 */
@WebServlet(name = "ReporteSaldosCierre", urlPatterns = {"/ReporteSaldosCierre"})
public class ReporteSaldosCierre extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JRException, SQLException {
        ServletOutputStream out = response.getOutputStream();
        try {
        // declarando variables
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/saldosCierre.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg"); 
        List<beansaldoscierre> datos = null;
        datos= new ArrayList<beansaldoscierre >();
        //List<Object> reports = new LinkedList<Object>();
        // sacando datos de la clase
        // de donde viene el flujo del programa
        String solicitud = "";
        HttpSession SessionActual = request.getSession();
        // tomando valores enviados
        String fechaInicio = null;
        String fechaFin = null;
        if ( request.getParameter("fechainicio") != null && request.getParameter("fechainicio") != "" )
            fechaInicio = request.getParameter("fechainicio");
        if ( request.getParameter("fechafin") != null && request.getParameter("fechafin") != "" )
            fechaFin = request.getParameter("fechafin");
        saldoCierre c = new saldoCierre(fechaInicio,fechaFin);
        c.sumaOtorgadoInicial();
        c.recorridoMensual();
        int mesesCalcular = c.mesesCalcular();
        for(int x=0; x < meses.size();x++)
        {
            beansaldoscierre bean = new beansaldoscierre();
            bean.setAnio( Integer.parseInt(meses.get(x).getAnio()));
            bean.setFechaFin(fechaFin);
            bean.setFechaInicio(fechaInicio);
            bean.setNumeroMeses(mesesCalcular);
            bean.setMes(meses.get(x).getMesString());
            bean.setLogo(rutaImg);
            bean.setCapital(meses.get(x).getCapital());
            bean.setCuota(meses.get(x).getSumaCuota());
            bean.setIva(meses.get(x).getIva());
            bean.setMonto_otorgado(meses.get(x).getSumaOtorgado());
            bean.setMora(meses.get(x).getMora());
            bean.setNormal(meses.get(x).getNormal());
            bean.setSaldo(meses.get(x).getSumaSaldo());
            datos.add(bean);
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(datos));
        JRExporter exporter = null;
        datos.clear();
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"CumpleaniosDelMes.pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        //view the report using JasperViewer
        exporter.exportReport();
       // redireccion al panel principal
       response.sendRedirect("/arcamacW/panelAdmin.jsp");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JRException ex) {
            Logger.getLogger(ReporteSaldosCierre.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteSaldosCierre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JRException ex) {
            Logger.getLogger(ReporteSaldosCierre.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteSaldosCierre.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
