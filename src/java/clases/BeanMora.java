/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Pochii
 */
public class BeanMora {
    private int no_meses;
    private double cuotas;
    private double total_pagar;
    private String codigoPrestamo;
    private String solicitante;
    private String fecha;
    private String diferencia;
    private String celular;
    private String img;
    /**
     * @constructor 
     */
    public BeanMora(){
        no_meses = 0;
        cuotas = 0.0;
        total_pagar = 0.0;
        codigoPrestamo = "";
        solicitante = "";
        fecha = "";
        diferencia = "";
        celular = "";
        img = "";
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the diferencia
     */
    public String getDiferencia() {
        return diferencia;
    }

    /**
     * @param diferencia the diferencia to set
     */
    public void setDiferencia(String diferencia) {
        this.diferencia = diferencia;
    }

    /**
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * @param celular the celular to set
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * @return the no_meses
     */
    public int getNo_meses() {
        return no_meses;
    }

    /**
     * @param no_meses the no_meses to set
     */
    public void setNo_meses(int no_meses) {
        this.no_meses = no_meses;
    }

    /**
     * @return the cuotas
     */
    public double getCuotas() {
        return cuotas;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setCuotas(double cuotas) {
        this.cuotas = cuotas;
    }

    /**
     * @return the total_pagar
     */
    public double getTotal_pagar() {
        return total_pagar;
    }

    /**
     * @param total_pagar the total_pagar to set
     */
    public void setTotal_pagar(double total_pagar) {
        this.total_pagar = total_pagar;
    }

    /**
     * @return the img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImg(String img) {
        this.img = img;
    } 
}
