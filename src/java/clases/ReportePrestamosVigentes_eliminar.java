/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "ReportePrestamosVigentes", urlPatterns = {"/ReportePrestamosVigentes"})
public class ReportePrestamosVigentes_eliminar extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, JRException {
         ServletOutputStream out = response.getOutputStream();
            
     try {
         String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        // tomando datos del request.
         String fechaInicio = request.getParameter("fechaInicio");
         String fechaFin = request.getParameter("fechaFin");
         JasperReport reporte = (JasperReport)
         JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/PrestamosOtorgados.jasper"));
         //Cargamos parametros del reporte (si tiene).
         Map parameters = new HashMap();
         //parameters.put("parametro", "parametro");
              
         List<Object> reports = new LinkedList<Object>();
         // creando instancia conexion
         conexion con = new conexion();
         String qr="select concat(s.nombre1,' ', s.nombre2,' ', s.apellido1,' ', apellido2) as nombre,\n" +
         "p.codigoPrestamo,p.estado,p.cuotas,p.fechaVencimiento,p.fechaAprobacion,p.cantidad,p.plazo,p.interes,p.saldo,p.estado from prestamos as p\n" +
         " inner join solicitante as s on p.idSolicitante = s.idSolicitante where p.fechaAprobacion between '"+fechaInicio+"' "
                 + "and '"+ fechaFin +"' and p.estado='Activo'";
         String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");  
         //instancia al javaBean
         // realizacion de consulta
         con.setRS(qr);
         ResultSet resultado = (ResultSet) con.getRs();
         double saldo = 0.00;
         double cuotas = 0.00;
        
         while (resultado.next())
         {
             saldo = resultado.getDouble("saldo");
             BeanCreditosOtorgados creditos = new BeanCreditosOtorgados();
             // llenando datos
             creditos.setCodigoPrestamo(resultado.getString("codigoPrestamo"));
             cuotas = resultado.getDouble("cuotas");
             cuotas = Math.rint(cuotas*100) / 100;
             creditos.setCuota(cuotas);
             creditos.setFechaFin(fechaFin);
             creditos.setFechaInicio(fechaInicio);
             creditos.setFechaOtorgamiento(resultado.getString("fechaAprobacion"));
             creditos.setFechaVencimiento(resultado.getString("fechaVencimiento"));
             creditos.setInteresNormal(resultado.getDouble("interes"));
             creditos.setMontoPrestamo(resultado.getDouble("cantidad"));
             creditos.setPlazo(resultado.getInt("plazo"));
             creditos.setSolicitante(resultado.getString("nombre"));
             creditos.setEstado(resultado.getString("estado"));
           
             creditos.setImg(rutaImg);
             
             if (saldo < 0.00)
                 creditos.setSaldoAfecha(0.00);
             else
                 creditos.setSaldoAfecha(resultado.getDouble("saldo"));
             
             
             reports.add((Object) creditos); 
         }
     
            //fill the ready report with data and parameter
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
            JRExporter exporter = null;
            response.setContentType("application/pdf");
            //Nombre del reporte
            response.setHeader("Content-Disposition","inline; filename=\"CreditosOtorgadosVigentes.pdf\";");
            //Exportar a pdf
            exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            //view the report using JasperViewer
            exporter.exportReport();
       
           // redireccion al panel principal
           response.sendRedirect(ruta + "/panelAdmin.jsp");
            
        } catch (JRException e) {
            e.printStackTrace();
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReportePrestamosVigentes_eliminar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportePrestamosVigentes_eliminar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReportePrestamosVigentes_eliminar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReportePrestamosVigentes_eliminar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
