/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// nuevas
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import clases.cumpleMes;
import java.sql.ResultSet;
import java.sql.SQLException;
import Conexion.conexion;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "ReporteCumpleanios", urlPatterns = {"/ReporteCumpleanios"})
public class ReporteCumpleanios extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
        ServletOutputStream out = response.getOutputStream();
        try {
        
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/reporteCumple.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        //parameters.put("parametro", "parametro");
        
        // obteniendo ruta para imagen temporal
        String rutaImg = getServletContext().getRealPath("images/cumple.jpg"); 
     
        List<Object> reports = new LinkedList<Object>();
       
        // sacando datos de los cumpleañeros
        cumpleMes mes = new cumpleMes();
        ResultSet datos = (ResultSet) mes.obtenerMensaje();
        //  regresando al registro antes del primero
        datos.previous();
        // creando instancia al bean cumpleanios
        
        // creando instancia conexion
        conexion con = new conexion();
        String qr="";
        ResultSet tmp = null;
        while (datos.next())
        {
            
            // obtener id
            beanCumpleanios p = new beanCumpleanios();
            String idSolicitante = datos.getString("idSolicitante");
         
            qr ="select * from solicitante where idSolicitante=" + idSolicitante;
            con.setRS(qr);
            tmp = (ResultSet) con.getRs();
            tmp.next();
            
            // cargando datos!
            p.setCargo(tmp.getString("cargo"));
            // sacando fecha a mostrar
            String fecha = tmp.getString("fecha_nacimiento");
            String[] fechaPartes = fecha.split("-");
            p.setFecha_cumple("Dia: " + fechaPartes[2]);
            int mesComparar =  Integer.parseInt(fechaPartes[1]);
            
            switch (mesComparar)
            {
                case 1:
                   fechaPartes[1] = "Enero";
                break;
                
                case 2:
                   fechaPartes[1] = "Febrero";
                break;
                case 3:
                   fechaPartes[1] = "Marzo";
                break;
                
                case 4:
                   fechaPartes[1] = "Abril";
                break;
                    
                case 5:
                   fechaPartes[1] = "Mayo";
                break;
                
                case 6:
                   fechaPartes[1] = "Junio";
                break;
                case 7:
                   fechaPartes[1] = "Julio";
                break;
                
                case 8:
                   fechaPartes[1] = "Agosto";
                break;
                case 9:
                   fechaPartes[1] = "septiembre";
                break;
                
                case 10:
                   fechaPartes[1] = "Octubre";
                break;
                    
                case 11:
                   fechaPartes[1] = "Noviembre";
                break;
                    
                case 12:
                   fechaPartes[1] = "Diciembre";
                break;
                    
            }
            
            p.setMes(fechaPartes[1]);
            p.setNombre(tmp.getString("nombre1") + " " + tmp.getString("nombre2") 
                    + " " + tmp.getString("apellido1") + " "+ tmp.getString("apellido2"));
            
            
            
            p.setTelefono(tmp.getString("tel_residencia"));
            p.setFoto(rutaImg);
            reports.add((Object) p);
        }
              
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
        JRExporter exporter = null;
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"CumpleaniosDelMes.pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        //view the report using JasperViewer
        exporter.exportReport();
        
       // redireccion al panel principal
       response.sendRedirect("/arcamacW/panelAdmin.jsp");
        
        } catch (JRException e) {
            e.printStackTrace();
        }
        
        }
   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteCumpleanios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteCumpleanios.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
