/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author ASUS
 */
public class beansaldoscierre {
    private int numeroMeses;
    private int anio;
    private String mes;
    private double monto_otorgado;
    private double cuota;
    private double saldo;
    private double capital;
    private double mora;
    private double normal;
    private double iva;
    private double promedio_capital;
    private double promedio_mora;
    private double promedio_normal;
    private double  promedio_iva;
    private String logo;
    private String fechaInicio;
    private String fechaFin;

    /**
     * @return the monto_otorgado
     */
    public double getMonto_otorgado() {
        return monto_otorgado;
    }

    /**
     * @param monto_otorgado the monto_otorgado to set
     */
    public void setMonto_otorgado(double monto_otorgado) {
        this.monto_otorgado = monto_otorgado;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the mora
     */
    public double getMora() {
        return mora;
    }

    /**
     * @param mora the mora to set
     */
    public void setMora(double mora) {
        this.mora = mora;
    }

    /**
     * @return the normal
     */
    public double getNormal() {
        return normal;
    }

    /**
     * @param normal the normal to set
     */
    public void setNormal(double normal) {
        this.normal = normal;
    }

    /**
     * @return the iva
     */
    public double getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(double iva) {
        this.iva = iva;
    }

    /**
     * @return the promedio_capital
     */
    public double getPromedio_capital() {
        return promedio_capital;
    }

    /**
     * @param promedio_capital the promedio_capital to set
     */
    public void setPromedio_capital(double promedio_capital) {
        this.promedio_capital = promedio_capital;
    }

    /**
     * @return the promedio_mora
     */
    public double getPromedio_mora() {
        return promedio_mora;
    }

    /**
     * @param promedio_mora the promedio_mora to set
     */
    public void setPromedio_mora(double promedio_mora) {
        this.promedio_mora = promedio_mora;
    }

    /**
     * @return the promedio_normal
     */
    public double getPromedio_normal() {
        return promedio_normal;
    }

    /**
     * @param promedio_normal the promedio_normal to set
     */
    public void setPromedio_normal(double promedio_normal) {
        this.promedio_normal = promedio_normal;
    }

    /**
     * @return the promedio_iva
     */
    public double getPromedio_iva() {
        return promedio_iva;
    }

    /**
     * @param promedio_iva the promedio_iva to set
     */
    public void setPromedio_iva(double promedio_iva) {
        this.promedio_iva = promedio_iva;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the fechaInicio
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * @return the fechaFin
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the numeroMeses
     */
    public int getNumeroMeses() {
        return numeroMeses;
    }

    /**
     * @param numeroMeses the numeroMeses to set
     */
    public void setNumeroMeses(int numeroMeses) {
        this.numeroMeses = numeroMeses;
    }

    /**
     * @return the anio
     */
    public int getAnio() {
        return anio;
    }

    /**
     * @param anio the anio to set
     */
    public void setAnio(int anio) {
        this.anio = anio;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }
    
    
}
   