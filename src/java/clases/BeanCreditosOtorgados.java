/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Jorge Luis
 */
public class BeanCreditosOtorgados {
    private String codigoPrestamo;
    private String solicitante;
    private String fechaOtorgamiento;
    private String fechaVencimiento;
    private double montoPrestamo;
    private int plazo;
    private double cuota;
    private double interesNormal; // tasa
    private double interessMora; // tasa
    private double saldoAfecha;
    private int cuotasCancelados;
    private String fechaInicio;
    private String fechaFin;
    private String img;
    private String estado;
    private int diasInteres;
    private int diasInteresMora;
    private Double iva;
    private double interesNormal_total; 
    private double interessMora_total;
    
    public BeanCreditosOtorgados()
    {
        codigoPrestamo = "";
        solicitante = "";
        fechaOtorgamiento ="";
        fechaVencimiento = "";
        montoPrestamo = 0.00;
        plazo = 0;
        cuota = 0.00;
        img="";
        interesNormal = 0.00;
        interessMora = 0.00;
        saldoAfecha = 0.00;
        cuotasCancelados = 0;
        fechaInicio = "";
        fechaFin = "";
        estado="";
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the fechaOtorgamiento
     */
    public String getFechaOtorgamiento() {
        return fechaOtorgamiento;
    }

    /**
     * @param fechaOtorgamiento the fechaOtorgamiento to set
     */
    public void setFechaOtorgamiento(String fechaOtorgamiento) {
        this.fechaOtorgamiento = fechaOtorgamiento;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the montoPrestamo
     */
    public double getMontoPrestamo() {
        return montoPrestamo;
    }

    /**
     * @param montoPrestamo the montoPrestamo to set
     */
    public void setMontoPrestamo(double montoPrestamo) {
        this.montoPrestamo = montoPrestamo;
    }

    /**
     * @return the plazo
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the interesNormal
     */
    public double getInteresNormal() {
        return interesNormal;
    }

    /**
     * @param interesNormal the interesNormal to set
     */
    public void setInteresNormal(double interesNormal) {
        this.interesNormal = interesNormal;
    }

    /**
     * @return the interessMora
     */
    public double getInteressMora() {
        return interessMora;
    }

    /**
     * @param interessMora the interessMora to set
     */
    public void setInteressMora(double interessMora) {
        this.interessMora = interessMora;
    }

    /**
     * @return the saldoAfecha
     */
    public double getSaldoAfecha() {
        return saldoAfecha;
    }

    /**
     * @param saldoAfecha the saldoAfecha to set
     */
    public void setSaldoAfecha(double saldoAfecha) {
        this.saldoAfecha = saldoAfecha;
    }

    /**
     * @return the cuotasCancelados
     */
    public int getCuotasCancelados() {
        return cuotasCancelados;
    }

    /**
     * @param cuotasCancelados the cuotasCancelados to set
     */
    public void setCuotasCancelados(int cuotasCancelados) {
        this.cuotasCancelados = cuotasCancelados;
    }

    /**
     * @return the fechaInicio
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * @return the fechaFin
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the img
     */
    public String getImg() {
        return img;
    }

    /**
     * @param img the img to set
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the diasInteres
     */
    public int getDiasInteres() {
        return diasInteres;
    }

    /**
     * @param diasInteres the diasInteres to set
     */
    public void setDiasInteres(int diasInteres) {
        this.diasInteres = diasInteres;
    }

    /**
     * @return the diasInteresMora
     */
    public int getDiasInteresMora() {
        return diasInteresMora;
    }

    /**
     * @param diasInteresMora the diasInteresMora to set
     */
    public void setDiasInteresMora(int diasInteresMora) {
        this.diasInteresMora = diasInteresMora;
    }

    /**
     * @return the iva
     */
    public Double getIva() {
        return iva;
    }

    /**
     * @param iva the iva to set
     */
    public void setIva(Double iva) {
        this.iva = iva;
    }

    /**
     * @return the interesNormal_total
     */
    public double getInteresNormal_total() {
        return interesNormal_total;
    }

    /**
     * @param interesNormal_total the interesNormal_total to set
     */
    public void setInteresNormal_total(double interesNormal_total) {
        this.interesNormal_total = interesNormal_total;
    }

    /**
     * @return the interessMora_total
     */
    public double getInteressMora_total() {
        return interessMora_total;
    }

    /**
     * @param interessMora_total the interessMora_total to set
     */
    public void setInteressMora_total(double interessMora_total) {
        this.interessMora_total = interessMora_total;
    }
    
}
