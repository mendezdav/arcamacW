/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

/**
 *
 * @author Pochii
 */
public class BeanHistorialPrestamo {
    private String codigoPrestamo;
    private String solicitante;
    private double cantidad;
    private double cuota;
    private String fechaAprobacion;
    private String fechaVencimiento;
    private double interes;
    private double saldoPendiente;
    private String estadoPrestamo;
    private int pagosCancelados;
    private int pagosMoraCancelados;
    private int pagosMoraPendiente;
    private String groupMora;
    private String nombreFiador;
    private String refinanciamiento;

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the cantidad
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the fechaAprobacion
     */
    public String getFechaAprobacion() {
        return fechaAprobacion;
    }

    /**
     * @param fechaAprobacion the fechaAprobacion to set
     */
    public void setFechaAprobacion(String fechaAprobacion) {
        this.fechaAprobacion = fechaAprobacion;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the saldoPendiente
     */
    public double getSaldoPendiente() {
        return saldoPendiente;
    }

    /**
     * @param saldoPendiente the saldoPendiente to set
     */
    public void setSaldoPendiente(double saldoPendiente) {
        this.saldoPendiente = saldoPendiente;
    }

    /**
     * @return the estadoPrestamo
     */
    public String getEstadoPrestamo() {
        return estadoPrestamo;
    }

    /**
     * @param estadoPrestamo the estadoPrestamo to set
     */
    public void setEstadoPrestamo(String estadoPrestamo) {
        this.estadoPrestamo = estadoPrestamo;
    }

    /**
     * @return the pagosCancelados
     */
    public int getPagosCancelados() {
        return pagosCancelados;
    }

    /**
     * @param pagosCancelados the pagosCancelados to set
     */
    public void setPagosCancelados(int pagosCancelados) {
        this.pagosCancelados = pagosCancelados;
    }

    /**
     * @return the pagosMoraCancelados
     */
    public int getPagosMoraCancelados() {
        return pagosMoraCancelados;
    }

    /**
     * @param pagosMoraCancelados the pagosMoraCancelados to set
     */
    public void setPagosMoraCancelados(int pagosMoraCancelados) {
        this.pagosMoraCancelados = pagosMoraCancelados;
    }

    /**
     * @return the pagosMoraPendiente
     */
    public int getPagosMoraPendiente() {
        return pagosMoraPendiente;
    }

    /**
     * @param pagosMoraPendiente the pagosMoraPendiente to set
     */
    public void setPagosMoraPendiente(int pagosMoraPendiente) {
        this.pagosMoraPendiente = pagosMoraPendiente;
    }

    /**
     * @return the groupMora
     */
    public String getGroupMora() {
        return groupMora;
    }

    /**
     * @param groupMora the groupMora to set
     * La propiedad groupMora es de la forma estado(pagosmora);
     */
    public void setGroupMora(String groupMora) {
        this.groupMora = groupMora;
        int indice1 = 0; //Banderas de los estados
        int indice2 = 1;
        /**Arreglo para manejar los strings***/
        String[] partes1 = groupMora.split(";");
        if(groupmora_estado(partes1[0]).equals("sa")){ /***Opcion devuelta para prestamos NO aprobados***/
            this.setPagosMoraCancelados(0);
            this.setPagosMoraPendiente(0);
        } //fin if
        
        if(partes1.length == 3)
        {
            indice1 = 1;
            indice2 = 2;
        }
        if(groupmora_estado(partes1[indice1]).equals("Pagado")){ 
             /**Partes1[0] -- pagos cancelados
             *  Partes1[1] -- Pagos pendientes 
             */
                this.setPagosMoraCancelados(groupmora_cantidad(partes1[indice1]));
                this.setPagosMoraPendiente(groupmora_cantidad(partes1[indice2]));
            }
            else if(groupmora_estado(partes1[0]).equals("Abonado")){ /***Verificacion, en caso que se alterara el orden***/
                this.setPagosMoraCancelados(groupmora_cantidad(partes1[indice1]));
                this.setPagosMoraPendiente(groupmora_cantidad(partes1[indice2]));
            }    
    }

    /**
     * @return the nombreFiador
     */
    public String getNombreFiador() {
        return nombreFiador;
    }

    /**
     * @param nombreFiador the nombreFiador to set
     */
    public void setNombreFiador(String nombreFiador) {
        String []partes = nombreFiador.split(";");  //En caso que hayan dos fiadores
        if(partes.length == 1)
            this.nombreFiador = partes[0];
        else if(partes.length == 2){
            this.nombreFiador = partes[0].trim() + "<br />" + partes[1].trim();
        }
    }

    /**
     * @return the refinanciamiento
     */
    public String getRefinanciamiento() {
        return refinanciamiento;
    }

    /**
     * @param refinanciamiento the refinanciamiento to set
     */
    public void setRefinanciamiento(String refinanciamiento) {
        this.refinanciamiento = refinanciamiento;
    }
    
    private String groupmora_estado(String parte){
        String [] subparte = parte.split("\\(");  //Separando a partir del primer parentesis
        return subparte[0];
    }
    private int groupmora_cantidad(String parte){
        String subparte = parte.substring(parte.indexOf('(') + 1, parte.indexOf(')')); /**Obteniendo lo que esta dentro de los parentesis**/
        return Integer.parseInt(subparte);
    }
}

