package clases;

import Conexion.conexion;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class classPagos {
    public int mensaje;                  //Mensaje que es devuelto a jsp
    String idPago;
    double monto;
    double monto_auxiliar;                  //En un momento, tendra el valor del monto sobrante (en caso que pague mas)
    int banderaMontoAux;                    // cero si monto aux == 0.00 , 1 si es mayor a 0.00
    String codigoPrestamo;                  //Codigo de prestamo BD
    String estadoMora;
    String fechaEstablecida;                //Fecha establecida del pago actual
    String comprobante;
    double intereses;                       //Intereses a pagar BD
    double interes_moratorio;               //Mora a pagar BD
    double iva_interes;
    double iva_mora;
    double capital;                         //Capital a pagar BD
    double pago_interes;                    //Abono de intereses BD
    double pago_interes_moratorio;          //Abono a mora BD
    double pago_capital;                    //Abono a capital BD
    double pago_iva_interes;                    //Abono de iva intereses BD
    double pago_iva_mora;                        //Abono a iva mora BD
    public double mpago_interes;                   //Parte del monto que va a intereses
    public double mpago_interes_moratorio;         //Parte del monto que va a intereses moratorios
    public double mpago_iva_interes;                   //Parte del monto que va a iva intereses
    public double mpago_iva_mora;                    //Parte del monto que va a iva intereses moratorios
    public double mpago_capital;                   //Parte del monto que va a capital
    int no_pago;
    conexion con;
    ResultSet rs;
    CachedRowSetImpl crPagos;
    
    public classPagos(){
        rs = null;
        con = null;
        no_pago = 0;
    }
    /**
     * @param pidPago id del pago a procesar
     * @param pmonto  Monto a abonar
     */
    public classPagos(String pidPago, double pmonto){
        this.monto  = pmonto;
        this.idPago = pidPago;
        crPagos = null;
    }
    
    public String getComprobateAbono()
    {
        return this.comprobante;
    }
    
    
    /**
     * Funcion para llenar las propiedades de la clase con los datos de la base de datos
     * @return Verdadero en caso que la seleccion se hiciera correctamente, falso en caso contrario
     * @throws java.sql.SQLException
     */
    public boolean llenarDatos(String idPago) throws SQLException{
        try{
            this.crPagos = getPagos("WHERE idPago = " + idPago + " AND estado != 'Pagado'");
            while(crPagos.next()){   
                this.codigoPrestamo         = crPagos.getString("codigoPrestamo");
                this.capital                = crPagos.getDouble("capital");
                this.intereses              = crPagos.getDouble("intereses");
                this.interes_moratorio      = crPagos.getDouble("interes_moratorio");
                this.pago_interes           = crPagos.getDouble("pago_interes");
                this.pago_interes_moratorio = crPagos.getDouble("pago_interes_moratorio");
                this.pago_capital           = crPagos.getDouble("pago_capital");   
                this.fechaEstablecida       = crPagos.getString("fechaEstablecida");
                this.no_pago                = crPagos.getInt("Npago");
                
                this.iva_interes              = crPagos.getDouble("iva_interes");
                this.iva_mora                 = crPagos.getDouble("iva_mora");
                this.pago_iva_interes         = crPagos.getDouble("pago_iva_interes");
                this.pago_iva_mora            = crPagos.getDouble("pago_iva_mora");
            }
            return true;
        }
        catch(SQLException e){
            System.out.println("Hubo un error: " + e);
            return false;
        } //fin catch
    }//fin llenar Datos
    /**
     * @param condicion filtrado de la query de pagos, puede ser un where, order by,group by
     * @return Resultset con los datos consultados
     * @throws java.sql.SQLException
     */
    public CachedRowSetImpl getPagos(String condicion) throws SQLException{
        CachedRowSetImpl crs = new CachedRowSetImpl();
        String query = "SELECT (@rownum:=@rownum+1) as fila,Npago, idPago, codigoPrestamo, comprobante, monto, fechaPago, fechaEstablecida," +
                       "intereses, capital, estado, pago_interes, pago_capital, interes_moratorio, pago_interes_moratorio,iva_interes, pago_iva_interes, iva_mora, pago_iva_mora, saldo, mora " +
                       "FROM (SELECT @rownum:=-1)r,pagos ";
        /**Agredando condicion a la query**/
        query += condicion;

        System.out.println("Query generado: " + query);
        try{
            con  = new conexion();
            con.setRS(query);
            rs = con.getRs();
            crs.populate(rs);
            return crs;
        }
        catch(SQLException e){
            System.out.println("query: " + query + "\n");
            System.out.println("Error: " + e.getMessage());
            return null;
        }
        finally{
            con.cerrarConexion();
        }
        
    }//fin getPagos()

    /**
     * Esta funcion es llamada desde insertarAbono
     * Funcion para separar el monto obtenido
     */
    public void desglosarMonto(){
        monto_auxiliar = this.monto;
        /*
         *  SE REALIZA INTERES_x - PAGO_INTERES_X, debido a que puede existir dicho pago
         *  con un abono anterior por lo que se realiza la resta para conocer la cantidad
         *  pendiente para dicho "gasto"
         */
        if(monto_auxiliar >= (interes_moratorio - pago_interes_moratorio))
        {
            mpago_interes_moratorio = (interes_moratorio - pago_interes_moratorio);
            monto_auxiliar -= mpago_interes_moratorio;
            if(monto_auxiliar >= (intereses - pago_interes))
            {
                mpago_interes = (intereses - pago_interes);
                monto_auxiliar -= mpago_interes;
                if(monto_auxiliar >= (capital - pago_capital))
                {
                    mpago_capital = (capital - pago_capital);
                    monto_auxiliar -= mpago_capital;
                }
                else{ //Si no cubre el capital
                    mpago_capital = monto_auxiliar;
                    monto_auxiliar = 0;
                }//fin if capital
            }
            else{ //Si no cubre intereses normales
                mpago_interes = monto_auxiliar;
                monto_auxiliar = 0;
            } //fin else interes
        }
        else{ //Si no cubre intereses moratorios
            mpago_interes_moratorio = monto_auxiliar;
            monto_auxiliar = 0;
        } //fin if interes moratorio
        monto_auxiliar = diferenciaDias.redondearBig(monto_auxiliar);
        mpago_interes_moratorio = diferenciaDias.redondearBig(mpago_interes_moratorio);
        mpago_interes = diferenciaDias.redondearBig(mpago_interes);
        mpago_capital = diferenciaDias.redondearBig(mpago_capital);
    }
    
    /**
     * Esta funcion se llama desde insertarAbono
     * @param idPago
     * @return
     * @throws SQLException 
     */
    public String generarComprobanteAbono(String idPago) throws SQLException
    {
        int correlativo=0;
        String comprobanteAnt="";
        String comprobantePago="";
        String query = "select p.comprobante as Cpago,a.comprobante  as Cabono from pagos as p\n" +
                        "inner join abonos as a on p.idPago = a.idPago where\n" +
                        "p.idPago=" + idPago+" order by Cabono DESC limit 1;";
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        if (rs.next())
        {
            comprobanteAnt = rs.getString("Cabono");
            comprobantePago = rs.getString("Cpago");
        }
        else
        {
            query ="select comprobante from pagos where idPago=" + idPago;
            con.setRS(query);
            rs = con.getRs();
            if (rs.next()){
                comprobanteAnt = rs.getString("comprobante") + "00";
                comprobantePago = rs.getString("comprobante");
            }//fin if
        }
        con.cerrarConexion();
        correlativo = Integer.parseInt(comprobanteAnt.substring(6,8));
        correlativo++;
        return (comprobantePago + String.format("%02d", correlativo));
    }
    /**
     * Metodo principal de la clase
     * @param idPago
     * @param monto
     * @return
     * @throws SQLException 
     */
    public boolean insertarAbono(String idPago, String codigoPrestamo, Double monto) throws SQLException
    {
        llenarDatos(idPago);
        String qr = "select fechaPago from pagos where estado != 'Pendiente' order by comprobante ASC limit 1"; 
        conexion con = new conexion();
        String fechapagoAnt = "";
        try{
            con.setRS(qr);
            rs = con.getRs();
            rs.next();
            fechapagoAnt = rs.getString("fechaPago");
        }
        catch (Exception e)
        {
            fechapagoAnt = "1000-01-01"; 
        }
        
        this.comprobante = generarComprobanteAbono(idPago);
        
        boolean estado = true;
        String query = "";
        
        mpago_interes = 0.0;
        mpago_interes_moratorio = 0.0;
        mpago_capital = 0.0;
        mpago_iva_interes = 0.0;
        mpago_iva_mora = 0.0;
        
        double interes_pendiente = intereses - pago_interes;
        double iva_interes_pendiente = iva_interes - pago_iva_interes;
        double interes_moratorio_pendiente = interes_moratorio - pago_interes_moratorio;
        double iva_mora_pendiente = iva_mora - pago_iva_mora;
        double total_interes_pendiente = interes_pendiente + interes_moratorio_pendiente + iva_interes_pendiente + iva_mora_pendiente;
        
        double capital_pendiente = capital - pago_capital;
        
        double total_pendiente = capital_pendiente + total_interes_pendiente;
        
        if(monto >= total_pendiente){
            /***
             * Si el monto es suficiente para saldar la deuda completa
             */
            monto -= total_pendiente; // restamos la cantidad a pagar al monto cancelado por el cliente
            pago_interes_moratorio += interes_moratorio_pendiente; // la deuda de interes moratorio se salda completa
            pago_interes += interes_pendiente;  // la deuda de interes corriente se paga completa
            pago_capital += capital_pendiente;  // el capital correspondiente se paga completo
            pago_iva_interes += iva_interes_pendiente;
            pago_iva_mora += iva_mora_pendiente;
            
            // se distribuye el monto cancelado para el posterior resumen del abono
            mpago_capital = capital_pendiente;  
            mpago_interes = interes_pendiente + iva_interes_pendiente;
            mpago_interes_moratorio = interes_moratorio_pendiente + iva_mora_pendiente;
            
            // se completa el monto del pago
            this.monto += total_pendiente;
        }else{
            /**
             * En este caso el monto no salda la deuda completa del pago correspondiente, 
             * por lo tanto será necesario distribuir el monto con cierto nivel de prioridad
             */
            
            // primero se cancela la mora (si existe)
            if(monto < interes_moratorio_pendiente){
                // Para este caso se asume que el monto no es suficiente para cancelar la mora
                pago_interes_moratorio += monto; // se actualiza el pago de los intereses moratorios
                mpago_interes_moratorio = monto; // se actualiza el monto para el resumen del abono
                this.monto += monto; // se actualiza el monto total del pago
                monto = 0.0;    // el efectivo proporcionado por el cliente llega a cero pues se consume todo en el pago
            }else{
                // Para este caso se asume que el monto cubre todos los intereses moratorios
                pago_interes_moratorio += interes_moratorio_pendiente;  // se actualiza el pago de los intereses moratorios
                mpago_interes_moratorio = interes_moratorio_pendiente; // se actualiza el monto para el resumen del abono
                this.monto += interes_moratorio_pendiente; // se actualiza el monto total del pago
                monto -= interes_moratorio_pendiente; // se resta la cantidad del efectivo consumida en el pago 
                // ahora se cancelan los interes corrientes (si existen)
                
                if(monto < iva_mora_pendiente){
                    // Para este caso se asume que el monto no es suficiente para cancelar la mora
                    pago_iva_mora += monto; // se actualiza el pago de los intereses moratorios
                    mpago_interes_moratorio += monto; // se actualiza el monto para el resumen del abono
                    this.monto += monto; // se actualiza el monto total del pago
                    monto = 0.0;    // el efectivo proporcionado por el cliente llega a cero pues se consume todo en el pago
                }else{
                    // Para este caso se asume que el monto cubre todos los intereses moratorios
                    pago_iva_mora += iva_mora_pendiente;  // se actualiza el pago de los intereses moratorios
                    mpago_interes_moratorio += iva_mora_pendiente; // se actualiza el monto para el resumen del abono
                    this.monto += iva_mora_pendiente; // se actualiza el monto total del pago
                    monto -= iva_mora_pendiente; // se resta la cantidad del efectivo consumida en el pago 
                    // ahora se cancelan los interes corrientes (si existen)
                    
                    if(monto < interes_pendiente){
                        // Pasa este caso se asume que el monto no es suficiente par cancelar los interes corrientes
                        pago_interes += monto; // se actualiza el pago de los intereses
                        mpago_interes = monto; // se actualiza el monto para el resumen del abono
                        this.monto += monto; // se actualiza el monto total del pago
                        monto = 0.0;  // el efectivo proporcionado por el cliente llega a cero pues se consume todo en el pago
                    }else{
                        // Para este caso se asume que el monto cubre todos los intereses
                        pago_interes += interes_pendiente; // se actualiza el pago de los intereses
                        mpago_interes = interes_pendiente; // se actualiza el monto para el resumen del abono
                        this.monto += interes_pendiente; // se actualiza el monto total del pago
                        monto -= interes_pendiente; // se resta la cantidad del efectivo consumida en el pago 
                        // ahora se cancela el capital pendiente
                        
                        if(monto < iva_interes_pendiente){
                            // Pasa este caso se asume que el monto no es suficiente par cancelar los interes corrientes
                            pago_iva_interes += monto; // se actualiza el pago de los intereses
                            mpago_interes += monto; // se actualiza el monto para el resumen del abono
                            this.monto += monto; // se actualiza el monto total del pago
                            monto = 0.0;  // el efectivo proporcionado por el cliente llega a cero pues se consume todo en el pago
                        }else{
                            // Para este caso se asume que el monto cubre todos los intereses
                            pago_iva_interes += iva_interes_pendiente; // se actualiza el pago de los intereses
                            mpago_interes += iva_interes_pendiente; // se actualiza el monto para el resumen del abono
                            this.monto += iva_interes_pendiente; // se actualiza el monto total del pago
                            monto -= iva_interes_pendiente; // se resta la cantidad del efectivo consumida en el pago 
                            // ahora se cancela el capital pendiente
                            if(monto < capital_pendiente){
                                // Para este caso se asume que el monto cubre todo el capital pendiente del pago correspondiente
                                pago_capital += monto; // se actualiza el pago del capital
                                mpago_capital = monto; // se actualiza el monto para el resumen del abono
                                this.monto += monto; // se actualiza el monto total del pago
                                monto = 0.0; // el efectivo proporcionado por el cliente llega a cero pues se consume todo en el pago
                            }else{
                                pago_capital += capital_pendiente; // se actualiza el pago del capital
                                mpago_capital = capital_pendiente; // se actualiza el monto para el resumen del abono
                                this.monto += capital_pendiente; // se actualiza el monto total del pago
                                monto -= capital_pendiente; // se resta la cantidad del efectivo consumida en el pago 
                            }
                        }
                    }
                }
            }
        }
        
        /**
         * En este punto ya ha sido distribuido el monto sobre todos los conceptos
         * del pago actual, ahora es necesario 
         * #1 Actualizar el pago en la base de datos
         * #2 Actualizar el estado del prestamo
         * #3 Si el monto restante es mayor que cero, abonar al siguiente pago disponible
         * #4 Generar el comprobante del abono
         */
        
        // #1 y #2 ACTUALIZANDO LOS DATOS DEL PAGO Y EL ESTADO DEL PRESTAMO
        actualizarPago(); // Esta funcion sufrio modificaciones significantes, revisarla para mayor informacion
        
        // #3 SI HAY MONTO RESTANTE SEGUIR CON LOS SIGUIENTES PAGOS
        if(monto > 0){
            // Verificar si el ultimo pago ha cancelado el prestamos
            // de ser asi no tiene sentido seguir cobrando
            if(this.prestamoPagado(this.codigoPrestamo) != 1){
                String nuevoIdPago = getPagoSiguiente(this.codigoPrestamo);
                if (nuevoIdPago != "0")
                {
                    classPagos nPago = new classPagos(nuevoIdPago, monto);
                    nPago.insertarAbono(nuevoIdPago, this.codigoPrestamo, monto);
                }
            }
        }
        
        // obtener saldo anterior del prestamo
        query ="select p.saldo from prestamos as p inner join pagos on pagos.codigoPrestamo = p.codigoPrestamo where pagos.idPago=" + idPago;
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        double saldoant = rs.getDouble("saldo");
        
        
        String diaActual = diferenciaDias.fechaActual();
        query = "insert into abonos(idPago,monto,fechaPago,pago_interes,pago_capital,pago_interes_moratorio,comprobante,fechaPagoAnt,saldoAnt,SaldoActual,codigoPrestamo) "
                    + "values("+idPago+","+(mpago_capital + mpago_interes + mpago_interes_moratorio)+",'"+diaActual+"',"+mpago_interes+","
                    + ""+mpago_capital+","+mpago_interes_moratorio+",'"+comprobante+"','"+ fechapagoAnt+"'," + (saldoant + mpago_capital) + "," + saldoant + ",'" +codigoPrestamo+ "');";
        
       
        estado = con.actualizar(query);
        con.cerrarConexion();
        
        return estado;
    }
    /**
     * Esta funcion es llamada desde ActualizarPagos
     * @param valores
     * @return
     * @throws SQLException 
     */
    public int compararAbonoPagos(List<Double> valores) throws SQLException
    {
        String query ="select intereses,capital,interes_moratorio from pagos where idPago=" + this.idPago;
        int estado = -1;
        try 
        {
            con = new conexion();
            con.setRS(query);
            rs = con.getRs();
            if (rs.next())
            {
                // obtener datos a cancelar en tabla pagos
                double interes=0.0,interes_moratorio=0.0,capital=0.0;
                interes_moratorio = rs.getDouble("interes_moratorio");
                interes = rs.getDouble("intereses");
                capital = rs.getDouble("capital");
                double capital_temp = capital;
                // realizando restas
                interes_moratorio = diferenciaDias.redondear(valores.get(0) - interes_moratorio);
                interes = diferenciaDias.redondear(valores.get(1) - interes);
                capital = diferenciaDias.redondear(valores.get(2) - capital);
                if (this.monto_auxiliar > 0.00)
                {
                    capital = valores.get(2) -  (this.monto_auxiliar +  capital_temp);
                }
               
                if ( interes_moratorio == 0.0)
                {   estado =1;
                    if (interes == 0.0)
                    {   estado =2;
                        if (capital == 0.0)
                            estado = 3;
                    }
                }
             }
            return estado;
        }
        catch(SQLException e){
            System.out.println("query: " + query + "\n");
            System.out.println("Error: " + e.getMessage());
            return -1;
        }
        finally
        {
            con.cerrarConexion();
        }
 
    }
    /**
     * Esta funcion es llamada desde actualizarPago
     * @param idPago
     * @return
     * @throws SQLException 
     */
    public List<Double> sumaAbono(String idPago) throws SQLException
    {
        List<Double> SUMAbono = new ArrayList<Double>();
        String query ="select sum(pago_interes) as interes, sum(pago_interes_moratorio) as moratorio,"
                + "sum(pago_capital) as capital from abonos where idPago=" + idPago;
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        SUMAbono.add(0, rs.getDouble("moratorio"));
        SUMAbono.add(1, rs.getDouble("interes"));
        SUMAbono.add(2, rs.getDouble("capital"));
        con.cerrarConexion();
        return SUMAbono;
    }
    /**
     * Esta funcion es llamada desde ActualizarPago
     * @param codPrestamo
     * @return
     * @throws SQLException 
     */
    private String getPagoSiguiente(String codPrestamo) throws SQLException{
        try{
            String id = "";
            con = new conexion();
            con.setRS("SELECT idPago FROM pagos WHERE estado!='Pagado' AND codigoPrestamo="
                    + "'" + codigoPrestamo + "' ORDER BY idPago ASC LIMIT 1");
            rs = con.getRs();
            if(rs.next())
                id =  rs.getString("idPago");
            else
                id = "0";
            return id;
        }
        catch(SQLException e){
            return "0";
        }
        finally{
            rs.close();
            con.cerrarConexion();   
        }
    } //fin getPagoSiguiente
   
    /**
     * @return true si el prestamo se finalizo
     *         false si hubo un error
     * @throws SQLException 
     */
    private boolean finalizarPrestamo() throws SQLException{
        try{
            con = new conexion();
            con.actualizar("UPDATE prestamos SET estado = 'Pagado', fechaFinalizacion = '" + diferenciaDias.fechaActual() +"' WHERE codigoPrestamo =  '" + this.codigoPrestamo + "'");
            return true;
        }
        catch(SQLException e){
            return false;
        }
        finally{
            con.cerrarConexion();   
        }
    } 
    /**
     * Funcion para determinar si el idPago es el ultimo
     * @param idPago 
     * @return 0 si no es el ultimo pago
     *         1 Si es el ultimo pago
     *        -1 Si es un error
     * @throws SQLException 
     */
    public int ultimoPago(String idPago) throws SQLException{
        String id_aux ="";
        int ret = 0;
        con = new conexion();
        con.setRS("SELECT idPago FROM pagos WHERE codigoPrestamo = '" + this.codigoPrestamo + "' ORDER BY idPago DESC limit 1");
        rs = con.getRs();
        if(rs.next()){
            id_aux = rs.getString("idPago");
            if(id_aux.equals(idPago))
                ret = 1;
            else
                ret = 0;
        }
        else
            ret = -1;
        con.cerrarConexion();
        return ret;
    }    
    
    /* ESTA FUNCION SERA MODIFICADA PARA ADAPTARSE A LOS NUEVOS REQUIRIMIENTOS DEL ALGORITMO */
    public void actualizarPago() throws SQLException
    {
        /**
         * Los montos en la clase pagos ya están actualizados por el nuevo algoritmo, por lo tanto
         * no es necesario hacer una asignación del tipo += para los elementos, en lugar de ellos
         * se actualiza el valor del campo correspondiente en la base de datos con el valor actual
         * de dicha propiedad de la clase pagos
         */
        String query = "update pagos set monto=" + (pago_interes_moratorio + pago_interes + pago_capital + pago_iva_interes + pago_iva_mora)+  ",pago_interes_moratorio="+pago_interes_moratorio+","
                        + "pago_interes="+pago_interes +",pago_capital=" +pago_capital+ ", "
                        + "pago_iva_interes="+pago_iva_interes +",pago_iva_mora=" +pago_iva_mora+ ", "
                        + "fechaPago='" + diferenciaDias.fechaActual() + "' where idPago=" + this.idPago;
        
        con = new conexion();
        con.actualizar(query);
        con.cerrarConexion();
        
        /**
         * No es necesario llamar a funciones que verifiquen el estado del pago, basta con consultar las propiedades
         * de la clase que guardan el estado actual del pago correspondiente
         */
        double interes_pendiente = intereses - pago_interes;
        double interes_moratorio_pendiente = interes_moratorio - pago_interes_moratorio;
        double iva_interes_pendiente = iva_interes - pago_iva_interes;
        double iva_mora_pendiente = iva_mora - pago_iva_mora;
        double total_interes_pendiente = interes_pendiente + interes_moratorio_pendiente + iva_interes_pendiente + iva_mora_pendiente;
        
        double capital_pendiente = capital - pago_capital;
        
        double total_pendiente = capital_pendiente + total_interes_pendiente;
        
        if(total_pendiente == 0){
            query = "update pagos set estado='Pagado' where idPago=" + this.idPago;
        }else{
            query = "update pagos set estado='Abonado' where idPago=" + this.idPago;
        }
        /*List<Double> resultadoAbono = new ArrayList<Double>();
        resultadoAbono = sumaAbono(this.idPago);
         
        int estadoAbono = compararAbonoPagos(resultadoAbono);
        if  (estadoAbono == 3)
        {
            // estadoAbono == 3, ya que se han cancelado mora,normal y capital
            // verificar si termino de cancelar el saldo de dicha cuota
            // si la funcion retorna 0 = capital de la cuota esta cubierto
            // si retorna -1, hay un error en el sql query
            // si retorna 1 hay capital pendiente
          
            int estadoCapital = this.CapitalPendiente( String.valueOf(this.no_pago) , this.codigoPrestamo);
           
            if ( estadoCapital == 0 )
                query = "update pagos set estado='Pagado' where idPago=" + this.idPago;
            else
                query = "update pagos set estado='Abonado' where idPago=" + this.idPago;
        }
        else
            query = "update pagos set estado='Abonado' where idPago=" + this.idPago;
            
        */
        
        con = new conexion();
        con.actualizar(query);
        con.cerrarConexion();
        
        
        ActualizarSaldoPrestamo(); // Esta funcion no sufre ningun cambio, su estructura actual sigue siendo funcional
        
        /*query = "select saldo from prestamos where codigoPrestamo='" + this.codigoPrestamo + "'";
        conexion con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();*/
        
       //Si el prestamo ya esta pagado
        if(this.prestamoPagado(this.codigoPrestamo) == 1){
            this.finalizarPrestamo();                     //Cambiar estado del prestamo
            mensaje = 2;
        }
        else{
            mensaje = 1;
        }
    }
    /**
     * Esta funcion es llamada desde actualizarPago
     * @throws SQLException 
     */
    public void ActualizarSaldoPrestamo() throws SQLException
    {
        String query ="select saldo from prestamos where codigoPrestamo='" + this.codigoPrestamo+ "'";
        con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        double saldo = rs.getDouble("saldo") - mpago_capital;
        if(saldo < 0.0)
            saldo = 0.0;
        query ="update prestamos set saldo=" + saldo + " where codigoPrestamo='" + this.codigoPrestamo+ "'";
        con.actualizar(query);
        query ="update pagos set saldo=" + saldo+" where codigoPrestamo='" + this.codigoPrestamo+ "' and idPago=" + this.idPago;
        con.actualizar(query);
        con.cerrarConexion();
    }
    /**
     * 
     * @param codigoPrestamo
     * @return 1 Si el prestamo esta pagado
     *         0 Si el prestamo aun tiene saldo pendiente
     *        -1 Si el codigo no existe o hubo algun error
     * @throws SQLException 
     */
    public int prestamoPagado(String codigoPrestamo) throws SQLException{
        int ret = 0;
        try{
            double saldo_aux = 0.0; //Variable auxiliar del saldo del prestamo
            
            con = new conexion();
            con.setRS("SELECT saldo FROM prestamos WHERE codigoPrestamo='" + codigoPrestamo + "'");
            rs = con.getRs();
            if(rs.next())
                saldo_aux =  rs.getDouble("saldo");
            else
                saldo_aux = -1;
            
            if(saldo_aux == 0.0) //Verificando si el saldo esta pagado
                ret =  1;       
            if(saldo_aux > 0.0)
                ret =  0;
            if(saldo_aux < 0.0)
                ret = -1;
        }
        catch(SQLException e){
            return -1;
        }
        finally{
            rs.close();
            con.cerrarConexion();   
        }
        return ret;
    }
    
    public void eliminarAbono(String idAbono) throws SQLException
    {
        double pago_interes=0.0, pago_capital=0.0, pago_interes_moratorio = 0.0,monto=0.0;
        String fechaAnt= "",idPago="",codigoPrestamo="";
        String qr = "select monto,idPago,pago_interes,pago_capital,pago_interes_moratorio, fechaPagoAnt from abonos where idabono=" + idAbono;
        conexion con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        // sacando datos
        idPago = rs.getString("idPago");
        pago_interes = rs.getDouble("pago_interes");
        pago_interes_moratorio = rs.getDouble("pago_interes_moratorio");
        pago_capital = rs.getDouble("pago_capital");
        fechaAnt = rs.getString("fechaPagoAnt");
        monto = rs.getDouble("monto");
        
        qr = "update pagos set fechaPago='" + fechaAnt+ "', pago_interes=(pago_interes-" + pago_interes + "),"
                + " pago_capital=(pago_capital- " + pago_capital + "),pago_interes_moratorio=(pago_interes_moratorio-" + pago_interes_moratorio + "),"
                + " saldo=(saldo+" + pago_capital+ "), monto=(monto-" + monto + ") where idPago=" + idPago;
        con.actualizar(qr);
        // obtener codigo prestamo
        qr = "select codigoPrestamo from pagos where idPago=" + idPago;
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        codigoPrestamo = rs.getString("codigoPrestamo");
        qr = "update prestamos set saldo=(saldo+ " + pago_capital + ") where codigoPrestamo='" + codigoPrestamo+ "'";
        con.actualizar(qr);
        EPactualizarPago(idPago);
        EPactualizarPrestamo(codigoPrestamo);
        qr ="delete from abonos where idabono=" + idAbono;
        con.actualizar(qr);
        con.cerrarConexion();
    }

    public void EPactualizarPago(String idPago) throws SQLException  // actualizar estado del pago
    {
        String qr = "select count(a.idabono) as total, p.estado from abonos as a inner join pagos as p on \n" +
                    "p.idPago = a.idPago where a.idPago=" + idPago;
        conexion con = new conexion();
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        int registrosAbonos = rs.getInt("total");
        String estado = rs.getString("estado");
        String query = "";
        if (estado.equals("Pagado") && registrosAbonos == 1)
            query ="update pagos set estado='Pendiente' where idPago=" + idPago;
        else if (registrosAbonos > 1)
            query ="update pagos set estado='Abonado' where idPago=" + idPago;
        else
            query ="update pagos set estado='Pendiente' where idPago=" + idPago;
        con.actualizar(query);
        con.cerrarConexion();
    }
    
    public void EPactualizarPrestamo(String codigoPrestamo) throws SQLException 
    {
        //JOptionPane.showMessageDialog(null, "2");
        double saldoActualPrestamo = 0.0;
        String query = "select saldo from prestamos where codigoPrestamo='" + codigoPrestamo + "'";
        conexion con = new conexion();
        con.setRS(query);
        rs = con.getRs();
        rs.next();
        saldoActualPrestamo = rs.getDouble("saldo");
        if (saldoActualPrestamo > 0.0)
            query = "update prestamos set estado='Activo' where codigoPrestamo='" + codigoPrestamo + "'";
        con.actualizar(query);
        con.cerrarConexion();
   }
    
    public static boolean CumplePorcentajeRefinanciamiento(String codigoPrestamo) throws SQLException
    {
        boolean estado = false;
        double porcentaje;
        int pagosRealizados = 0 ;
        int pagosTotales = 0;
        String qr = "select porcentaje_refinanciamiento from configuraciones";
        conexion con = new conexion();
        con.setRS(qr);
        ResultSet rs = con.getRs();
        rs.next();
        porcentaje = rs.getDouble("porcentaje_refinanciamiento");
       
        // obtener numero de pagos cancelados
        qr = "select count(*) as total, pre.plazo  from pagos as p inner join prestamos as pre\n" +
             "on p.codigoPrestamo = pre.codigoPrestamo where pre.codigoPrestamo='"+codigoPrestamo+"' and p.estado='Pagado'";
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        pagosRealizados = rs.getInt("total");
        pagosTotales = rs.getInt("plazo");
        // comparar!
        double totalPorcentajeGanado = (pagosRealizados*100)/pagosTotales;
        
        if (totalPorcentajeGanado >= porcentaje)
            estado = true;
        rs.close();
        con.cerrarConexion();
        return estado;
    }
    
    public static double total_deuda(String codigoPrestamo, double saldo) throws SQLException{
        conexion con = new conexion();
        String qr = "SELECT SUM(((intereses+iva_interes) - (pago_interes+pago_iva_interes))) as interes, SUM((interes_moratorio+iva_mora) - (pago_interes_moratorio+pago_iva_mora)) AS interes_mora from pagos "
                + " WHERE estado!='Pagado' AND codigoPrestamo ='" + codigoPrestamo + "'";
        con.setRS(qr);
        ResultSet rs = con.getRs();
        
        double intereses = 0.0;
        double mora = 0.0;
        if(rs.next()){
            intereses = rs.getDouble("interes");
            mora = rs.getDouble("interes_mora");
        } 
        rs.close();
        con.cerrarConexion();
        return intereses + mora + saldo;
    }
    public static int cuotas_canceladas(String codigoPrestamo) throws SQLException{
        conexion con = new conexion();
        try {
            con.setRS("SELECT count(*)as total from pagos as p inner join prestamos as pre\n" +
             "on p.codigoPrestamo = pre.codigoPrestamo where pre.codigoPrestamo='"+codigoPrestamo+"' and p.estado='Pagado'");
            ResultSet rs = con.getRs();
            int cuotas = 0;
            if (rs.next())
                cuotas = rs.getInt("total");
            return cuotas;
        } catch (SQLException ex) {
            Logger.getLogger(classPagos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        finally{
            con.cerrarConexion();
        }
    }
    /**
     * @param monto
     * @return 
     */
    public static boolean verificar_monto_refinanciamiento(double monto, double total_deuda){
        if(monto >= total_deuda)
            return true;
        else
            return false;
    }//fin verificar monto
    
    public static void nuevoPago(int tope, String fechaInicial, double nuevoSaldo, String codigoP, String fechaPago) throws SQLException{
        int ultimoPago = 0;
        String fechaE = ""; //Fecha establecida para el pago siguiente
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String[] split = fechaInicial.split("-");
        
        String comprobante = "0";         //Dato que almacenara el comprobante
        Calendar c = Calendar.getInstance();
        Calendar original = Calendar.getInstance();               //Calendario con la fecha original
        
        conexion Con = new conexion();
        c.set(Integer.parseInt(split[0]), Integer.parseInt(split[1])-1, Integer.parseInt(split[2]));
        original.setTime(c.getTime()); //Inicializando el calendario original 
        System.out.println("Calendario inicial: "+ formato.format(original.getTime()));
        
        Con.setRS("SELECT comprobante FROM pagos ORDER BY comprobante DESC LIMIT 1");
        ResultSet Rs = Con.getRs();

        if(Rs.next())
            ultimoPago = Rs.getInt("comprobante");
        /**
         * Llenando la tabla pagos
         */
       
        for(int i = 1; i <= tope; i++)
        {
            ultimoPago++; //Sumando al comprobante
            c.add(Calendar.MONTH, i);   //Agregando al calendario el salto.  
            
            fechaE = formato.format(c.getTime()); 
            comprobante = String.format("%06d", ultimoPago); 
            System.out.println("fecha original: " + formato.format(original.getTime())); 
            System.out.println("Fecha de pago establecida: " + fechaE);
            if (i == 1)
               Con.actualizar("INSERT INTO pagos(codigoPrestamo,comprobante,fechaEstablecida,fechaPago, estado,saldo, Npago) VALUES('" + codigoP + "','"
                            + comprobante + "','" + fechaE +"','" + fechaPago + "','Pendiente', " + nuevoSaldo+  ", " + i + ");");
            else 
            {
                Con.actualizar("INSERT INTO pagos(codigoPrestamo,comprobante,fechaEstablecida,fechaPago, estado, Npago) VALUES('" + codigoP + "','"
                            + comprobante + "','" + fechaE +"','" + fechaPago + "','Pendiente', " + i + ");");
            }
            c.setTime(original.getTime()); //Reiniciando calendario
        }//fin for
        //Actualizando el prestamo 
        ActualizacionPagos p = new ActualizacionPagos(codigoP);
        p.actualizar();
        Con.cerrarConexion();
    }//fin nuevo Pago
    
    public int CapitalPendiente(String no_pago, String codigoPrestamo) throws SQLException
    {
        int estado = 0;
        String qr = "select t.saldoFinal as `ideal`, p.saldo as `real`,p.codigoPrestamo "
                + "from tabla_amortizacion as t inner join pagos as p on p.codigoPrestamo = t.codigoPrestamo "
                + "where (p.Npago=" + no_pago  + " and t.NPago=" + no_pago +") and p.codigoPrestamo='" + codigoPrestamo + "'";
        conexion con = new conexion();
        con.setRS(qr);
        ResultSet rs = con.getRs();
        if  (rs.next()) 
        {
            double ideal = rs.getDouble("ideal");
            double real  = rs.getDouble("real");
            real = real -  mpago_capital; // el saldo real en este punto no se le ha 
            // decrementado el abono que se realizara al prestamo
            double resta = ideal - real;
            //redondeando resta
            resta = Math.rint((resta *100/100));
        
            if (resta < 0.0)
                estado = 1; // existe capital pendiente
        }
        else 
            return -1;
        return estado;
    }
    
    public static void main(String[] args) throws SQLException
    {
        classPagos c = new classPagos();
        String x = c.generarComprobanteAbono("25");
        System.out.print(x);
        
    }
    
}