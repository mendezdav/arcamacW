/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 */
public class beanDetalleCuenta {
    
    private String fecha_impresion;
    private String cliente;
    private String codigoPrestamo;
    private double montoOriginal;
    private double montoActual;
    private String cuotas;
    private String ultimoPago;
    private int mora;
    private String imagen;
    private String proximoPago;


    
    public beanDetalleCuenta()
    {
        fecha_impresion ="";
        cliente = "";
        codigoPrestamo="";
        montoOriginal = 0.0;
        montoActual = 0.0;
        cuotas = "";
        ultimoPago = "";
        mora = 0; 
        imagen ="";
        proximoPago="";
    }

    /**
     * @return the fecha_impresion
     */
    public String getFecha_impresion() {
        return fecha_impresion;
    }

    /**
     * @param fecha_impresion the fecha_impresion to set
     */
    public void setFecha_impresion(String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the montoOriginal
     */
    public double getMontoOriginal() {
        return montoOriginal;
    }

    /**
     * @param montoOriginal the montoOriginal to set
     */
    public void setMontoOriginal(double montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    /**
     * @return the montoActual
     */
    public double getMontoActual() {
        return montoActual;
    }

    /**
     * @param montoActual the montoActual to set
     */
    public void setMontoActual(double montoActual) {
        this.montoActual = montoActual;
    }

    /**
     * @return the cuotas
     */
    public String getCuotas() {
        return cuotas;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setCuotas(String cuotas) {
        this.cuotas = cuotas;
    }

    /**
     * @return the ultimoPago
     */
    public String getUltimoPago() {
        return ultimoPago;
    }

    /**
     * @param ultimoPago the ultimoPago to set
     */
    public void setUltimoPago(String ultimoPago) {
        this.ultimoPago = ultimoPago;
    }

    /**
     * @return the mora
     */
    public int getMora() {
        return mora;
    }

    /**
     * @param mora the mora to set
     */
    public void setMora(int mora) {
        this.mora = mora;
    }

    /**
     * @return the imagen
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * @param imagen the imagen to set
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * @return the proximoPago
     */
    public String getProximoPago() {
        return proximoPago;
    }

    /**
     * @param proximoPago the proximoPago to set
     */
    public void setProximoPago(String proximoPago) {
        this.proximoPago = proximoPago;
    }

   
}
