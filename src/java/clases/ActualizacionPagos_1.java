/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Jorge Luis
 */
public class ActualizacionPagos_1 {
    static  int no_pagos;
    private String codigoPrestamo;
    private ResultSet prestamos;
    private CachedRowSetImpl pagos;
    private CachedRowSetImpl rs;
    private conexion con; 
    private conexion conAux;
    private classPagos classPagos;
    private String qr;
    private double saldo;
    private double tasa_interes;
    private int plazos;
    private String estado;
    private String fechaEstablecida;
    private String fechaActual;
    private diferenciaDias diasDif;
    private double diasRetraso;
    private double cuota;
     
    private int dias_interes_normal;
    private int dias_interes_moratorio;
    private double iva;  //El iva es sobre la sumatoria de interes normal e interes moratorio
    /**Variables auxiliares***/
    private int dias_interes_normal_anterior;
    private int dias_interes_moratorio_anterior;
 
    private double interesApagar;
    private double interesAnterior;
    private double pagoCapitalAnt;
    private double saldoAnt;
    
    private String fechaEstablecidaAnterior;
    private String fechaUltimoPago;
    private String fechaAprobacion;
    private String id;
    private String fechaPago;
    /**Propiedades de pago abonado***/
    private double pago_interes=0.0;
    private double pago_capital=0.0;
    private double pago_interes_moratorio=0.0;
    private double mora =0.0;
    private double capital = 0.0;
    private double interes_moratorio;
    private double interes = 0.0;
    private String fila;
   
    
    public ActualizacionPagos_1(String parametro){ 
        this.codigoPrestamo = parametro;
        prestamos = null;
        pagos = null;
        con = null;
        conAux = null;
      
        qr ="";
        diasDif = new diferenciaDias();
        id="";
        capital = 0.0;
        interes_moratorio = 0.0; 
    }
    
    public void actualizar() throws SQLException
    {
        if (this.codigoPrestamo.equals(""))
        {
            /*
             * No se ha enviado codigo de Prestamos, por lo que
             * la actualizacion se realizara en todos los prestamos activos
             */
            qr = "select * from prestamos where estado='Activo'";
        }
        else
            qr = "SELECT * from prestamos where estado='Activo' and codigoPrestamo = '" + codigoPrestamo + "'";
        recorrido(qr, "");
    }
    
    public void recorrido(String qr, String fechaInicio) throws SQLException{
            /**
             * Instanciando la clase de pagos
             */
            classPagos = new classPagos(); 
            con = new conexion();
           
            con.setRS(qr);
            prestamos = (ResultSet) con.getRs();
            
            while (prestamos.next())
            {
                // sacar datos necesario de los prestamos!
                this.codigoPrestamo = prestamos.getString("codigoPrestamo");
                this.saldo = prestamos.getDouble("saldo");
                this.plazos = prestamos.getInt("plazo");
                this.tasa_interes = prestamos.getDouble("tasa_interes");
                this.cuota = prestamos.getDouble("cuotas");
                this.fechaAprobacion = prestamos.getString("fechaAprobacion");
                this.saldo = prestamos.getDouble("saldo");
                // instanciar e inicializar el beanNuevoPrestamo
                nuevoPrestamo bean = new nuevoPrestamo();
                bean.setCantidad(saldo);
                bean.setPlazo(plazos);
                bean.setInteres(tasa_interes);
                if(fechaInicio.equals(""))
                    fechaActual = bean.getFechaEmision();
                else
                    fechaActual = fechaInicio;
                
                nuevoPrestamo bean_aux = new nuevoPrestamo();
                bean_aux.setPlazo(plazos);
                bean_aux.setInteres(tasa_interes);
                /*
                 * sacar los pagos del prestamo actual que no esten Pagados
                 */
                qr ="where codigoPrestamo='"+ prestamos.getString("codigoPrestamo") + "'";
                pagos = classPagos.getPagos(qr);
                /**
                *COMPROBANDO SI SE TRATA DEL PRIMER PAGO
                **/
                String idPrimerPago = "";  //ID del primer pago
               
                rs = classPagos.getPagos("WHERE codigoPrestamo = '" + codigoPrestamo + "' ORDER BY idPago ASC LIMIT 1");
                if(rs.next()){
                          idPrimerPago = rs.getString("idPago"); 
                          fechaEstablecidaAnterior = fechaAprobacion;
                          fechaUltimoPago = fechaAprobacion;
                       }
                   rs.close();
                   System.out.println("id del primer pago: " + idPrimerPago);
                /*
                 * recorriendo los pagos de los prestamos
                 */
                while(pagos.next())
                {
                   /*sacar dato de pagos*/
                   this.estado = pagos.getString("estado");
                   this.fila = pagos.getString("fila");
                   this.id = pagos.getString("idPago");
                   this.fechaEstablecida = pagos.getString("fechaEstablecida");
                   this.fechaPago = (String)pagos.getDate("fechaPago").toString(); 
                   this.capital = pagos.getDouble("capital"); 
                   this.mora = pagos.getDouble("interes_moratorio"); 
                   this.interes = pagos.getDouble("intereses");
                   this.pago_capital = pagos.getDouble("pago_capital");
                   
                   System.out.println("\tEvaluando pago: " + id + "\n\n");
                   /**Tomando datos para calculo de intereses normales**/
                   
                   if(!id.equals(idPrimerPago)){ /***Si no se trata del primer pago***/
                        String c = "";
                        c = "WHERE codigoPrestamo = '" + codigoPrestamo + "' HAVING fila= (" + fila + "-1)";
                        rs = classPagos.getPagos(c);
                        if(rs.next()){
                              pagoCapitalAnt = rs.getDouble("pago_capital");
                              saldoAnt = rs.getDouble("saldo");
                              fechaEstablecidaAnterior = rs.getString("fechaEstablecida"); 
                              fechaUltimoPago = rs.getString("fechaPago");
                         }
                         rs.close();
                         
                         saldoAnt = saldoAnt + pagoCapitalAnt;
                   }
                   /*Pagos pendientes**/
                    if (estado.equals("Pendiente"))
                    {
                        diasRetraso =  diasDif.obtenerDiasRetraso(fechaActual,fechaEstablecida); //Dias de retraso de mora
                        diasRetraso = (int) diasRetraso;
                  
                        bean.setInteres_moras(cuota,diasRetraso);
                        /**Estableciendo los dias de interes moratorio***/
                        this.dias_interes_moratorio = (int) diasRetraso;
                        
                        interes_moratorio = bean.getInteres_mora();
                        if(fechaUltimoPago.equals("1000-01-01") || id.equals(idPrimerPago)){
                           interesApagar = bean.getIntereses_pago(fechaEstablecidaAnterior, fechaUltimoPago, fechaActual, estado);
                           this.dias_interes_normal = bean.getDias_Interes(fechaEstablecidaAnterior, fechaUltimoPago, fechaActual, estado);
                        }
                        else
                        {
                           bean_aux.setCantidad(saldoAnt);                           
                           interesAnterior = bean_aux.getIntereses_pago(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaUltimoPago, estado);
                           this.dias_interes_normal_anterior = bean_aux.getDias_Interes(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaUltimoPago, estado);
                           
                           interesApagar = bean.getIntereses_pago(fechaUltimoPago, fechaUltimoPago, fechaActual, estado);
                           this.dias_interes_normal = bean.getDias_Interes(fechaUltimoPago, fechaUltimoPago, fechaActual, estado);
                        }
                        Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "ActualizacionPagos: Estado de pago: Pendiente,CodigoPrestamo:{0}", codigoPrestamo);
                        Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "Dias de interes MORATORIO: Pendiente,{0}", this.dias_interes_moratorio);
                        Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "Dias de interes normal: Pendiente, {0}", this.dias_interes_normal);
                    }
                    else if(estado.equals("Abonado")){
                        /**
                         * Calculos de mora
                         * 1. Calculamos la mora desde fechaEstablecida hasta fechaPago
                         * 2. Sumamos a esa mora, la mora desde la fecha de Pago hasta la fechaActual
                         **/
                        double dias_aux = diasDif.obtenerDiasRetraso(fechaPago, fechaEstablecida);
                        if(dias_aux <= 0.0){
                            mora = 0;
                            dias_aux = 0.0;
                        }
                        bean.setInteres_moras(cuota, dias_aux);
                        interes_moratorio = bean.getInteres_mora();
                        /**Estableciendo los dias de interes moratorio anterior**/
                        this.dias_interes_moratorio_anterior = (int) dias_aux;
                        
                        dias_aux = diferenciaDias.retrasoAbono(fechaPago, fechaEstablecida, fechaActual); 
                        
                        if(dias_aux == -0.0001){
                            mora = 0;
                            dias_aux = 0.0;
                        }
                        bean.setInteres_moras(capital- pago_capital, dias_aux); 
                       
                        interes_moratorio += bean.getInteres_mora();
                        this.dias_interes_moratorio = (int)dias_aux;
                        /**Sumando los dias de interes moratorio anterior y actual***/
                        this.dias_interes_moratorio += this.dias_interes_moratorio_anterior;
                        
                        /**
                         * Calculos de interes
                         * 1. Calculamos el interes normal desde la fecha de pago hasta la fecha actual
                         * 2. Sumamos a esa cantidad, los intereses que ya estaban generados hasta esa fecha
                         */
                        saldoAnt = saldo + pago_capital;
                        bean_aux.setCantidad(saldoAnt);
                        interesAnterior = bean_aux.getIntereses_pago(fechaEstablecidaAnterior, fechaEstablecidaAnterior, fechaPago, estado);
           
                        interesApagar = bean.getIntereses_pago(fechaEstablecidaAnterior, fechaPago, fechaActual, "Abonado");
                
                    } //fin if
                    /**
                     * OPERACIONES GENERALES
                     **/
                    if (diasRetraso <= 0)
                            diasRetraso = 0;
                    if(estado.equals("Abonado") || (estado.equals("Pendiente")&& !fechaUltimoPago.equals("1000-01-01"))){
                            interesApagar += interesAnterior;
                    }
                    /**
                     * Estableciendo IVA
                     */
                    this.iva = diferenciaDias.redondear((interes_moratorio + interesApagar)*0.13);
                    
                    interes_moratorio = interes_moratorio + diferenciaDias.redondear(interes_moratorio*0.13);
                    bean.setIva(interesApagar);
                    interesApagar += diferenciaDias.redondear(interesApagar * 0.13);
                      
                    /**
                     * Sumando al nuevo interes, el interes anterior
                     */  
                    /**Verificando si el saldo es mayor que la cuota,
                     * de caso contrario, se deben restar los intereses al saldo
                     */
                    if(saldo > cuota)
                        capital = cuota - interesApagar;
                    else
                        capital = saldo - interesApagar;
                    /**
                     * Validando si el capital es menor a cero
                     */
                    if(capital <= 0.0)
                        capital = 0;
                    /**Aproximando todas las cantidades**/
                    interes_moratorio   = diferenciaDias.redondear(interes_moratorio);
                    interesApagar       = diferenciaDias.redondear(interesApagar);
                    capital             = diferenciaDias.redondear(capital);
        
                   /* verificar si el saldo es menor a la cuota  */
                   if (( this.saldo - capital) < cuota)
                   {
                     double tmp = saldo - capital;
                     if (capital + tmp < cuota)
                     {
                         capital = capital + tmp;
                     }  
                   }//fin if
                   
                   /**Estableciendo la consulta**/
                   qr ="update pagos set diasInteres = " + this.dias_interes_normal 
                                    + ", diasInteresMora = " + this.dias_interes_moratorio
                                    + ",iva = " + this.iva 
                                    + ",intereses="+ interesApagar+",interes_moratorio="+ interes_moratorio+","
                                    + "capital=" + capital + ",mora=";
                   
                   /*****VERIFICAMOS SI EL PAGO YA TENIA ESTADO DE MORA*/
                   int moraant = 0;
                   conexion conmora =  new conexion();
                    conmora.setRS("SELECT mora FROM pagos WHERE idPago = " + this.id);
                    ResultSet rsmora = conmora.getRs();
                    if(rsmora.next())
                          moraant = rsmora.getInt("mora");
                    rsmora.close();
                   conmora.cerrarConexion();
                   
                    if (diasRetraso <= 0){
                        if(moraant == 0)
                             qr+="0";       //Si el pago no estaba en mora antes de la actualizacion 
                        else
                             qr +="1";      //Si el pago ya estaba en mora antes de la actualizacion se deja el estado
                    }
                    else
                         qr+="1";
                    
                    qr+=" where idPago="+pagos.getString("idPago");
                    Logger.getLogger(ActualizacionPagos.class.getName()).log(Level.INFO, "Query:, {0}", qr);
                    /*
                    * realizar actualizacion
                    */
                    conAux = new conexion();
                    if(!estado.equals("Pagado")){
                       conAux.actualizar(qr);
                    }
                    conAux.cerrarConexion();
                } // fin recurrido de los pagos 
            }// fin while, fin recorrido de los prestamos activos
            con.cerrarConexion();
    }
    /***Metodo para calcular interes normal para un dia especifico***/
    private double getInteres_sd(String codigoPrestamo, String fechaPago){
        double interes = 0.0;
        return interes;
    }
    /**Metodo para calcular interes moratorio para un dia especifico***/
    /**Metodo para calcular abono a capital para un dia especifico***/
    /**Metodo para calcular cargo por IVA para un dia especifico**/
    private double getIva_sd(String codigoPrestamo, String fechaPago){
        double iva = 0.0;
        return iva;
    } //fin getIva_setdate
} //fin de clase