package clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author Pochii
 */
@WebServlet(urlPatterns = {"/ActualizarPagoAnteriores"})
public class ActualizarPagoAnteriores extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            // tomando datos 
            int idPago = Integer.valueOf(request.getParameter("idPago"));
            String fechaPagoAnt = request.getParameter("fechaPagoAnterior");
            String codigoPrestamo = "";
            String query = "";
            conexion con = new conexion();
            con.setRS("select codigoPrestamo from pagos where idPago="+ idPago + " limit 1");
            ResultSet rs = con.getRs();
            if (rs.next())
                codigoPrestamo = rs.getString("codigoPrestamo");
            else 
                response.sendRedirect("/arcamacW/Pagos/PrincipalPago.jsp?erno=2");
            
           query += "SELECT * from prestamos where estado='Activo' and codigoPrestamo ='"+ codigoPrestamo + "'" ;
           ActualizacionPagos  ap = new ActualizacionPagos(codigoPrestamo);
           try{
                ap.recorrido(query, fechaPagoAnt,1);
                response.sendRedirect("/arcamacW/Pagos/realizarPago.jsp?idPago=" + idPago+"&fechaA=" + fechaPagoAnt);
           } 
           catch(Exception c){
             
           }
            
                 
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ActualizarPagoAnteriores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ActualizarPagoAnteriores.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
