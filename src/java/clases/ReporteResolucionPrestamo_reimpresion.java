/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "ReporteResolucionPrestamo_reimpresion", urlPatterns = {"/ReporteResolucionPrestamo_reimpresion"})
public class ReporteResolucionPrestamo_reimpresion extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JRException, NamingException, SQLException {
        ServletOutputStream out = response.getOutputStream();
        try {
                HttpSession SessionActual = request.getSession(false);
                String codigoPrestamo = SessionActual.getAttribute("codigoReporte").toString();
                JasperReport reporte = (JasperReport)
                JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/prueba_resolucion.jasper"));
                //Cargamos parametros del reporte (si tiene).
                Map parameters = new HashMap();
                // obteniendo ruta para imagen temporal
                String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg"); 
                List<Object> reports = new LinkedList<Object>();
                // sacando datos de los cumpleañeros
                // creando instancia conexion
        conexion con = new conexion();
        String query = "select count(*) as fiadores from resolucionPrestamo where codigoPrestamo='" + codigoPrestamo + "'";
        con.setRS(query);
        ResultSet resultado =  con.getRs();
        int fiadores = 0;
        if (resultado.next())
            fiadores = resultado.getInt("fiadores");
        
        // llenando datos generales del bean    
        query = "select * from resolucionPrestamo where codigoPrestamo='" + codigoPrestamo + "'";
        con.setRS(query);
        resultado = con.getRs();
        BeanResolucionPrestamo beanPrestamo = new BeanResolucionPrestamo();
            if (resultado.next()){
                    beanPrestamo.setIdSolicitante(resultado.getString("idSolicitante"));
                    beanPrestamo.setNombres(resultado.getString("nombres"));
                    beanPrestamo.setApellidos(resultado.getString("apellidos"));
                    beanPrestamo.setSueldo(resultado.getDouble("sueldo"));
                    beanPrestamo.setDescuentos(resultado.getDouble("descuentos"));
                    beanPrestamo.setLiquidez(resultado.getDouble("liquidez"));
                    beanPrestamo.setCantidad(resultado.getDouble("cantidad"));
                    beanPrestamo.setCodigoPrestamo(resultado.getString("codigoPrestamo"));
                    String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                    beanPrestamo.setImg_url(rutaImg);
                    beanPrestamo.setCorrelativo("0");
                    
                    // llenando datos fiador 1
                    beanPrestamo.setNombreFiador1(resultado.getString("nombreFiador"));
                    beanPrestamo.setApellidoFiador1(resultado.getString("apellidoFiador"));
                    beanPrestamo.setSalarioF1(resultado.getDouble("salarioF"));
                    beanPrestamo.setDeduccionesF1(resultado.getDouble("deducciones"));
                    beanPrestamo.setLiquidezF1(resultado.getDouble("liquidezF"));
             
                    if (fiadores == 2) // es solo un fiador?
                    {
                        resultado.next();
                        // llenando segundo fiador
                        beanPrestamo.setNombreFiador2(resultado.getString("nombreFiador"));
                        beanPrestamo.setApellidoFiador2(resultado.getString("apellidoFiador"));
                        beanPrestamo.setSalarioF2(resultado.getDouble("salarioF"));
                        beanPrestamo.setDeduccionesF2(resultado.getDouble("deducciones"));
                        beanPrestamo.setLiquidezF2(resultado.getDouble("liquidezF"));

                    }
                }
            
                // agregando el bean al object list
                reports.add((Object) beanPrestamo);
                
                //-----
                JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
                JRExporter exporter = null;
                response.setContentType("application/pdf");
                //Nombre del reporte
                response.setHeader("Content-Disposition","inline; filename=\"ReporteResolucionPrestamo.pdf\";");
                //Exportar a pdf
                exporter = new JRPdfExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
                //view the report using JasperViewer
                exporter.exportReport();

               // redireccion al panel principal
               response.sendRedirect("/arcamacW/panelAdmin.jsp");

        } 
        finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (NamingException ex) {
                Logger.getLogger(ReporteResolucionPrestamo_reimpresion.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ReporteResolucionPrestamo_reimpresion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (JRException ex) {
            Logger.getLogger(ReporteResolucionPrestamo_reimpresion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            try {
                processRequest(request, response);
            } catch (NamingException ex) {
                Logger.getLogger(ReporteResolucionPrestamo_reimpresion.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(ReporteResolucionPrestamo_reimpresion.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (JRException ex) {
            Logger.getLogger(ReporteResolucionPrestamo_reimpresion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
