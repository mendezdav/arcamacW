/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge Luis
 */
public class generarTabla {
    
    private String qr;
    private String codigoPrestamo;
    private ResultSet prestamo;
    private ResultSet pagos;
    private ResultSet rs;
    private conexion con;
    private conexion con2;
    private conexion conAux;
    private diferenciaDias diasDif;
    private String fechaAnterior;
    private String fechaEstablecida;
    private String fechaAprobacion;
    private String idPrimerPago;
    private double dias;
    private double saldo;
    private double saldoAnterior;
    private double saldoAnteriorP;
    private double tasa_interes;
    private int plazos;
    private double interes;
    private double cuota;
    private double cantidad;
    private double ideal;
    private double iva;
    private double interes_diario;
    public generarTabla(String codigoPrestamo) throws SQLException
    {
        // sacando fechaAprobacion
        this.codigoPrestamo = codigoPrestamo;
        qr ="select fechaAprobacion,saldo,plazo,tasa_interes,cuotas,cantidad from prestamos where codigoPrestamo='"+codigoPrestamo+"'";
        con = new conexion();
        con.setRS(qr);
        prestamo = con.getRs();
        if(prestamo.next())
        {
            this.fechaAprobacion = prestamo.getString("fechaAprobacion");
            saldo = prestamo.getDouble("saldo");
            tasa_interes = prestamo.getDouble("tasa_interes");
            plazos = prestamo.getInt("plazo");
            cuota = prestamo.getDouble("cuotas");
            cantidad = prestamo.getDouble("cantidad");
            // obteniendo saldo Anterior
            saldoAnteriorP = prestamo.getDouble("saldo");
        }
     
       // primer pago 
        qr = "select * from pagos where codigoPrestamo='"+ codigoPrestamo +"' ORDER BY idPago ASC limit 1";
       
        con.setRS(qr);
        pagos = con.getRs();
        if(pagos.next())
        {
           idPrimerPago = pagos.getString("idPago");
           fechaAnterior =  this.fechaAprobacion;
           
        }  
        // demas pagos
        qr ="select * from pagos where codigoPrestamo='"+codigoPrestamo+"'";
        con.setRS(qr);
        pagos = con.getRs();
        int contador = 0;
        diasDif = new diferenciaDias();
         
        while(pagos.next())
        {
            // si es primer pago
            if (contador==0)
            {
                dias = diasDif.obtenerDiasRetraso(pagos.getString("fechaEstablecida"),fechaAnterior);
                fechaAnterior = pagos.getString("fechaEstablecida");
                this.saldoAnterior = saldoAnteriorP;
            }
            else 
            {
                dias = diasDif.obtenerDiasRetraso(pagos.getString("fechaEstablecida"),fechaAnterior);
                fechaAnterior = pagos.getString("fechaEstablecida");
                
            }
                // guardando en la base
                conAux = new conexion();
                cantidad       = saldoAnterior;
                interes_diario = diferenciaDias.redondear((cantidad * (tasa_interes/100))/30);
                interes        = diferenciaDias.redondear(interes_diario * dias);
                iva            =  diferenciaDias.redondear(interes * 0.13);
                saldo          = cuota-interes-iva;
                ideal          = cantidad - saldo;
             
                if (ideal < 0.0){
                    ideal = 0;}
               
                    qr ="update pagos set intereses=" + interes + ", saldo="+ this.cantidad  + ", saldoIdeal=" + 
                            ideal  +", capital = " + saldo+" where codigoPrestamo='"+codigoPrestamo+"' and idPago=" + pagos.getString("idPago"); 
                conAux.actualizar(qr);
                contador++;
                this.saldoAnterior = saldoAnterior - saldo;
        }
        
    }
    
}
