/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import clases.beanFiadorMutuo;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.swing.JOptionPane;
/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "GenerarMutuo", urlPatterns = {"/GenerarMutuo"})
public class GenerarMutuo extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        ServletOutputStream out = response.getOutputStream();
        try {
        
        // obteniendos datos de la session actual (codigo del prestamo)
        HttpSession SessionActual = request.getSession();
        String codigoPrestamo = "";
        try 
        {
           codigoPrestamo = SessionActual.getAttribute("codigoReporte").toString();
        }
        catch (Exception e1)
        {
            System.out.printf("Error: ", e1.getMessage().toString());
        }
        NumberToLetterConverter n = new NumberToLetterConverter();
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/ReporteMutuo.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        // ESTABLECEMOS CONEXION CON BD
        conexion con = new conexion();
        // verificar el numero de fiadores del prestamo
        String qr = "select concat(f.nombreFiador, ' ', f.apellidoFiador) as nombre,  f.profesionF,f.edadF, m.municipio,d.departamento,"
                + " f.NITF, f.DUIF from prestamo_fiador  inner join  fiadores as f on id_fiador=f.idFiadores inner join  municipio as"
                + " m on m.id_municipio = f.id_municipio inner join departamento  as d on d.id_departamento = f.id_departamento "
                + " where prestamo_fiador.id_prestamo='" + codigoPrestamo+"'";
        con.setRS(qr);
        ResultSet rs = con.getRs();
        // creando lista para almacenar los bean para los fiadores
        List<beanFiadorMutuo> listaFiadores = new ArrayList<beanFiadorMutuo>();
        int contadorFiadores = 0;
        while (rs.next())
        {
            contadorFiadores++;
            // instancia del bean
            beanFiadorMutuo bean = new beanFiadorMutuo();
            bean.setDepartamento(rs.getString("departamento"));
            bean.setMunicipo(rs.getString("municipio"));
            bean.setNit(rs.getString("NITF"));
            bean.setNombre(rs.getString("nombre"));
            bean.setProfesion(rs.getString("profesionF"));
            bean.setEdad(rs.getInt("edadF"));
            bean.setDui(rs.getString("DUIF"));
            // agregando a lista
            listaFiadores.add(bean);
        }
        
        // obteniendo fechaEstablecida primer pago
        qr  = "select fechaEstablecida from pagos where codigoPrestamo='"+ codigoPrestamo +"' limit 1";
        con.setRS(qr);
        rs = con.getRs();
        String fechaEstablecidaPrimerPago = "";
        if (rs.next())
            fechaEstablecidaPrimerPago = rs.getString("fechaEstablecida");
        
        String fechaPrimerPago = n.fechaAletra(fechaEstablecidaPrimerPago);
        String primerPago[] = fechaPrimerPago.split(" ");
        String diaPago = n.convertNumeroEnteroLetras(Double.parseDouble(primerPago[0]));
        fechaPrimerPago = diaPago + " " + primerPago[1] + " " + primerPago[2];
        String AnioPrimerPago = primerPago[4] + " " + primerPago[5] + " " + primerPago[6]; 
        
        qr = "select concat(s.nombre1, ' ', s.nombre2, ' ', s.apellido1, ' ', s.apellido2) as nombre,"
                + "s.edad,s.profesion,p.cantidad,p.tasa_interes,s.NIT,s.DUI,p.plazo, p.fechaEmision,p.fechaAprobacion "
                + ",p.cuotas,m.municipio,d.departamento,p.fechaAprobacion from prestamos as p inner join solicitante"
                + " as s  on p.idSolicitante = s.idSolicitante inner join municipio as m "
                + "on s.id_municipio = m.id_municipio inner join departamento as d "
                + "on s.id_departamento = d.id_departamento where codigoPrestamo='"+ codigoPrestamo+"';";
        con.setRS(qr);
        rs = con.getRs();
        if (rs.next())
        {
        // datos pre-procesador
        String fechaEmision = n.fechaAletra(rs.getString("fechaAprobacion"));
        String eliminarMinutos[] = fechaEmision.split(" ");
        String fechaEmisionSinMinutos = eliminarMinutos[0] + " " +  eliminarMinutos[2]+ " " + eliminarMinutos[3] + " " 
                + eliminarMinutos[4] + " " + eliminarMinutos[5] + " " + eliminarMinutos[6] + " " +  eliminarMinutos[7];
        String fechaEmisionPartes[] = fechaEmision.split(" ");
        String fechaEmisionDias = fechaEmisionPartes[0] + " " + fechaEmisionPartes[2] + " " + fechaEmisionPartes[3];
        String fechaEmisionAnio = fechaEmisionPartes[4] + " " + fechaEmisionPartes[5] + " " + fechaEmisionPartes[6];
        // HORA DE APROBACION
        String horaAprobacionPartes[] = fechaEmisionPartes[1].split(":");
        String HoraAprobacion = n.convertNumeroEnteroLetras( Double.parseDouble(horaAprobacionPartes[0])).toUpperCase() 
                + " HORAS";
        if (!horaAprobacionPartes[2].equals("00"))
        {
            HoraAprobacion += " CON " + n.convertNumeroEnteroLetras( Double.parseDouble(horaAprobacionPartes[1])).toUpperCase() 
                + " MINUTOS";
        }
        
        // INICIO CONSTRUCCION DEL STRING DEL DOCUMENTO
        String cadena = "<p>Yo, <b><u>" +  rs.getString("nombre").toUpperCase() + "</u>,</b> mayor de edad, <b><u>" + rs.getString("profesion").toUpperCase()+ "</u>,</b> del domicilio  de"
                + " <b><u>"+ rs.getString("municipio").toUpperCase() +"</u></b>, jurisdicción del departamento de <b>"
                + "<u>" + rs.getString("departamento").toUpperCase() + "</u></b><b>, </b>que me  denominare el deudor,"
                + " OTORGO: <b><u>I)-  Monto.</u></b> Que en esta fecha, he recibido de la SOCIEDAD AYUDA Y RESERVA  COLECTIVA PARA AEROTECNICOS DEL ALA MAC, "
                + "SOCIEDAD COOPERATIVA DE  RESPONSABILIDAD LIMITADA DE CAPITAL VARIABLE, del domicilio de Ilopango, la cual  en adelante se denominará &ldquo;"
                + "LA   ACREDORA&rdquo;, la suma de <b><u>"+ n.convertNumberToLetter(rs.getDouble("cantidad")) +"</u></b>, a título de MUTUO <b><u>II)INTERESES.</u></b>"
                + " Que reconoceré sobre la suma mutuada, el  interés del <b><u>"+ n.convertInteresAletra(rs.getDouble("tasa_interes")) +"</u></b> por ciento mensual sobre saldos;"
                + " <b><u>III)-  PLAZO, FORMA Y LUGAR DE PAGO.</u></b> Que pagaré la suma mutuada en un plazo de <b><u>"
                + n.convertNumeroEnteroLetras(rs.getDouble("plazo")) + "</u></b> meses contados "
                + " a partir del día <b><u>" + fechaEmisionDias + "</u></b> del año <b><u>"+ fechaEmisionAnio+"</u></b> por medio de <b><u>"
                + n.convertNumeroEnteroLetras(rs.getDouble("plazo")) +"</u></b> cuotas mensuales, fijas y sucesivas de <b><u>"
                + n.convertNumberToLetter(rs.getDouble("cuotas")) + "</u></b> cada una que "
                + " comprenden abono a capital, intereses e IVA, siendo exigible la primera cuota  el día <b><u>"
                + fechaPrimerPago.trim() + "</u></b> del  año <b><u>" + AnioPrimerPago  + "</u></b>, y así  sucesivamente las restantes "
                + "cuotas el día <b><u> " + diaPago.trim() + "</u></b> de cada mes del plazo estipulado hasta la total cancelación de la deuda,  obligándome a hacer todo "
                + "pago en las oficinas de la acreedora."
                + "<b><u>IV)- MORA</u></b><u>.</u> Que la mora en  el pago de una de las cuotas de abono a capital e intereses en la forma  estipulada,"
                + " hará caducar el plazo y será exigible de  inmediato la totalidad de  la presente obligación, como si fuere de plazo vencido, y el "
                + "interés de la suma  mutuada se elevará en <b><u>" + n.convertInteresAletra(rs.getDouble("tasa_interes")) + "</u></b> por ciento de "
                + "interés mensual, adicional al interés pactado, desde la fecha de  la mora sin que ello signifique prórroga del plazo estipulado <b><u>"
                + "V)-GARANTÍA PERSONAL SOLIDARIA</u>.</b>";

                if (contadorFiadores == 1)
                {
                    cadena +=  "Presente  el señor <b><u>" + listaFiadores.get(0).getNombre().toUpperCase() + "</u>,</b> mayor de edad, <b><u>"
                        +listaFiadores.get(0).getProfesion().toUpperCase() + "</u></b>, del domicilio de <b><u>" + listaFiadores.get(0).getMunicipo().toUpperCase()+
                        "</u></b> ,jurisdicción del departamento de <b><u>" + listaFiadores.get(0).getDepartamento().toUpperCase()+"</u></b><b>,</b>"
                        + "  y enterado de los conceptos vertidos en el presente documento,"
                        + " se constituyen fiador y codeudor solidario con  renuncia al beneficio de excusión de bienes, para responder de las obligaciones"
                        + " que el deudor contrae a favor de la acreedora. <b><u>";
                }
                
                if (contadorFiadores == 2)
                {
                    cadena +=  "Presentes  los señores <b><u>" + listaFiadores.get(0).getNombre().toUpperCase() + "</u>,</b> mayor de edad, <b><u>"
                        +listaFiadores.get(0).getProfesion().toUpperCase() + "</u></b>, del domicilio de <b><u>" + listaFiadores.get(0).getMunicipo().toUpperCase()+
                        "</u></b> ,jurisdicción del departamento de <b><u>" + listaFiadores.get(0).getDepartamento().toUpperCase()+"</u></b><b>,</b>"
                        + " Y <b><u>" + listaFiadores.get(1).getNombre().toUpperCase() + "</u>,</b> mayor de edad, <b><u>"+ listaFiadores.get(1).getProfesion().toUpperCase()
                        +"</u></b>, del domicilio de <b><u>" + listaFiadores.get(1).getMunicipo().toUpperCase() + "</u></b>,jurisdicción del  departamento "
                        + "de <b><u>" + listaFiadores.get(1).getDepartamento().toUpperCase() + "</u></b><b>,</b> y enterados"
                        + " de los conceptos vertidos en el presente documento,"
                        + " se constituyen fiadores y codeudores solidarios con  renuncia al beneficio de excusión de bienes, para responder de las obligaciones"
                        + " que el deudor contrae a favor de la acreedora. <b><u>";
                }
                if (contadorFiadores == 1)
                {
                   cadena +="VI).-DOMICILIO ESPECIAL.</u></b> Que el deudor y el fiador y  codeudor solidario señalan ésta ciudad como domicilio especial "
                        + "para los  efectos legales de la presente obligación;";
                }
                
                if (contadorFiadores == 2)
                {
                   cadena +="VI).-DOMICILIO ESPECIAL.</u></b> Que el deudor y los fiadores y  codeudores solidarios señalamos ésta ciudad como domicilio especial "
                        + "para los  efectos legales de la presente obligación;";
                }
                
                cadena += " renunciamos al derecho de apelar del  decreto de embargo, sentencia  de remate y "
                + "demás providencias del juicio ejecutivo e incidentes; y no exigiremos cuentas  ni fianza al depositario que se designare; y los gastos"
                + " que ocasionare el cobro  de la deuda serán por nuestra cuenta, inclusive los llamados personales aún  cuando conforme a las reglas "
                + "generales no fuéremos condenados en costas. Así  nos expresamos, leemos lo escrito, lo ratificamos y firmamos, en la ciudad de Ilopango,"
                + " el día <b><u>" + fechaEmisionSinMinutos.trim() + ".</u></b></p><p>&nbsp;</p>"
                + "<p>En  la ciudad de Ilopango, a las <b><u>" +HoraAprobacion+ "</u>, </b>"
                + "del día <b><u>"+ fechaEmisionDias+"</u></b> del  año <b><u>" +fechaEmisionAnio+ "</u></b>."
                + " Ante mí  VICTOR RENE GUZMAN, Notario, de este domicilio y de la ciudad de SAN SALVADOR, COMPARECEN:  señor <b><u>"
                + rs.getString("nombre").toUpperCase()  + "</u></b>,  de <b><u>"+ n.convertNumeroEnteroLetras(rs.getDouble("edad")).trim() +"</u>"
                + "</b> años, <b><u>"+ rs.getString("profesion").toUpperCase() +"</u>,</b> del domicilio de <b><u>"
                + rs.getString("municipio").toUpperCase()  + "</u></b>, jurisdicción del departamento de <b><u>" + rs.getString("departamento").toUpperCase() + 
                "</u></b><b>,</b> con  Tarjeta de Identificación Tributaria <b><u>"
                + n.NIT_DUI(rs.getString("nit")).trim() +"</u></b> y ";
                
                if (contadorFiadores == 1)
                {
                cadena +="el señor <b><u>"
                    + listaFiadores.get(0).getNombre().toUpperCase() + "</u></b>,  de"
                    + " <b><u>"+  n.convertNumeroEnteroLetras( listaFiadores.get(0).getEdad()).toUpperCase().trim()
                    +"</u></b> años, <b><u>" + listaFiadores.get(0).getProfesion().toUpperCase() + "</u></b>, del  domicilio de <b><u>"
                    + listaFiadores.get(0).getMunicipo().toUpperCase() +"</u></b>, jurisdicción del departamento de"
                    + " <b><u>"+ listaFiadores.get(0).getDepartamento().toUpperCase() +"</u>,</b> con Tarjeta de  Identificación Tributaria <b><u>"
                    +  n.NIT_DUI(listaFiadores.get(0).getNit()) +"</u></b>, a  "
                    + "quienes no conozco pero identifico con su Documento Único de Identidad número: "
                    + "<b><u>"+n.NIT_DUI(rs.getString("DUI")).trim() +"</u></b>, "
                    + n.NIT_DUI(listaFiadores.get(0).getDui()).toUpperCase().trim() +
                    "</u></b>Y <b><u>" + n.NIT_DUI(listaFiadores.get(0).getDui()).toUpperCase().trim()
                    + "</u></b> respectivamente y DICEN: ";
                }
                if (contadorFiadores == 2)
                {
                cadena +="los señores <b><u>"
                    + listaFiadores.get(0).getNombre().toUpperCase() + "</u></b>,  de"
                    + " <b><u>"+  n.convertNumeroEnteroLetras( listaFiadores.get(0).getEdad()).toUpperCase().trim()
                    +"</u></b> años, <b><u>" + listaFiadores.get(0).getProfesion().toUpperCase() + "</u></b>, del  domicilio de <b><u>"
                    + listaFiadores.get(0).getMunicipo().toUpperCase() +"</u></b>, jurisdicción del departamento de"
                    + " <b><u>"+ listaFiadores.get(0).getDepartamento().toUpperCase() +"</u>,</b> con Tarjeta de  Identificación Tributaria <b><u>"
                    +  n.NIT_DUI(listaFiadores.get(0).getNit()) +"</u></b>"
                    + " Y <b><u>"+ listaFiadores.get(1).getNombre().toUpperCase()+"</u></b>, de <b><u>"
                    +  n.convertNumeroEnteroLetras( listaFiadores.get(1).getEdad()).toUpperCase()+"</u></b> años, <b><u>"
                    +  listaFiadores.get(1).getProfesion().toUpperCase() +"</u></b>, del domicilio de <b><u>"+ listaFiadores.get(1).getMunicipo().toUpperCase()
                    +"</u></b>, jurisdicción del departamento de <b><u>"+ listaFiadores.get(1).getDepartamento().toUpperCase() +"</u></b><b>,"
                    + "</b> con Tarjeta  de Identificación Tributaria <b><u>"+ n.NIT_DUI(listaFiadores.get(1).getNit()).trim() +"</u></b>, a  "
                    + "quienes no conozco pero identifico con su Documento Único de Identidad número: "
                    + "<b><u>"+n.NIT_DUI(rs.getString("DUI")).trim() +"</u></b>, "
                    + n.NIT_DUI(listaFiadores.get(0).getDui()).toUpperCase().trim() +
                    "</u></b>Y <b><u>" + n.NIT_DUI(listaFiadores.get(0).getDui()).toUpperCase().trim()
                    + "</u></b> respectivamente y DICEN: ";
                }
                
                cadena += "Que  son suyas las firmas, conceptos y  obligaciones que aparecen en el documento que  antecede, fechando en esta "
                       + "ciudad este mismo día, y el cual declara el primer  compareciente: Que en esta fecha, ha recibido de la sociedad "
                       + "AYUDA Y RESERVA  COLECTIVA PARA AEROTECNICOS DEL ALA MAC, SOCIEDAD COOPERATIVA DE  RESPONSABILIDAD"
                       + " LIMITADA DE CAPITAL VARIABLE, LA    SUMA DE <b><u>"+ n.convertNumberToLetter(rs.getDouble("cantidad")) +"</u></b>"
                       + " a título de mutuo, al interés del <b><u>" + n.convertInteresAletra(rs.getDouble("tasa_interes")).trim() + "</u></b>"
                       + " por ciento mensual sobre saldos, para el plazo  de <b><u>" + n.convertNumeroEnteroLetras(rs.getDouble("plazo")).trim() + ""
                       + "</u></b> meses,  pagaderos por medio de <b><u>"+n.convertNumeroEnteroLetras(rs.getDouble("plazo")).trim()+"</u></b> cuotas mensuales"
                       + " de <b><u>" + n.convertNumberToLetter(rs.getDouble("cuotas")) + "</u></b>, cada una a partir  del día <b><u>"
                       + fechaPrimerPago  +"</u></b> del  año <b><u>" + AnioPrimerPago + "</u>,</b> y bajo  las demás condiciones que en el indicado documento se consignan;";
                
                if (contadorFiadores==1)
                {
                cadena += " y el segundo compareciente: Que se constituye fiador y codeudor solidario para  responder de las obligaciones ya indicadas, "
                    + "renunciando al beneficio de  excusión de bienes. DOY FE: De ser auténticas la expresadas firmas, por haber  sido puestas a mi presencia por los"
                    + " comparecientes al pié del referido  documento, así como de que reconocieron como suyos todos los conceptos vertidos  en el mismo; que les "
                    + "expliqué los efectos legales de esta acta notarial que  consta en dos hojas; y leído que les hube todo lo escrito en un solo acto,  ratificaron "
                    + "su contenido y firmamos, DOY FE.</p> ";
                }
                
                if (contadorFiadores==2)
                {
                cadena += " y el segundo y  tercer comparecientes: Que se constituyen fiadores y codeudores solidarios para  responder de las obligaciones ya indicadas, "
                    + "renunciando al beneficio de  excusión de bienes. DOY FE: De ser auténticas la expresadas firmas, por haber  sido puestas a mi presencia por los"
                    + " comparecientes al pié del referido  documento, así como de que reconocieron como suyos todos los conceptos vertidos  en el mismo; que les "
                    + "expliqué los efectos legales de esta acta notarial que  consta en dos hojas; y leído que les hube todo lo escrito en un solo acto,  ratificaron "
                    + "su contenido y firmamos, DOY FE.</p> ";
                }
        /// FIN CONSTRUCCION DEL STRING DEL DOCUEMENTO
        parameters.put("parametro", cadena);
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JREmptyDataSource());
        JRExporter exporter = null;
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"Mutuo_"+ codigoPrestamo  +".pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        //view the report using JasperViewer
        exporter.exportReport();
        
       // redireccion al panel principal
       response.sendRedirect("/arcamacW/panelAdmin.jsp");
        } // fin if
        else 
        {
            // marcar error
        }
        } catch (JRException e) {
            e.printStackTrace();
        }
      }
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GenerarMutuo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GenerarMutuo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
