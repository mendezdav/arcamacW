/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 */
import Conexion.conexion;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class diferenciaDias {
    private java.util.Calendar fechaPago;
    private java.util.Calendar fechaEstablecida;
    private double dias;
    private int idPago;
    
    public diferenciaDias()
    {
        dias = 0;
        idPago = 0 ;
        fechaPago = Calendar.getInstance();
        fechaEstablecida = Calendar.getInstance();
    }
 
    public double obtenerDiasRetraso(String fechaEstablecidaS,String fechaPagos) throws SQLException
    {
         conexion con = new conexion();
         con.setRS("SELECT DATEDIFF('" + fechaEstablecidaS + "', '" + fechaPagos + "') as dias");
         ResultSet rs = con.getRs();
         rs.next();
            dias = rs.getDouble("dias");
         rs.close();
         con.cerrarConexion();
         return dias;
    }
    
    public static double retrasoAbono(String fechaEstablecida, String fechaUltimoPago, String fechaActual) throws SQLException{
         diferenciaDias diasF = new diferenciaDias();
         double dias_aux=0.0;
             //convertir string a date
         Date FE =null; // fecha establecida (fecha ultimo pago)
         Date FEO = null; // fecha establecida original 
         SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
         try 
             {
            FE = formato.parse(fechaEstablecida);
            FEO = formato.parse(fechaUltimoPago);
         }
         catch(Exception e)
         {
            System.out.print("Hubo un error de conversion: " + e.getMessage());
         }

        //Comparando si la fecha del ultimo pago es mayor a la fecha establecida
         if(FE.compareTo(FEO) >= 0){

            dias_aux = diasF.obtenerDiasRetraso( fechaActual,fechaEstablecida);
            if (dias_aux < 0)
                dias_aux = 0;
            }
        else
        {
            if(diasF.obtenerDiasRetraso(fechaActual, fechaUltimoPago) >= 0){ 
                    dias_aux = diasF.obtenerDiasRetraso(fechaActual, fechaUltimoPago); 
                    if (dias_aux < 0)
                        dias_aux = 0;
            }
            else
               return -0.0001; //Retorna -1 indicando que la mora es cero
        }
         return dias_aux;
    }
    public static double redondear(double cantidad){
        return Math.rint(cantidad*100)/100;
    }
    
    
    public static double redondearBig(double valor)
    {
        String val = valor+"";
        BigDecimal big = new BigDecimal(val);
        big = big.setScale(2, RoundingMode.HALF_UP);
        return big.doubleValue();
    }
    
    public static String fechaActual()
    {
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(fecha);
    }
    
    public static String fechaActual(String formato)
    {
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat(formato);
        return format.format(fecha);
    }
    
    public static String fechaActualFH(){
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMMM 'del' yyyy 'a las' hh:mm a");
        return format.format(fecha);
    }
     public static String FechaDMA(String fecha){
        String[] cad = fecha.split("-");
        String  nuevaFecha = cad[2] + "-" + cad[1] + "-" + cad[0];
        return nuevaFecha;
    }
    /**
      * 
      * @param fecha fecha de la cual se desea obtener sus partes
      * @param index Si es 0 se obtiene el anio. 1 El mes. 2 El dia
      * @return int
    */
    public static int parte(String fecha, int index){
        try {
            int parte = 0;
            Date dfecha = null;
            String fecha_split[];
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            dfecha = format.parse(fecha); //formateando fecha de destino
            fecha = format.format(dfecha);
            fecha_split = fecha.split("-");
            
            parte = Integer.parseInt(fecha_split[index]);
            return parte;
        } //fin mes
        catch (ParseException ex) {
            Logger.getLogger(diferenciaDias.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }//fin parte
}
