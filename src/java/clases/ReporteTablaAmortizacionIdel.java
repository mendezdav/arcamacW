/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author jorge
 */
@WebServlet(name = "ReporteTablaAmortizacionIdel", urlPatterns = {"/ReporteTablaAmortizacionIdel"})
public class ReporteTablaAmortizacionIdel extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException, JRException {
            ServletOutputStream out = response.getOutputStream();
        try {
        // declarando variables
        generarTablaAmortizacion tabla = null;
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/tablaAmortizacionIdeal.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg"); 
        List<beanTablarAmortizacionIdeal> datos = new ArrayList<beanTablarAmortizacionIdeal>();
        //List<Object> reports = new LinkedList<Object>();
        // sacando datos de la clase
        // de donde viene el flujo del programa
        String solicitud = "";
        String codigo = "";
        HttpSession SessionActual = request.getSession();
        if ( request.getParameter("tabla") != null && request.getParameter("tabla") != "" )
            solicitud = request.getParameter("tabla");
        // desde analisis de nuevos prestamos
        if (solicitud.equals("a"))
        {
            // tomar demas parametros
            nuevoPrestamo nuevoPrestamo =  (nuevoPrestamo) SessionActual.getAttribute("beanAnalisis");
            String fechaAprobacion      =  (String) SessionActual.getAttribute("fecha");
            tabla = new generarTablaAmortizacion(fechaAprobacion,nuevoPrestamo.getCantidad(),
                    nuevoPrestamo.getInteres(),nuevoPrestamo.getPlazo() );
            datos = tabla.cargarDatos(rutaImg);
            codigo= "analisis";
        }
        else if (solicitud.equals("s") )  // desde panel reimpresion por solicitante
        {
            // tomar demas parametros
            String codigoPrestamo = "";
            if (SessionActual.getAttribute("codigoReporte") != null &&  SessionActual.getAttribute("codigoReporte") != "")
                codigoPrestamo = SessionActual.getAttribute("codigoReporte").toString();
            tabla = new generarTablaAmortizacion(codigoPrestamo);
            datos = tabla.cargarDatos(rutaImg);
            codigo= codigoPrestamo;
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(datos));
        JRExporter exporter = null;
        response.setContentType("application/pdf");
        //Nombre del reporte
        response.setHeader("Content-Disposition","inline; filename=\"TablaAmortizacion_" + codigo+ ".pdf\";");
        //Exportar a pdf
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
        //view the report using JasperViewer
        exporter.exportReport();
       // redireccion al panel principal
       response.sendRedirect("/arcamacW/panelAdmin.jsp");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteTablaAmortizacionIdel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReporteTablaAmortizacionIdel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteTablaAmortizacionIdel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ReporteTablaAmortizacionIdel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
