/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pochii
 */
public class consultasConfiguraciones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            int tipo = Integer.parseInt(request.getParameter("tipo")); 
            String query = "";
            String campo = "";
            String mensaje = "0";
            switch(tipo){
                case 1: //Refinanciamiento
                    query  = "select porcentaje_refinanciamiento FROM configuraciones";
                    campo = "porcentaje_refinanciamiento";
                    break;
                case 2: //Tasa de interes maxima solicitada por el BCR
                    query = "select porcentaje_maximo_bcr FROM configuraciones";
                    campo = "porcentaje_maximo_bcr";
                    break;
                case 3: //Cantidad maxima a prestar
                    query = "select cantidad_maxima_prestamo FROM configuraciones";
                    campo = "cantidad_maxima_prestamo";
                    break;
                case 4: //Solicitud a partir de la cual se solicita mas de un fiador
                    query = "select cantidad_minima_fiadores FROM configuraciones";
                    campo = "cantidad_minima_fiadores";
                    break;
                default:
                    mensaje = "1"; //No llamada desde formulario
                    break;
            }
            conexion con = new conexion();
            con.setRS(query);
            ResultSet rs = con.getRs();
            if(rs.next())
                mensaje = rs.getString(campo);
            else
                mensaje = "1";//error
            out.println("<div id='mensaje_config'>" + mensaje + "</div>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(consultasConfiguraciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(consultasConfiguraciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
