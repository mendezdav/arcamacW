/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 */
public class BeanOtorgamiento {
    
    private String fecha;
    private String solicitante;
    private String monto;
    private String plazo;
    private String cuota;
    private String fiador;
    private String codeudor;

    // constructor
    public BeanOtorgamiento()
    {
        fecha = "" ;
        solicitante = "" ;
        monto = "" ;
        plazo = "" ;
        cuota = "" ;
        fiador = "" ;
        codeudor = "" ;
    }
    
    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the monto
     */
    public String getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * @return the plazo
     */
    public String getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the cuota
     */
    public String getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(String cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the fiador
     */
    public String getFiador() {
        return fiador;
    }

    /**
     * @param fiador the fiador to set
     */
    public void setFiador(String fiador) {
        this.fiador = fiador;
    }

    /**
     * @return the codeudor
     */
    public String getCodeudor() {
        return codeudor;
    }

    /**
     * @param codeudor the codeudor to set
     */
    public void setCodeudor(String codeudor) {
        this.codeudor = codeudor;
    }
    
    
    
}
