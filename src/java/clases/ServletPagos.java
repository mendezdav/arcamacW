/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import clases.nuevoPrestamo;
import clases.diferenciaDias;
import Conexion.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author jorge
 */
@WebServlet(name = "ServletPagos", urlPatterns = {"/ServletPagos"})
public class ServletPagos extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            // declarando variables
            nuevoPrestamo prestamo = null;
            ResultSet rs = null;
            Statement s = null;
            String fechaActual="",id = "",codigoPrestamo="",query="",fechaEstablecida="",estado="";
            conexion con = null;
            double diasRetraso = 0.0,tasa_interes=0.0,saldo=0.0,cuota=0.0;
            int dias = 0,plazos=0;
                      
            // sacando datos del objeto request.
            // si el id del pago esta definido, guardar en variable
            if (request.getParameter("idPago") != "")
            {
                id =  request.getParameter("idPago");
                query="select pre.saldo,p.idPago,p.estado,p.comprobante,p.fechaEstablecida, "
                        + "p.fechaPago,pre.interes,pre.cuotas,pre.codigoPrestamo,pre.saldo,"
                        + "pre.tasa_interes,pre.plazo from pagos as p inner join prestamos "
                        + "as pre on pre.codigoPrestamo = p.codigoPrestamo where p.idPago=" + id + ";";
                out.print(query);
                con = new conexion();
                con.setRS(query);
                rs = con.getRs();
               
                // hay pagos con dicho idPago
                if (rs.next())
                {
                  // obtener datos 
                    codigoPrestamo = rs.getString("codigoPrestamo");
                    tasa_interes = rs.getDouble("tasa_interes");
                    plazos = rs.getInt("plazo");
                    saldo = rs.getDouble("saldo");
                    estado  = rs.getString("estado");
                    if (estado.equals("Abonado"))
                    {
                        fechaEstablecida = rs.getString("fechaPago");
                    }else
                        fechaEstablecida = rs.getString("fechaEstablecida");
                    
                    cuota = rs.getDouble("cuotas");
                    
                    // instanciar bean nuevo Prestamo
                    prestamo = new nuevoPrestamo();
                    prestamo.setCantidad(saldo);
                    prestamo.setPlazo(plazos);
                    prestamo.setInteres(tasa_interes);
                    
                    /**Cargando la mora**/
                    // obteniendo fecha actual
                    fechaActual = prestamo.getFechaEmision();
                    diferenciaDias diasF = new diferenciaDias();
                    diasRetraso = diasF.obtenerDiasRetraso(fechaActual,fechaEstablecida);
                    // si dias retraso es negativo
                    if (diasRetraso < 0)
                        diasRetraso = 0;
                    else
                        prestamo.setInteres_moras(cuota,diasRetraso);

                      /**Estableciendo IVA**/
                    prestamo.setIva(prestamo.getIntereses_pago() + prestamo.getInteres_mora());
                    
                                      
                    // verificar el estado del pago , es pendiente o abonado?
                    if (estado.equals("Pendiente") || estado.equals("En Mora"))
                    {
                        
                        
                    }
                    else // el pago fue abonado, anteriormente
                    {
                        
                    }
                    
                    
                   
                }
                
                // prueba imprimiendo datos obtenidos
              /* JOptionPane.showMessageDialog(null,"Codigo Prestamo: " + codigoPrestamo + "\n idPago:" + id + "\nTasa interes:" + tasa_interes 
                        +"\n plazo:"+ plazos + "\nsaldo actual: "+ saldo +"\nfechaEstablecida:" + fechaEstablecida 
                       + "\nDias retraso:" +diasRetraso + "\n cuota: " + cuota);
               
               
               JOptionPane.showMessageDialog(null, "interes moratoria: " + prestamo.getInteres_mora());
               JOptionPane.showMessageDialog(null, "Pago interes iva: "  + prestamo.getIntereses_pago_iva());
               JOptionPane.showMessageDialog(null, "capital: "  + prestamo.getCapital_cantidad(cuota));*/
     
            }
            
            
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServletPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServletPagos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
