package clases;

/**
 *
 * @author Jorge Luis
 */
public class beanEstadoCuenta {
    private String logo;
    private String fecha;
    private double monto_otorgado;
    private double tasa;
    private int plazo_meses;
    private String apertura; // fechaAprobacion
    private String vencimiento; // fechaVencimiento
    private String fecha_pago;
    private double cuota;
    private String forma_pago;
    private String asociado;
    private String direccion;
    private String tel_residencia;
    private String celular;
    private String codeudor;
    private String direccion_codeudor;
    private String tel_residencia_codeudor;
    private String celular_codeudor;
    private double saldo_prestamo;
    private double saldo_ideal;
    private double capital_adelantado;
    private double interes_normales;
    private double interes_mora;
    private double iva_interes_normales;
    private double iva_interes_mora;
    private double total_a_fecha;
    private double aportaciones;
    private double simultaneos;
    private double para_cancelar;
    private String codigoPrestamo;
    private String signo_debe_ser;
    private String fecha_ultima_pago;

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the monto_otorgado
     */
    public double getMonto_otorgado() {
        return monto_otorgado;
    }

    /**
     * @param monto_otorgado the monto_otorgado to set
     */
    public void setMonto_otorgado(double monto_otorgado) {
        this.monto_otorgado = monto_otorgado;
    }

    /**
     * @return the tasa
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the plazo_meses
     */
    public int getPlazo_meses() {
        return plazo_meses;
    }

    /**
     * @param plazo_meses the plazo_meses to set
     */
    public void setPlazo_meses(int plazo_meses) {
        this.plazo_meses = plazo_meses;
    }

    /**
     * @return the apertura
     */
    public String getApertura() {
        return apertura;
    }

    /**
     * @param apertura the apertura to set
     */
    public void setApertura(String apertura) {
        this.apertura = apertura;
    }

    /**
     * @return the vencimiento
     */
    public String getVencimiento() {
        return vencimiento;
    }

    /**
     * @param vencimiento the vencimiento to set
     */
    public void setVencimiento(String vencimiento) {
        this.vencimiento = vencimiento;
    }

    /**
     * @return the fecha_pago
     */
    public String getFecha_pago() {
        return fecha_pago;
    }

    /**
     * @param fecha_pago the fecha_pago to set
     */
    public void setFecha_pago(String fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    /**
     * @return the cuota
     */
    public double getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(double cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the forma_pago
     */
    public String getForma_pago() {
        return forma_pago;
    }

    /**
     * @param forma_pago the forma_pago to set
     */
    public void setForma_pago(String forma_pago) {
        this.forma_pago = forma_pago;
    }

    /**
     * @return the asociado
     */
    public String getAsociado() {
        return asociado;
    }

    /**
     * @param asociado the asociado to set
     */
    public void setAsociado(String asociado) {
        this.asociado = asociado;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the tel_residencia
     */
    public String getTel_residencia() {
        return tel_residencia;
    }

    /**
     * @param tel_residencia the tel_residencia to set
     */
    public void setTel_residencia(String tel_residencia) {
        this.tel_residencia = tel_residencia;
    }

    /**
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * @param celular the celular to set
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    /**
     * @return the codeudor
     */
    public String getCodeudor() {
        return codeudor;
    }

    /**
     * @param codeudor the codeudor to set
     */
    public void setCodeudor(String codeudor) {
        this.codeudor = codeudor;
    }

    /**
     * @return the direccion_codeudor
     */
    public String getDireccion_codeudor() {
        return direccion_codeudor;
    }

    /**
     * @param direccion_codeudor the direccion_codeudor to set
     */
    public void setDireccion_codeudor(String direccion_codeudor) {
        this.direccion_codeudor = direccion_codeudor;
    }

    /**
     * @return the tel_residencia_codeudor
     */
    public String getTel_residencia_codeudor() {
        return tel_residencia_codeudor;
    }

    /**
     * @param tel_residencia_codeudor the tel_residencia_codeudor to set
     */
    public void setTel_residencia_codeudor(String tel_residencia_codeudor) {
        this.tel_residencia_codeudor = tel_residencia_codeudor;
    }

    /**
     * @return the celular_codeudor
     */
    public String getCelular_codeudor() {
        return celular_codeudor;
    }

    /**
     * @param celular_codeudor the celular_codeudor to set
     */
    public void setCelular_codeudor(String celular_codeudor) {
        this.celular_codeudor = celular_codeudor;
    }

    /**
     * @return the saldo_prestamo
     */
    public double getSaldo_prestamo() {
        return saldo_prestamo;
    }

    /**
     * @param saldo_prestamo the saldo_prestamo to set
     */
    public void setSaldo_prestamo(double saldo_prestamo) {
        this.saldo_prestamo = saldo_prestamo;
    }

    /**
     * @return the saldo_ideal
     */
    public double getSaldo_ideal() {
        return saldo_ideal;
    }

    /**
     * @param saldo_ideal the saldo_ideal to set
     */
    public void setSaldo_ideal(double saldo_ideal) {
        this.saldo_ideal = saldo_ideal;
    }

    /**
     * @return the capital_adelantado
     */
    public double getCapital_adelantado() {
        return capital_adelantado;
    }

    /**
     * @param capital_adelantado the capital_adelantado to set
     */
    public void setCapital_adelantado(double capital_adelantado) {
        this.capital_adelantado = capital_adelantado;
    }

    /**
     * @return the interes_normales
     */
    public double getInteres_normales() {
        return interes_normales;
    }

    /**
     * @param interes_normales the interes_normales to set
     */
    public void setInteres_normales(double interes_normales) {
        this.interes_normales = interes_normales;
    }

    /**
     * @return the interes_mora
     */
    public double getInteres_mora() {
        return interes_mora;
    }

    /**
     * @param interes_mora the interes_mora to set
     */
    public void setInteres_mora(double interes_mora) {
        this.interes_mora = interes_mora;
    }

    /**
     * @return the iva_interes_normales
     */
    public double getIva_interes_normales() {
        return iva_interes_normales;
    }

    /**
     * @param iva_interes_normales the iva_interes_normales to set
     */
    public void setIva_interes_normales(double iva_interes_normales) {
        this.iva_interes_normales = iva_interes_normales;
    }

    /**
     * @return the iva_interes_mora
     */
    public double getIva_interes_mora() {
        return iva_interes_mora;
    }

    /**
     * @param iva_interes_mora the iva_interes_mora to set
     */
    public void setIva_interes_mora(double iva_interes_mora) {
        this.iva_interes_mora = iva_interes_mora;
    }

    /**
     * @return the total_a_fecha
     */
    public double getTotal_a_fecha() {
        return total_a_fecha;
    }

    /**
     * @param total_a_fecha the total_a_fecha to set
     */
    public void setTotal_a_fecha(double total_a_fecha) {
        this.total_a_fecha = total_a_fecha;
    }

    /**
     * @return the aportaciones
     */
    public double getAportaciones() {
        return aportaciones;
    }

    /**
     * @param aportaciones the aportaciones to set
     */
    public void setAportaciones(double aportaciones) {
        this.aportaciones = aportaciones;
    }

    /**
     * @return the simultaneos
     */
    public double getSimultaneos() {
        return simultaneos;
    }

    /**
     * @param simultaneos the simultaneos to set
     */
    public void setSimultaneos(double simultaneos) {
        this.simultaneos = simultaneos;
    }

    /**
     * @return the para_cancelar
     */
    public double getPara_cancelar() {
        return para_cancelar;
    }

    /**
     * @param para_cancelar the para_cancelar to set
     */
    public void setPara_cancelar(double para_cancelar) {
        this.para_cancelar = para_cancelar;
    }

    /**
     * @return the codigoPrestamo
     */
    public String getCodigoPrestamo() {
        return codigoPrestamo;
    }

    /**
     * @param codigoPrestamo the codigoPrestamo to set
     */
    public void setCodigoPrestamo(String codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    /**
     * @return the signo_debe_ser
     */
    public String getSigno_debe_ser() {
        return signo_debe_ser;
    }

    /**
     * @param signo_debe_ser the signo_debe_ser to set
     */
    public void setSigno_debe_ser(String signo_debe_ser) {
        this.signo_debe_ser = signo_debe_ser;
    }

    /**
     * @return the fecha_ultima_pago
     */
    public String getFecha_ultima_pago() {
        return fecha_ultima_pago;
    }

    /**
     * @param fecha_ultima_pago the fecha_ultima_pago to set
     */
    public void setFecha_ultima_pago(String fecha_ultima_pago) {
        this.fecha_ultima_pago = fecha_ultima_pago;
    }
}
