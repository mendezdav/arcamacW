/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 */
public class BeanFactura {
    
    private String solicitante;
    private String comprobante;
    private String fecha;
    private String cantidad;
    private String formaPago;
    private String capital;
    private String interes;
    private String interes_moratorio;
    private String totalAbonado;
    private String url_imagen;
    private String texto_moratorio;
    private String pago_moratorio;

    // contructor
    public BeanFactura()
    {
            solicitante = "";
            comprobante = "";
            fecha = "";
            cantidad = "";
            formaPago = "";
            capital = "";
            interes = "";
            interes_moratorio = "";
            totalAbonado = "";
            url_imagen = "";
            texto_moratorio ="";
            pago_moratorio = "";
    }
    
    
    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the comprobante
     */
    public String getComprobante() {
        return comprobante;
    }

    /**
     * @param comprobante the comprobante to set
     */
    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the cantidad
     */
    public String getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the formaPago
     */
    public String getFormaPago() {
        return formaPago;
    }

    /**
     * @param formaPago the formaPago to set
     */
    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    /**
     * @return the capital
     */
    public String getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(String capital) {
        this.capital = capital;
    }

    /**
     * @return the interes
     */
    public String getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(String interes) {
        this.interes = interes;
    }

    /**
     * @return the interes_moratorio
     */
    public String getInteres_moratorio() {
        return interes_moratorio;
    }

    /**
     * @param interes_moratorio the interes_moratorio to set
     */
    public void setInteres_moratorio(String interes_moratorio) {
        this.interes_moratorio = interes_moratorio;
    }

    /**
     * @return the totalAbonado
     */
    public String getTotalAbonado() {
        return totalAbonado;
    }

    /**
     * @param totalAbonado the totalAbonado to set
     */
    public void setTotalAbonado(String totalAbonado) {
        this.totalAbonado = totalAbonado;
    }

    /**
     * @return the url_imagen
     */
    public String getUrl_imagen() {
        return url_imagen;
    }

    /**
     * @param url_imagen the url_imagen to set
     */
    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }

    /**
     * @return the texto_moratorio
     */
    public String getTexto_moratorio() {
        return texto_moratorio;
    }

    /**
     * @param texto_moratorio the texto_moratorio to set
     */
    public void setTexto_moratorio(String texto_moratorio) {
        this.texto_moratorio = texto_moratorio;
    }

    /**
     * @return the pago_moratorio
     */
    public String getPago_moratorio() {
        return pago_moratorio;
    }

    /**
     * @param pago_moratorio the pago_moratorio to set
     */
    public void setPago_moratorio(String pago_moratorio) {
        this.pago_moratorio = pago_moratorio;
    }
    
    
}
