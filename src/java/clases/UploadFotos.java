/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;


import com.oreilly.servlet.MultipartRequest;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.sql.Statement;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
/**
 *
 * @author Pochii
 */
@WebServlet(name = "UploadFotos", urlPatterns = {"/UploadFotos"})
public class UploadFotos extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            String Ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            String url = getServletContext().getRealPath("solicitantes\\images");
            String tipo = request.getParameter("tipo");
            MultipartRequest multi = new MultipartRequest(request, url, 1024*1024*1024); //Tamanio maximo 10 MB
                
            String destino = multi.getParameter("rutared");
            try
            {
                HttpSession sesion = request.getSession(); //Variable de sesion
                
                File ficheroTemp = multi.getFile("pic");
                String nombreArchivo = ficheroTemp.getName();
                String nombreArchivoNuevo = "tmp." + getFileExtension(nombreArchivo);
                if(tipo.equals("edit"))
                {
                    String idSolicitante = request.getParameter("idSolicitante");
                    if(getImageName(url,"thumbnail_" + idSolicitante, Ruta).equals("nodisponible.jpg")){ //En el caso que la imagen no se encuentre
                        ficheroTemp = new File(url + "\\nodisponible.jpg");
                        nombreArchivo = "nodisponible.jpg";
                        nombreArchivoNuevo = "thumbnail_" + idSolicitante + ".jpg";
                    }else
                          nombreArchivoNuevo = getImageName(url,"thumbnail_" + idSolicitante, Ruta);
                }
                if(Upload(url, ficheroTemp, nombreArchivo, nombreArchivoNuevo))
                {
                    if(tipo.equals("new")){
                        /**Agregando ruta de imagen al bean Solicitantes**/    
                        solicitantes sol = (solicitantes) sesion.getAttribute("solicitantes");
                        sol.setImg_url("tmp." + getFileExtension(nombreArchivo));
                        sesion.setAttribute("solicitantes", sol);
                    }
                    response.sendRedirect(Ruta + destino + "&exito=upl");
                }
                else
                    response.sendRedirect(Ruta + destino + "&erno=5"); 
            }//fin try
            catch(FileNotFoundException e){
               out.println("La ruta de ubicacion no existe");
            }
            catch (IOException e)
            {
                response.sendRedirect(Ruta+ destino  + "&erno=11");
            }
            finally {
                out.close();
            }
    }
    public static boolean Upload(String url, File ficheroTemp, String nombreArchivo, String nombreArchivoNuevo){
        try
            {
                if(getFileExtension(nombreArchivo).toLowerCase().equals("jpg") || getFileExtension(nombreArchivo).toLowerCase().equals("png") || getFileExtension(nombreArchivo).toLowerCase().equals("jpeg"))
                {
                    /**Eliminando por si acaso, la imagen tmp***/
                    File ficherotmp = new File(url + "\\" + nombreArchivoNuevo);
                    if(ficherotmp.exists()) //Si ya existe, se elimina
                        ficherotmp.delete();
                    /**Renombrando la imagen a tmp**/
                    ficheroTemp.renameTo(new File(url + "\\" + nombreArchivoNuevo));
                    nombreArchivo= ficheroTemp.getName();
                    return true;
                }   
                else{
                    System.out.println("extension incorrecta " + getFileExtension(nombreArchivoNuevo));
                    return false;
                }
            }//fin try
            catch (HeadlessException e)
            {
                System.out.println("Error al subir foto " + e.getMessage());
                return false;
            }    
    }
  
    public static String getFileExtension(String fileName){
        String extension = "";
        int i = fileName.lastIndexOf('.');
        if (i >= 0) {
            extension = fileName.substring(i+1);
        }
        return extension.toLowerCase();
    }
    public static String getImageName(String url, String nombreImagen, String ruta){
        File f = new File(url + "\\" + nombreImagen + ".jpg");
        if(f.exists())
            return nombreImagen + ".jpg";
        else{
            if(new File(url + "\\" + nombreImagen + ".png").exists())
                return nombreImagen + ".png";
            else{
                if(new File(url + "\\" + nombreImagen + ".jpeg").exists())
                    return nombreImagen + ".jpeg";
                else
                    return "nodisponible.jpg";
            }
        }//fin else
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
