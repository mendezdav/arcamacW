/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Jorge Luis
 */
public class PagosExtraordinarios {
    private conexion con;
    private ResultSet rs;
    private String idSolicitante;
    private String tipo;
    private String qr;
    private String codigoPrestamo;
    private double total_capital;
    private double total_interes;
    private double total_interes_moratorio;
    
    private double iva_total_capital; // El capital tiene iva?
    private double iva_total_interes;
    private double iva_total_interes_moratorio;
    
    private double saldoActual;
    private double total_capital_ideal;
    
    
    public PagosExtraordinarios(String idSolicitante, String tipo) throws SQLException
    {
        // Reestablece todos los datos
        saldoActual = 0.0;
        total_capital  = 0.0;
        total_interes  = 0.0;
        total_interes_moratorio = 0.0;
        this.idSolicitante = idSolicitante;
        this.tipo          = tipo;
        con = null;
        rs = null;
        qr = "";
        codigoPrestamo = "";
        
        // Depende del tipo realiza una u otra operacion
        if (this.tipo.equals("dia"))
            calcularDia();
        else
            cancelarPrestamo();
    }

    
    private void calcularDia() throws SQLException
    {
        ////// Hasta este punto se ha asumido que todos lo montos de interes ya incluyen iva al guardarse en la base, 
        ////// Lo cual, con los nuevos cambios dejaria de ser cierto y los calculos en este punto deben variar
        /* calcular todos los pagos pendientes para ponerse al dia */
        // obtener codigo prestamo activo
        qr = "select idpago,SUM(intereses) as intereses,SUM(capital) as capital,SUM(interes_moratorio) interes_moratorio,"
                + " SUM(iva_interes) as iva_interes, SUM(iva_mora) as iva_mora, "
                + " SUM(pago_interes) as pago_interes, SUM(pago_interes_moratorio) as pago_interes_moratorio, "
                + " SUM(pago_iva_interes) as pago_iva_interes, SUM(pago_iva_mora) as pago_iva_mora, "
                + "SUM(pago_capital) as pago_capital, mora, pre.codigoPrestamo,pre.saldo from pagos as p inner join prestamos as pre "
                + "on p.codigoPrestamo = pre.codigoPrestamo where pre.estado='Activo' "
                + "and p.intereses != 0.00000 and pre.idSolicitante=1";
        
        con =  new conexion();
        con.setRS(qr);
        rs = con.getRs();
        rs.next();
        this.codigoPrestamo = rs.getString("codigoPrestamo");
        // lo que debe en concepto de interes y interes moratorio
        total_interes = rs.getDouble("intereses") - rs.getDouble("pago_interes");
        total_interes_moratorio = rs.getDouble("interes_moratorio") - rs.getDouble("pago_interes_moratorio");
        iva_total_interes = rs.getDouble("iva_interes") - rs.getDouble("pago_iva_interes");
        iva_total_interes_moratorio = rs.getDouble("iva_mora") - rs.getDouble("pago_iva_mora");
        // es necesario saber cuanto capital debe para estar al dia!
        // obtener el capital que deberia de tener al dia de hoy!
        total_capital_ideal = capitalIdealDia();
        saldoActual = rs.getDouble("saldo");
        total_capital = Math.abs(getSaldoActual() - getTotal_capital_ideal());
       
    }
    
    private double cancelarPrestamo() throws SQLException 
    {
        calcularDia();
        return this.getSaldoActual() + total_deuda(); 
    }

    private double capitalIdealDia() throws SQLException 
    {
        generarTablaAmortizacion tabla = new generarTablaAmortizacion(this.codigoPrestamo);
        return tabla.cargarDatosPagosExtraordinarios("");
    }

    /**
     * @return the total_capital
     */
    public double getTotal_capital() {
        return total_capital;
    }

    /**
     * @return the total_interes
     */
    public double getTotal_interes() {
        //return total_interes / 1.13;
        // Interes ya no se almacena con iva
        return total_interes;
    }

    /**
     * @return the total_interes_moratorio
     */
    public double getTotal_interes_moratorio() {
        //return total_interes_moratorio / 1.13;
        // Interes ya no se almacena con iva
        return total_interes_moratorio;
    }

    /**
     * @return the iva_total_capital
     */
    public double getIva_total_capital() {
        return (total_capital/1.13)*0.13;
    }

    /**
     * @return the iva_total_interes
     */
    public double getIva_total_interes() {
        //return (total_interes/1.13)*0.13;
        // Interes ya no se almacena con iva
        return iva_total_interes;
    }

    /**
     * @return the iva_total_interes_moratorio
     */
    public double getIva_total_interes_moratorio() {
        //return (total_interes_moratorio/1.13)*0.13;
        // Interes ya no se almacena con iva
        return iva_total_interes_moratorio;
    }
    
    public double total_deuda()
    {
        // es necesario re incluir el iva
        return (total_interes) + (total_interes_moratorio) + this.total_capital + (iva_total_interes) + (iva_total_interes_moratorio);
    }
     public double nuevoSaldo()
     {
         if (this.getSaldoActual() - this.total_capital >= 0.0)
             return this.getSaldoActual() - this.total_capital;
         else
             return 0.0;
     }

    /**
     * @return the saldoActual
     */
    public double getSaldoActual() {
        return saldoActual;
    }

    /**
     * @return the total_capital_ideal
     */
    public double getTotal_capital_ideal() {
        return total_capital_ideal;
    }
}
