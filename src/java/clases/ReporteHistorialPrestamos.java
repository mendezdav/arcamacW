/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Pochii
 */
public class ReporteHistorialPrestamos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        ServletOutputStream out = response.getOutputStream();
        try {
            String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            
            String id_solicitante = request.getParameter("idSolicitante");
            JasperReport reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/ReporteHistorialPrestamo.jasper"));
            
            //Cargamos parametros del reporte (si tiene).
            Map parameters = new HashMap();
              
            List<Object> reports = new LinkedList<Object>();
            // creando instancia conexion
            conexion con = new conexion();
            String qr="";
            String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");
            parameters.put("img_url", rutaImg);
            /**Capturando todos los datos excepto de fiadores**/
            String queryview = "SELECT *, GROUP_CONCAT(estadoPago, '(' , pagosMora, ')' ORDER BY estadoPago ASC SEPARATOR ';') as groupMora FROM viewhistorialprestamo "
                                + " WHERE idSolicitante = " + id_solicitante  
                                + " GROUP BY codigoPrestamo ORDER BY codigoPrestamo ASC";
           
            out.print(queryview);
            con.setRS(queryview);
            ResultSet rsview  = con.getRs();
           
            //if(rsview.next()) {
                while(rsview.next()){
                    /**Creando instancia a bean**/
                    BeanHistorialPrestamo historial = new BeanHistorialPrestamo();
                    historial.setCodigoPrestamo(rsview.getString("codigoPrestamo"));
                    historial.setSolicitante(rsview.getString("solicitante"));
                    historial.setCantidad(rsview.getDouble("cantidad"));
                    historial.setInteres(rsview.getDouble("interes"));
                    historial.setSaldoPendiente(rsview.getDouble("saldoPendiente"));
                    historial.setEstadoPrestamo(rsview.getString("estadoPrestamo"));
                    //historial.setPagosCancelados(rsview.getInt("pagosCancelados"));
                    historial.setGroupMora(rsview.getString("groupMora"));
                    historial.setRefinanciamiento(rsview.getString("refinanciamiento"));
                    reports.add(historial);
                }
            //} else
            //   response.sendRedirect(ruta + "/reportes/panelReportes.jsp?erno=4"); //fin de else
            rsview.close();
            /***Obteniendo informacion de los fiadores***/
            for(int i = 0; i < reports.size(); i++){                                        /**Recorriendo la lista***/
                BeanHistorialPrestamo aux = (BeanHistorialPrestamo) reports.get(i);         /**Haciendo el cast**/
                
                con.setRS("SELECT codigoPrestamo, TRIM(GROUP_CONCAT(nombreFiador SEPARATOR ';')) as nombreFiador\n" +
                          "FROM viewdetalleprestamo WHERE codigoPrestamo = '" + aux.getCodigoPrestamo() + "' GROUP BY codigoPrestamo ORDER BY codigoPrestamo ASC");
                rsview = con.getRs();
                
                while(rsview.next())
                    aux.setNombreFiador(rsview.getString("nombreFiador"));
                reports.set(i, aux);
            } //fin for
            rsview.close();
            for(int i = 0; i < reports.size(); i++){                                        /**Recorriendo la lista***/
                BeanHistorialPrestamo aux = (BeanHistorialPrestamo) reports.get(i);         /**Haciendo el cast**/
                 String query  = "SELECT pagosCancelados FROM viewhistorialprestamo WHERE"
                        + " codigoPrestamo = '" + aux.getCodigoPrestamo() + "' AND estadoPago = 'Pagado'";
                con.setRS(query);          
                System.out.println("Query: " + query);
            
                rsview = con.getRs();
                
                while(rsview.next())
                    aux.setPagosCancelados(rsview.getInt("pagosCancelados"));
                reports.set(i, aux);
            } //fin for
            rsview.close();
            
            con.cerrarConexion();
           
           //fill the ready report with data and parameter
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
            JRExporter exporter = null;
            response.setContentType("application/pdf");

            //Nombre del reporte
            response.setHeader("Content-Disposition","inline; filename=\"ReporteHistorialPrestamos.pdf\";");
            //Exportar a pdf
            exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            //view the report using JasperViewer
            exporter.exportReport();
            
        } catch (JRException ex) { //Excepcion en el reporte
            System.out.println("ERROR: " + ex.getMessage());
            Logger.getLogger(ReporteHistorialPrestamos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) { //Excepcion de SQL
            System.out.println("ERROR: " + ex.getMessage());
            Logger.getLogger(ReporteHistorialPrestamos.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
