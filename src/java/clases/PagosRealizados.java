/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "PagosRealizados", urlPatterns = {"/PagosRealizados"})
public class PagosRealizados extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /*Propiedad de la clase**/
    private List<Object> reports;
    // estableciendo variables globales
    beanPagoEfectuados pagos = new beanPagoEfectuados();
    Map parameters = new HashMap();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         ServletOutputStream out = response.getOutputStream();
         try {
         String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
         // tomando datos del request.
         JasperReport reporte = null; 
        // String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");  
         //Cargamos parametros del reporte (si tiene).
         HttpSession SessionActual = request.getSession();
         reports = new LinkedList<Object>();                //Inicializando lista
        /**Capturando datos***/
         String tipo = "";
         String busqueda="";
         String completo ="";
         String codigoPrestamo = "";
         String fechaInicio ="";
         String fechaFin ="";
           
        // almacenando variables
        if (request.getParameter("tipopago") != null)
            tipo = request.getParameter("tipopago");
        if (request.getParameter("busqueda") != null)
             busqueda = request.getParameter("busqueda");
        if (request.getParameter("completo") != null)
             completo = request.getParameter("completo");
        if (request.getParameter("codigoPrestamo") != null)
             codigoPrestamo = request.getParameter("codigoPrestamo");
        if (busqueda.equals("c"))
        {
             fechaInicio = request.getParameter("fechaInicio");
             fechaFin = request.getParameter("fechaFin");
             reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/ReportePagos_Prestamo.jasper"));
             /**En el caso que se trate de una busqueda por código de prestamos**/
             llenarDatosGenerales(codigoPrestamo);
             /**Se llama a la función de datos especificos**/
             llenarDatosEspecificos(fechaInicio, fechaFin, tipo, codigoPrestamo,busqueda);
        }
        if(completo.equals("completo")){
            reporte = (JasperReport) JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/ReportePagosEfectuados.jasper"));
            fechaInicio = request.getParameter("fechaInicio");
            fechaFin = request.getParameter("fechaFin");
            /**Se llena el Bean**/
            llenarDatosEspecificos(fechaInicio, fechaFin, tipo, "",busqueda);
        } //En el caso del reporte normal
        //reports.add((Object) pagos); 
        //fill the ready report with data and parameter
        JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
        JRExporter exporter = null;
        response.setContentType("application/pdf");
            //Nombre del reporte
            response.setHeader("Content-Disposition","inline; filename=\"PagosRealizados.pdf\";");
            //Exportar a pdf
            exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            //view the report using JasperViewer
            exporter.exportReport();
       
           // redireccion al panel principal
           response.sendRedirect(ruta + "/panelAdmin.jsp");
       
        } catch (JRException e) {
            e.printStackTrace();
        }
    }
    
    public int llenarDatosGenerales(String codigoPrestamo){
        try {
            int bandera = 1;
            conexion con = new conexion();
            /**Llenando los datos generales del prestamo***/
            String queryview = "SELECT *, GROUP_CONCAT(nombreFiador SEPARATOR ';') as fiadores FROM "
                    + "viewdetalleprestamo  WHERE codigoPrestamo='" + codigoPrestamo+ "' GROUP BY codigoPrestamo;";
            con.setRS(queryview);
            ResultSet rs = con.getRs();
            if(rs.next()){
                // llenando primeros datos
                pagos.setEstado(rs.getString("estadoPrestamo"));
                pagos.setFecha_aprobacion(rs.getString("fechaAprobacion"));
                pagos.setCodigoPrestamo(rs.getString("codigoPrestamo").toUpperCase());
                pagos.setSolicitante(rs.getString("nombreSolicitante"));
                pagos.setCantidad(rs.getDouble("cantidad"));
                pagos.setTipo_pago(rs.getString("tipoPago"));
                pagos.setFiador(rs.getString("fiadores"));
                pagos.setDestino_prestamo(rs.getString("destinoPrestamo"));
                pagos.setTasa_interes(rs.getDouble("tasaInteres"));
                pagos.setCuota(rs.getDouble("cuotas"));
                pagos.setPlazo(rs.getInt("plazo"));
                pagos.setEstadoPrestamo(rs.getString("estadoPrestamo"));
                if (rs.getString("refinanciado").equals("Sin Refinanciamiento"))
                    pagos.setRefinanciado("No");
                else
                    pagos.setRefinanciado("Si");
                bandera = 1;
            }
            else
                bandera = 0;
            return bandera;
        } //fin llenar datos generales
        catch (SQLException ex) {
            Logger.getLogger(PagosRealizados.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    } //fin llenar datos generales
    /**
     * 
     * @param fechaInicio fecha de inicio de generacion
     * @param fechaFin    fecha de fin de generacion
     * @param tipo        tipo de pago seleccionado
     * @return bandera    1 significa exito
     *                    0 significa fracaso
     */
    public int llenarDatosEspecificos(String fechaInicio, String fechaFin, String tipo, String codigoPrestamo,String busqueda){
        String condicion = "";
        int status_sentencia = 0;
        int status_cod = 0;
        String qr = "";
        
        if(!codigoPrestamo.equals("")){
            condicion = " and codigoPrestamo = '" + codigoPrestamo + "'";
            status_cod = 1;
        } //fin if
        try {
            int bandera = 1;
            conexion con = new conexion();
            if (fechaInicio.equals(fechaFin)){ // todos los pagos!
                if(status_cod == 1)
                    qr = "select * from viewabonos WHERE codigoPrestamo = '" + codigoPrestamo + "'";
                else{
                    qr = "SELECT * FROM viewabonos";
                    status_sentencia = 1;
                } //fin else
                parameters.put("labelFechas", "");
            }
            else{
                parameters.put("labelFechas", "Pagos comprendidos desde " + fechaInicio + " al " + fechaFin);
                if(status_cod == 1)
                    qr = "select * from viewabonos where fechaPago between '"+fechaInicio+"' and '"+fechaFin+"' and codigoPrestamo ='" + codigoPrestamo + "'";
                else
                    qr = "select * from viewabonos where fechaPago between '" + fechaInicio + "' and '" + fechaFin + "'";
            } //fin else
                
            if (!tipo.equals("Ambos")){
                if (!busqueda.equals("c"))
                {
                    if(status_sentencia == 0)
                        qr += " and tipoPago='" + tipo + "'";
                    else
                        qr += " WHERE tipoPago = '" + tipo + "'";
                }
            }
            System.out.println("Query establecida: " + qr);
            con.setRS(qr);
            ResultSet rs = con.getRs();
            if (rs.next())
            {
                rs.previous();
                while(rs.next())
                {
                       pagos.setFechaInicio(fechaInicio);
                       pagos.setFechaFin(fechaFin);
                       pagos.setComprobante(rs.getString("comprobante"));
                       pagos.setFechaPago(rs.getString("fechaPago"));
                       pagos.setFechaEstablecida(rs.getString("fechaEstablecida"));
                       pagos.setCodigoPrestamo(rs.getString("codigoPrestamo").toUpperCase());
                       pagos.setSolicitante(rs.getString("solicitante"));
                       pagos.setSaldo(rs.getDouble("saldoActual"));
                       pagos.setSaldoActual(rs.getDouble("saldoAnterior"));
                       pagos.setAbono(rs.getDouble("monto"));
                       pagos.setInteresMoratorio(rs.getDouble("pago_interes_moratorio"));
                       pagos.setInteresNormal(rs.getDouble("pago_interes"));
                       pagos.setCapital(rs.getDouble("pago_capital"));
                       pagos.setLogo(getServletContext().getRealPath("images/logoFactura.jpg"));
                       reports.add((Object) pagos); 
                   }
                   bandera = 1;
                }
                else
                    bandera = 0;
            return bandera;
        } 
        catch (SQLException ex) {
            Logger.getLogger(PagosRealizados.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    } //fin metodo
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
