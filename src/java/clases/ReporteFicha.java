/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// nuevas
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import clases.cumpleMes;
import java.sql.ResultSet;
import java.sql.SQLException;
import Conexion.conexion;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
/**
 *
 * @author Jorge Luis
 */
@WebServlet(name = "ReporteFicha", urlPatterns = {"/ReporteFicha"})
public class ReporteFicha extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
       
        ServletOutputStream out = response.getOutputStream();
        conexion con = new conexion();
        List<Object> reports = new LinkedList<Object>();
        String url = getServletContext().getRealPath("solicitantes\\images");
        String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        try {
            /**Creando objeto conexion**/
            ResultSet rs = null;
            JasperReport reporte = (JasperReport)
            JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/fichaSolicitante.jasper"));
            //Cargamos parametros del reporte (si tiene).
            Map parameters = new HashMap();
            /**Capturando id de solicitante**/
            String idSolicitante = request.getParameter("idSolicitante");
            con.setRS("SELECT * FROM solicitante WHERE idSolicitante=" + idSolicitante + "");    /**Estableciendo RS**/
            rs = con.getRs();                                                           

            rs.next();                                                                           /**Recorriendo RS**/
            solicitantes beanSolicitantes = new solicitantes();                                  /**Estableciendo propiedades**/
            
            beanSolicitantes.setIngresos(rs.getDouble("ingresos"));
            beanSolicitantes.setLiquidez(rs.getDouble("liquidez"));
            beanSolicitantes.setDescuentos(rs.getDouble("descuentos"));

            beanSolicitantes.setIdSolicitante(rs.getString("idsolicitante"));
            beanSolicitantes.setNombre1(rs.getString("nombre1"));
            beanSolicitantes.setNombre2(rs.getString("nombre2"));
            beanSolicitantes.setApellido1(rs.getString("apellido1"));
            beanSolicitantes.setApellido2(rs.getString("apellido2"));
            beanSolicitantes.setDUI (rs.getString("DUI"));
            beanSolicitantes.setNIT(rs.getString("NIT"));
            beanSolicitantes.setDomicilio(rs.getString("domicilio"));
            beanSolicitantes.setProfesion(rs.getString("profesion"));
            beanSolicitantes.setLugarTrabajo(rs.getString("lugarTrabajo"));
            beanSolicitantes.setEdad(rs.getString("edad"));
            beanSolicitantes.setTiempo_servicio(rs.getString("tiempo_servicio"));
            beanSolicitantes.setCargo(rs.getString("cargo"));
            beanSolicitantes.setTel_residencia(rs.getString("tel_residencia"));
            beanSolicitantes.setCelular(rs.getString("celular"));
            beanSolicitantes.setTel_trabajo(rs.getString("tel_trabajo"));
            beanSolicitantes.setExt(rs.getInt("ext"));
            beanSolicitantes.setNombre_conyuge(rs.getString("nombre_conyuge"));
            beanSolicitantes.setApellidos_conyuge(rs.getString("apellidos_conyuge"));
            beanSolicitantes.setEdad_conyuge(rs.getInt("edad_conyuge"));
            beanSolicitantes.setProfesion_conyuge(rs.getString("profesion_conyuge"));
            beanSolicitantes.setLugar_direccion_trabajo_conyuge(rs.getString("lugar_direccion_trabajo_conyuge"));
            beanSolicitantes.setFecha_nacimiento(rs.getString("fecha_nacimiento"));
            beanSolicitantes.setEstado_sistema(rs.getString("estado_sistema"));
            /**
             * Estableciendo imagen
             */
            beanSolicitantes.setImg_url(ruta+ "/solicitantes/images/" + UploadFotos.getImageName(url, "thumbnail_" + idSolicitante, ruta)); 
            
            con.cerrarConexion();
            reports.add((Object) beanSolicitantes);
            
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
            JRExporter exporter = null;
            response.setContentType("application/pdf");
            //Nombre del reporte
            response.setHeader("Content-Disposition","inline; filename=\"fichaSolicitante.pdf\";");
            //Exportar a pdf
            exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            //view the report using JasperViewer
            exporter.exportReport();

           // redireccion al panel principal
           //response.sendRedirect("/arcamacW/panelAdmin.jsp");
       }
       catch(SQLException s){
          // JOptionPane.showMessageDialog(null, "error: " + s.getMessage());
           out.println("error" + s.getMessage());
            con.cerrarConexion();
       }
        catch (JRException e) {
            e.printStackTrace();
        }
        
    }
   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteFicha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteFicha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
