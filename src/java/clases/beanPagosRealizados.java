/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author jorge
 */
public class beanPagosRealizados {
    private String comprobante;
    private String prestamo;
    private String fechaEstablecida;
    private String fechaPago;
    private double monto;
    private double capitalAnt;
    private double capitalNuevo;
    private double interes;
    private double interes_mora;
    private double pago_capital;
    private String fecha_impresion;
    private String solicitante;
    private String logo;
    
    public beanPagosRealizados()
    {
    
        comprobante = "";
        prestamo = "";
        fechaEstablecida = "";
        fechaPago = "";
        monto = 0.0;
        capitalAnt = 0.0;
        capitalNuevo = 0.0;
        interes = 0.0;
        interes_mora = 0.0;
        pago_capital = 0.0;
        fecha_impresion ="";
        solicitante = "";
    }

    
      /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }
    /**
     * @return the comprobante
     */
    public String getComprobante() {
        return comprobante;
    }

    /**
     * @param comprobante the comprobante to set
     */
    public void setComprobante(String comprobante) {
        this.comprobante = comprobante;
    }

    /**
     * @return the prestamo
     */
    public String getPrestamo() {
        return prestamo;
    }

    /**
     * @param prestamo the prestamo to set
     */
    public void setPrestamo(String prestamo) {
        this.prestamo = prestamo;
    }

    /**
     * @return the fechaEstablecida
     */
    public String getFechaEstablecida() {
        return fechaEstablecida;
    }

    /**
     * @param fechaEstablecida the fechaEstablecida to set
     */
    public void setFechaEstablecida(String fechaEstablecida) {
        this.fechaEstablecida = fechaEstablecida;
    }

    /**
     * @return the fechaPago
     */
    public String getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the monto
     */
    public double getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }

    /**
     * @return the capitalAnt
     */
    public double getCapitalAnt() {
        return capitalAnt;
    }

    /**
     * @param capitalAnt the capitalAnt to set
     */
    public void setCapitalAnt(double capitalAnt) {
        this.capitalAnt = capitalAnt;
    }

    /**
     * @return the capitalNuevo
     */
    public double getCapitalNuevo() {
        return capitalNuevo;
    }

    /**
     * @param capitalNuevo the capitalNuevo to set
     */
    public void setCapitalNuevo(double capitalNuevo) {
        this.capitalNuevo = capitalNuevo;
    }

    /**
     * @return the interes
     */
    public double getInteres() {
        return interes;
    }

    /**
     * @param interes the interes to set
     */
    public void setInteres(double interes) {
        this.interes = interes;
    }

    /**
     * @return the interes_mora
     */
    public double getInteres_mora() {
        return interes_mora;
    }

    /**
     * @param interes_mora the interes_mora to set
     */
    public void setInteres_mora(double interes_mora) {
        this.interes_mora = interes_mora;
    }

    /**
     * @return the pago_capital
     */
    public double getPago_capital() {
        return pago_capital;
    }

    /**
     * @param pago_capital the pago_capital to set
     */
    public void setPago_capital(double pago_capital) {
        this.pago_capital = pago_capital;
    }

    /**
     * @return the fecha_impresion
     */
    public String getFecha_impresion() {
        return fecha_impresion;
    }

    /**
     * @param fecha_impresion the fecha_impresion to set
     */
    public void setFecha_impresion(String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }

    /**
     * @return the solicitante
     */
    public String getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }
    
    
}
