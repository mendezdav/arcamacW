/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import Conexion.conexion;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author jorge
 */
@WebServlet(name = "ReporteFactura", urlPatterns = {"/ReporteFactura"})
public class ReporteFactura extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
       ServletOutputStream out = response.getOutputStream();
            
     try {
        String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        // obteniendo id del pago!
        HttpSession Session = request.getSession();
        //String idPago = (String) Session.getAttribute("comprobante");
        //String codigoPrestamo = (String) Session.getAttribute("codigoPrestamo");
        String idPago = (String) request.getParameter("comprobante");
        String codigoPrestamo = (String) request.getParameter("codigoPrestamo");
        JasperReport reporte = (JasperReport)
        JRLoader.loadObject(getServletContext().getRealPath("reportes_jrxml/ReciboAbono.jasper"));
        //Cargamos parametros del reporte (si tiene).
        Map parameters = new HashMap();
        //parameters.put("parametro", "parametro");
              
        List<Object> reports = new LinkedList<Object>();
        // creando instancia conexion
        conexion con = new conexion();
        String qr="";
        ResultSet tmp = null;
      
        String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");  
        //instancia al javaBean
        // obteniendo nombre y cuota del prestamo con dicho idPago
        
        qr ="select  concat(s.nombre1, ' ' , s.nombre2, ' ', s.apellido1, ' ' , s.apellido2) as nombre, pre.cuotas as cuota from prestamos as pre inner join solicitante as s on pre.idSolicitante = s.idSolicitante  where pre.codigoPrestamo='" +codigoPrestamo+"'";
        
        
        con.setRS(qr);
        ResultSet res = (ResultSet) con.getRs();
        res.next();
        String nombre = res.getString("nombre");
        double cuota = res.getDouble("cuota");
       
        System.out.print(cuota);
        
       qr ="select * from abonos where idabono=" + idPago;
       System.out.print("************** "+qr+" *****************");
       con.setRS(qr);
       res = (ResultSet) con.getRs();
       while (res.next())
       {
            beanReciboAbono abono = new beanReciboAbono();
            // llenando datos en el bean instaniado
            abono.setNombre(nombre);
            abono.setCuota(cuota);
            abono.setInteres(res.getDouble("pago_interes"));
            abono.setCapital( res.getDouble("pago_capital") );
            abono.setComprobante( res.getString("idabono") );
            // fechaActual
            abono.setFechaPago( diferenciaDias.FechaDMA(diferenciaDias.fechaActual()) );
            //fecha pago
            abono.setFechaVencimiento( diferenciaDias.FechaDMA(res.getString("fechaPago")));
            abono.setMora( res.getDouble("pago_interes_moratorio"));
            
            
            double IVA = res.getDouble("pago_iva");
            IVA = diferenciaDias.redondear(IVA);
            abono.setIva(IVA);
            
            abono.setLogo(rutaImg);
            abono.setMonto( res.getDouble("monto") );
            abono.setPrestamo(codigoPrestamo);
            abono.setSaldoActual( res.getDouble("SaldoActual") );
            abono.setSaldoAnt( res.getDouble("SaldoAnt") );
            
            // creando reporte
            reports.add((Object) abono); 
        }
            //fill the ready report with data and parameter
            JasperPrint jasperPrint = JasperFillManager.fillReport(reporte,parameters, new JRBeanCollectionDataSource(reports));
            JRExporter exporter = null;
            response.setContentType("application/pdf");
            //Nombre del reporte
            response.setHeader("Content-Disposition","inline; filename=\"Abono.pdf\";");
            //Exportar a pdf
            exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            //view the report using JasperViewer
            exporter.exportReport();
            // redireccion al panel principal
            response.sendRedirect(ruta + "/panelAdmin.jsp");
      
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteFactura.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ReporteFactura.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
