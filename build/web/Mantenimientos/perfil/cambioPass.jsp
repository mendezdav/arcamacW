

<%-- 
    Document   : cambioPass
    Created on : 12-07-2013, 03:45:10 PM
    Author     : jorge
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../administracion/header.jsp" %>
<style>
             .container
            {
                max-width:  500px;
                margin: 0 auto;
                text-align: justify;
               
            }
        </style>
<script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>

<div class="container">
    <div align="center">
    <h2>Cambio De Contrase&ntilde;a </h2>
    <br /><br /> 
          
        <form action="actionCambioPass.jsp" method="POST">
            <table>
                <tr>
                    <td><b>Contrase&ntilde;a Actual:</b></td>
                    <td><input  class="form-control requerido" type="password"  placeholder="Contrase&ntilde;a actual" name="passActual" id="passActual" required onblur="validar('passActual');" ></td>
                    <td><div id='divpassActual'> </div></td>
                </tr>
                
                <tr>
                    <td><br /><b>Contrase&ntilde;a Nueva:</b></td>
                    <td><br /><input type="password" class="form-control requerido" placeholder="Nueva Contrase&ntilde;a" name="pass1" id="pass1" required ></td>
                    <td><div id='divpass1'> </div></td>
                </tr>
                
                <tr>
                    <td><br /><b>Repetir Contrase&ntilde;a:</b></td>
                    <td><br /><input  class="form-control requerido" type="password"  placeholder="Repetir" name="pass2" id="pass2" required onblur="validarPass();" ></td>
                    <td><div id='divpass2'> </div></td>
                </tr>
            </table>
                       
            <br /><br />
           <button class="btn btn-lg btn-primary btn-block" type="submit">Realizar Cambio</button>
        </form>
       
    </div>
    
</div>
       <script language="javascript">
         function validarPass()
        {
            
            var p1 = document.getElementById("pass1").value;
            var p2 = document.getElementById("pass2").value;
            
            
            if (p1 != null && p2 != null)
            {
                    if (p1 != p2) {
                    alert("Las contraseñas deben coincidir");
                    document.getElementById("pass1").value ="";
                    document.getElementById("pass2").value ="";
                    return false;
                  } 
            }
            else 
            {
                alert("Debe llenar las contraseñas");
            }
        }
        
    </script>
<%@include file="../../administracion/footer.jsp" %>