<%-- 
    Document   : mostrarFiadorMantenimiento
    Created on : 12-11-2013, 06:29:40 PM
    Author     : jorge
--%>

    <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <style media="all" type="text/css" >
        @import url("../../css/screen.css");
    </style>
        <%
            String criterio; 
            criterio = request.getParameter("criterio"); 
            if(criterio != null)
             {   
          %>
        
          <sql:query var="consulta1" dataSource="jdbc/mysql" >
              select idUsuarios,concat(nombreUsuario,' ' , apellidoUsuario) as nombre,substring(fechaCreacion,1,10) as fecha,usuario from Usuarios 
              where concat(nombreUsuario,' ' , apellidoUsuario) like '%<%= criterio %>%'
          </sql:query>
              
          <display:table id="usuario" name="${consulta1.rows}" pagesize="10" export="true"  >
               <display:column title="Nombre" property="nombre"  />
               <display:column title="Usuario" property="usuario"  />
               <display:column title="Fecha Alta" property="fecha" />
               <display:column  value="Eliminar" url="/Mantenimientos/Usuarios/EliminarUsuarios.jsp" paramId="idUsuarios" paramProperty="idUsuarios"  style="text-align:center;"/>
               <display:column  value="<i class='fa fa-edit'></i>Modificar" url="/Mantenimientos/Usuarios/ModificarUsuarios.jsp" paramId="idUsuarios" paramProperty="idUsuarios"  style="text-align:center;"/>
          </display:table>
              
        </ajax:displaytag>
              <%
                  }else // mostrar todos los solicitantes 
                
                    {
                %>
          <sql:query var="consulta1" dataSource="jdbc/mysql" >
              select idUsuarios,concat(nombreUsuario,' ' , apellidoUsuario) as nombre,substring(fechaCreacion,1,10) as fecha,usuario from Usuarios 
              where concat(nombreUsuario,' ' , apellidoUsuario) like '%<%= criterio %>%'
          </sql:query>
              
          <display:table id="usuario" name="${consulta1.rows}" pagesize="10" export="true"  >
               <display:column title="Nombre" property="nombre"  />
               <display:column title="Usuario" property="usuario"  />
               <display:column title="Fecha Alta" property="fecha" />
               <display:column  value="Eliminar" url="/Mantenimientos/Usuarios/EliminarUsuarios.jsp" paramId="idUsuarios" paramProperty="idUsuarios"  style="text-align:center;"/>
               <display:column  value="<i class='fa fa-edit'></i>Modificar" url="/Mantenimientos/Usuarios/ModificarUsuarios.jsp" paramId="idUsuarios" paramProperty="idUsuarios"  style="text-align:center;"/>
          </display:table>
              
        </ajax:displaytag>
                
         <%
                }
          %>