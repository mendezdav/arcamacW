<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        
        .container
        {
            max-width:  500px;
            margin: 0 auto;
            text-align: justify;
        }
    </style>
  <script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
  
  <div class="container">
       
      <center>
        <h2 class="form-signin-heading">Nuevo Usuario</h2><br />
        <form class="form-signin" action="agregarUsuarioAction.jsp" method="post" name="formulario" id="formulario">
                
        
        <table>
            <tr>
                <td><b>Nombres:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Nombres" name="nombre" id="nombre" required onblur="validar('nombre');" ></td>
                <td><div id='divnombre'> </div></td>
            </tr>
            
            <tr>
                <td><b>Apellidos:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Apellidos" name="apellido" id="apellido" required  onblur="validar('apellido');"></td>
                <td><div id='divapellido'> </div></td>
            </tr>
            
            <tr>
                <td><b>Usuario:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Usuario" name="usuario" id="usuario" required  onblur="validar('usuario');"></td>
                <td><div id='divusuario'> </div></td>
            </tr>
            
            <tr>
                <td><b>Contrase&ntilde;a:</b></td>
                <td><br /><input type="password" class="form-control requerido" placeholder="Contrase&ntilde;a" name="pass" id="pass" required ></td>
                <td><div id='divpass'> </div></td>
            </tr>
            
            <tr>
                <td><b>Repetir:</b></td>
                <td><br /><input type="password" class="form-control requerido" placeholder="Repetir" name="pass2" id="pass2" required onblur="validarPass();"></td>
                <td><div id='divpass2'> </div></td>
            </tr>
            
            <tr>
                <td><b>Email:</b></td>
                <td><br /><input type="email" class="form-control requerido" placeholder="Email" name="email" id="email" required onblur="validar('email');" ></td>
                <td><div id='divemail'> </div></td>
            </tr>
            
            
            <tr>
                <td><b>Tipo Usuario:</b></td>
                  <!--tipo de usuario -->
                 <td><br />
                <select name="tipousuario" class="form-control">
                    <%
                      conexion con = new conexion();
                      con.setRS("select * from tipousuario");
                      ResultSet resultado = (ResultSet)con.getRs();
                      while (resultado.next())
                      {
                      %>
                      <option value="<%=resultado.getString("tipoUsuario") %>"><%=resultado.getString("tipoUsuario") %></option>
                      <%
                      }

                    %>
                </select></td>
            </tr>
            <tr><td colspan="3">
                <center><br />
                <input type="submit" class="btn btn-lg btn-primary btn-block"  value="Agregar" /></center>
                </td></tr>
        </table>
         
        </form></center>
        
          <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                  if (estado.equals("1"))
                  {
                      out.print("<div class=\"alert alert-success\">Usuario Agregado Exitosamente!</div>");
                  }
                  if (estado.equals("2"))
                  {
                      out.print("<div class=\"alert alert-danger\">Lo Sentimos ha ocurrido un error!</div>");
                  }
              }
              
              %>
    </div> <!-- /container -->
    
    <script language="javascript">
         function validarPass()
        {
            
            var p1 = document.getElementById("pass").value;
            var p2 = document.getElementById("pass2").value;
            
            
            if (p1 != null && p2 != null)
            {
                    if (p1 != p2) {
                    alert("Las contraseņas deben coincidir");
                    document.getElementById("pass").value ="";
                    document.getElementById("pass2").value ="";
                    return false;
                  } 
            }
            else 
            {
                alert("Debe llenar las contraseņas");
            }
            
            
        }
        
    </script>
<%@include file="../../administracion/footer.jsp" %>