<%-- 
    Document   : EliminarFiador
    Created on : 12-11-2013, 06:33:47 PM
    Author     : jorge
--%>


<%@page import="Conexion.conexion, java.sql.*, java.util.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
       <style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        
        .container
        {
        max-width: 500px;
        margin: 0 auto;
       text-align: justify;
        }
            
      
    </style>
 <%! ResultSet resultado=null; %>
 <%! String idfiadores= "";%>


 <%
     // creando instancia conexion
     conexion con = new conexion();
         
     // capturando datos!
     if (request.getParameter("idfiadores") != null)
     {
         idfiadores = (String) request.getParameter("idfiadores");
         con.setRS("select concat(nombreFiador,' ', apellidoFiador) as nombre, DUIF,idfiadores from fiadores where idfiadores=" + idfiadores);
         
         resultado = (ResultSet) con.getRs();
         resultado.next();
     }
     
 %>
  <div class="container"><br />
      <center>
          <h2>Advertencia!</h2><p />
          <h3>Esta a punto de eliminar el siguiente fiador: </h3><br /><br />
      <div class="panel panel-default">
        <div class="panel-body">
          <table>
              
              <tr>
              <td><b>Nombre:</b></td> <td><% out.print(resultado.getString("nombre"));  %></td>
              </tr>
              
              <tr>
                  <td><b>DUI:&nbsp;&nbsp;</b></td><td><% out.print(resultado.getString("DUIF")); %></td>
              </tr>
              
              <tr>
              <td colspan="2"><center><br />
                <a href="<%= ruta%>/Mantenimientos/fiadores/principalFiadores.jsp?estado=1">Cancelar</a></center>
               </td>
              </tr>
              
              <tr>
                <td colspan="2">
                <center>
                    <%
                    //  generando link para que "pase" el id del usuario a eliminar
                    out.print("<a href=\"" + ruta+ "/Mantenimientos/fiadores/EliminarFiadorAction.jsp?idfiadores=" + idfiadores.toString() +"\" >Lo Entiendo, eliminar el fiador </a>");
                    %>
                </center>
                </td>
             </tr>
          </table></div>
      </div>
    </center>
  </div>
<%@include file="../../administracion/footer.jsp" %>

