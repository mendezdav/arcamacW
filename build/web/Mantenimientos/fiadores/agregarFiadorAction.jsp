<%@page import="clases.diferenciaDias"%>
<%@page import="java.util.TreeMap"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page  import="Conexion.conexion, java.sql.*" %>
<%
    //Capturando valores
    String requeridos[] = {"idSolicitante","nombreFiador", "edadF","DUIF", "celularF", "NITF", "salario"};  
    String campos[] = {"idSolicitante","nombreFiador", "apellidoFiador","edadF","DUIF", "DomicilioF", "profesionF","lugarTrabajoF","cargoF","telefono_trabajo", "direccionParticular", "id_departamento", "id_municipio", "telResidenciaF", "celularF", "NITF", "ext", "deducciones", "liquidez","salario"};
    
    TreeMap<String, String>fiadoresPost = new TreeMap<String, String>();
    String url = request.getParameter("url"); 
    String lugar_fecha = "";
    try
    {
        for(String nulo : requeridos)   
        {
            if(request.getParameter(nulo).equals("")){
                //JOptionPane.showMessageDialog(null, "El campo nulo es: " + nulo);
                response.sendRedirect(url + "?erno=1&campo=" + nulo);
            }
        }//fin for 
        for(String campo: campos)
        {
            if(!request.getParameter(campo).equals(""))
                fiadoresPost.put(campo, request.getParameter(campo));
        }//fin for
        /**Agregando el campo de lugar y fecha**/
        lugar_fecha = "Ilopango, " + diferenciaDias.fechaActualFH();
        
        fiadoresPost.put("lugar_fecha",lugar_fecha);
    }
    catch(Exception e){
       // JOptionPane.showMessageDialog(null, "Hubo un error debido a: " + e.getMessage());
    }
    // tomando datos enviados por medio del request
    //int idSolicitante = Integer.parseInt(request.getParameter("id"));
    String type = request.getParameter("type");
    conexion con = new conexion();
    boolean estado = con.insertQuery(fiadoresPost, "fiadores");
    if (estado)
    {
        if(type.equals("new")) 
            response.sendRedirect("agregarFiadorPaso1.jsp?estado=1");
        else
        {
            con.setRS("SELECT idfiadores, CONCAT(nombreFiador,' ', apellidoFiador) as nombres FROM fiadores ORDER BY idfiadores DESC LIMIT 1 LOCK IN SHARE MODE");
            ResultSet rs = con.getRs();
            rs.next();
            String id = rs.getString("idfiadores");
            String nombre = rs.getString("nombres"); 
%>
<div id="id_newfiador"><%=id%></div>
<div id="nombre"><%=nombre%></div>
<%
        }
    }
    else 
    {
        if(type.equals("new"))
             response.sendRedirect("agregarFiadorPaso1.jsp?estado=2");
    }
%>