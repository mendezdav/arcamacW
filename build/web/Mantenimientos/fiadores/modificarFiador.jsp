<%@page import="javax.swing.JOptionPane"%>
<%@page  import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<style type="text/css">
     body {
          padding-top: 60px;
          padding-bottom: 60px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 650px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        
        .container
        {
            max-width:  700px;
            margin: 0 auto;
            text-align: justify;
        }
        
        .table
         {
            width:700px;
            
         }
         
    </style>
 
  <div class="container">
      <!-- Estableciendo la query !-->
      <c:if test="${empty param.idfiadores}">
          <c:redirect url="principalFiadores.jsp?erno=1" />
      </c:if>
      <sql:query var="fiador" dataSource="jdbc/mysql">
          SELECT * FROM viewfiadores WHERE idfiadores = ${param.idfiadores}
      </sql:query>
      <c:forEach var="fiador_f" items="${fiador.rows}">
      <center>
        <h2 class="form-signin-heading">Modificar Fiador</h2><br />
        <form class="form-signin" action="ModificarFiadorAction.jsp" method="post" name="formulario" id="formulario">
        
       <table class="table">
            <tr>
                <td><b>* Nombres:</b></td>
                <input type="hidden" name="id" id="id" value="${fiador_f.idfiadores}" />
                <td><br /><input type="text" class="form-control requerido" size="40" placeholder="Nombre" name="nombreFiador" id="nombreFiador" required onblur="validar('nombreFiador');" value="${fiador_f.nombreFiador}" ></td>
                <td><div id='divnombreFiador'> </div></td>
           
                <td><b>* Apellido:</b></td>
                <td><br /><input type="text" class="form-control requerido"  size="40" placeholder="Apellido" name="apellidoFiador" id="apellidoFiador" required onblur="validar('apellidoFiador');" value="${fiador_f.apellidoFiador}" ></td>
                <td><div id='divapellidoFiador'> </div></td>
            </tr>
            
            <tr>
                <td><b>* Edad:</b></td>
                <td><br /><input type="number" min="18" max="85" size="40" class="form-control requerido" placeholder="Edad" name="edadF" id="edadF" required onblur="validar('edadF');" value="${fiador_f.edadF}" ></td>
                <td><div id='divedadF'> </div></td>
            
                <td><b>* DUI:</b></td>
                <td><br /><input type="text" size="40" class="form-control requerido" placeholder="DUI" name="DUIF" id="DUIF" size="10" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('DUIF');" value="${fiador_f.DUIF}" /> </td>
                <td><div id='divDUIF'> </div></td>
        
            </tr>
          
            
            <tr>
                <td><b>* NIT:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="NIT" name="NITF" id="NITF" required pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" onblur="validar('NITF');" value="${fiador_f.NITF}"/></td>
                <td><div id='divNITF'> </div></td>
           
                <td><b>* Domicilio:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Domicilio" name="DomicilioF" id="DomicilioF" required onblur="validar('DomicilioF');"  value="${fiador_f.DomicilioF}"></td>
                <td><div id='divDomicilioF'> </div></td>
            </tr>
           
            <tr>
                <td><b>* Profesi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Profesi&oacute;n" name="profesionF" id="profesionF" required onblur="validar('profesionF');" value="${fiador_f.profesionF}" ></td>
                <td><div id='divprofesionF'> </div></td>
            
                <td><b>* Lugar Trabajo</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Lugar Trabajo" name="lugarTrabajoF" id="lugarTrabajoF" required onblur="validar('lugarTrabajoF');" value="${fiador_f.lugarTrabajoF}" ></td>
                <td><div id='divlugarTrabajoF'> </div></td>
            </tr>
            
            
            <tr>
                <td><b>* Cargo:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Cargo" name="cargoF" id="cargoF" required onblur="validar('cargoF');"  value="${fiador_f.cargoF}"></td>
                <td><div id='divcargoF'> </div></td>
           
                <td><b>* Tel&eacute;fono Trabajo:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Tel&eacute;fono" name="telefono_trabajo"  id="telefono_trabajo" required onblur="validar('telefono_trabajo');" value="${fiador_f.telefono_trabajo}"></td>
                <td><div id='divtelefono_trabajo'> </div></td>
            </tr>
           
            <tr>
                <td><b>Extensi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Extensi&oacute;n" name="ext" id="ext" value="${fiador_f.ext}" ></td>
                <td><div id='divext'> </div></td>
            
                <td><b>* Direcci&oacute;n Particular:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Direcci&oacute;n" name="direccionParticular" id="direccionParticular" required  onblur="validar('direccionParticular');" value="${fiador_f.direccionParticular}"></td>
                <td><div id='divdireccionParticular'> </div></td>
            </tr>
            <tr>
                <td><b>Departamento:</b></td>
                <td>
                    <select name="id_departamento" id="id_departamento" class="form-control" onchange="cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0)" style="width: 150px; display: inline;">
                            <!--Cargando valor actual -->
                            <option value="${fiador_f.id_departamento}">${fiador_f.departamento}</option>
                            <sql:query dataSource="jdbc/mysql" var="depto">
                                SELECT id_departamento, departamento FROM departamento WHERE id_departamento != ${fiador_f.id_departamento}</sql:query>
                            <c:forEach var="depto_f" items="${depto.rows}">
                                <option value="${depto_f.id_departamento}">${depto_f.departamento}</option>
                            </c:forEach>
                    </select>
                </td>
                <td><div id='divid_depto'></div></td>
            </tr>
            <tr>
                <td><b>Municipio:</b></td>
                <td>
                     <c:set var="id_municipio" value="${fiador_f.id_municipio}" />
                    <select name="id_municipio" id="id_municipio" class="form-control" style="width: 150px; display: inline;"></select>
                </td>
                <td><div id='divid_municipio'></div></td>
            </tr>
            <tr>
                <td><b>Tel&eacute;fono Residencia:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Tel. Residencia" name="telResidenciaF" id="telResidenciaF" value="${fiador_f.telResidenciaF}"></td>
                <td><div id='divtelResidenciaF'> </div></td>
         
                <td><b>Celular:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Celular" name="celularF" id="celularF" required  value="${fiador_f.celularF}"></td>
                <td><div id='divcelularF'> </div></td>
            </tr>
            <!--        
            <tr>
                <td><b>* Lugar/Fecha</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="lugar_fecha" name="lugar_fecha" id="lugar_fecha" required onblur="validar('lugar_fecha');" value="${fiador_f.lugar_fecha}" ></td>
                <td><div id='divlugar_fecha'> </div></td>
            </tr> -->
           
            <tr>
                <td colspan="6"><center><br />
        
            <br />
                      <input type="submit" class="btn btn-info" value="Guardar Modificaciones" />
            </center></td>
            </tr>
           
       </table>
                  
        <br /><br />
        </form></center>
      </c:forEach>        
    </div> <!-- /container -->
    <script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
<%@include file="../../administracion/footer.jsp" %>
<script>
    $(document).ready(function(){
        /**Cargando los municipios**/
        cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp', ${id_municipio});
    })
</script>