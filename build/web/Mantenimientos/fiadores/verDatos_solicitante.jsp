<%-- 
    Document   : verFiador
    Created on : 12-11-2013, 07:34:29 PM
    Author     : jorge
--%>


<%@page import="java.sql.ResultSet"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../administracion/header.jsp" %>
<%@page  import="java.util.Date,Conexion.conexion" %>
<style type="text/css">
     body {
          padding-top: 60px;
          padding-bottom: 60px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 650px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        
        .container
        {
            max-width:  700px;
            margin: 0 auto;
            text-align: justify;
        }
        
        .table
         {
            width:700px;
            
         }
         
    </style>
<%! ResultSet resultado = null; %>
<%
    
    if (request.getParameter("id") != null)
    {
        int id = Integer.parseInt(request.getParameter("id"));
        String qr ="select * from solicitante where idSolicitante="  + id;
        // estableciendo conexion
        conexion con = new conexion();
        con.setRS(qr);
        resultado = (ResultSet) con.getRs();
        resultado.next();
       
    }
%>
<div class="container">
      
    <div align="center"><br /><br />
        <h2>Datos Fiador</h2><br />
            
           <table class="table">
            <tr>
                <td><b>* Nombres:</b></td>
                <td><br /><input type="text" class="form-control" size="40" name="nombreFiador" id="nombreFiador" ReadOnly value="<%= resultado.getString("nombreFiador") %>"></td>
                
           
                <td><b>* Apellido:</b></td>
                <td><br /><input type="text" class="form-control"  size="40"  name="apellidoFiador" id="apellidoFiador" ReadOnly value="<%= resultado.getString("apellidoFiador") %>" ></td>
               
            </tr>
            
            <tr>
                <td><b>* Edad:</b></td>
                <td><br />
                    <input type="number" class="form-control" placeholder="Edad" name="edadF" id="edadF" readonly value="<%= resultado.getString("edadF") %>" /></td>
               
            
                <td><b>* DUI:</b></td>
                <td><br /><input type="text" size="40" class="form-control" name="DUIF" id="DUIF"  readonly value="<%= resultado.getString("DUIF") %>" /></td>
               
        
            </tr>
          
            
            <tr>
                <td><b>* NIT:</b></td>
                <td><br /><input type="text" class="form-control"  name="NITF" id="NITF" readonly value="<%= resultado.getString("NITF") %>" /></td>
                
           
                <td><b>* Domicilio:</b></td>
                <td><br /><input type="text" class="form-control"  name="DomicilioF" id="DomicilioF"  readonly value="<%= resultado.getString("DomicilioF") %>" ></td>
               
            </tr>
           
            <tr>
                <td><b>* Profesi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control"  name="profesionF" id="profesionF" readonly value="<%= resultado.getString("profesionF") %>" ></td>
                
            
                <td><b>* Lugar Trabajo</b></td>
                <td><br /><input type="text" class="form-control"  name="lugarTrabajoF" id="lugarTrabajoF" readonly value="<%= resultado.getString("lugarTrabajoF") %>" ></td>
              
            </tr>
            
            
            <tr>
                <td><b>* Cargo:</b></td>
                <td><br /><input type="text" class="form-control"  name="cargoF" id="cargoF" readOnly value="<%= resultado.getString("cargoF") %>" ></td>
                
           
                <td><b>* Tel&eacute;fono Trabajo:</b></td>
                <td><br /><input type="text" class="form-control"  name="telefono_trabajo"  id="telefono_trabajo" readonly value="<%= resultado.getString("telefono_trabajo") %>" ></td>
                
            </tr>
           
            <tr>
                <td><b>Extensi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control" name="ext" id="ext" readonly value="<%= resultado.getString("ext") %>" ></td>
                
            
                <td><b>* Direcci&oacute;n Particular:</b></td>
                <td><br /><input type="text" class="form-control"  name="direccionParticular" id="direccionParticular" readonly value="<%= resultado.getString("direccionParticular") %>" ></td>
                
            </tr>
            
            <tr>
                <td><b>Tel&eacute;fono Residencia:</b></td>
                <td><br /><input type="text" class="form-control"  name="telResidenciaF" id="telResidenciaF" readonly value="<%= resultado.getString("telResidenciaF") %>" ></td>
                
                <td><b>Celular:</b></td>
                <td><br /><input type="text" class="form-control"  name="celularF" id="celularF" readonly value="<%= resultado.getString("celularF") %>" ></td>
               
            </tr>
            
                       
            <tr>
                <td><b>* Lugar/Fecha</b></td>
                <td><br /><input type="text" class="form-control"  name="lugar_fecha" id="lugar_fecha" readonly value="<%= resultado.getString("lugar_fecha") %>" ></td>
               
            </tr>
           
            <tr>
                <td colspan="6"><center><br />
           
            <br />
            <form action="principalFiadores.jsp" method="post">
                      <input type="submit" class="btn btn-info" value="Regresar" />
            </form>
                
            </center></td>
            </tr>
           
       </table>
         
     <div>
  </div>
   
 <br /><br /><br />
  
 </div>
 
<%@include file="../../administracion/footer.jsp" %>