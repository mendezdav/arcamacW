<%-- 
    Document   : nuevoPrestamo
    Created on : 15-oct-2013, 11:39:07
    Author     : Pochii
--%>

<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.ResultSet" %> 
<%@page import="clases.ActualizacionPagos" %> 
<%@page import="Conexion.conexion" %>
<%@page import="java.util.Calendar" %>    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="clases.BeanResolucionPrestamo" id="resolucion" scope="session" />   
<%@include file="../administracion/header.jsp" %> 
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                align:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
            }
            .container h2
            {
                font-family: Century Gothic;
            }  
            .container h4{font-weight: bold; text-decoration: underline; text-align: left;}
            .left{text-align: left; float: left;}
            fieldset{padding-left: 50px;}
        </style>
    <script language="JavaScript">
        function imprimir(ruta)
        {
            window.open(ruta,'','resizable=yes,scrollbars=yes,height=400,width=800');
            window.history.back(-1);

        }
    </script>
    <script language="JavaScript" >
            function llamar(ruta)
            {
                //window.location.href = ruta + "/reportes/prestamosPlazo/principalPrestamosPlazo.jsp";
                window.open(ruta + '/ReporteResolucionPrestamoRefinanciado','','resizable=yes,scrollbars=yes,height=400,width=800');
            }
    </script>
    
<%
        //Estableciendo Conexion
        
        conexion con = new conexion();
        String codigo = request.getParameter("codigo");
        String correlativo = ""; 
        double montoPrestamo = 0.0;
        String prestamoAnterior = "";
        Calendar c = new GregorianCalendar();
        String dia_a = Integer.toString(c.get(Calendar.DATE));
        String mes_a = Integer.toString(c.get(Calendar.MONTH)+1);
        String anio_a = Integer.toString(c.get(Calendar.YEAR));
        String fecha = anio_a+"-"+mes_a+"-"+dia_a;
                
        ResultSet rs = null;
        int idSolicitante = 0;
        con.setRS("SELECT count(*) as correlativo FROM prestamos");
        rs = con.getRs();
        if(rs.next())
            correlativo = rs.getString("correlativo"); 
        con.setRS("SELECT * FROM resolucionPrestamo WHERE codigoPrestamo='" + codigo + "'");   
        rs = con.getRs(); 
        int count = 1;
        /*
         *  DECLARANDO VARIABLES
         */
        while(rs.next())
        {
            if(count ==1){
                idSolicitante = Integer.parseInt(rs.getString("idSolicitante"));
                montoPrestamo = Double.parseDouble(rs.getString("cantidad"));
%>
<%
            String nombreImg = "logoFactura";
            nombreImg = ruta + "/images/" + nombreImg + ".jpg";
%>
            <%--Estableciendo datos en los beans --%>
            <jsp:setProperty name="resolucion" property="idSolicitante"     value='<%=rs.getString("idSolicitante")%>' />  
            <jsp:setProperty name="resolucion" property="nombres"            value='<%=rs.getString("nombres")  %>'/> 
            <jsp:setProperty name="resolucion" property="apellidos"         value='<%=rs.getString("apellidos")  %>'/>
            <jsp:setProperty name="resolucion" property="sueldo"            value='<%=rs.getDouble("sueldo")  %>'/>
            <jsp:setProperty name="resolucion" property="descuentos"        value='<%=rs.getDouble("descuentos")  %>'/>
            <jsp:setProperty name="resolucion" property="liquidez"          value='<%=rs.getDouble("liquidez")  %>'/>
            <jsp:setProperty name="resolucion" property="cantidad"          value='<%=rs.getDouble("cantidad")  %>'/>
            <jsp:setProperty name="resolucion" property="codigoPrestamo"    value='<%=rs.getString("codigoPrestamo")  %>'/>
            <jsp:setProperty name="resolucion" property="nombreFiador1"     value='<%=rs.getString("nombreFiador")  %>'/>
            <jsp:setProperty name="resolucion" property="apellidoFiador1"   value='<%=rs.getString("apellidoFiador")  %>'/>
            <jsp:setProperty name="resolucion" property="salarioF1"         value='<%=rs.getDouble("salarioF")  %>'/>
            <jsp:setProperty name="resolucion" property="deduccionesF1"     value='<%=rs.getDouble("deducciones")  %>'/>
            <jsp:setProperty name="resolucion" property="liquidezF1"        value='<%=rs.getDouble("liquidezF")  %>'/>
            <jsp:setProperty name="resolucion" property="img_url"           value='<%=nombreImg%>' />
            <jsp:setProperty name="resolucion" property="correlativo"           value='<%=correlativo%>' />
<%       
            }
            if(count == 2){
%>
                <jsp:setProperty name="resolucion" property="nombreFiador2"     value='<%=rs.getString("nombreFiador")  %>'/>
                <jsp:setProperty name="resolucion" property="apellidoFiador2"   value='<%=rs.getString("apellidoFiador")  %>'/>
                <jsp:setProperty name="resolucion" property="salarioF2"         value='<%=rs.getDouble("salarioF")  %>'/>
                <jsp:setProperty name="resolucion" property="deduccionesF2"     value='<%=rs.getDouble("deducciones")  %>'/>
                <jsp:setProperty name="resolucion" property="liquidezF2"        value='<%=rs.getDouble("liquidezF")  %>'/> 
<%
            }
            count++;
        }
               
       con.setRS("SELECT plazo, cuotas, tasa_interes FROM prestamos WHERE codigoPrestamo ='"+codigo+"'");
        ResultSet ots = con.getRs();
        while(ots.next()){
            %>
                <jsp:setProperty name="resolucion" property="cuotas"  value='<%=ots.getDouble("cuotas")%>'/>
                <jsp:setProperty name="resolucion" property="plazo" value='<%=ots.getInt("plazo")%>'/>
                <jsp:setProperty name="resolucion" property="tasaInteres" value='<%=ots.getDouble("tasa_interes")%>'/>
                <jsp:setProperty name="resolucion" property="tasaInteresMoratorio" value='<%=ots.getDouble("tasa_interes")%>'/>
            <%
        }
       
       if(request.getParameter("r")!=null && request.getParameter("r")!=""){
            con.setRS("SELECT codigoPrestamo FROM prestamos WHERE estado = 'Activo' AND idSolicitante="+idSolicitante);
            ResultSet cod = con.getRs();
            
            while(cod.next()){
                prestamoAnterior = cod.getString("codigoPrestamo");
            }
            String fechaInicio = "";
            if(request.getParameter("fecha")!=null && request.getParameter("fecha")!=""){
                fechaInicio = request.getParameter("fecha");
            }else{
                fechaInicio = fecha;
            }
            ActualizacionPagos act = new ActualizacionPagos(prestamoAnterior);
            act.recorrido("SELECT * FROM prestamos WHERE codigoPrestamo = '"+prestamoAnterior+"'", fechaInicio, 0);
            %>
            <jsp:setProperty name="resolucion" property="capital" value='<%=act.getSaldo()%>'/> 
            <jsp:setProperty name="resolucion" property="intereses" value='<%=act.getInteres()%>'/> 
            <jsp:setProperty name="resolucion" property="interesMoratorio" value='<%=act.getInteresMoratorio()%>'/>
            <jsp:setProperty name="resolucion" property="totalSaldo" value='<%=act.getSaldo() + act.getInteres() + act.getInteresMoratorio()%>'/>
            <%
        }
        
        con.setRS("SELECT costo FROM papeleria WHERE "+montoPrestamo+">=de AND "+montoPrestamo+"<=a");
        ResultSet mt = con.getRs();
        while(mt.next()){
            %>
            <jsp:setProperty name="resolucion" property="papeleria" value='<%=mt.getDouble("costo")%>'/>
            <%
        }
        
        con.cerrarConexion();
        HttpSession Session = request.getSession();
        Session.setAttribute("resolucion", resolucion); 
        // cambios refinanciamiento
        String refinanciamiento = request.getParameter("r");
        if (refinanciamiento.equals("Refinanciado"))
        {
            %>
             <body onload="llamar('<%=ruta%>');" >
            <%
        }
%>


        <ol class="breadcrumb">
            <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
            <li><a href="<%=ruta%>/prestamos/panelPrestamos.jsp">Pr&eacute;stamos</a></li>
            <li class="active">Resoluci&oacute;n de pr&eacute;stamos </li>
        </ol>
        <div class="container" align="center">
            <h2>Resoluci&oacute;n de nuevo pr&eacute;stamo No._________</h2>
            <label>Informaci&oacute;n de pr&eacute;stamo</label>
            <fieldset style="border: 2px outset #003bb3;">
                <form name="frmResolucion" action="nuevoSolicitanteStep2.jsp" method="POST">
                    <br />
                    <div class="row">
                        <div class="col-md-3">
                           <b>CANTIDAD SOLICITADA ($): </b>
                        </div>
                        <div class="col-md-3">
                             <%=resolucion.getCantidad()%>
                        </div>
                        <div class="col-md-2">
                            <b> C&Oacute;DIGO No.: </b>                            
                        </div>
                        <div class="col-md-2">
                             <%=resolucion.getCodigoPrestamo()%>
                        </div>
                    </div>
                    <br />
                    <h4>A- DATOS GENERALES DEL SOLICITANTE </h4>
                    <div class='row'>
                        <div class='col-md-3'>
                           <b> 1. Nombres seg&uacute;n DUI: </b>
                        </div>
                        <div class="col-md-2 left">
                            <%=resolucion.getNombres()%>
                        </div> 
                         <div class="col-md-2 left"> 
                             <b> Apellidos: </b>
                        </div> 
                        <div class="col-md-2 left"> 
                            <%=resolucion.getApellidos()%> 
                        </div> 
                    </div> 
                    <br />
                    <div class="row"> 
                        <div class="col-md-2"> 
                            &nbsp;&nbsp;<b> 2. Salario ($): </b>
                        </div>
                        <div class="col-md-1">
                            <%=resolucion.getSueldo()%>
                        </div>
                        <div class="col-md-2 left"> 
                            <b> Deducciones($):  </b>
                        </div>
                        <div class="col-md-1 left"> 
                            <%=resolucion.getDescuentos()%>
                        </div> 
                         <div class="col-md-2 left"> 
                            <b>Liquidez($):  </b>
                        </div>
                        <div class="col-md-1 left"> 
                            <%=resolucion.getLiquidez()%>
                        </div>
                    </div>
                    <br /><br />
                    <h4>B- DATOS DEL 1er FIADOR. </h4>
                    <div class='row'>
                        <div class='col-md-3'>
                            <b>1. Nombres seg&uacute;n DUI: </b>
                        </div>
                        <div class="col-md-2 left">
                            <%=resolucion.getNombreFiador1()%>
                        </div>
                         <div class="col-md-2 left">
                           <b> Apellidos:</b>
                        </div>
                        <div class="col-md-3 left">
                            <%=resolucion.getApellidoFiador1()%>
                        </div> 
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-2">
                            &nbsp;&nbsp;<b> 2. Salario ($) </b>:
                        </div>
                        <div class="col-md-1">
                            <%=resolucion.getSalarioF1()%>
                        </div>
                        <div class="col-md-2">
                           <b> Deducciones($): </b>
                        </div>
                        <div class="col-md-1">
                            <%=resolucion.getDeduccionesF1() %>
                        </div>
                         <div class="col-md-2">
                           <b> Liquidez($) </b>: 
                        </div>
                        <div class="col-md-1">
                            <%=resolucion.getLiquidezF1() %>
                        </div>
                    </div>
                    <br /><br />
                    <%
                        if(count > 2) 
                        {
                    %>
                    <h4>C- DATOS DEL 2do FIADOR</h4>
                    <div class='row'>
                        <div class='col-md-3'>
                            <b>1. Nombres seg&uacute;n DUI: </b>
                        </div>
                        <div class="col-md-2 left">
                            <%=resolucion.getNombreFiador2() %> 
                        </div> 
                         <div class="col-md-2 left"> 
                            <b>Apellidos: </b>
                        </div> 
                        <div class="col-md-3 left"> 
                            <%=resolucion.getApellidoFiador2() %>
                        </div> 
                    </div> 
                    <br />
                    <div class="row"> 
                        <div class="col-md-2"> 
                            &nbsp;&nbsp;<b>2. Salario ($): </b> 
                        </div>
                        <div class="col-md-1"> 
                            <%=resolucion.getSalarioF2() %>
                        </div> 
                        <div class="col-md-2"> 
                            <b>Deducciones($):  </b>
                        </div>
                        <div class="col-md-1"> 
                            <%=resolucion.getDeduccionesF2() %>
                        </div>
                         <div class="col-md-2">
                            <b>Liquidez($): </b> 
                        </div>
                        <div class="col-md-1">
                            <%=resolucion.getLiquidezF2() %>
                        </div>
                    </div>
                       <%
                            }
                        %>
                    <H4>D- OBSERVACIONES</h4>
                    <div class="row">
                        <div class="col-md-1">
                            APROBADA
                        </div>
                        <div class="col-md-2">
                           ___________________
                        </div>
                        <div class="col-md-1">
                            DENEGADA
                        </div>
                        <div class="col-md-2">
                            __________________
                        </div>
                        <div class="col-md-1">
                            PENDIENTE
                        </div>
                        <div class="col-md-2">
                            __________________
                        </div>
                    </div>
                    <div class="row">
                        <br />
                        <div class="col-md-3">
                            SEG&Uacute;N ACTA No. ____________,
                        </div>
                        <div class="col-md-8">                            
                           DEL CONSEJO DE ADMINISTRACI&Oacute;N DE FECHA__________________________________
                        </div>
                    </div>
                    <br />
                    <div class='row'>
                        <div class='col-md-2 left'>
                            MONTO ($): _____
                        </div>
                        <div class='col-md-2 left'>
                           PLAZO: ____ MESES
                        </div>
                        <div class='col-md-2  left'>
                            CUOTA ($): _____
                        </div>
                        <div class='col-md-3  left'>
                            INTER&Eacute;S NORMAL (%): _______
                        </div>
                        <div class='col-md-3  left'>
                            INTER&Eacute;S MORA: _________
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class='row' style='margin-left: 20%'>
                        <div class='col-md-4'>
                            F._____________________ PRESIDENTE 
                        </div>
                        <div class='col-md-4'>
                            F._____________________ VICEPRESIDENTE
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class='row' style='margin-left: 20%'>
                        <div class='col-md-4'>
                            F.____________________ SECRETARIO
                        </div>
                        <div class='col-md-4'>
                            F._____________________ PRIMER VOCAL
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class='row' style='margin-left: 35%'>
                        <div class='col-md-4'>
                            F.____________________ SEGUNDO VOCAL
                        </div>
                    </div>
                </form>
            </fieldset>
                    <a style="cursor:pointer;" onclick="imprimir('<%=ruta%>/ReporteResolucionPrestamo')">Imprimir</a>
        <hr />
      </diV>
<%@include file="../administracion/footer.jsp" %>