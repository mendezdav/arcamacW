<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="analisis" class="clases.nuevoPrestamo" scope="page" />
<jsp:setProperty name="analisis" property="cantidad" param="cantidad" />
<jsp:setProperty name="analisis" property="plazo" param="plazo" />
<jsp:setProperty name="analisis" property="interes" param="interes" />
<%--<jsp:setProperty name="analisis" property="interes_mora" />--%>

<%
    analisis.setIva();
    analisis.setInteres_mora();
    if(request.getParameter("type").equals("an")){ 
%>
<!--Estableciendo propiedades del bean -->

<br />
<fieldset style="width:500px; border: 1px #0099FF dashed;">
    <legend>Detalle del pr&eacute;stamo</legend>
    <p>
        El pr&eacute;stamo solicitado se desglosar&iacute;a de la siguiente manera: 
    </p>
    <table class="table">
        <tr>
            <td colspan="2"><b>Cuota mensual:</b></td>
            <td colspan="2">
                <div id="cuota">
                    $ <jsp:getProperty name="analisis" property="cuotas" />
                </div>
            </td>
        </tr>
        <tr>
            <td><b>Intereses aplicados al pr&eacute;stamo:</b></td>
            <td>
                $ <jsp:getProperty name="analisis" property="intereses_pago" />
            </td>
            <td><b>IVA aplicado: </b></td>
            <td>
                $ <jsp:getProperty name="analisis" property="iva" />
           </td>           
        </tr> 
        <tr>
            <td><b>Cargo por mora diario:</b></td>
            <td>
                $ <jsp:getProperty name="analisis" property="interes_mora" />
            </td>
        </tr>
        <!-- colocando el session datos necesarios para generar la tabla de amortizacion ideal
        -->
        <%
        HttpSession SessionActual = request.getSession();
        SessionActual.setAttribute("beanAnalisis", analisis);
        Calendar calendario = Calendar.getInstance();
        String fecha = calendario.get(Calendar.DATE) + "/";
        String mes = String.format("%02d", (calendario.get(Calendar.MONTH) +1));
        fecha+=mes + "/" + calendario.get(Calendar.YEAR);
        String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
        SessionActual.setAttribute("fecha", fecha);
        %>
        <tr>
            <td colspan="2"><br />
                <input type="button" class="btn btn-info" value="Tabla Amortizaci&oacute;n" 
                       onclick="window.open('/arcamacW/ReporteTablaAmortizacionIdel?tabla=a','','resizable=yes,scrollbars=yes,height=400,width=800');" />
            </td>
        </tr>
    </table>     
</fieldset>
<%
    }
    else if(request.getParameter("type").equals("new")){%>
<jsp:getProperty name="analisis" property="cuotas" />
<%
    }//fin else
%>


