<%-- 
    Document   : panelPrestamos
    Created on : 18-oct-2013, 0:21:06
    Author     : Pochii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<%--<%@include  file="../administracion/ValSession.jsp" %> --%>
        <style>
             body{ padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 30px 15px;}
            .col-s a{height: 76px;}
            .col-p{background-color: #7b34a5;}
        </style>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Pr&eacute;stamos</li>
           </ol>
                <h2>Panel principal de pr&eacute;stamos</h2>
                <br/>
                <br/>
            <div class="row">
                <div class="col-md-2 col-l" style="text-align: center;">
                    <a class="btn btn-success" style="display:block; width: 150px;" href="prePrestamo.jsp?opc=noref">
                        <br/>
                        <i class="fa fa-file-text-o fa-3x"></i> 
                        <br/>
                        <br/>
                        Nuevo pr&eacute;stamo
                        
                    </a>
                </div>
                <div class="col-md-2 col-l" style="text-align: center;">
                    <!-- <a class="btn btn-success" href="listadoPrestamo.jsp"><i class="fa fa-list-alt fa-3x"></i> Listado de pr&eacute;stamos</a> -->
                    <a class="btn btn-success" style="display:block; width: 150px;" href="panelPrestamoEstado.jsp">
                        <br/>
                        <i class="fa fa-list-alt fa-3x"></i> 
                        <br/><br/>
                        Lista de pr&eacute;stamos
                    </a>
                </div>
                <div class="col-md-2 col-l" style="text-align: center;">
                   <a class="btn btn-success" style="display:block; width: 150px;" href="<%=ruta%>/Pagos/PrincipalPago.jsp">
                       <br/>
                       <i class="fa fa-money fa-3x"></i>
                       <br/>
                       <br/>
                       Cobro de cuotas
                   </a>
               </div>
              <!-- <div class="col-md-2 col-l" style="text-align: center;">
                   <a class="btn btn-success" style="display:block; width: 150px;" href="ruta/prestamos/cancelar/principal.jsp">
                       <br/>
                       <i class="fa fa-money fa-3x"></i>
                       <br/>
                       <br/>
                       Ponerse al D&iacute;a
                   </a>
               </div> --> 
           </div>
           <div class="row">
               <br/>
               <div class="col-md-2 col-l" style="text-align: center;">
                   <a class="btn btn-success" style="display:block; width: 150px;" href="preRefinanciamiento.jsp?opc=ref">
                       <br/>
                       <i class="fa fa-repeat fa-3x"></i> 
                       <br/>
                       <br/>
                       Refinanciamiento 
                   </a>
               </div>
               <div class="col-md-2 col-l" style="text-align: center;">
                        <a class="btn btn-success" style="display:block; width: 150px;" href="analisisNuevo.jsp">
                            <br/>
                            <i class="fa fa-usd fa-3x"></i> 
                            <br/>
                            <br/>
                            An&aacute;lisis de nuevos
                        </a>
                </div>
               
               <div class="col-md-2 col-l" style="text-align: center;">
                    <a class="btn btn-success" style="display:block; width: 150px;" href="EliminarPrestamo/principalPrestamosEliminar.jsp">
                        <br/>
                        <i class="fa fa-list-alt fa-3x"></i> 
                        <br/>
                        <br/>
                        Eliminar pr&eacute;stamos
                    </a>
                </div>
           </div>         
        </div>
<%@include file="../administracion/footer.jsp" %>