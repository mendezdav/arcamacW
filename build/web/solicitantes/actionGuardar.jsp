<%-- 
    Document   : actionGuardar
    Created on : 08-dic-2013, 21:16:08
    Author     : Pochii
--%>
<%@page import="clases.UploadFotos"%>
<%@page import="java.io.File"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.TreeMap"%>
<%@page import="Conexion.conexion" %>
<jsp:useBean id="solicitantes" class="clases.solicitantes" scope="session" /> 
<%
    String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    TreeMap<String, String>solicitantesPost = new TreeMap<String, String>();
    conexion con = new conexion();
    /***Tomando parametros*******/
    if(request.getParameter("complete")==null)
    {
%>
        <jsp:forward page="panelSolicitantes.jsp">  
            <jsp:param name="erno" value="Hubo un error, la p&aacute;gina no fue llamada desde el formulario" />
        </jsp:forward>
<%
    }
    if(request.getParameter("complete").equals("1"))
    {
 %>
        <%--Estableciendo las propiedades recibidas del paso 1 --%>
        <jsp:setProperty name="solicitantes" property="idSolicitante" value="null" />
        <jsp:setProperty name="solicitantes" property="nombre1" param="nombre1" />
        <jsp:setProperty name="solicitantes" property="nombre2" param="nombre2" />
        <jsp:setProperty name="solicitantes" property="apellido1" param="apellido1" />
        <jsp:setProperty name="solicitantes" property="apellido2" param="apellido2" />
        <jsp:setProperty name="solicitantes" property="DUI" param="DUI" />
        <jsp:setProperty name="solicitantes" property="NIT" param="NIT" />
        <jsp:setProperty name="solicitantes" property="domicilio" param="domicilio" />
        <jsp:setProperty name="solicitantes" property="edad" param="edad" />
        <jsp:setProperty name="solicitantes" property="tel_residencia" param="tel_residencia" />
        <jsp:setProperty name="solicitantes" property="celular" param="celular" />
        <jsp:setProperty name="solicitantes" property="fecha_nacimiento" param="fecha_nacimiento" />
        <jsp:setProperty name="solicitantes" property="estado_sistema" param="estado_sistema" />
        <jsp:setProperty name="solicitantes" property="correo" param="correo" />
        <jsp:setProperty name="solicitantes" property="estado_civil" param="estado_civil" />
        <jsp:setProperty name="solicitantes" property="id_departamento" param="id_departamento" />
        <jsp:setProperty name="solicitantes" property="id_municipio" param="id_municipio" />
        <jsp:setProperty name="solicitantes" property="fecha_expedicion_DUI" param="fecha_expedicion_DUI" />
        <jsp:setProperty name="solicitantes" property="lugar_nacimiento" param="lugar_nacimiento" />
        <jsp:setProperty name="solicitantes" property="lugar_expedicion_DUI" param="lugar_expedicion_DUI" />
 <%
    }
   else if(request.getParameter("complete").equals("2"))
   {
 %>
        <jsp:setProperty name="solicitantes" property="profesion" param="profesion" />
        <jsp:setProperty name="solicitantes" property="lugarTrabajo" param="lugarTrabajo" />
        <jsp:setProperty name="solicitantes" property="tiempo_servicio" param="tiempo_servicio" />
        <jsp:setProperty name="solicitantes" property="cargo" param="cargo" />
        <jsp:setProperty name="solicitantes" property="tel_trabajo" param="tel_trabajo" />
        <jsp:setProperty name="solicitantes" property="ext" param="ext" />
        <%--Informacion financiera --%>
        <jsp:setProperty name="solicitantes" property="ingresos" param="ingresos" />
        <jsp:setProperty name="solicitantes" property="descuentos" param="descuentos" />
        <jsp:setProperty name="solicitantes" property="liquidez"  param="liquidez" />
 <%
    }
    else if(request.getParameter("complete").equals("3"))
    {
%>
        <%--Estableciendo las propiedades recibidas del paso 1 --%>
        <jsp:setProperty name="solicitantes" property="nombre_conyuge" param="nombre_conyuge" />
        <jsp:setProperty name="solicitantes" property="apellidos_conyuge" param="apellidos_conyuge" />
        <jsp:setProperty name="solicitantes" property="edad_conyuge" param="edad_conyuge" />
        <jsp:setProperty name="solicitantes" property="profesion_conyuge" param="profesion_conyuge" />
        <jsp:setProperty name="solicitantes" property="lugar_direccion_trabajo_conyuge" param="lugar_direccion_trabajo_conyuge" />
<%
    }
%>
<%--Armando el Mapa --%>
<%
    solicitantesPost = solicitantes.mapa(); //Llamando al metodo que forma el mapa
    try
    {
        out.println(con.insertQuery(solicitantesPost, "solicitante"));
        
        String query ="";
        /**En el caso que solo tenga un apellido**/
        if(solicitantes.getApellido2().equals(""))
            query = "SELECT idSolicitante, CONCAT(LEFT(apellido1, 1), LEFT(apellido1,1)) as cod FROM solicitante order by idSolicitante DESC LIMIT 1";
        /**Caso contrario***/
        else
            query = "SELECT idSolicitante, CONCAT(LEFT(apellido1, 1), LEFT(apellido2,1)) as cod FROM solicitante order by idSolicitante DESC LIMIT 1";
       
        con.setRS(query);
        ResultSet rs = con.getRs();
        rs.next();
        int idnext = rs.getInt("idSolicitante");
        String correlativo = "";
        /**
         * apellidoCod, variable que contiene las iniciales de los apellidos
         */
        String apellidoCod = rs.getString("cod");
        rs.close();
        con.cerrarConexion();
        
        con  = new conexion();
        ResultSet rs2;
        /**Estableciendo la query**/
        query = "SELECT count(idSolicitante) as correlativo FROM solicitante WHERE LEFT(codigo, 2)='" + apellidoCod + "' LOCK IN SHARE MODE";
        
        con.setRS(query);
        rs2 = con.getRs(); 
        rs2.next();
        correlativo = rs2.getString("correlativo");
        //En el caso que sea el primero
        int cor = 0;
        cor = (Integer.parseInt(correlativo) + 1);     
        correlativo = cor + "";
        if(Integer.parseInt(correlativo) < 10)
            correlativo = "0" + correlativo;      //Concatenando el numero correlativo
        apellidoCod = apellidoCod + correlativo; 
        con.actualizar("UPDATE solicitante SET codigo = '" + apellidoCod + "' WHERE idSolicitante = " + idnext);
 
        try{
            /***************PARTE DE IMAGEN*******************/
            /**
            *Renombrando imagen de archivo
            */
            String url;
            File pic;
            /**Si no subio ninguna imagen**/
            if(solicitantes.getImg_url().equals(""))
            {
                url = getServletContext().getRealPath("images");
                pic = new File(url + "\\nodisponible.jpg");
                url =  getServletContext().getRealPath("solicitantes\\images"); //Actualizando ruta
                solicitantes.setImg_url(url +"\\nodisponible.jpg");
                if(UploadFotos.Upload(url, pic, "nodisponible.jpg", "tmp.jpg"))
                    pic = new File(url + "\\tmp.jpg"); 
                else
                    out.println("Hubo un error al subir ");
            }
            else
            {
                url =  getServletContext().getRealPath("solicitantes\\images");
                pic =  new File(url + "\\" + solicitantes.getImg_url());
            }
            
            url =  getServletContext().getRealPath("solicitantes\\images");
            pic.renameTo(new File(url + "\\" + "thumbnail_" + idnext + "." + UploadFotos.getFileExtension(solicitantes.getImg_url())));
            /**Reiniciando el bean**/
            solicitantes.reiniciar();
        }
        
        catch(Exception e){
            response.sendRedirect(ruta+"/solicitantes/panelSolicitantes.jsp?erno='" + e.getMessage() + "'");
        }
        if(session.getAttribute("tipo").equals("pre"))
        {
            response.sendRedirect(ruta + "/prestamos/nuevoPrestamo.jsp?exito=in&idSolicitante="+idnext); 
        }
        else if(session.getAttribute("tipo").equals("sol")) 
            response.sendRedirect(ruta + "/solicitantes/panelSolicitantes.jsp?exito=in"); 
        con.cerrarConexion();
    }
    catch(Exception e) 
    {
        //response.sendRedirect(ruta+"/solicitantes/panelSolicitantes.jsp?erno=1");
        out.println("no pude por: " + e.getMessage()); 
    }
    con.cerrarConexion();
%> 
