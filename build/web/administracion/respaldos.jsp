<%-- 
    Document   : respaldos
    Created on : 12-06-2013, 08:03:39 PM
    Author     : jorge
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<style>
    .container
    {
        max-width:  900px;
        margin: 0 auto;
        text-align: justify;
    }
</style>
<div class="container"><br />
    <center>
    <h2>Herramienta Generar Respaldo </h2>
    <br /><br /></center>
    <h4>
        Pregunta Frecuente:<br /> </h4>
    <h5>¿De que me sirve dichos respaldos, solo me consumen espacio?<br /><br />
        La respuesta es dicha es sencilla, al tener un sistema inform&aacute;tico que maneje cualquier tipo
        de informaci&oacute;n, sin importar la naturaleza de la misma, es necesario tener una copia
        de seguridad, ya que los sistemas fallan, ya sea por su longevidad o por desperfectos, as&iacute; como 
        tambi&eacute;n ataques por agentes externos(virus, malware, spyware,etc). <b>Se recomienza tener copias 
            mensuales, en lugares diferentes a la maquina central</b>
    </h5>
    
    <div align="center"><br /><br />
        
        <form action="ActionRespaldos.jsp" method="POST" name="formulario" id="formulario">
             <input type="button" class="btn btn-info" onclick="envio(1,'formulario','<%=ruta%>');" value="Generar Respaldo " >
             <input type="button" class="btn btn-info" onClick="envio(2,'formulario','<%=ruta%>');" value=" Respaldos " >
             
             <!-- poner mensaje de estado! --><br /><br />
             <font color="red">
             <%
                
                 if (request.getParameter("estado") != null)
                 {
                     String estado = request.getParameter("estado");
                     if (estado.equals("1"))
                        out.print("<div class=\"alert alert-warning\">--- Archivo no pudo ser generado ---</div>");
                     if (estado.equals("2"))
                        out.print("<div class=\"alert alert-success\">--- Archivo Generado con exito ---</div>");
                     if (estado.equals("3"))
                        out.print("<div class=\"alert alert-danger\">--- Operaci&oacute;n cancelada por el usuario ---</div>");
                 }
             %></font>
        </form>
    </div>
</div>
 <script language="javascript">
     
  function envio(tipo,form,ruta)
    {
         if(tipo == 1)
            {
                document.getElementById(form).action   = ruta + "/administracion/ActionRespaldos.jsp";
                document.getElementById(form).submit();
            }
            else
            {
               document.getElementById(form).action   = ruta + "/administracion/VerRespaldos.jsp";
                document.getElementById(form).submit();
            }
    }
    </script>
<%@include file="../administracion/footer.jsp" %>