<%-- 
    Document   : mostrarFiadorMantenimiento
    Created on : 12-11-2013, 06:29:40 PM
    Author     : jorge
--%>

<%@page import="javax.swing.JOptionPane"%>
<link rel="stylesheet" href="../../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../../js/paginador/funciones.js"></script>
<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script lang="javascript" src="../js/selectAjax.js"></script>
<script lang="javascript" src="../js/validacion.js"></script>
<%

    int RegistrosAMostrar=10;
    int PaginasIntervalo=3;
    int RegistrosAEmpezar=0;
    int PagAct = 1;
    int PagAct2 = 0;
    String cprt = "";
    String codigoPrestamo = request.getParameter("codigoPrestamo");
    if(codigoPrestamo!=null && codigoPrestamo!=""){
        cprt = "&codigoPrestamo=" + codigoPrestamo + "&r=1";
    }

    /** parametros que debe recibir **/
    String criterio = request.getParameter("criterio"); // criterio de busqueda en los registros
    if (criterio != null && criterio != "")
        criterio = criterio.replace("#","").replace("'","").replace("-","").replace("%"," ").replace("\\","").replace("$","")
                .replace("/","").replace("(","").replace(")","").replace("=","").replace("?","").replace(";","").replace(".","")
                .replace("{","").replace("}","").replace("[","").replace("]","").replace("*","").replace(",","");
    
    String pagR = request.getParameter("pagR");
    String pagina = request.getParameter("pagina");
    if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
    {
        RegistrosAEmpezar = 0;
        PagAct2 = Integer.parseInt(pagR);
    }
    try
    {
        PagAct2 = Integer.parseInt(pagR);
    }
    catch (Exception e1)
    {
        System.out.print("Error: " + e1.getMessage());
        PagAct2 = 1;
    }

    if (pagina==null || pagina==""){
        RegistrosAEmpezar=0;
        PagAct=2; // tendria q ser la 1, pero no lo es, por que mas adeante PagAct2-1
    }else{
        RegistrosAEmpezar=(Integer.valueOf(pagina).intValue()-1)* RegistrosAMostrar;
        PagAct=Integer.valueOf(pagina).intValue();
    }
    
    conexion con = new conexion();
    String qr="";
    int tmp = RegistrosAMostrar * ( PagAct2-1);
    if (tmp < 0) 
        tmp = 0;
    
    if (criterio != null || criterio !="" ) // si criterio no es nulo, colocar en cuadro busqueda
    {
      //qr ="select * from pagos where codigoPrestamo like '%" + request.getParameter("criterio")+ "%' limit " + tmp + "," + RegistrosAMostrar;
      qr ="select concat(nombreFiador,' ', apellidoFiador) as nombre, celularF,idfiadores,telResidenciaF,estadoFiador from fiadores"
      + " where  concat(nombreFiador,' ', apellidoFiador) like '%" + criterio + "%' limit " + tmp + "," + RegistrosAMostrar;
    }
    else
      qr ="select * from fiadores  limit " + tmp+ "," + RegistrosAMostrar;

      con.setRS(qr);
      ResultSet resultado = con.getRs();
      if (resultado.next())
      {
      out.println("<a href=\"javascript: void(0);\" onclick=\"$('#bcl').css('display', 'none');$('#contenedor').css('display', 'none');\">Cerrar</a>");
      out.println("<br/><br/>");
      out.println("<h4>Selección de fiador</h4><hr/>");
      out.println("<table class='tablita'>");
      out.println("<tr>");
      out.println("<th>Nombre</th>");
      out.println("<th>Celular</th>");
      out.println("<th>Telefono</th>");
      out.println("<th>Estado</th>");
      out.println("<th colspan=\"3\">Opciones</th>");
      out.println("</tr>");
      out.println("<tbody>");
      }
      resultado.previous();
      int contador = 0;
      while (resultado.next())
      {
      contador++;
      %>
      <tr id="grid">
        <td id="formnuevo"><%= resultado.getString("nombre") %></td>
        <td id="formnuevo"><%= resultado.getString("celularF") %></td>
        <td id="formnuevo"><%= resultado.getString("telResidenciaF") %></td>
        <td id="formnuevo">
            <%
                String estado = resultado.getString("estadoFiador");
                if (estado.equals("1"))
                    out.print("Activo");
                else
                    out.print("Inactivo");
            %>
        </td>
            <td id="formnuevo"><a href="javascrip: void(0)" onclick="seleccionarFiador(<%= resultado.getString("idfiadores")%>, '<%= resultado.getString("nombre")%>') " >Seleccionar</a></td>
        </tr>
   <%
        }
        if (contador == 0)
        {
            out.println("<div class=\"noData\"><br />No hay datos que mostrar</div>");
        }
        %>
        <%

          if (criterio != null || criterio !="" )
                qr ="select count(*) as total from fiadores"
                + " where  concat(nombreFiador,' ', apellidoFiador) like '%" + criterio + "%'";
          else
                qr ="select count(*) as total from fiadores";
          con.setRS(qr);
          ResultSet rs = con.getRs();
          rs.next();
          String NroRegistros =  rs.getString("total");
          int PagAnt=PagAct-1;
          int PagSig=PagAct+1;
          double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
          int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
          if(Res>0){
              PagUlt=Math.floor(PagUlt)+1;
          }
          out.println("</tbody>");
          out.println("</table><br />");
          out.println("<div style='width:300px; float:right'>");

          if(PagAct>(PaginasIntervalo+1)) {
            out.println("<a onclick=listaproductos('1'); class='paginador'><< Primero</a>");
            out.println("&nbsp;");
          }
          for ( int i = (PagAct2-PaginasIntervalo) ; i <= (PagAct2-1) ; i ++) {
            if(i>=1)
            {
              out.println("<a href='nuevoPrestamo.jsp?idSolicitante="+request.getParameter("idSolicitante")+"&pag="+ i+"&cantidadP="+request.getParameter("cantidadP")+"&plazoP="+request.getParameter("plazoP")+"&nfst="+request.getParameter("nfst")+cprt+"' class='paginador'>"+i+"</a>");
              out.println("&nbsp;");
            }
          }
          if (contador != 0){
              out.println("<span class='paginadoractivo'>"+PagAct2+"</span>");
              out.println("&nbsp;");
          }
          for ( int i = (PagAct2+1) ; i <= (PagAct2+PaginasIntervalo) ; i ++) 
          {
            if(i<=PagUlt) 
            {
              out.println("<a href='nuevoPrestamo.jsp?idSolicitante="+request.getParameter("idSolicitante")+"&pag="+ i+"&cantidadP="+request.getParameter("cantidadP")+"&plazoP="+request.getParameter("plazoP")+"&nfst="+request.getParameter("nfst")+cprt+"' class='paginador'>"+i+"</a>");
              out.println("&nbsp;");
            }
          }
          if(PagAct<(PagUlt-PaginasIntervalo)) 
          {
              out.println("<a href='nuevoPrestamo.jsp?idSolicitante="+request.getParameter("idSolicitante")+"&pag="+ PagUlt+"&cantidadP="+request.getParameter("cantidadP")+"&plazoP="+request.getParameter("plazoP")+"&nfst="+request.getParameter("nfst")+cprt+"' class='paginador'>Ultimo</a>");
          }
          out.println("</div>");
    %>
