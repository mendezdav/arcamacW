<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="idSolicitante" value='<%=request.getParameter("idSolicitante")%>' /> 
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:750px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                margin-left: 10%; 
            }
            .container h2
            {
                font-family: Century Gothic; 
            }
            legend{margin-left: 5%;}
        </style>
        <ol class="breadcrumb">
            <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
            <li><a href="<%=ruta%>/prestamos/panelPrestamos.jsp">Pr&eacute;stamos</a></li>
            <li class="active">Listado de pr&eacute;stamos</li>
        </ol>
        <div class="container">
            <div class="input-group" style='width: 300px; float: right;'>
                <span class="input-group-addon"><i class='fa fa-search'></i></span>
                 <input type='text' name='buscar' id='buscar' class='form-control' placeholder="Buscar"  onkeyup="buscarUsuario('<%=ruta%>/prestamos/mostrarLista.jsp', 'buscar','contenedor', 'sol&idSolicitante=${idSolicitante}');" />
            </div>
        </div>
            <legend>Lista de Pr&eacute;stamos </legend>
        <br />
        <div class="container" id="contenedor">
            
        </div>
        <div id="box_aprobar" style="visibility:hidden;">
            <div class="container" id="caja_aprobar">
                <input type="hidden" id="hidden" name="codigoPrestamo" />
                <label><b>Est&aacute; seguro/a que desea aprobar este pr&eacute;stamo?</b></label>
                <span>Esta opci&oacute;n es irreversible y la tabla de pagos se generar&aacute; a partir de 
                    el d&iacute;a de hoy.
                </span>
                <br />
                <center><a onclick="redireccionar();" class="btn btn-primary">Aprobar Pr&eacute;stamo</a></center>
            </div>
        </div>
<%@include file="../administracion/footer.jsp" %>
<script lang="javascript" src="<%=ruta%>/js/selectAjax.js"></script>
<script>
    $(function(){
       //Enviamos el parametro sol de Solicitante
       buscarUsuario('<%=ruta%>/prestamos/mostrarLista.jsp','buscar','contenedor', 'sol&idSolicitante=${idSolicitante}'); 
    });
    var div; //Div que contendrá el form a cargar
    function DialogoConBoton(event, header, caja, box, param){
            Dialog.header = header;
            Dialog.width  = "300px";
            Dialog.height = "200px";
            $("#hidden").val(param);
            div = $("#"+box).html();
            Dialog.content =  document.getElementById(caja).innerHTML;
            $("#" +caja).remove();
           
            Dialog.showDialog();
    }
    function redireccionar(){
      window.location.replace("cambiarEstado.jsp?codigoPrestamo=" + $("#hidden").val());
    }
</script>