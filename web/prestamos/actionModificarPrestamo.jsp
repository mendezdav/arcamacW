<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.TreeMap"%>
<%@page import="Conexion.conexion" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
   // tomando datos
    try
    {
        double cantidad = Double.parseDouble(request.getParameter("cantidadMandar").toString());
        int plazo       = Integer.parseInt(request.getParameter("plazo"));
        double tasa_interes = Double.parseDouble(request.getParameter("interes"));
        double cuotas = Double.parseDouble(request.getParameter("cuotas"));
        String tipo_pago = request.getParameter("tipo_pago");
        String destinoPrestamos = request.getParameter("destinoPrestamos");
        String estado = request.getParameter("estado");
        String fiador1 = request.getParameter("fiador1");
        String fiador2 = "";
        String codigoPrestamo = request.getParameter("codigoPrestamo");
        //codigoPrestamo = "rr0101";
        conexion con = new conexion();
        // eliminando los fiadores anteriores
        String qr = "delete from prestamo_fiador where id_prestamo='" + codigoPrestamo + "'";
        con.actualizar(qr);
        
        // modificando prestamo
        double interes = cantidad * (tasa_interes/100);
        qr = "update prestamos set cantidad=" +cantidad+ ",saldo="+ cantidad +","
                + "plazo="+ plazo +",interes=" + interes +",tasa_interes="+ tasa_interes +",cuotas="+ cuotas +",tipo_pago="+ tipo_pago +","
                + "destinoPrestamos='"+ destinoPrestamos +"',estado='"+ estado+"' where codigoPrestamo='" + codigoPrestamo + "'";
       
        con.actualizar(qr); 
        
        // ingresando fiador o fiadores
         String fiadorCadena = "INSERT INTO prestamo_fiador(id_prestamo, id_fiador) VALUES('" + codigoPrestamo + "'," + request.getParameter("fiador1") + ")";
         /**Obteniendo la cantidad minima para fiadores**/
         String query_config = "SELECT cantidad_minima_fiadores FROM configuraciones LOCK IN SHARE MODE";
         con.setRS(query_config);
         ResultSet rs = con.getRs(); rs.next();
         double cantidad_minima_fiadores = rs.getDouble("cantidad_minima_fiadores"); 
         if(Double.valueOf(request.getParameter("cantidadMandar")) >= cantidad_minima_fiadores)
            fiador2 = ",('" + codigoPrestamo + "'," + request.getParameter("fiador2") + ")"; 
         fiadorCadena += fiador2; 
         //JOptionPane.showMessageDialog(null, "qr: " + fiadorCadena);
         con.actualizar(fiadorCadena);
         response.sendRedirect("/arcamacW/prestamos/principalPrestamosMostrar.jsp?t=3&exito=up");
    }
    catch (Exception e)
    {
        response.sendRedirect("/arcamacW/prestamos/principalPrestamosMostrar.jsp?t=3&erno=2");
    }
   
%>