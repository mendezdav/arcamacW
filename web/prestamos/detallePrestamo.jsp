<%-- 
    Document   : detallePrestamo
    Created on : 22-jun-2014, 21:19:42
    Author     : Pochii
--%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="clases.diferenciaDias"%>
<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<%@include file="../administracion/header.jsp" %>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border:none;}
    .tg td{font-family:Calibri, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
    .tg th{font-family:Calibri, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
    .tg .tg-e3zv{font-weight:bold;}
    .tg .tg-p7ly{font-weight:bold;font-size:20px;text-align:center}
    .tg .tg-733k{font-family:"Calibri", "Corbel", sans-serif !important;}
    .container{max-width: 80%;margin:30px 0px 20px 0;margin-left: 10%;}
     body{padding-top: 30px;}
</style>
 <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li><a href="<%=ruta%>/prestamos/panelPrestamos.jsp">Pr&eacute;stamos</a></li>
                <li class="active">Listado de pr&eacute;stamos</li>
 </ol>
<div class="container">
    <!--Capturando parametro !-->
    <c:if test="${empty param.codigoPrestamo}">
        <c:redirect url="listadoPrestamo.jsp" />
    </c:if>
    <c:if test="${!empty param.codigoPrestamo}">
        <c:set var="codigoPrestamo" value="${param.codigoPrestamo}" />
    </c:if>
    <!--Estableciendo consultas !-->
    <sql:query dataSource="jdbc/mysql" var="q1">
        SELECT * FROM viewDetallePrestamo WHERE codigoPrestamo = "${codigoPrestamo}"
    </sql:query>
    <sql:query var="q2" dataSource="jdbc/mysql">
        SELECT a.fechaPago, a.monto FROM abonos as a
        INNER JOIN pagos as p
        ON a.idPago = p.idPago
        WHERE p.codigoPrestamo = "${codigoPrestamo}"
        ORDER By a.idPago DESC Limit 1
    </sql:query>
    <sql:query var="q3" dataSource="jdbc/mysql">
        SELECT count(idPago) as cuenta FROM pagos WHERE mora = 1 AND codigoPrestamo = "${codigoPrestamo}"
    </sql:query>
    <!-- Estableciendo las propiedades de pago !-->
    <c:forEach var="row" items="${q2.rows}">
        <c:set var="fechaUltimoPago"  value="${row.fechaPago}" />
        <c:set var="montoUltimoPago" value="${row.monto}" />
    </c:forEach>
    <c:forEach var="row" items="${q3.rows}">
        <c:set var="mora" value="${row.cuenta}" />
    </c:forEach>
    <c:set var="contador" value="1" />
    
    <c:forEach var="info" items="${q1.rows}">
    <table class="tg" >
      <c:if test="${contador ==1}">
        <tr>
          <th class="tg-p7ly" colspan="4">DETALLE DE PR�STAMO No. ${codigoPrestamo}</th>
        </tr>
        <tr>
          <td class="tg-e3zv">Solicitante:</td>
          <td class="tg-733k">${info.nombreSolicitante}</td>
          <td class="tg-e3zv">Estado:</td>
          <td class="tg-733k">${info.estadoPrestamo}</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Cantidad:</td>
          <td class="tg-733k">$ ${info.cantidad}</td>
          <td class="tg-e3zv">Inter�s aplicado:</td>
          <td class="tg-733k">${info.tasaInteres}%</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Saldo&nbsp;Pendiente:</td>
          <td class="tg-733k">$ ${info.saldo}</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">Cuota mensual:</td>
          <td class="tg-733k">$ ${info.cuotas}</td>
          <td class="tg-e3zv">Tipo de pago:</td>
          <td class="tg-733k">$ ${info.tipoPago}</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Plazo:</td>
          <td class="tg-733k">${info.plazo} meses</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">Destino de Pr�stamo:</td>
          <td class="tg-733k">${info.destinoPrestamo}</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">Fecha de solicitud:</td>
          <td class="tg-733k">${info.fechaEmision}</td>
          <td class="tg-e3zv">Fecha de aprobaci�n:</td>
          <td class="tg-031e">${info.fechaAprobacion}</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Fecha de finalizaci�n:</td>
          <td class="tg-733k">${info.fechaFinalizacion}</td>
          <td class="tg-e3zv">Fecha de vencimiento:</td>
          <td class="tg-031e">${info.fechaVencimiento}</td>
        </tr>
        <tr>
          <td class="tg-e3zv" colspan="4">INFORMACI�N DE PAGO</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Fecha de �ltimo pago:</td>
          <td class="tg-031e">${fechaUltimoPago}</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">Monto de �ltimo pago:</td>
          <td class="tg-031e">$ ${montoUltimoPago}</td>
          <td class="tg-e3zv">Pagos en mora:</td>
          <td class="tg-031e">${mora}</td>
        </tr>
        <tr>
          <td class="tg-e3zv">INFORMACI�N DE FIADORES</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">FIADOR I</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">Nombre:</td>
          <td class="tg-031e">${info.nombreFiador}</td>
          <td class="tg-e3zv">Edad:</td>
          <td class="tg-031e">${info.edadFiador} a&ntilde;os</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Tel�fono:</td>
          <td class="tg-031e">${info.telefonoFiador}</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
      </table>
      </c:if>
        
      <c:if test="${contador > 1}">
      <table class="tg">
        <tr>
          <td class="tg-e3zv">FIADOR II</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        <tr>
          <td class="tg-e3zv">Nombre:</td>
          <td class="tg-031e">${info.nombreFiador}</td>
          <td class="tg-e3zv">Edad:</td>
          <td class="tg-031e">${info.edadFiador} a&ntilde;os</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Tel�fono:</td>
          <td class="tg-031e">${info.telefonoFiador}</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
      </c:if>
        <c:set var="contador" value="${contador +1}" />
    </table></center>
    </c:forEach>
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-4">
            <a class="btn btn-info" onclick="">
                Imprimir
            </a>
        </div>
    </div>
</div>
<%@include file="../administracion/footer.jsp" %>