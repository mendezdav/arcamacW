<%-- 
    Document   : preRefinanciamiento
    Created on : 18-oct-2013, 0:22:38
    Author     : Pochii
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<<%--%@include  file="../administracion/ValSession.jsp" %>--%>
<%@include file="../administracion/header.jsp" %>
 <c:choose>
    <c:when test="${empty param.opc}">
        <c:redirect url="panelPrestamos.jsp">
            <c:param name="erno" value="2" />
        </c:redirect>
    </c:when>
    <c:when test="${!empty param.opc}">
        <c:set var="opc" scope="session" value="${param.opc}" />
    </c:when>
</c:choose>
 <style>
    body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
    .table {width:600px;alignment-baseline:central;}
    .container{max-width:  1200px;margin: 0 auto;}
    .container h2{font-family: Century Gothic;}
</style>
        <%
            String activar ;
            if (request.getParameter("act") != null && request.getParameter("act") !="")
            { activar = request.getParameter("act");
              if (activar.equals("1"))
              {
                  %>
                  <body onLoad="mostrardiv();">
                  <%
              }
            }
            %>
     <div class="container">
            <h2>Refinanciamiento</h2><br />
     </div> <!-- /container -->
     <!--                                                                 -->
     <div class="container"  id="divprestamo">
        <%
        String pagR = request.getParameter("pag");
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('listaprestamosref.jsp', 'txtNombre','busqueda',<%=pagR%>,'preRefinanciamiento.jsp');" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
       <fieldset>
        <legend>Buscar pr&eacute;stamo</legend>
        <%
            String criterio  = request.getParameter("criterio");
            if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="Buscar Solicitante"  
                                oninput="paginar('listaprestamosref.jsp', 'txtNombre','busqueda',<%=pagR%>,'preRefinanciamiento.jsp');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            {
                %>
                 <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="Buscar pr&eacute;stamos"  
                                oninput="paginar('listaprestamosref.jsp', 'txtNombre','busqueda',<%=pagR%>,'preRefinanciamiento.jsp');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
                
                <%
            }
                %>
    </fieldset>
</div>
<%@include file="../administracion/footer.jsp" %>
<script lang="javascript" src="<%=ruta%>/js/selectAjax.js"></script>
<script>
   $(function(){
      //Enviamos el tipo pre de prestamo
       paginar('listaprestamosref.jsp', 'txtNombre','busqueda',<%=pagR%>,'preRefinanciamiento.jsp');
    });
    function mostrardiv(){
        document.getElementById("divprestamo").style.visibility = "visible"; 
    }
</script>
