<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.TreeMap"%>
<%@page import="Conexion.conexion" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="analisis" class="clases.nuevoPrestamo"  scope="request" /> 
<%
    String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    //Capturando valores
    String requeridos[] = {"idSolicitante","cantidad", "plazo","interes", "cuotas", "tipo_pago", "destinoPrestamos", "estado", "fiador1","r"};   
    TreeMap<String, String>prestamoPost = new TreeMap<String, String>();
    String idSolicitante = request.getParameter("idSolicitante");
    for(String nulo : requeridos)   
    {
	if(request.getParameter(nulo).equals(""))
           response.sendRedirect("nuevoPrestamo.jsp?idSolicitante=" + idSolicitante + "&erno=1&campo=" + nulo);
    }//fin for   
    /* MODIFICACION VERIFICAR SI EL SOLICITANTE TIENE ALGUN PRESTAMO EN PROCESO DE APROBACION */
    String query = "select estado from prestamos where idSolicitante = " + idSolicitante + " and estado='En proceso'";
    conexion con = new conexion();
    con.setRS(query);
    ResultSet rs = con.getRs();
    if (rs.next())
    {
         response.sendRedirect(ruta + "/prestamos/panelPrestamos.jsp?erno=15");
    }
    else
    {
        con.cerrarConexion();
        rs.close();
        //Capturando los datos del solicitante
        //conexion con = new conexion();
        //JOptionPane.showMessageDialog(null, "refinanciamiento: " + request.getParameter("r"));
        String refinanciamiento = request.getParameter("r");
        query = "SELECT count(codigoPrestamo) as prestamo FROM  prestamos as p  WHERE p.idsolicitante = " + request.getParameter("idSolicitante")+ " LOCK IN SHARE MODE";
        out.println(query);
        con = new conexion();
        con.setRS(query);
        rs = con.getRs(); rs.next();
        String codigoP = rs.getString("prestamo");
        //Sumamos 1
        int codigonew = Integer.parseInt(codigoP) + 1;
        codigoP = Integer.toString(codigonew);
        double cantidadMandar = Double.parseDouble(request.getParameter("cantidadMandar"));
        if(Integer.parseInt(codigoP) < 10) 
            codigoP = "0" + codigoP;
        //Reiniciando el rs
        con.setRS("SELECT codigo FROM solicitante WHERE idsolicitante = " + request.getParameter("idSolicitante") + " LOCK IN SHARE MODE");
        rs = con.getRs(); rs.next();
        codigoP = rs.getString("codigo") + codigoP;
    %>
        <!--Estableciendo propiedades --!> 
        <jsp:setProperty name="analisis" property="cantidad" value="<%=cantidadMandar%>" />
        <jsp:setProperty name="analisis" property="plazo"    param="plazo" />
        <jsp:setProperty name="analisis" property="interes"  param="interes" />
        <jsp:setProperty name="analisis" property="tipo_pago" param="tipo_pago" /> 
        <jsp:setProperty name="analisis" property="estado" param="estado" />
        <jsp:setProperty name="analisis" property="destinoPrestamos" param="destinoPrestamos" />
        <jsp:setProperty name="analisis" property="codigoPrestamo" value="<%=codigoP%>" /> 
        <jsp:setProperty name="analisis" property="idSolicitante"  param="idSolicitante" />
        <jsp:setProperty name="analisis" property="saldo"          value="<%=cantidadMandar%>" />
        <jsp:setProperty name="analisis" property="fechaEmision"   value="0" /> 
        <jsp:setProperty name="analisis" property="refinanciamiento"   value="<%=refinanciamiento%>" /> 

    <%
        //JOptionPane.showMessageDialog(null,"cantidad resivida:" + analisis.getCantidad());
        analisis.setIva();
        analisis.setInteres_mora();
        //Creando el mapa
        try 
        { 
            prestamoPost = analisis.mapa();
            if(con.insertQuery(prestamoPost, "prestamos"))
            {
                /***PROCESANDO PARTE DE FIADORES**********/
                String fiador1 = "INSERT INTO prestamo_fiador(id_prestamo, id_fiador) VALUES('" + codigoP + "'," + request.getParameter("fiador1") + ")";
                String fiador2 = "";
                /**Obteniendo la cantidad minima para fiadores**/
                String query_config = "SELECT cantidad_minima_fiadores FROM configuraciones LOCK IN SHARE MODE";
                con.setRS(query_config);
                rs = con.getRs(); rs.next();

                double cantidad_minima_fiadores = rs.getDouble("cantidad_minima_fiadores"); 

                if(cantidadMandar >= cantidad_minima_fiadores)
                    fiador2 = ",('" + codigoP + "'," + request.getParameter("fiador2") + ")"; 
                fiador1 += fiador2;
                try{
                    out.println(con.actualizar(fiador1));
                    // guardando bean en session actual
                    HttpSession SessionActual = request.getSession();
                    SessionActual.setAttribute("beanAnalisis", analisis);
                    SessionActual.setAttribute("codigoReporte", codigoP);
                    response.sendRedirect("impresionDocumentacion.jsp?t=1&exito=in&codigo=" + codigoP +"&r=" + refinanciamiento); 
                    // original a prurba
                    //response.sendRedirect("resolucionPrestamo.jsp?exito=in&codigo=" + codigoP +"&r=" + refinanciamiento); 
                }
                catch(Exception e){
                    response.sendRedirect("nuevoPrestamo.jsp?erno=6&cause=fiador");
                }
            } //fin if
            else
            {
                response.sendRedirect("nuevoPrestamo.jsp?erno=6&cause=prestamo");
            }
        }
        catch(Exception e){
            out.println("Fallo al insertar prestamo debido a: " + e.getMessage());
        }
       con.cerrarConexion();
  }
%>

