<%-- 
    Document   : principalPrestamosMostrar
    Created on : 10-15-2014, 03:35:42 PM
    Author     : Jorge Luis
--%>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<link rel="stylesheet" type="text/css" href="../../css/simple-dialog.min.css" media="screen" />
<script lang="javascript" src="../../js/selectAjax.js"></script>
<script lang="javascript" src="../../js/validacion.js"></script>
<script language="javascript" src="../../js/simple-dialog.min.js"></script>
<script lang="javascript" src="../../js/paginador/funciones.js"></script>
<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
    <link rel="stylesheet" type="text/css" href="../../css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:750px;
                align:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                margin-left: 10%; 
            }
            .container h2
            {
                font-family: Century Gothic; 
            }
            legend{margin-left: 5%;}
            .spinner {
                position: fixed;
                top: 29.1%;
                left: 42.6%; padding: 10px 10px 10px 10px;
                margin-left: -50px; /* half width of the spinner gif */
                margin-top: -50px; /* half height of the spinner gif */
                text-align:center;
                z-index:1000000000;
                overflow: auto;
                opacity: 0.9;
                background-color: ghostwhite;
                width: 300px; /* width of the spinner gif */
                height: 202px; /*hight of the spinner gif +2px to fix IE8 issue */
            }
            .spinner p{text-align: justify; font-family: "Century Gothic"; margin-top: 40px; font-weight: bold;}
        </style>
  </head>
        <div class="container">
        <center>
            <h3>Prestamos del sistema</h3><p />
        </center>
        <br /><br />
        <%
        String criterio  = request.getParameter("criterio");
        String pagR = request.getParameter("pag");
        
        if (criterio == null || criterio.equals("") ) // si criterio no es nulo, colocar en cuadro busqueda
        {
        %>
        <body onload="listaproductoss('<%=criterio%>','<%=pagR%>',null,'7')">
        <%
        }
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('prestamosEliminar.jsp', 'txtNombre','busqueda',<%=pagR%>,'principalPrestamosEliminar.jsp','7');" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
       <fieldset>
       <legend>Buscar Prestamo</legend>
        <%
            // JOptionPane.showMessageDialog(null,pagR);
             if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="C&oacute;digo o Solicitante"  
                                oninput="paginar('prestamosEliminar.jsp', 'txtNombre','busqueda',1,'principalPrestamosEliminar.jsp','7');" />
             </div> 
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            { 
            %>
                 <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="C&oacute;digo o Solicitante"  
                                oninput="paginar('prestamosEliminar.jsp', 'txtNombre','contenedor', 1,'principalPrestamosEliminar.jsp','7');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="contenedor">
                    </div>
             </center>
                
            <%
            }
            %>
<!-- modificaciones para el dialog -->

        <div id="spinner" class="spinner" style="display:none">
            <li class="fa fa-cog fa-spin fa-3x"></li>
            <p>Procesando petici&oacute;n, por favor espere</p>
        </div>
        <div id="box_aprobar" style="visibility:hidden;">
            <div class="container" id="caja_aprobar">
                <input type="hidden" id="hidden" name="codigoPrestamo" />
                <label><b>Est&aacute; seguro/a que desea aprobar este pr&eacute;stamo?</b></label>
                <span>Esta opci&oacute;n es irreversible y la tabla de pagos se generar&aacute; a partir de 
                    el d&iacute;a de hoy.
                </span>
                <br />
                
                <center><a onclick="redireccionar(1);" class="btn btn-primary">Aprobar Pr&eacute;stamo</a></center>
            </div>
        </div>
        <div id="box_aprobarAnterior" style="visibility: hidden;">
            <div class="container" id="caja_aprobarAnterior">
                <form name="frmaprobarant" id="frmAprobarAnt" method='post' >
                    <input type="hidden" id="hiddenAnterior" name="codigoPrestamo" />
                    <label><b>Est&aacute; seguro/a que desea aprobar este pr&eacute;stamo?</b></label>
                    <span>Esta opci&oacute;n es irreversible y la tabla de pagos se generar&aacute; a partir de 
                        el d&iacute;a de hoy.
                    </span>
                    <br />
                    <label>Fecha de aprobaci&oacute;n: </label>
                    
                    <input type="date" id="fechaAprobacionss" name="fechaAprobacionss" onblur="validar('fechaAprobacionss');" required />
                    <div id='divfechaAprobacionss' class='infor'></div>
                    <center><input type="button" onclick="redireccionar(2);" class="btn btn-primary" value="Aprobar Pr&eacute;stamo" /></center>
                </form>
            </div>
        </div>

<%@include file="../../administracion/footer.jsp" %>
<script lang="javascript" src="../../js/selectAjax.js"></script>
<script lang="javascript" src="../../js/validacion.js"></script>

<script>
    $(function(){
       //Enviamos el parametro sol de Solicitante
       buscarUsuario('mostrarLista.jsp','buscar','contenedor', 'pre'); 
    });
    function cambiarfecha(){
        alert("Fecha: " + document.getElementById("fechaAprobacionss").value);
    }
    var div; //Div que contendr� el form a cargar
    function rechazarPrestamo(codigoPrestamo, tipo){
        var msj;
        if(tipo == 1)
            msj = "�Est� seguro/a que dese eliminar este pr�stamo?\nEsta operaci�n no se puede deshacer";
        else
            msj = "Esta seguro que desea rechazar la solicitud de prestamo\nEsta operacion no se puede deshacer";
        if(confirm(msj)){
            window.location.replace("actionDeletePrestamo.jsp?codigoPrestamo="+codigoPrestamo);
        }
    }
    function DialogoConBoton(event, header, caja, box, param,hidden){
            Dialog.header = header;
            Dialog.width  = "300px";
            Dialog.height = "200px";
            $("#"+hidden).val(param);
            div = $("#"+box).html();
            Dialog.content =  document.getElementById(caja).innerHTML;
            $("#" +caja).remove();
            Dialog.showDialog();
    }
  
    function redireccionar(tipo){
      /**MODIFICACION CON AJAX***/
      var ruta;
      var codigoPrestamo;
      var fechaAprobacion = "";
      if (tipo == 1) {
        ruta = "cambiarEstado.jsp";
        codigoPrestamo = $("#hidden").val();
      }
      else{ 
        var val = sumarize('frmAprobarAnt');
        if (val){
            ruta = "cambiarEstadoAnterior.jsp";
            codigoPrestamo = $("#hiddenAnterior").val();
            fechaAprobacion = $("#fechaAprobacionss").val();
        }
        else
            alert("datos incorrectos");
      }
            $("#spinner").show();
            $.ajax({
                  data: {codigoPrestamo: codigoPrestamo, fechaAprobacion: fechaAprobacion},
                  type: "POST", 
                  url: ruta
              }
            ).done(function(){
               //codigoPrestamo = $("#hidden").val();     
               //window.location.replace("principalPrestamosMostrar.jsp?t=&exito=1");
               window.location.replace("impresionDocumentacion.jsp?t=&exito=1&codigoPrestamo="+codigoPrestamo);
            }).fail(function(){
                window.location.replace("principalPrestamosMostrar.jsp?t=&erno=1");
            });   
        }
      
</script>
