<%-- 
    Document   : detallesPrestamoEliminar
    Created on : 12-18-2014, 05:37:10 PM
    Author     : Jorge Luis
--%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../administracion/header.jsp" %>
<style>
      body{ padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
      table{width:600px;margin-left: 10%; position: relative;}
      .container{max-width:  1000px;margin: 0 auto; }
      .container h2 {font-family: Century Gothic;}
      .col-s{margin: 10px 50px 30px 15px; height: 35px; min-width: 200px;}
      table a{position:relative; margin-left: 40%;}
      .col-p{font-weight: bold;}
      #alerta span{position: fixed; width: 750px;margin-left: 10px;}
</style>
<%
    // variables de control
    boolean dias = false;
    boolean pagos = false;
    // tomando datos
    String codigoPrestamo = "";
    String fechaAprobacion = "";
    double cantidad = 0.00;
    double cuota = 0.00;
    double tasa_interes = 0.00;
    double saldo = 0.00;
    String nombre = "";
    String estado = "";
    
    if (request.getParameter("codigoPrestamo") != null &&  request.getParameter("codigoPrestamo") != "")
        codigoPrestamo = request.getParameter("codigoPrestamo");
    // ver fecha actual
    Date fecha = new Date();
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
    // conectando con la base de datos
    conexion con = new conexion();
    ResultSet rs = null;
    String qr = "select p.fechaAprobacion,p.plazo,p.codigoPrestamo,p.estado,p.cantidad,p.fechaAprobacion,p.cuotas,"
            + "p.tasa_interes, p.saldo, p.fechaFinalizacion, concat(s.nombre1, ' ', s.nombre2,' ',apellido1, ' ',apellido2) as nombre "
            + "from prestamos as p inner join solicitante as s on p.idSolicitante = s.idSolicitante where codigoPrestamo='" + codigoPrestamo+ "'";
    int plazo = 0;
    int pendientes = 0;
    con.setRS(qr);
    rs = con.getRs();
    if (rs.next())
    {
        // llenando datos
        codigoPrestamo = rs.getString("codigoPrestamo");
        fechaAprobacion = rs.getString("fechaAprobacion");
        cantidad = rs.getDouble("cantidad");
        cuota = rs.getDouble("cuotas");
        tasa_interes = rs.getDouble("tasa_interes");
        estado = rs.getString("estado");
        saldo = rs.getDouble("saldo");
        nombre = rs.getString("nombre");
        plazo = rs.getInt("plazo");
        String fechaPrestamoPartes[] = rs.getString("fechaAprobacion").split(" ");
        fechaAprobacion = fechaPrestamoPartes[0];
        String fechaActual = formato.format(fecha).toString();
        if (fechaPrestamoPartes[0].equals(fechaActual))
            dias = true; // cumple con requisitos de q sea el mio dia pra que no hayan generado intereses
        if (fechaPrestamoPartes[0].equals("1000-01-01"))
            dias = true;    
    }
    else 
    {
        JOptionPane.showMessageDialog(null, " tpm");
        /// es finisterra, es el fin del camino es finisterra
    }
    // verificando dias
    qr = "select count(*) as total from pagos where codigoPrestamo='" + codigoPrestamo+ "' and estado='Pendiente'";
    con.setRS(qr);
    rs = con.getRs();
    if (rs.next())
        pendientes = rs.getInt("total");
    if (pendientes == plazo) // no se han realizado ningun abono
        pagos = true;
    if (pendientes == 0) // no se han realizado ningun abono
        pagos = true;
    
%>
<div class="container">
    <h2>Datos Pr&eacute;stamo Eliminar</h2><br /> 
<fieldset>
    <legend>Datos de pr&eacute;stamo</legend>
    <table>
          <tr>
              <td colspan="4">
                  <%if (pagos && dias){
                    %>
                        <div class="alert alert-success fade in" id="alerta"> 
                            <i class="fa fa-check-square-o fa-3x"></i> Este pr&eacute;stamo cumple con los requisitos para el ser eliminado
                        </div>
                  <% }else{ %>
                  
                        <div class="alert alert-danger fade in" id="alerta"> 
                            <i class="fa fa-times-circle fa-3x"></i> 
                            <span>
                            Este pr&eacute;stamo NO cumple con los requisitos para el ser eliminado.<br />
                            <ul>
                            <%
                                if(!dias)
                                    out.print("<li>No es el mismo dia de la fecha de aprobaci&oacute;n ( " + fechaAprobacion + ")</li>");
                                if(!pagos)
                                    out.print("<li>El prestamo presenta movimiento (abonos)</li>");
                                
                            %>
                            </span>
                        </div>
                  <% } %>
              </td>
          </tr>
          <tr>
              <td class="col-s col-p">C&oacute;digo pr&eacute;stamo:</td>
              <td class="col-s"><%=codigoPrestamo.toUpperCase() %></td>
          </tr>
          <tr>
              <td class="col-s col-p">Solicitante:</td>
              <td class="col-s"><%=nombre%></td>
          </tr>
          <tr>
              <td class="col-s col-p">Estado pr&eacute;stamo:</td>
              <td class="col-s"><%=estado%></td>
              <td class="col-s col-p">Tasa inter&eacute;s:</td>
              <td class="col-s"><%=tasa_interes%> %</td>
          </tr>
          <tr>
              <td class="col-s col-p">Cantidad del pr&eacute;stamo:</td>
              <td class="col-s">$ <%=cantidad%></td>
              <td class="col-s col-p">Saldo actual:</td>
              <td class="col-s">$ <%=saldo%></td>
          </tr>
          <tr>
              <td class="col-s col-p">Fecha aprobaci&oacute;n:</td>
              <td class="col-s"><%=fechaAprobacion%></td>
          </tr>
          <tr>
              <td class="col-s col-p">Cuotas</td>
              <td class="col-s"><%=plazo%></td>
          </tr>
          <tr>
              <td colspan="4">
                  <%
                      // poniendo codigoPrestamo en session
                      SessionActual.setAttribute("codigoPrestamo",codigoPrestamo);
                      if(dias && pagos)
                          out.print("<a href='actionEliminarPrestamos.jsp' class='btn btn-primary'>Eliminar</a>");
                      else
                          out.print("<a href='principalPrestamosEliminar.jsp' class='btn btn-primary'>Regresar</a>");
                  %>
              </td>
          </tr>
      </table>
</fieldset>
</div>
<%@include file="../../administracion/footer.jsp" %>
