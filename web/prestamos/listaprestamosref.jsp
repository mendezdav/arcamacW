<%-- 
    Document   : listadoSolicitante
    Created on : 06-21-2014, 09:15:33 PM
    Author     : jorge
--%>


<%@page import="javax.swing.JOptionPane"%>
<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>

<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    int RegistrosAMostrar=10;
    int PaginasIntervalo=3;
    int RegistrosAEmpezar=0;
    int PagAct = 1;
    int PagAct2 = 0;

    String criterio = request.getParameter("criterio");
    String pagR     = request.getParameter("pagR");

    if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
    {
        RegistrosAEmpezar = 0;
        PagAct2 = Integer.parseInt(pagR);
    }
 
    String pag = request.getParameter("pagina");
    if (pag==null || pag==""){
        RegistrosAEmpezar=0;
        PagAct=2; // tendria q ser la 1
    }else{
        RegistrosAEmpezar=(Integer.valueOf(pag).intValue()-1)* RegistrosAMostrar;
        PagAct=Integer.valueOf(pag).intValue();
    }

conexion con = new conexion();
String qr="";
int tmp = RegistrosAMostrar * ( PagAct2-1);
if (tmp < 0)
tmp =0;
 if (criterio != null || criterio !="" ) // si criterio no es nulo, colocar en cuadro busqueda
  {
      //qr ="select * from pagos where codigoPrestamo like '%" + request.getParameter("criterio")+ "%' limit " + tmp + "," + RegistrosAMostrar;
     qr = "SELECT codigoPrestamo, idSolicitante,nombreSolicitante, cantidad,saldo, fechaAprobacion,estadoPrestamo,fechaVencimiento"
             + " FROM viewdetalleprestamo where (nombreSolicitante like '%" +criterio + "%' or codigoPrestamo like '%"+criterio+"%' )"
             + " and estadoPrestamo='Activo' limit " + tmp + "," + RegistrosAMostrar;

  }
 else 
 {
    // qr ="select * from pagos  limit " + tmp+ "," + RegistrosAMostrar;
      qr  =  "SELECT codigoPrestamo,idSolicitante, nombreSolicitante, cantidad,saldo, fechaAprobacion,estadoPrestamo,fechaVencimiento"
             + " FROM viewdetalleprestamo where estadoPrestamo='Activo' limit " + tmp + "," + RegistrosAMostrar;
 }
      
    con.setRS(qr);
    ResultSet resultado = con.getRs();
    if (resultado.next())
    {
        out.println("<table class='tablita'>");
        out.println("<caption>Solicitantes Disponibles</caption>");
        out.println("<tr>");
        out.println("<th>Código Prestamo</th>");
        out.println("<th>Solicitante</th>");
        out.println("<th>Cantidad</th>");
        out.println("<th>Saldo</th>");
        out.println("<th>Fecha de Aprobaci&oacute;n</th>");
        out.println("<th>Fecha de Vencimiento</th>");
        out.println("<th>Estado</th>");
        out.println("<th colspan=\"2\">Opciones</th>");
        out.println("</tr>");
        out.println("<tbody>");
    }
    resultado.previous();
    int contador = 0;
    String idSolicitante ="";
    String codigoPrestamo ="";
    while (resultado.next())
    {
        contador++;
        idSolicitante = resultado.getString("idSolicitante");
        codigoPrestamo = resultado.getString("codigoPrestamo");
    %>
        <tr id="grid">
            <td id="formnuevo"><%= resultado.getString("codigoPrestamo") %></td>
            <td id="formnuevo"><%= resultado.getString("nombreSolicitante") %></td>
            <td id="formnuevo"><%= resultado.getString("cantidad") %></td>
            <td id="formnuevo"><%= resultado.getString("saldo") %></td>
            <td id="formnuevo"><%= resultado.getString("FechaAprobacion") %></td>
            <td id="formnuevo"><%= resultado.getString("FechaVencimiento") %></td>
            <td id="formnuevo"><%= resultado.getString("estadoPrestamo") %></td>
            <td id="formnuevo"><a href="datosrefinanciamiento.jsp?idSolicitante=<%=idSolicitante%>&codigoPrestamo=<%=codigoPrestamo%>" >Seleccionar</a></td>
        </tr>
    <%
    } 
if (contador == 0)
{ 
    out.println("<div class=\"noData\"><br />No hay datos que mostrar</div>");
}
%>
<%

 if (criterio != null || criterio !="" ) 
    //qr ="select count(*) as total from Usuarios  where concat(nombreUsuario,' ' , apellidoUsuario)  like '%"  + criterio + "%'";
     qr ="SELECT count(*) as total"
             + " FROM viewdetalleprestamo where (nombreSolicitante like '%" +criterio + "%' or codigoPrestamo like '%"+criterio+"%' )"
             + " and estadoPrestamo='Activo' limit " + tmp + "," + RegistrosAMostrar;
 else 
    qr ="SELECT count(*) as total FROM viewdetalleprestamo where estadoPrestamo='Activo' limit " + tmp + "," + RegistrosAMostrar;
 
con.setRS(qr);
ResultSet rs = con.getRs();
rs.next();
String NroRegistros =  rs.getString("total");

int PagAnt=PagAct-1;
int PagSig=PagAct+1;

double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
if(Res>0){
    PagUlt=Math.floor(PagUlt)+1;
}

out.println("</tbody>");
out.println("</table><br />");
out.println("<div style='width:300px; float:right'>");

    if(PagAct>(PaginasIntervalo+1)) {
    out.println("<a onclick=listaproductos('1'); class='paginador'><< Primero</a>");
    out.println("&nbsp;");
    }
    for (int i = (PagAct2-PaginasIntervalo) ; i <= (PagAct2-1) ; i ++) {
        if(i>=1) {
            out.println("<a href='preRefinanciamiento.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"&act=1' class='paginador'>"+i+"</a>");
            out.println("&nbsp;");
        }
    }
    if (contador != 0){
    out.println("<span class='paginadoractivo'>"+PagAct2+"</span>");
    out.println("&nbsp;");
    }
    for ( int i = (PagAct2+1) ; i <= (PagAct2+PaginasIntervalo) ; i ++) {
        if(i<=PagUlt) {
        out.println("<a href='preRefinanciamiento.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"&act=1' class='paginador'>"+i+"</a>");
        out.println("&nbsp;");
        }
    }
    if(PagAct<(PagUlt-PaginasIntervalo)) {
     out.println("<a href='preRefinanciamiento.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ PagUlt+"&act=1' class='paginador'>Ultimo</a>");
    }
    out.println("</div>");
%>