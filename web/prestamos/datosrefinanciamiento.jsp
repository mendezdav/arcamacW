<%-- 
    Document   : datosrefinanciamiento
    Created on : 16-jul-2014, 12:32:47
    Author     : Pochii
--%>

<%@page import="clases.classPagos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
      body{ padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
      table{width:600px;margin-left: 10%; position: relative;}
      .container{max-width:  1000px;margin: 0 auto; }
      .container h2 {font-family: Century Gothic;}
      .col-s{margin: 10px 50px 30px 15px; height: 35px; min-width: 200px;}
      table a{position:relative; margin-left: 40%;}
      .col-p{font-weight: bold;}
      #alerta span{position: fixed; width: 750px;margin-left: 10px;}
</style>
<div class="container">
    <h2>Datos Pr&eacute;stamo Refinanciar</h2><br /> 
 <c:choose>
    <c:when test="${empty param.codigoPrestamo}">
        <c:redirect url="panelPrestamos.jsp">
            <c:param name="erno" value="2" />
        </c:redirect>
    </c:when>
    <c:when test="${!empty param.codigoPrestamo}">
        <c:set var="codigoPrestamo" value="${param.codigoPrestamo}" />
    </c:when>
</c:choose>
<c:choose>
    <c:when test="${empty param.idSolicitante}">
        <c:redirect url="panelPrestamos.jsp">
            <c:param name="erno" value="2" />
        </c:redirect>
    </c:when>
    <c:when test="${!empty param.idSolicitante}">
        <c:set var="idSolicitante" value="${param.idSolicitante}" />
    </c:when>
</c:choose>
<fieldset>
    <legend>Datos de pr&eacute;stamo</legend>
    <sql:query var="q1" dataSource="jdbc/mysql">
        SELECT nombreSolicitante, estadoPrestamo, codigoPrestamo, cantidad, plazo, saldo, 
        fechaAprobacion, fechaVencimiento, tasaInteres
        FROM viewdetalleprestamo WHERE codigoPrestamo = "${codigoPrestamo}"
    </sql:query>
    <table>
      <c:forEach var="items" items="${q1.rows}">
          <c:set var="saldo" value="${items.saldo}" />
          <%
             // double saldo_actual = Double.valueOf(pageContext.getAttribute("saldo").toString());
              String codigoPrestamo = pageContext.getAttribute("codigoPrestamo").toString();  
              
              int cuotas_canceladas = classPagos.cuotas_canceladas(codigoPrestamo);
              boolean cumplimiento = classPagos.CumplePorcentajeRefinanciamiento(codigoPrestamo);
              pageContext.setAttribute("cuotas_canceladas", cuotas_canceladas); 
              pageContext.setAttribute("cumplimiento", cumplimiento);
          %>
          <tr>
              <td colspan="4">
                  <c:choose>
                      <c:when test="${cumplimiento == true}">
                        <div class="alert alert-success fade in" id="alerta"> 
                            <i class="fa fa-check-square-o fa-2x"></i> Este pr&eacute;stamo cumple con los requisitos para el refinanciamiento
                        </div>
                      </c:when>
                      <c:when test="${cumplimiento == false}">
                        <div class="alert alert-danger fade in" id="alerta"> 
                            <i class="fa fa-times-circle fa-2x"></i> 
                            <span>
                            Este pr&eacute;stamo NO cumple con los requisitos m&iacute;nimos para el refinanciamiento.
                            El n&uacute;mero de cuotas no alcanza el porcentaje m&iacute;nimo para dicho procedimiento.
                            </span>
                        </div>
                      </c:when>
                  </c:choose>
              </td>
          </tr>
          <tr>
              <td class="col-s col-p">C&oacute;digo pr&eacute;stamo:</td>
              <td class="col-s">${items.codigoPrestamo}</td>
          </tr>
          <tr>
              <td class="col-s col-p">Solicitante:</td>
              <td class="col-s">${items.nombreSolicitante}</td>
          </tr>
          <tr>
              <td class="col-s col-p">Estado pr&eacute;stamo:</td>
              <td class="col-s">${items.estadoPrestamo}</td>
              <td class="col-s col-p">Tasa inter&eacute;s:</td>
              <td class="col-s">${items.tasaInteres} %</td>
          </tr>
          <tr>
              <td class="col-s col-p">Cantidad del pr&eacute;stamo:</td>
              <td class="col-s">$ ${items.cantidad}</td>
              <td class="col-s col-p">Saldo actual:</td>
              <td class="col-s">$ ${items.saldo}</td>
          </tr>
          <tr>
              <td class="col-s col-p">Fecha aprobaci&oacute;n:</td>
              <td class="col-s">${items.fechaAprobacion}</td>
              <td class="col-s col-p">Fecha de vencimiento:</td>
              <td class="col-s">${items.fechaVencimiento}</td>
          </tr>
          
          <tr>
              <td class="col-s col-p">Cuotas</td>
              <td class="col-s"> ${cuotas_canceladas}/${items.plazo}</td>
          </tr>
          <tr>
              <td colspan="4">
                  <c:url var="ruta" value="nuevoPrestamo.jsp">
                      <c:param name="codigoPrestamo" value="${codigoPrestamo}" />
                      <c:param name="idSolicitante" value="${idSolicitante}" />
                      <c:param name="r" value="1" />
                  </c:url>
                  <c:choose>
                      <c:when test="${cumplimiento == true}">
                          <a href="<c:out value='${ruta}' />" class="btn btn-primary">Continuar</a>
                      </c:when>
                      <c:when test="${cumplimiento == false}">
                        <a href="<c:out value='${ruta}' />" class="btn btn-danger">Continuar</a>
                      </c:when>
                  </c:choose>
              </td>
          </tr>
      </c:forEach>
    </table>
</fieldset>
</div>
<%@include file="../administracion/footer.jsp" %>
