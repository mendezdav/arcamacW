<%-- 
    Document   : panelPrestamoTipo
    Created on : 10-15-2014, 03:23:18 PM
    Author     : Jorge Luis
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 30px 15px;}
            .col-s a{height: 76px;}
            .col-small{padding-top: 20%; width: 150px;}
            .col-p{background-color: #7b34a5;}
            
       .wrapper{
                position: relative;
                float: left;
                left: 15%;
                width: 616px;
                margin-bottom: 4px;
                background-color: #ffffff
        }
        .left1{
           position: relative;
           float: left;
           left: 2px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left2{
           position: relative;
           float: left;
           left: 6px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left3{
           position: relative;
           float: left;
           left: 10px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }

        .left1:hover { background-color: gray;  }
        .left2:hover { background-color: gray;  }
        .left3:hover { background-color: gray;  }
</style>
<%
    String codigoP = request.getParameter("codigoPrestamo");
    SessionActual.setAttribute("codigoReporte", codigoP);
%>
        
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Reportes</li>
           </ol>
                <h2>Prestamos en el Sistema </h2><br /><br />
            
           <div class="wrapper">
                <div class="left1" onclick="llamar('principalPrestamosMostrar.jsp?t=1');">
                   <br />Prestamos Activos 
                </div>

                <div class="left2" onclick="llamar('principalPrestamosMostrar.jsp?t=2');">
                    <br />Prestamos Cancelados
                </div>

                <div class="left3" onclick="llamar('principalPrestamosMostrar.jsp?t=3');">
                    <br />Prestamos No aprobados
                </div>
           </div> 
    </div>
<%@include file="../administracion/footer.jsp" %>
<script>
  function llamar(dir)
    {window.location.href = dir;}
</script>