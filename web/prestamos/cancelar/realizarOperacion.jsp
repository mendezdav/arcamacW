<%-- 
    Document   : realizarOperacion
    Created on : 04-20-2015, 03:08:56 PM
    Author     : Jorge Luis
--%>
<%@page import="Conexion.conexion"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="clases.diferenciaDias"%>
<%@page import="clases.PagosExtraordinarios "%>

<%@include file="../../administracion/header.jsp" %>
<style type="text/css">
 .content{background-position: center; margin: 100px 30px 20px 12%;} 
.tg  {border-collapse:collapse;border-spacing:0;border:none;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal; width: 150px;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.form-control{width: 200px;}
fieldset{margin: 30px 5px 30px 0px; border: 1px #006dcc solid; width:550px;padding-left: 15px;background-color: #ceeaea;}
legend{border: 1px #0099FF solid;padding: 5px 5px 5px 20px; width: 150px; border-radius: 5px; background-color: #fff;}
#detalle td{width: 200px; margin-left: 20px; text-align: justify;}
#detalle tr{height: 25px;}
</style>
<%
String idSolicitante = "";
String opc="";
if (request.getParameter("id") == null || request.getParameter("id") == ""  || request.getParameter("opc") == null 
        || request.getParameter("opc")== "" )
    response.sendRedirect("principal.jsp?erno=2");
else
{
    idSolicitante = request.getParameter("id");
    opc           = request.getParameter("opc");
}
    String codigoPrestamo="";
    String idPago = "";
    // creando instancia
    PagosExtraordinarios extra = new PagosExtraordinarios(idSolicitante,opc);    
    conexion con = new conexion();
    con.setRS("select concat(s.nombre1, ' ', s.nombre2,' ',s.apellido1,' ', s.apellido2) as nombre,"
            + " pre.codigoPrestamo, pre.estado,pre.saldo, pre.cuotas from solicitante as s inner join "
            + " prestamos as pre on s.idSolicitante = pre.idSolicitante where pre.estado='Activo' "
            + "and s.idSolicitante=" + idSolicitante);
    ResultSet rs  = con.getRs();
    if (rs.next()){
        codigoPrestamo = rs.getString("codigoPrestamo");
        conexion con2 = new conexion();
        con2.setRS("select * from pagos where codigoPrestamo='" + codigoPrestamo + "' and estado !='Pagado' limit 1 ");
        ResultSet rs2 = con2.getRs();

    if (rs2.next())
        idPago = rs2.getString("idPago");
    else
        response.sendRedirect("principal.jsp?erno=2");
%>

<div class="content">
    <h2>Pago de cuotas</h2>
    <hr />
<table class="tg">
            <tr>
              <td class="tg-031e"><strong>Solicitante:</strong></td>
              <td class="tg-031e" colspan="3"><%=rs.getString("nombre")%></td>
            </tr>
            <tr>
              <th class="tg-031e"><strong>Pr�stamo No.</strong></th>
              <th class="tg-031e"><%=rs.getString("codigoPrestamo")%></th>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Estado:</strong></td>
              <td class="tg-031e"><%=rs.getString("estado")%></td>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Saldo Actual:</strong></td>
              <td class="tg-031e">$ <%=rs.getString("saldo")%></td>
              <td class="tg-031e" colspan="2" rowspan="2"></td>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Cuota:</strong></td>
              <td class="tg-031e">$<%=rs.getString("cuotas")%> </td>
            </tr>
          </table>

            
          <fieldset>
              <legend>DETALLE</legend>
               <table id="detalle">
                          <tr>
                            <td></td>
                            <td><strong>Cantidad requerida</strong></td>
                            <td></td>
                          </tr>
                  <tr>
                      <td><strong>Intereses ordinarios:</strong></td>
                      <td>$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%=extra.getTotal_interes()  %>" /></td>
                  </tr>
                   <tr>
                      <td><strong>IVA:</strong></td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%=extra.getIva_total_interes() %>" />
                      </td>  
                  </tr>
                  <tr>
                      <td><strong>Mora:</strong></td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%= extra.getTotal_interes_moratorio() %>" /> 
                      </td>
                  </tr>
                  <tr>
                      <td><strong>IVA:</strong></td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%= extra.getIva_total_interes_moratorio() %>" />   
                      </td>
                  </tr>
                  <% if (opc.equals("dia"))
                  {
                      %>
                       <tr>
                            <td><strong>Abono a capital:</strong></td>
                            <td>
                               $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%=extra.getTotal_capital() %>" />   
                            </td>
                         </tr>
                      <%
                  }else if (opc.equals("prestamo")) {
                    %>
                    <tr>
                            <td><strong>Abono a capital:</strong></td>
                            <td>
                               $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%=extra.getTotal_capital()  + extra.nuevoSaldo() %>" />   
                            </td>
                         </tr>
                    <%
                    }%>
                 
                    <% if (opc.equals("dia"))
                    {
                    %>
                    <tr>
                        <td><strong>Total a pagar:</strong></td>
                        <td>
                            $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%= extra.total_deuda() %>" />    
                        </td>
                        <td></td>
                    </tr>
                    <%}
                    else if (opc.equals("prestamo")){%>
                    <tr>
                      <td><strong>Total a pagar:</strong></td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%= extra.total_deuda() + extra.nuevoSaldo() %>" />    
                      </td>
                      <td></td>
                  </tr>
                    <% } 
                    // extra.total_deuda()  total a pagar!
                    if (opc.equals("dia"))
                    {
                        %>
                        <tr>
                            <td><strong>Nuevo saldo:</strong></td>
                            <td>
                                $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%= extra.nuevoSaldo() %>" />    
                            </td>
                            <td></td>
                        </tr>
                        <%
                    }else if (opc.equals("prestamo")){
                    %>
                        <tr>
                            <td><strong>Nuevo saldo:</strong></td>
                            <td>
                                $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="<%= extra.nuevoSaldo() - extra.getTotal_capital_ideal() %>" />    
                            </td>
                            <td></td>
                        </tr>
                        <%
                    }
                    %>

                  <tr>
                      <td></td>
                      <td></td>
                      <td class="mora"></td>
                  </tr>
              </table>
          </fieldset>
          <form action="../../Pagos/finalizarPago.jsp" method="POST">
            <input type="hidden" name="codigoPrestamo" value="<%=codigoPrestamo%>" />
            <input type="hidden" name="idPago" value="<%=idPago%>" />
            <input type="hidden" name="tipoPago" value="1" />
            <input type="hidden" name="fechaA" value="2015-01-01" />
            
            <div class="row">
                <div class="col-md-2"><strong>Cantidad:</strong></div>
                <div class="col-md-3">

                    <%
                    if (opc.equals("dia"))
                    {
                    %>
                        <input type="text" class="form-control" name="monto" value="<%= diferenciaDias.redondearBig(extra.total_deuda()) %>" required />
                    <%
                    }else if (opc.equals("prestamo"))
                    {
                    %>
                        <input type="text" class="form-control" name="monto" value="<%= diferenciaDias.redondearBig(extra.total_deuda() + extra.nuevoSaldo() ) %>" required />
                    <%
                    }
                    %>
                    
                    
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="submit" value="Realizar pago" class="btn btn-primary" />
                </div>
            </div>
          </form>
</div>
            
<%
    }
    else
        response.sendRedirect("principal.jsp?erno=2");
%>              
           
<%@include file="../../administracion/footer.jsp" %>