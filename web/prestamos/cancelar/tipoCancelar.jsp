<%-- 
    Document   : tipoCancelar
    Created on : 04-20-2015, 02:51:09 PM
    Author     : Jorge Luis
--%>
<!-- idSolicitante -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../../administracion/header.jsp" %>
<%
    /* revisando parametro pasado*/
    String idSolicitante = "";
    if (request.getParameter("idSolicitante") == null || request.getParameter("idSolicitante") == "")
        response.sendRedirect("principal.jsp?erno=2");
    else
        idSolicitante = request.getParameter("idSolicitante").toString();
%>
<style>
    body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
    .table {width:600px;alignment-baseline:central;}
    .container{max-width:  1200px;margin: 0 auto;}
    .container h2{font-family: Century Gothic;}
</style>
     <div class="container">
         <div id="divOpciones" style="width: 500px; border: solid 1px #d3d3d3; padding: 20px; border-radius: 5px; text-align: center; margin: 0 auto;">
            <h3>Seleccione la operaci&oacute;n a realizar</h3>
            <br/>
            <br/>
            <a class="btn btn-primary" href="realizarOperacion.jsp?id=<%= idSolicitante%>&opc=dia" >Ponerme al<br /> D&iacute;a&nbsp;</a>
            <a class="btn btn-primary" href="realizarOperacion.jsp?id=<%= idSolicitante%>&opc=prestamo">Cancelar Totalidad <br /> 
                del pr&eacute;stamos </a>
         </div>
        <hr />
     </div> 
<%@include file="../../administracion/footer.jsp" %>
<script lang="javascript" src="../../js/selectAjax.js"></script>
