<%-- 
    Document   : prestamosActivos
    Created on : 10-15-2014, 03:53:14 PM
    Author     : Jorge Luis
--%>
<link rel="stylesheet" href="../../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../../js/paginador/funciones.js"></script>
<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script lang="javascript" src="../../js/selectAjax.js"></script>
<script lang="javascript" src="../../js/validacion.js"></script>
<%

    int RegistrosAMostrar=10;
    int PaginasIntervalo=3;
    int RegistrosAEmpezar=0;
    int PagAct = 1;
    int PagAct2 = 0;

    /** parametros que debe recibir **/
    String criterio = request.getParameter("criterio"); // criterio de busqueda en los registros
    if (criterio != null && criterio != "")
        criterio = criterio.replace("#","").replace("'","").replace("-","").replace("%"," ").replace("\\","").replace("$","")
                .replace("/","").replace("(","").replace(")","").replace("=","").replace("?","").replace(";","").replace(".","")
                .replace("{","").replace("}","").replace("[","").replace("]","").replace("*","").replace(",","");
    
    String pagR = request.getParameter("pagR");
    String pagina = request.getParameter("pagina");
    if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
    {
        RegistrosAEmpezar = 0;
        PagAct2 = Integer.parseInt(pagR);
    }
    try
    {
        PagAct2 = Integer.parseInt(pagR);
    }
    catch (Exception e1)
    {
        System.out.print("Error: " + e1.getMessage());
        PagAct2 = 1;
    }

    if (pagina==null || pagina==""){
        RegistrosAEmpezar=0;
        PagAct=2; // tendria q ser la 1, pero no lo es, por que mas adeante PagAct2-1
    }else{
        RegistrosAEmpezar=(Integer.valueOf(pagina).intValue()-1)* RegistrosAMostrar;
        PagAct=Integer.valueOf(pagina).intValue();
    }
    
    conexion con = new conexion();
    String qr="";
    int tmp = RegistrosAMostrar * ( PagAct2-1);
    if (tmp < 0) 
        tmp = 0;

 if (criterio != null && criterio !="" ) // si criterio no es nulo, colocar en cuadro busqueda
 {
        qr = "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,codigo,idSolicitante,DUI, edad,"
            + "estado_sistema, celular, profesion,codigoPrestamo FROM solicitante where estado_sistema='activo' and idSolicitante "
            + "IN (select idSolicitante,codigoPrestamo from prestamos where estado='Activo' group by idSolicitante) and "
            + "( concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%" + criterio+ "%' or codigo like '%" + criterio+  "%') limit " + tmp + "," + RegistrosAMostrar;
 
 }      
 else 
 {
       qr = "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,codigo,idSolicitante,DUI, edad,"
            + "estado_sistema, celular, profesion FROM solicitante where estado_sistema='activo' and idSolicitante "
            + "IN (select idSolicitante from prestamos where estado='Activo' group by idSolicitante) limit " + tmp + "," + RegistrosAMostrar;
 
 }
    con.setRS(qr);
    ResultSet resultado = con.getRs();
    if (resultado.next())
    {
        out.println("<table class='tablita'>");
        out.println("<caption>Solicitantes Disponibles</caption>");
        out.println("<tr>");
        out.println("<th>Nombre</th>");
        out.println("<th>DUI</th>");
        out.println("<th>Edad</th>");
        out.println("<th>Profesi&oacute;n</th>");
        out.println("<th>Tel&eacute;fono</th>");
        out.println("<th>Estado</th>");
        out.println("<th colspan=\"2\">Opciones</th>");
        out.println("</tr>");
        out.println("<tbody>");
    }
    resultado.previous();
    int contador = 0;
    String idSolicitante ="";
while (resultado.next())
{
    contador++;
    idSolicitante = resultado.getString("idSolicitante");
%>
    <tr id="grid">
        <td id="formnuevo"><%= resultado.getString("solicitante") %></td>
        <td id="formnuevo"><%= resultado.getString("DUI") %></td>
        <td id="formnuevo"><%= resultado.getString("edad") %></td>
        <td id="formnuevo"><%= resultado.getString("profesion") %></td>
        <td id="formnuevo"><%= resultado.getString("celular") %></td>
        <td id="formnuevo"><%= resultado.getString("estado_sistema") %></td>
        <td id="formnuevo"><a href="tipoCancelar.jsp?idSolicitante=<%=idSolicitante%>" >Seleccionar</a></td>
    </tr>
<%
} 
if (contador == 0)
{ 
    out.println("<div class=\"noData\"><br />No hay datos que mostrar</div>");
}
%>
<%

 if (criterio != null || criterio !="" ) 
    //qr ="select count(*) as total from Usuarios  where concat(nombreUsuario,' ' , apellidoUsuario)  like '%"  + criterio + "%'";
      qr = "select count(*) as total FROM solicitante where estado_sistema='activo' and idSolicitante "
            + "IN (select idSolicitante from prestamos where estado='Activo' group by idSolicitante) and "
            + "( concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%" + criterio+ "%' or codigo like '%" + criterio+  "%')";
 
 else 
     qr = "select count(*) as total FROM solicitante where estado_sistema='activo' and idSolicitante "
            + "IN (select idSolicitante from prestamos where estado='Activo' group by idSolicitante)";
 
 
con.setRS(qr);
ResultSet rs = con.getRs();
rs.next();
String NroRegistros =  rs.getString("total");

int PagAnt=PagAct-1;
int PagSig=PagAct+1;

double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
if(Res>0){
PagUlt=Math.floor(PagUlt)+1;
}

out.println("</tbody>");
out.println("</table><br />");
out.println("<div style='width:300px; float:right'>");

    if(PagAct>(PaginasIntervalo+1)) {
    out.println("<a onclick=listaproductos('1'); class='paginador'><< Primero</a>");
    out.println("&nbsp;");
    }
    for (int i = (PagAct2-PaginasIntervalo) ; i <= (PagAct2-1) ; i ++) {
        if(i>=1) {
            out.println("<a href='principal.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"&act=1' class='paginador'>"+i+"</a>");
            out.println("&nbsp;");
        }
    }
    if (contador != 0){
    out.println("<span class='paginadoractivo'>"+PagAct2+"</span>");
    out.println("&nbsp;");
    }
    for ( int i = (PagAct2+1) ; i <= (PagAct2+PaginasIntervalo) ; i ++) {
        if(i<=PagUlt) {
        out.println("<a href='principal.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"&act=1' class='paginador'>"+i+"</a>");
        out.println("&nbsp;");
        }
    }
    if(PagAct<(PagUlt-PaginasIntervalo)) {
     out.println("<a href='principal.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ PagUlt+"&act=1' class='paginador'>Ultimo</a>");
    }
    out.println("</div>");
%>