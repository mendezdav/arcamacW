<%-- 
    Document   : nuevoPrestamo
    Created on : 15-oct-2013, 11:39:07
    Author     : Pochii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../administracion/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />
<jsp:useBean id="analisis" class="clases.nuevoPrestamo"  scope="page" /> 
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>  
<script language="javascript" src="<%=ruta%>/js/tags.js"></script> 
<script> 
    function DialogoConBoton(event){
            var div; //Div que contendrá el form a cargar
            Dialog.header = "Nuevo fiador";
            Dialog.width  = "600px";
            loadContent("box");
            Dialog.content =  document.getElementById("box").innerHTML;;     
            Dialog.showDialog();
    }
    function loadContent(box)
    {
         $("#"+box).load("../Mantenimientos/fiadores/formFiador.jsp",{idSolicitante:'primer valor'}, function(response, status, xhr) {
                          if (status == "error") {
                            var msg = "Error!, algo ha sucedido: ";
                            $("#"+box).html(msg + xhr.status + " " + xhr.statusText);
                        }
                    });
    }
    function buscar(id, caja)
    {
        $('#'+id).sugerir(caja);
    }
</script>
<style>
             body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
            .tabla {width:600px; margin-bottom: 20px; }
            .container { max-width:  1000px; margin: 0 auto;}
            .form-control, .input-group {width:200px;} 
            .container h2 {font-family: Century Gothic; }  
            .tabla  tfoot, tr, td{ padding: 8px; border-collapse: separate; border-spacing: 10px 10px 10px 0px;}
            legend{text-align: left;}
            .seleccion{font-style: italic; min-height: 40px;padding: 5px; background: #ffffc1;}
	    .seleccion a{display: inline-block; padding-left: 6px; color: red;}
            #busqueda .sugerencias{position: absolute; top: 35px;left: 15px; z-index: 100; width: 300px;}
            #busqueda .sugerencias ul{padding: 5px;list-style-type: none;border: 1px solid black;background: white;}
            #busqueda .sugerencias ul li{margin: 0;border-bottom: 1px dotted #ccc;background: white; padding: 5px; }
	    #busqueda .sugerencias ul li:hover{background: #fcf8e3; cursor: pointer;}
        </style>
        <!--Verificando parametro -->
        <c:if test="${!empty param.idSolicitante}">
            <c:set var="idSolicitante" value="${param.idSolicitante}" />
        </c:if>
        <div class="container" align="center">
            <h2>Solicitud de nuevo pr&eacute;stamo</h2>
            <label>Por favor complete los siguientes campos </label>
            <form name="frmNuevoSolicitante" action="resolucionPrestamo.jsp" method="POST">
            <fieldset>
                <legend>Informaci&oacute;n del pr&eacute;stamo </legend>
                 <input type="hidden" value="${idSolicitante}" />
               <table class="tabla">
                <tr>
                    <td>
                        <label>*Cantidad a solicitar (USD): </label>
                    </td>
                    <td>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input class="form-control" type="text" name="txtCantidad" placeholder="Cantidad" required autofocus />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Plazo a pagar(meses): </label>
                    <td>
                        <input type="text" class="form-control" name="txtPlazo" placeholder="Ej. 12" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Interes a aplicar: </label>
                    <td>
                       <div class="input-group">
                          <span class="input-group-addon">%</span>
                          <input class="form-control" style="width: 150px;" type="text" id="interes" name="interes" value="3.35" required autofocus />
                        </div>
                    </td>
                    <td></td>
                    <td><a onclick="" class="btn btn-info"><i class="fa fa-repeat"></i> Calcular cuota</a>
                </tr>
                <tr>
                    <td>
                        <label>*Cuotas (USD): </label>
                    </td>
                    <td>
                        <div class="input-group">
                          <span class="input-group-addon">$</span>
                            <input class="form-control" type="text" name="txtCuotas" placeholder="Valor de cuota" required readonly />
                        </div>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Destino de pr&eacute;stamo: </label>
                    </td>
                    <td>
                        <select name="txtDestinoPrestamo" class="form-control">
                            <option value="1">Gastos personales</option>
                            <option value="2">Gastos m&eacute;dicos</option>
                            <option value="3">Consolidaci&oacute;n de deudas</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Estado de pr&eacute;stamo: </label>
                    </td>
                    <td>
                        <select name="estado" class="form-control">
                            <option value="1">Aprobado </option>
                            <option value="2">En proceso </option>
                            <option value="3">Cancelado </option>
                        </select>
                    </td>
                </tr>
                <hr />
                <tr>
                    <td colspan="2">
                        <label>Campos marcados con * son obligatorios </label>
                    </td>
                </tr>
            </table>
         </fieldset>
               <br />
               <fieldset style="border: 2px #84a3b4 outset; width:600px;">
                   <legend>Fiadores</legend>
                     <div class="row">
                        <div class="col-md-2">
                            <b>*Fiador 1:</b>
                        </div>
                        <div id="busqueda" class="col-md-5">    
                            <input id="consulta1" type="text" class="form-control" style="width: 250px;" name="consulta" onkeyup="buscar('consulta1', 'sug1')" />
                            <div class="sugerencias" id='sug1'></div>
                        </div>
                        <div class="col-md-2">
                            <a onclick="DialogoConBoton(event);" style="cursor: pointer;"> <i class="fa fa-plus"></i> Nuevo</a>
                            <input type="hidden" name="fiador" id="fiador_1" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Fiador 2:</b>
                        </div>
                        <div id="busqueda" class="col-md-5">   
                                <input id="consulta2" type="text" class="form-control" style="width: 250px;" name="fiador2" onkeyup="buscar('consulta2', 'sug2')" />
                                <div class="sugerencias" id='sug2'></div>
                        </div>
                        <div class="col-md-2">
                            <a onclick="DialogoConBoton(event);" style="cursor: pointer;"> <i class="fa fa-plus"></i> Nuevo</a>
                            <input type="hidden" name="fiador" id="fiador_2" />
                        </div>
                    </div>
                    <br />
               </fieldset>
               <br />
               <table> 
                    <tr align="center">
                        <td colspan = "2">
                            <input type="submit" class="btn btn-primary" value="Finalizar" />
                        </td>
                    </tr> 
               </table>
                </form> 
        <hr />
      </div>
      <div id="box" style="visibility:hidden;"></div>
<%@include file="../administracion/footer.jsp" %>

