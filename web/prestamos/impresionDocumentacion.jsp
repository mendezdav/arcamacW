<%-- 
    Document   : impresionDocumentacion
    Created on : 09-05-2014, 11:31:25 PM
    Author     : jorge
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 30px 15px;}
            .col-s a{height: 76px;}
            .col-small{padding-top: 20%; width: 150px;}
            .col-p{background-color: #7b34a5;}
            
       .wrapper{
                position: relative;
                float: left;
                left: 12%;
                width: 616px;
                margin-bottom: 4px;
                background-color: #ffffff
        }
        .left1{
           position: relative;
           float: left;
           left: 2px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left2{
           position: relative;
           float: left;
           left: 6px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left3{
           position: relative;
           float: left;
           left: 10px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left4{
           position: relative;
           float: left;
           left: 14px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }

        .left1:hover { background-color: gray;  }
        .left2:hover { background-color: gray;  }
        .left3:hover { background-color: gray;  }
        .left4:hover { background-color: gray;  }
</style>
    <div class="container">
           <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Reportes</li>
           </ol>
                <h2>Impresi&oacute;n de Documentaci&oacute;n</h2><br /><br />
           <div class="wrapper">
<%
    //SessionActual.setAttribute("codigoReporte", request.getParameter("codigoPrestamo"));
   // String codigoP = request.getParameter("codigo");
    String r = request.getParameter("r");
    String codigoP="";
    if (request.getParameter("codigo") != null && request.getParameter("codigo") != "")
    {
            SessionActual.setAttribute("codigoReporte", request.getParameter("codigo"));
            codigoP = request.getParameter("codigo");
    }
    else
    {    SessionActual.setAttribute("codigoReporte", request.getParameter("codigoPrestamo"));
         codigoP = request.getParameter("codigoPrestamo");
    }
    String tipo = request.getParameter("t");
    
    if (tipo != null && tipo != "")
    {
 %>
             <div class="left1" onclick="llamar('<%="resolucionPrestamo.jsp?exito=in&codigo=" + codigoP +"&r=" + r%>');">
                <br />Resoluci&oacute;n del Pr&eacute;stamo <br />
             </div>
               
	     <div class="left2" onclick="llamarReporte('<%=ruta%>');">
                 <br />Solicitud de Pr&eacute;stamo
	     </div>
             <div class="left3" onclick="llamarReporteTablaAmortizacion('<%=ruta%>');">
                 <br />Tabla Amortizaci&oacute;n
	     </div>
            <%
            if (!tipo.equals("3"))
            { }
            else 
            {
            %>
                <div class="left4" onclick="llamarReporteMutuo('<%=ruta%>');">
                    <br />Generar Mutuo
                </div>
           <%
            }
      }
    %>
 	   </div> 
    </div>
<%@include file="../administracion/footer.jsp" %>
<script>
  function llamar(dir)
    {window.location.href = dir;}
    
  function llamarReporte(ruta)
  {
    window.open(ruta + '/reporteSolicitud_fiadores','','resizable=yes,scrollbars=yes,height=400,width=800');
  }
  
  function llamarReporteMutuo(ruta)
  {
    window.open(ruta + '/GenerarMutuo','','resizable=yes,scrollbars=yes,height=400,width=800');
  }
  
  function llamarReporteTablaAmortizacion(ruta)
  {
    window.open(ruta + '/ReporteTablaAmortizacionIdel?tabla=s','','resizable=yes,scrollbars=yes,height=400,width=800');
  }
  
</script>