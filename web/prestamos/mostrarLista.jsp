<%-- 
    Document   : mostrarLista
    Created on : 18-ene-2014, 12:08:33
    Author     : Pochii
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <style media="all" type="text/css" >
        @import url("../css/displaytag.css");
    </style>
    <c:set var="ruta" value='<%=request.getScheme() +  "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()%>' /> 
   <c:set var="tipo" value='<%=request.getParameter("tipo")%>' />   
   <c:if test="${(tipo == NULL) || (tipo=='')}">
        <c:redirect url="${ruta}/panelAdmin.jsp">
            <c:param name="erno" value="2" /> 
        </c:redirect> 
   </c:if>
   
        <%
            /**
             * Declaracion de variable
             */
            String criterio;                            //Criterio de busqueda
            String query, queryActivo, queryCancelado;  //Consultas para prestamos cancelados, Activos y en proceso
            String tipo = request.getParameter("tipo"); //pagina de procedencia
            /**
             * url, es la ruta a la que redirigira la ultima columna
             * titulo, titulo que tendra la ultima columna en el displaytag
             */ 
            String url = "/estadoPrestamo.jsp";
            String titulo = "";
            criterio = request.getParameter("criterio"); 
            if(!criterio.equals(null) || !criterio.equals("")){
              query  =  " SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado, p.fechaEmision, p.tipo_pago "
                        + "FROM prestamos as p INNER JOIN solicitante as s ON "
                        + "s.idSolicitante = p.idSolicitante "
                        + "WHERE p.estado = 'En proceso' AND p.codigoPrestamo like '%" + criterio + "%'";
              queryActivo  = " SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado, p.fechaAprobacion,p.fechaVencimiento, p.tipo_pago "
                        + "FROM prestamos as p INNER JOIN solicitante as s ON "
                        + "s.idSolicitante = p.idSolicitante "
                        + "WHERE p.estado = 'Activo' AND p.codigoPrestamo like '%" + criterio + "%'";
              queryCancelado  = " SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado, p.fechaAprobacion,p.fechaVencimiento,  p.fechaFinalizacion, p.tipo_pago "
                        + "FROM prestamos as p INNER JOIN solicitante as s ON "
                        + "s.idSolicitante = p.idSolicitante "
                        + "WHERE p.estado = 'Pagado' AND p.codigoPrestamo like '%" + criterio + "%'";
            }
            else
            {
              query  = " SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado, p.fechaEmision, p.tipo_pago "
                        + "FROM prestamos as p INNER JOIN solicitante as s ON "
                        + "s.idSolicitante = p.idSolicitante "
                        + "WHERE p.estado = 'En proceso'";
              queryActivo  = " SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado, p.fechaAprobacion,p.fechaVencimiento, p.fechaFinalizacion, p.tipo_pago "
                        + "FROM prestamos as p INNER JOIN solicitante as s ON "
                        + "s.idSolicitante = p.idSolicitante "
                        + "WHERE p.estado = 'Activo'";
              queryCancelado  = " SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado, p.fechaAprobacion,p.fechaVencimiento, p.tipo_pago "
                        + "FROM prestamos as p INNER JOIN solicitante as s ON "
                        + "s.idSolicitante = p.idSolicitante "
                        + "WHERE p.estado = 'Pagado'";
            }
            /**Si se requiere ver la ficha del solicitante***/
            
            if(tipo.equals("sol")){
                
                query = query + " AND s.idSolicitante=" + request.getParameter("idSolicitante");
                queryActivo = queryActivo + " AND s.idSolicitante=" + request.getParameter("idSolicitante");
                queryCancelado = queryCancelado + " AND s.idSolicitante=" + request.getParameter("idSolicitante");
            }//fin if
            queryActivo = queryActivo + " order by p.fechaAprobacion DESC";
        %>
    <sql:query var="q1" dataSource="jdbc/mysql">
        <%=query%>
    </sql:query>
    <c:set var="title" value="<%=titulo%>" />
    <c:set var="url" value="<%=url%>" />
    
    <sql:query var="q2" dataSource="jdbc/mysql">
        <%=queryActivo%>
    </sql:query>
    <sql:query var="q3" dataSource="jdbc/mysql">
        <%=queryCancelado%>
    </sql:query>
    <h4>Pr&eacute;stamos activos en el sistema </h4>
    <ajax:displayTag id="displayTagFrame2" ajaxFlag="displayAjax">
     <display:table  id="prestamos_Act" name="${q2.rows}" pagesize="15" export="false">
            <display:column title="C&oacute;digo Pr&eacute;stamo" property="codigoPrestamo" sortable="true" />
            <display:column title="Solicitante" property="nombre" sortable="false" style="width:200px;" />
            <display:column title="Estado" property="estado" sortable="false" />
            <c:if test="${prestamos_Act.tipo_pago == 2}">
                <display:column title="Tipo pago" value="Efectivo" sortable="false" />
            </c:if>
            <c:if test="${prestamos_Act.tipo_pago == 1}">
                <display:column title="Tipo pago" value="Planilla" sortable="false" />
            </c:if>
            <display:column title="Fecha de aprobaci&oacute;n" property="fechaAprobacion" sortable="false" />
            <display:column title="Fecha de vencimiento" property="fechaVencimiento" sortable="false" />
            <display:column value="<i class='fa fa-list'></i>Ver detalle" url="/prestamos/detallePrestamo.jsp" paramId="codigoPrestamo" paramProperty="codigoPrestamo"  style="width:170px;text-align:center;"/>
            <display:column>
                <i class="fa fa-times"></i>
                <a onclick="rechazarPrestamo('${prestamos_Act.codigoPrestamo}', 1);" style="cursor:pointer;">
                    Eliminar Pr&eacute;stamo
                </a>
            </display:column>
     </display:table>            
    </ajax:displaytag>
    <h4>Pr&eacute;stamos NO aprobados en el sistema </h4>
    <ajax:displayTag id="displayTagFrame" ajaxFlag="displayAjax">
     <display:table  id="prestamos" name="${q1.rows}" pagesize="15" export="true">
            <display:column title="C&oacute;digo Pr&eacute;stamo" property="codigoPrestamo" sortable="true" />
            <display:column title="Solicitante" property="nombre" sortable="false" />
            <display:column title="Estado" property="estado" sortable="false" />
            <c:if test="${prestamos.tipo_pago == 2}">
                <display:column title="Tipo pago" value="Efectivo" sortable="false" />
            </c:if>
            <c:if test="${prestamos.tipo_pago == 1}">
                <display:column title="Tipo pago" value="Planilla" sortable="false" />
            </c:if>
            <display:column title="Fecha de emisi&oacute;n" property="fechaEmision" sortable="false" />
            <display:column>
                <i class='fa fa-edit'></i>
                    <a onclick="DialogoConBoton(event, 'Aprobar prestamo', 'caja_aprobar', 'box_aprobar', '${prestamos.codigoPrestamo}','hidden');" style="cursor:pointer;">
                        Aprobar
                    </a>
            </display:column>
            <display:column>
                <i class='fa fa-edit'></i>
                    <a onclick="DialogoConBoton(event, 'Aprobar prestamo anterior', 'caja_aprobarAnterior', 'box_aprobarAnterior', '${prestamos.codigoPrestamo}','hiddenAnterior');" style="cursor:pointer;">
                        Aprobar pr&eacute;stamo anterior
                    </a>
            </display:column>
            <display:column>
                <i class="fa fa-times"></i>
                <a onclick="rechazarPrestamo('${prestamos.codigoPrestamo}', 2);" style="cursor:pointer;">
                    Rechazar solicitud
                </a>
            </display:column>
   
      </display:table>            
    </ajax:displaytag>
    
    <h4>Pr&eacute;stamos cancelados</h4>
    <ajax:displayTag id="displayTagFrame2" ajaxFlag="displayAjax">
     <display:table  id="prestamos_C" name="${q3.rows}" pagesize="15" export="false">
            <display:column title="C&oacute;digo Pr&eacute;stamo" property="codigoPrestamo" sortable="true" />
            <display:column title="Solicitante" property="nombre" sortable="false" style="width:200px;" />
            <display:column title="Estado" property="estado" sortable="false" />
            <c:if test="${prestamos_C.tipo_pago == 2}">
                <display:column title="Tipo pago" value="Efectivo" sortable="false" />
            </c:if>
            <c:if test="${prestamos_C.tipo_pago == 1}">
                <display:column title="Tipo pago" value="Planilla" sortable="false" />
            </c:if>
            <display:column title="Fecha de aprobaci&oacute;n" property="fechaAprobacion" sortable="false" />
            <display:column title="Fecha de vencimiento" property="fechaVencimiento" sortable="false" />
            <display:column title="Fecha de finalizacion" property="fechaFinalizacion" sortable="false" />
            <display:column value="<i class='fa fa-list'></i>Ver detalle" url="/prestamos/detallePrestamo.jsp" paramId="codigoPrestamo" paramProperty="codigoPrestamo"  style="width:170px;text-align:center;"/>
      </display:table>            
    </ajax:displaytag>
    