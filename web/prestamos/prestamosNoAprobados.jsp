<%-- 
    Document   : prestamosActivos
    Created on : 10-15-2014, 03:53:14 PM
    Author     : Jorge Luis
--%><%@page import="javax.swing.JOptionPane"%>
<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../js/paginador/funciones.js"></script>
<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<script lang="javascript" src="../js/selectAjax.js"></script>
<script lang="javascript" src="../js/validacion.js"></script>
<%

    int RegistrosAMostrar=10;
    int PaginasIntervalo=3;
    int RegistrosAEmpezar=0;
    int PagAct = 1;
    int PagAct2 = 0;

    /** parametros que debe recibir **/
    String criterio = request.getParameter("criterio"); // criterio de busqueda en los registros
    if (criterio != null && criterio != "")
        criterio = criterio.replace("#","").replace("'","").replace("-","").replace("%"," ").replace("\\","").replace("$","")
                .replace("/","").replace("(","").replace(")","").replace("=","").replace("?","").replace(";","").replace(".","")
                .replace("{","").replace("}","").replace("[","").replace("]","").replace("*","").replace(",","");
    
    String pagR = request.getParameter("pagR");
    String pagina = request.getParameter("pagina");
    if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
    {
        RegistrosAEmpezar = 0;
        PagAct2 = Integer.parseInt(pagR);
    }
    try
    {
        PagAct2 = Integer.parseInt(pagR);
    }
    catch (Exception e1)
    {
        System.out.print("Error: " + e1.getMessage());
        PagAct2 = 1;
    }

    if (pagina==null || pagina==""){
        RegistrosAEmpezar=0;
        PagAct=2; // tendria q ser la 1, pero no lo es, por que mas adeante PagAct2-1
    }else{
        RegistrosAEmpezar=(Integer.valueOf(pagina).intValue()-1)* RegistrosAMostrar;
        PagAct=Integer.valueOf(pagina).intValue();
    }
    conexion con = new conexion();
    String qr="";
    int tmp = RegistrosAMostrar * ( PagAct2-1);
    if (tmp < 0) 
        tmp = 0;
    if (criterio != null && criterio !="" ) // si criterio no es nulo, colocar en cuadro busqueda
    {
            qr = "SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado,"
                    + "p.fechaEmision,p.refinanciado, tp.tipoPago FROM prestamos as p INNER JOIN solicitante as s "
                    + "ON s.idSolicitante = p.idSolicitante inner join tipopago as tp on p.tipo_pago = tp.idtipoPago WHERE  ( codigoPrestamo like '%"+ criterio +"%' "
                    + "or CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2)"
                    + " like '%" + criterio+ "%' )  and p.estado = 'En proceso' limit " + tmp + "," + RegistrosAMostrar;
      }
     else 
     {
            qr = "SELECT p.codigoPrestamo, p.cantidad, CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2) as nombre, p.estado,"
                    + "p.fechaEmision,p.refinanciado, tp.tipoPago FROM prestamos as p INNER JOIN solicitante as s "
                    + "ON s.idSolicitante = p.idSolicitante inner join tipopago as tp on p.tipo_pago = tp.idtipoPago WHERE p.estado = 'En proceso' limit " + tmp + "," + RegistrosAMostrar;
     }
    con.setRS(qr);
    ResultSet resultado = con.getRs();
    if (resultado.next())
    {
        out.println("<table class='tablita'>");
        out.println("<caption>Prestamos</caption>");
        out.println("<tr>");
        out.println("<th>Codigo Prestamo</th>");
        out.println("<th>Solicitante</th>");
        out.println("<th>Cantidad</th>");
        out.println("<th>Estado</th>");
        out.println("<th>Tipo Pago</th>");
        out.println("<th>Refinanciado</th>");
        out.println("<th>Fecha Emisi&oacute;n</th>");
        out.println("<th colspan='3'>Opciones</th>");
        out.println("</tr>");
        out.println("<tbody>");
    }
    resultado.previous();
    int contador = 0;
    while (resultado.next())
    {
        contador++;
        %>
        <tr id="grid">
        <td id="formnuevo">
            <a href="nuevoPrestamo.jsp?codigo=<%=resultado.getString("codigoPrestamo")%>"><%= resultado.getString("codigoPrestamo") %></a>
            
        
        </td>
        <td id="formnuevo"><%= resultado.getString("nombre") %></td>
        <td id="formnuevo"><%= resultado.getString("cantidad") %></td>
        <td id="formnuevo"><%= resultado.getString("estado") %></td>
        <td id="formnuevo"><%= resultado.getString("tipoPago") %></td>
        <td id="formnuevo">
            <%
            String refinanciado = resultado.getString("refinanciado");
            if (refinanciado.equals("Sin Refinanciamiento"))
                out.print("No");
            else out.print("Si");
            %>
        </td>
        <td id="formnuevo"><%= resultado.getString("fechaEmision") %></td>
        <td id="formnuevo">
        <i class='fa fa-edit'></i>
            <a onclick="DialogoConBoton(event, 'Aprobar prestamo', 'caja_aprobar', 'box_aprobar', '<%=resultado.getString("codigoPrestamo") %>','hidden');" style="cursor:pointer;">
            Aprobar
            </a>
        </td>
<!--
        <td id="formnuevo">
        <i class='fa fa-edit'></i>
            <a onclick="DialogoConBoton(event, 'Aprobar prestamo anterior', 'caja_aprobarAnterior', 'box_aprobarAnterior', '<%=resultado.getString("codigoPrestamo") %>','hiddenAnterior');" style="cursor:pointer;">
            Aprobar pr&eacute;stamo anterior
            </a>
        </td>
-->
        <td id="formnuevo">
        <i class="fa fa-times"></i>
            <a onclick="rechazarPrestamo('<%=resultado.getString("codigoPrestamo") %>', 2);" style="cursor:pointer;">
            Rechazar solicitud
            </a>
        </td>
        </tr>
        <%
    } 
    if (contador == 0) 
        out.println("<div class=\"noData\"><br />No hay datos que mostrar</div>");

    if (criterio != null || criterio !="" ) 
      {
        qr = "SELECT count(*) as total FROM prestamos as p INNER JOIN solicitante as s "
                    + "ON s.idSolicitante = p.idSolicitante WHERE  ( codigoPrestamo like '%"+ criterio +"%' "
                    + "or CONCAT(s.nombre1 ,' ' , s.nombre2, ' ' ,  ' ' , s.apellido1, ' ', s.apellido2)"
                    + " like '%" + criterio+ "%' )  and p.estado = 'En proceso'";
    }
    else 
    {
         qr = "SELECT count(*) as total FROM prestamos as p INNER JOIN solicitante as s "
                    + "ON s.idSolicitante = p.idSolicitante WHERE  p.estado = 'En proceso'";
    }
    con.setRS(qr);
    ResultSet rs = con.getRs();
    rs.next();
    String NroRegistros =  rs.getString("total");
    int PagAnt=PagAct-1;
    int PagSig=PagAct+1;
    double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
    int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
    if(Res>0){
        PagUlt=Math.floor(PagUlt)+1;
    }
    out.println("</tbody>");
    out.println("</table><br />");
    //principio del paginador
    out.println("<div style='width:300px; float:right'>");
    if(PagAct>(PaginasIntervalo+1)) {
        out.println("<a onclick=listaproductos('1'); class='paginador'><< Primero</a>");
        out.println("&nbsp;");
    }
    for ( int i = (PagAct2-PaginasIntervalo) ; i <= (PagAct2-1) ; i ++) {
        if(i>=1) {
            out.println("<a href='principalPrestamosMostrar.jsp?t=3&criterio=" +criterio + "&pag="+ i+"' class='paginador'>"+i+"</a>");
            out.println("&nbsp;");
        }
    }
    if (contador != 0){
        out.println("<span class='paginadoractivo'>"+PagAct2+"</span>");
        out.println("&nbsp;");}

        for ( int i = (PagAct2+1) ; i <= (PagAct2+PaginasIntervalo) ; i ++) 
        {
            if(i<=PagUlt) {
                out.println("<a href='principalPrestamosMostrar.jsp?t=3&criterio=" + criterio + "&pag="+ i+"' class='paginador'>"+i+"</a>");
                out.println("&nbsp;");
            }
        }

    if(PagAct<(PagUlt-PaginasIntervalo)) {
        out.println("<a href='principalPrestamosMostrar.jsp?t=3&criterio=" + criterio + "&pag="+ PagUlt+"' class='paginador'>Ultimo</a>");
    }
    out.println("</div>");
%>