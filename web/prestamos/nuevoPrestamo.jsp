<%@page import="Conexion.conexion"%>
<%@page import="Permisos.validador"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../administracion/header.jsp" %>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />
<jsp:useBean id="analisis" class="clases.nuevoPrestamo"  scope="request" /> 
<%
    validador ck = new validador();
    
    if(!ck.tieneAcceso(usuario, "Crear prestamo")){
        //response.sendRedirect("../administracion/e403.jsp");
    }
    
   if ( request.getParameter("codigo") == null ||   request.getParameter("codigo") == "") // no existe parametro de codigoPrestamo
   {
%>
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>  
<script language="javascript" src="<%=ruta%>/js/tags.js"></script> 
<script lang="javascript" src="<%=ruta%>/js/jquery.mask.js"></script>
<script lang="javascript" src="<%=ruta%>/js/selectAjax.js"></script>
<script lang="javascript" src="<%=ruta%>/js/paginador/funciones.js"></script>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script>
    /**
     * 
     * @param {String} id_caja id de la caja a evaluar
     * @param {type} tipo de campo a consultar: refinanciamiento, tasa_bcr, valor maximo etc
     * @returns {mensaje} valor de campo consultado
     */
    function verificacion(id_caja, tipo){
        var mensaje = "";
        if(id_caja != "" && tipo != ""){
            $.ajax({
               data: {tipo: tipo},
               type: "POST",
               url: "/arcamacW/consultasConfiguraciones"
              }
            ).done(function(respuesta_config){
                $("#respuesta_config").html(respuesta_config);
                mensaje = $("#mensaje_config").html(); //Obteniendo dato del servlet
            }).fail(function(){
                mensaje = 0;
            });
            return mensaje;
        }
    }
    /**
     * @param {type} event        evento en el cual se dispara la funcion
     * @param {String} consulta   input en el que se escribe el nombre del fiador
     * @param {String} hidden     input en el que se guarda el id del fiador
     */
    var div; //Div que contendrá el form a cargar
    var hidden; 
    function DialogoConBoton(event, consulta, hid){
            Dialog.header = "Nuevo fiador";
            Dialog.width  = "700px";
            hidden = hid;
            //Asignando los valores
            /**Estableciendo mascaras***/
            $("#consultas").val(consulta);
            div = $("#box").html();
            Dialog.content =  document.getElementById("caja").innerHTML;
            $("#caja").remove();
            Dialog.showDialog();
    }
    
    function fin(form, url, diverror)
    {
        var val = sumarize(form);
        if(val){
            /**Verificando si la tasa es menor a la maxima**/
            verificacion('interes',2);
            
            if($("#mensaje_config").html() >= $("#interes").val()){
                $("#btnfinalizar").attr("disabled",true);       //Deshabilitando el boton
                document.getElementById(form).action= url;document.getElementById(form).submit();
            }
            else{
                document.getElementById(diverror).innerHTML =" <div class=\"alert alert-warning fade in\"> "
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">X</button>"
                    + "El porcentaje de inter&eacute;s ingresado sobrepasa a la tasa m&aacute;xima establecida<br /></div>";
            }
        }
        else{
            document.getElementById(diverror).innerHTML =" <div class=\"alert alert-warning fade in\"> "
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">X</button>"
                    + "Los datos ingresados contienen errores, por favor aseg&uacute;rese de ingresarlos en el formato correcto<br /></div>";
        }
    }//fin 
    function loadContent(form, nombre, apellido)
    {
        var val = sumarize(form);
        if(val){ //Validando los datos
            $("#btnnuevo_fiador").attr("disabled",true);
            var input  = $("#consultas").val();
            //Tomando los valores del formulario
            var id1             = $("#id").val();
            var nombreFiador1   = nombre;
            var apellidoFiador1 = apellido;
            var edadF1          = $("#edadF").val(); 
            var DUIF1           = $("#DUIF").val();
            var NITF1           = $("#NITF").val();
            var DomicilioF1     = $("#DomicilioF").val();
            var profesionF1     = $("#profesionF").val();
            var lugarTrabajoF1  = $("#lugarTrabajoF").val();
            var cargoF1         = $("#cargoF").val();
            var id_departamento1 = $("#id_departamento").val();
            var id_municipio1    = $("#id_municipio").val();
            var telefono_trabajo1 = $("#telefono_trabajo").val();
            var ext1 = $("#ext").val();
            var direccionParticular1 = $("#direccionParticular").val();
            var telResidenciaF1 = $("#telResidenciaF").val();
            var celularF1 = $("#celularF").val();
            //var lugar_fecha1 = $("#lugar_fecha").val();
            var salario1 = $("#salario").val();
            var deducciones1 = $("#deducciones").val();
            var liquidez1 = $("#liquidez").val();
            var url1 = $("#url").val();
            
            $("#respuesta").empty();
            $.post("../Mantenimientos/fiadores/agregarFiadorAction.jsp?type=con",
             {idSolicitante: id1, nombreFiador: nombreFiador1, apellidoFiador: apellidoFiador1, edadF: edadF1, DUIF:DUIF1, NITF: NITF1,DomicilioF: DomicilioF1, profesionF: profesionF1, lugarTrabajoF: lugarTrabajoF1,cargoF: cargoF1, telefono_trabajo: telefono_trabajo1, ext: ext1,direccionParticular: direccionParticular1, telResidenciaF: telResidenciaF1,
                 celularF: celularF1,id_departamento: id_departamento1, id_municipio: id_municipio1, salario: salario1, deducciones: deducciones1, liquidez: liquidez1, url: url1}, 
             function(respuesta){
                 $("#respuesta").html(respuesta); 
                 $("#" + input).val($("#nombre").html());
                 $("#" + hidden).val($("#id_newfiador").html());
             });
             $("#simpleDialogInstance").fadeOut("slow"); 
             $("#simpleDialogBackScreenInstance").fadeOut("fast");
             $("#box").append(div);
             //Eliminando simpledialog
             $("#simpleDialogInstance").remove(); 
             $("#simpleDialogBackScreenInstance").remove();
         }
         else{
            alert("error en los datos del fiador");
            document.getElementById("error").innerHTML =" <div class=\"alert alert-warning fade in\"> "+ 
                "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>" + "Los datos ingresados contienen errores, por favor aseg&uacute;rese de ingresarlos en el formato correcto<br /></div>";}
    }
    function buscar(id, caja, input, opc, codigoPrestamo)
    {
        $('#'+id).sugerir(id, caja, input, opc, codigoPrestamo);
    }
    /**Funciones de calculo de cuota**/
    /**
     * 
     * @param {String} url
     * @param {String} id
     * @param {double} cantidad
     * @param {double} plazo
     * @param {double} interes
     */
    function carga(url, id, can, p, i, nfst)
    {
        window._cant = can;
        window._plaz = p;
       
        if($("#cantidadMandar").val() != "" && $("#plazo").val() != ""){
            $.get(url,{cantidad: can, plazo: p, interes: i, type: "new"}, function(respuesta){
                $("#"+id).val(respuesta);
                $("#fiadores").fadeIn("slow");
                $("#fiador1").fadeIn();
                 var cantidad_minima = $("#cantidad_minima_fiadores").html();
                 if(parseFloat(can) >= parseFloat(cantidad_minima))
                    $("#fiador2").fadeIn("middle");
                 else
                     $("#fiador2").fadeOut("middle");
            });
        }
        else{
            $('#costoPapeleria').html("");
            alert("Los campos cantidad y plazo son obligatorios");
        }
    }
    
function obtenerCostoPapeleria(){
    var cantidad = $('#cantidad').val();
    if(isNaN(cantidad)){
        $('#cantidad').val("");
    }else{
        $.ajax({
               data:  {cantidad: cantidad},
               type: "POST",
               url: 'consultarCostoPapeleria.jsp'
              }
            ).done(function (respuesta){
                $('#costoPapeleria').html("<br/>Se cargará un monto de $"+respuesta+" en concepto de papelería");
            }).fail(function(repuesta){
                $('#costoPapeleria').html("");
            });
    }
 }
    
    function seleccionarFiador(idFiador, nombreFiador){
        if(window._nfst==1){
            $('#consulta1').val(nombreFiador);
            $('#fiador_1').val(idFiador);
        }
        
        if(window._nfst==2){
            $('#consulta2').val(nombreFiador);
            $('#fiador_2').val(idFiador);
        }
        
        $('#contenedor').css('display', 'none');
        $('#bcl').css('display', 'none');
    }
    
    function mostrarCuadroSeleccion(nf){
        window._nfst = nf;
        paginarFiadores('mostrarFiador.jsp', 'txtNombre','contenedor', <% if(request.getParameter("pag")!="") out.print(request.getParameter("pag")); else out.print(1);%>,'nuevoPrestamo.jsp','6',<% out.print(request.getParameter("idSolicitante")); %>, window._cant, window._plaz);
        $('#bcl').css('display', 'block');
        $('#contenedor').css('display', 'block');
    }
    
    function paginarFiadores(url,cajaPrincipal,id, pagR, BaseUrl,listado, idSolicitante, cantidad, plazo)
    {
    var elemento = document.getElementById(id);
    var valorP = document.getElementById(cajaPrincipal);
    var y = valorP.value;
    if ( y != "")
    {
        if (pagR == null)
            pagR = 1;
        var cpr = "";
        if($('#codigoPrestamo').val()!="") cpr = "&codigoPrestamo="+$('#codigoPrestamo').val();
        var url1 = url + "?criterio=" + y + "&pagR=" + pagR + "&s=1&idSolicitante="+idSolicitante+"&cantidadP="+cantidad+"&plazoP="+plazo+"&nfst="+window._nfst + cpr;
        
        //var tm2 =  BaseUrl + "?criterio=" + y + "&pagR=" + pagR;
        elemento.innerHTML = "<i class='fa fa-spinner fa-spin'></i>";
        peticion.open("GET", url1);
        peticion.onreadystatechange = function() 
        {
            if(peticion.readyState == 4)
                elemento.innerHTML = peticion.responseText;
        }
        peticion.send(null);
    }
    else{
     if (listado === "1")
     {
         peticion.open("GET", "prestamosActivos.jsp?t=1&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";   
     }
     
     if (listado === "2")
     {
         peticion.open("GET", "prestamosCancelados.jsp?t=2&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";   
     }
     
     if (listado === "3")
     {
         peticion.open("GET", "prestamosNoAprobados.jsp?t=3&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalDocumetnos, de reimpresion
     if (listado === "4")
     {
         peticion.open("GET", "listadoPrestamos.jsp?t=3&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalPago, principalPago
     if (listado === "5")
     {
         peticion.open("GET", "listadoPrestamos.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     // viene de principalFiadores, mantenimiento
     if (listado === "6")
     {
         
         peticion.open("GET", "mostrarFiadorMantenimiento.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalPrestamosEliminar
     if (listado === "7")
     {
         peticion.open("GET", "prestamosEliminar.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     if (listado === "8")
     {
         peticion.open("GET", "mostrarPagos.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
   }
 }
 
 function verificarMonto(){
    var deudaTotal = $('#deudaTotal').val();
    var cantidad   = $('#cantidadMandar').val();
    if(cantidad < deudaTotal){
        $('#costoPapeleria').html("");
        alert("La cantidad no cumple con los requerimientos");
        $('#cantidadMandar').val("");
        $('#cantidadMandar').focus();
    }
 }
    
    $(document).ready(function(){
        window._nfst = null;
        window._cant = 0;
        window._plaz = 0;
        $('#txtNombre').val(" ");
        
        if(<% out.print(request.getParameter("pag")); %>!="" && <% out.print(request.getParameter("pag")); %>!=null) { 
            document.getElementById('cantidadMandar').value = <% out.print(request.getParameter("cantidadP"));%>;
            document.getElementById('plazo').value = <% out.print(request.getParameter("plazoP"));%>;
            $('#contenedor').css('display', 'block');
            $('#bcl').css('display','block');
            window._nfst = <% out.print(request.getParameter("nfst"));%>;
            mostrarCuadroSeleccion(window._nfst);
            carga('actionAnalisisNuevo.jsp','cuotas',
            document.getElementById('cantidadMandar').value, document.getElementById('plazo').value, document.getElementById('interes').value, window._nfst);
        }
});



</script>
<style>
             body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
            .tabla {width:600px; margin-bottom: 20px; }
            .container { max-width:  1000px; margin: 0 auto;}
            .form-control, .input-group {width:200px;} 
            .container h2 {font-family: Century Gothic; }  
            .tabla  tfoot, tr, td{ padding: 8px; border-collapse: separate; border-spacing: 10px 10px 10px 0px;}
            legend{text-align: left;}
            .seleccion{font-style: italic; min-height: 40px;padding: 5px; background: #ffffc1;}
	    .seleccion a{display: inline-block; padding-left: 6px; color: red;}
            #busqueda .sugerencias{position: absolute; top: 35px;left: 15px; z-index: 100; width: 300px;}
            #busqueda .sugerencias ul{padding: 5px;list-style-type: none;border: 1px solid black;background: white;}
            #busqueda .sugerencias ul li{margin: 0;border-bottom: 1px dotted #ccc;background: white; padding: 5px; }
	    #busqueda .sugerencias ul li:hover{background: #fcf8e3; cursor: pointer;}
            .transparent {
	/* Required for IE 5, 6, 7 */
	/* ...or something to trigger hasLayout, like zoom: 1; */
	width: 100%; 
		
	/* Theoretically for IE 8 & 9 (more valid) */	
	/* ...but not required as filter works too */
	/* should come BEFORE filter */
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
	
	/* This works in IE 8 & 9 too */
	/* ... but also 5, 6, 7 */
	filter: alpha(opacity=50);
	
	/* Older than Firefox 0.9 */
	-moz-opacity:0.5;
	
	/* Safari 1.x (pre WebKit!) */
	-khtml-opacity: 0.5;
    
	/* Modern!
	/* Firefox 0.9+, Safari 2?, Chrome any?
	/* Opera 9+, IE 9+ */
	opacity: 0.5;
}
</style>


     
        <!--Verificando parametros -->
        <c:choose>
            <c:when test="${empty sessionScope.opc}">
                <c:redirect url="panelPrestamos.jsp">
                    <c:param name="erno" value="2" />
                </c:redirect>
            </c:when>
            <c:when test="${!empty sessionScope.opc}">
                <c:set var="opc" value="${sessionScope.opc}" />
            </c:when>
        </c:choose>
        <c:if test="${empty param.idSolicitante}">
            <c:redirect url="panelPrestamos.jsp">
                <c:param name="erno" value="2" />
            </c:redirect>
        </c:if>
        <c:if test="${!empty param.idSolicitante}">
            <c:set var="idSolicitante" value="${param.idSolicitante}" />
        </c:if>
        <c:if test="${!empty param.codigoPrestamo}">
            <c:set var="codigoPrestamo" value="${param.codigoPrestamo}" />
            <c:set var="tituloVista" value="Solicitud de refinanciamiento" />
        </c:if>
        <c:if test="${empty param.codigoPrestamo}">
            <c:set var="codigoPrestamo" value="" />
            <c:set var="tituloVista" value="Solicitud de nuevo pr&eacute;stamo" />
            <c:set var="lblLimite" value="" />
            <c:set var="deudaTotal" value="0" />
        </c:if>
        
          <c:choose>
            <c:when test="${empty param.r}">
                <c:set var="r" value="Sin Refinanciamiento" />
            </c:when>
            <c:when test="${!empty param.r}">
                <c:set var="r" value="Refinanciado" />
            </c:when>
        </c:choose>
          <sql:query dataSource="jdbc/mysql" var="q1">
              SELECT ( p.saldo + SUM(pv.intereses) + SUM(pv.interes_moratorio) + SUM(pv.iva_interes) + SUM(pv.iva_mora) - SUM(pv.pago_interes) - SUM(pv.pago_interes_moratorio) - SUM(pv.pago_iva_interes) - SUM(pv.pago_iva_mora)) as deuda FROM pagosview as pv JOIN prestamos as p on pv.codigoPrestamo = p.codigoPrestamo  WHERE pv.codigoPrestamo = '${codigoPrestamo}' AND pv.estado != "Pagado" GROUP BY pv.codigoPrestamo
          </sql:query>
          <c:forEach var="info" items="${q1.rows}">
              <c:set var="deudaTotal" value="${info.deuda}" />
          </c:forEach>
         <c:if test="${!empty param.codigoPrestamo}">
            <c:set var="lblLimite" value="Debe superar la deuda pendiente de $${deudaTotal}" />
        </c:if>
              
        <input type='hidden' name='txtNombre' id='txtNombre' class='form-control'
               placeholder="Fiador"  
              oninput="paginar('mostrarFiador.jsp', 'txtNombre','contenedor', 1,'nuevoPrestamo.jsp','6');" />
        
        <div id="contenedor" style="box-shadow: 2px 2px 2px #000; position: fixed; z-index: 100001; width: 600px; margin-left: 50%; left: -300px; top: 25px; border: solid 1px #d3d3d3; background: #fff; padding: 20px;display: none;">
            
        </div>
        <div id="bcl" class="transparent" style="display:none; width:100%; height: 100%; background: #000;position:fixed;z-index: 100000; top: 0px;"></div>
        <div class="container" align="center">
            <h2>${tituloVista}</h2>
            <label>Por favor complete los siguientes campos </label>
            <form name="frmNuevoSolicitante" id="frmnuevoprestamo" action="actionGuardarPrestamo.jsp" method="POST">
            <fieldset>
                
                 <input type="hidden" value="${idSolicitante}" name="idSolicitante" />
                 <input type="hidden" id="consultas" />
                 <input type="hidden" id="deudaTotal" value="${deudaTotal}"/>
                 <input type="hidden" id="hidden" />
                 <input type="hidden" id="codigoPrestamo" value="${codigoPrestamo}"/>
                <div id="errorprestamo"></div>
                <label>Campos marcados con * son obligatorios </label>
               <table class="tabla">
                <tr>
                    <td>
                        <label>*Cantidad a solicitar (USD): </label>
                    </td>
                    <td>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                        <!--    <input class="form-control" type="text" name="cantidad" id="cantidad" placeholder="Cantidad" 
                                   onblur="verificacion('cantidad', 3);" 
                                   onchange="verificarMonto();" required autofocus />-->
                        
                         <input class="form-control" type="text" name="cantidad" id="cantidad" placeholder="Cantidad" 
                                   onkeyup="obtenerCostoPapeleria();"
                                   onblur="formatoCantidad(1);" 
                                   onchange="formatoCantidad(2);" required autofocus />
                          <input type="hidden" id="cantidadMandar" name="cantidadMandar" />
                          
                        </div>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Plazo a pagar(meses): </label>
                    <td>
                        <input type="text" class="form-control" name="plazo" id="plazo"  pattern="^[0-9]{1,2}$" placeholder="Ej. 12" 
                          onblur="validarMeses();" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Interes a aplicar: </label>
                    <td>
                       
                       <div class="input-group">
                          <!--Creando conexion !-->
                          <sql:query dataSource="jdbc/mysql" var="q1">
                              SELECT porcentaje_maximo_bcr FROM configuraciones
                          </sql:query>
                          <c:forEach var="f" items="${q1.rows}">
                              <span class="input-group-addon">%</span>
                          <input class="form-control" style="width: 150px;" type="text" id="interes" onchange="carga('actionAnalisisNuevo.jsp','cuotas',document.getElementById('cantidadMandar').value, document.getElementById('plazo').value, document.getElementById('interes').value);" name="interes" value="${f.porcentaje_maximo_bcr}" required />
                          </c:forEach>
                        </div>
                    </td>
                    <td></td>
                    <td><input type="button" class="btn btn-info" value="Analizar" onclick="carga('actionAnalisisNuevo.jsp','cuotas',
                         document.getElementById('cantidadMandar').value, document.getElementById('plazo').value, document.getElementById('interes').value);" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Cuotas (USD): </label>
                    </td>
                    <td>
                        <div class="input-group">
                          <span class="input-group-addon">$</span>
                            <input class="form-control" type="text" name="cuotas" id="cuotas" placeholder="Valor de cuota" required readonly />
                        </div>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tipo de pago: </label>
                    </td>
                    <td>
                        <select name="tipo_pago" class="form-control">
                            <option value="1">Planilla</option>
                            <option value="2">Efectivo</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Destino de pr&eacute;stamo: </label>
                    </td>
                    <td>
                        <select name="destinoPrestamos" class="form-control">
                            <option value="GP">Gastos personales</option>
                            <option value="GM">Gastos m&eacute;dicos</option>
                            <option value="CD">Consolidaci&oacute;n de deudas</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Estado de pr&eacute;stamo: </label>
                    </td>
                    <td>
                        <select name="estado" class="form-control" readonly>
                            <option value="En proceso">En proceso</option>
                        </select>
                    </td>
                </tr>
                <hr />
                <tr>
                    <td colspan="2">
                        <span style="font-size: 10px; color: red;">${lblLimite}</span>
                        <span id="costoPapeleria" style="font-size: 10px; color: red;"></span>
                    </td>
                </tr>
            </table>
         </fieldset>
               <br />
               <fieldset style="border: 2px #fff outset; width:650px; display: none; padding: 20px;" id ='fiadores' >
                   <legend>Fiadores</legend>
                   <div class="row" id="fiador1" style="display:none;">
                        <div class="col-md-2">
                            <b>*Fiador 1:</b>
                        </div>
                        <div id="busqueda" class="col-md-5">    
                            <input id="consulta1" type="text" class="form-control" style="width: 250px;" name="consulta" autocomplete="off" onkeyup="buscar('consulta1', 'sug1', 'fiador_1','${opc}', '${codigoPrestamo}')" required />
                            <div class="sugerencias" id='sug1'></div>
                            <input type="hidden" id="fiador_1" name="fiador1"   />
                        </div>
                        <div class="col-md-2">
                            <a onclick="mostrarCuadroSeleccion(1)" style="cursor: pointer;">Listado</a>
                            <br/>
                            <a onclick="DialogoConBoton(event, 'consulta1', 'fiador_1');" style="cursor: pointer;"> <i class="fa fa-plus"></i> Nuevo</a>
                        </div>
                    </div>
                    <br />
                    <div class="row" id="fiador2" style="display:none;">
                        <div class="col-md-2">
                            <b>*Fiador 2:</b>
                        </div>
                        <div id="busqueda" class="col-md-5">   
                                <input id="consulta2" type="text" class="form-control" style="width: 250px;"  onkeyup="buscar('consulta2', 'sug2', 'fiador_2', '${opc}', '${codigoPrestamo}')" autocomplete="off" />
                                <div class="sugerencias" id='sug2'></div>
                                 <input type="hidden" id="fiador_2" name="fiador2"  />
                        </div>
                        <div class="col-md-2">
                             <a onclick="mostrarCuadroSeleccion(2)" style="cursor: pointer;">Listado</a>
                             <br/>
                            <a onclick="DialogoConBoton(event, 'consulta2', 'fiador_2');" style="cursor: pointer;"> <i class="fa fa-plus"></i> Nuevo</a>
                        </div>
                    </div>
                    <br />
                    <sql:query dataSource="jdbc/mysql" var="q1">
                        SELECT cantidad_minima_fiadores,cantidad_maxima_prestamo FROM configuraciones
                    </sql:query>
                    <c:forEach var="f1" items="${q1.rows}">
                        Si el monto solicitado excede los ${f1.cantidad_minima_fiadores} (USD)  ser&aacute; necesario presentar dos fiadores.
                        <!-- Div que guardan la informacion de los parametros de configuracion !-->
                        <div id='cantidad_maxima_prestamo' style="display:none">${f1.cantidad_maxima_prestamo}</div>
                        <div id='cantidad_minima_fiadores' style="display:none">${f1.cantidad_minima_fiadores} </div>
                    </c:forEach>
                    
               </fieldset>
               <br />
               <table> 
                    <tr align="center">
                        <td colspan = "2">
                            <input type="button" id="btnfinalizar" onclick="fin('frmnuevoprestamo', 'actionGuardarPrestamo.jsp?r=${r}', 'errorprestamo')" class="btn btn-primary" value="Finalizar" />
                        </td>
                    </tr> 
               </table>
            </form> 
        <hr />
      </div>
      <div id="box" style="visibility: hidden;">
          <div class="container" id="caja">
            <fieldset>
            <legend>Nuevo Fiador</legend><br />
            <form class="form-signin" action="<%=ruta%>/Mantenimientos/fiadores/agregarFiadorAction.jsp?type=con" method="post" name="formulario" id="formulario">
            <div id="error">
            </div>
           <table class="tabla">
                <tr>
                    
                    <td><b>* Nombres:</b></td>
                    <input type="hidden" name="id" id="id" value="<%=request.getParameter("idSolicitante") %>" />
                    <input type="hidden" name="url"  id="url" value="<%=ruta%>/Mantenimientos/fiadores/agregarFiadorPaso2.jsp" />
                    <td><br /><input type="text" class="form-control requerido" size="40" placeholder="Nombre" id="nombreFiador"  name="nombreFiador" required onblur="cargarMascaras();validar('nombreFiador');" ></td>
                    <td><div id='divnombreFiador'> </div></td>
               </tr>
               <tr>
                    <td><b>* Apellido:</b></td>
                    <td><br /><input type="text" class="form-control requerido"  size="40" placeholder="Apellido" name="apellidoFiador" id="apellidoFiador" required onblur="validar('apellidoFiador');" ></td>
                    <td><div id='divapellidoFiador'> </div></td>
                </tr>

                <tr>
                    <td><b>* Edad:</b></td>
                    <td><br /><input type="text" size="40" class="form-control requerido" placeholder="Edad" name="edadF" id="edadF" pattern="^[0-9]{1,2}$" maxlenght="2" required onblur="validar('edadF');" ></td>
                    <td><div id='divedadF'> </div></td>
                </tr>
                <tr>
                    <td><b>* DUI:</b></td>
                    <td><br /><input type="text" size="40" class="form-control requerido" placeholder="DUI" name="DUIF" id="DUIF" size="10" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('DUIF');" /> </td>
                    <td><div id='divDUIF'> </div></td>

                </tr>
                <tr>
                    <td><b>* NIT:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="NIT" name="NITF" id="NITF" required pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" onblur="validar('NITF');" /></td>
                    <td><div id='divNITF'> </div></td>
               </tr>
               <tr>
                    <td><b>Domicilio:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Domicilio" name="DomicilioF" id="DomicilioF" onblur="validar('DomicilioF');" ></td>
                    <td><div id='divDomicilioF'> </div></td>
                </tr>

                <tr>
                    <td><b>Profesi&oacute;n:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Profesi&oacute;n" name="profesionF" id="profesionF" onblur="validar('profesionF');"  required ></td>
                    <td><div id='divprofesionF'> </div></td>
                </tr>
                <tr>
                    <td><b>Lugar Trabajo</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Lugar Trabajo" name="lugarTrabajoF" id="lugarTrabajoF" onblur="validar('lugarTrabajoF');" ></td>
                    <td><div id='divlugarTrabajoF'> </div></td>
                </tr>


                <tr>
                    <td><b>* Cargo:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Cargo" name="cargoF" id="cargoF" required onblur="validar('cargoF');" ></td>
                    <td><div id='divcargoF'> </div></td>
               </tr>
               <tr>
                    <td><b>Tel&eacute;fono Trabajo:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Tel&eacute;fono" name="telefono_trabajo" pattern="^[0-9]{4}-[0-9]{4}$"  id="telefono_trabajo" onblur="validar('telefono_trabajo');" ></td>
                    <td><div id='divtelefono_trabajo'> </div></td>
                </tr>

                <tr>
                    <td><b>Extensi&oacute;n:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Extensi&oacute;n" name="ext" id="ext" pattern="^[0-9]{1,8}$" onblur="validar('ext');"></td>
                    <td><div id='divext'> </div></td>
                </tr>
                <tr>
                    <td><b>* Direcci&oacute;n Particular:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Direcci&oacute;n" name="direccionParticular" id="direccionParticular" required  onblur="validar('direccionParticular');"></td>
                    <td><div id='divdireccionParticular'> </div></td>
                </tr>
                <tr>
                <td><b>Departamento:</b></td>
                <td>
                    <select name="id_departamento" id="id_departamento" class="form-control" onchange="cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0)" style="width: 150px; display: inline;">
                            <sql:query dataSource="jdbc/mysql" var="depto">
                                SELECT id_departamento, departamento FROM departamento</sql:query>
                            <c:forEach var="depto_f" items="${depto.rows}">
                                <option value="${depto_f.id_departamento}">${depto_f.departamento}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div id='divid_depto'></div></td>
                </tr>
                <tr>
                    <td><b>Municipio:</b></td>
                    <td>
                        <select name="id_municipio" id="id_municipio" class="form-control" style="width: 150px; display: inline;"></select>
                    </td>
                    <td><div id='divid_municipio'></div></td>
                </tr>
                <tr>
                    <td><b>Tel&eacute;fono Residencia:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Tel. Residencia" pattern="^[0-9]{4}-[0-9]{4}$" name="telResidenciaF" id="telResidenciaF" onblur="validar('telResidenciaF');"></td>
                    <td><div id='divtelResidenciaF'> </div></td>
                </tr>
                <tr>
                    <td><b>*Celular:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Celular" name="celularF" id="celularF" pattern="^[0-9]{4}-[0-9]{4}$" required onblur="validar('celularF');" ></td>
                    <td><div id='divcelularF'> </div></td>
                </tr>
             
                </table>
       <fieldset>
                <h3>Informaci&oacute;n financiera</h3>
                <table class='table'>
                    <tr>
                        <td><b>*Salario:</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='salario' id='salario' class='form-control requerido' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' placeholder="Salario" required onblur="validar('salario');" />
                            </div>
                        </td>
                        <td><div id='divsalario' class='infor'></div></td>
                        <td rowspan="5"><b>*Liquidez:</b></td>
                        <td>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='liquidez' id='liquidez' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' onblur="blurF();" placeholder="Liquidez" />
                            </div>
                        </td>
                        <td><div id='divliquidez' class='infor'></div></td>
                    </tr>
                    <tr>
                        <td><b>Descuentos (deducciones)</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='deducciones' id='deducciones' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' placeholder="Descuento" required readonly />
                            </div>
                        </td>
                    </tr>
                     <tr>
                <td colspan="6"><center><br />
              <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                  if (estado.equals("1"))
                  {
                      out.print("<div class=\"alert alert-success\">Fiador Agregado Exitosamente!</div>");
                  }
                  if (estado.equals("2"))
                  {
                      out.print("<div class=\"alert alert-danger\">Lo Sentimos ha ocurrido un error!</div>");
                  }
              }
              
              %>
            <br />
                <input type="button" class="btn btn-info" value="Agregar Fiador" id="btnnuevo_fiador" onclick="loadContent('formulario', document.getElementById('nombreFiador').value, document.getElementById('apellidoFiador').value);" />
                </center></td>
                </tr>

           </table>

            <br /><br />
            </form>
            </fieldset>
        </div> <!-- /container -->
      </div>

      <div id='respuesta' style="display: none;"></div>
      <div id='respuesta_config' style="display: none"></div>
  <% 
   } else // si tiene parametro de codigo de prestamo
   {
       String codigoPrestamo = request.getParameter("codigo");
       int plazo=0;
       int fiador=0;
       double tasa_interes =0.0;
       double cantidad     =0.0;
       String idSolicitante = "";
       String fiadores[] = null;
       String qr = "";
       conexion con = new conexion();
       qr = "select group_concat(concat(f.idFiadores,'-',f.nombreFiador,' ',f.apellidoFiador) SEPARATOR ';' )as fiador from "
               + "fiadores as f  inner join prestamo_fiador as p_f on p_f.id_fiador = f.idFiadores where id_prestamo='"+ codigoPrestamo +"'";     
       con.setRS(qr);
       ResultSet rs  = con.getRs();
       if (rs.next())
       {
           try 
           {
               fiadores = rs.getString("fiador").split(";");
               fiador = fiadores.length;
           }
           catch (Exception ex2)
           {
               System.out.printf("ex2: " + ex2.getMessage());
           }
           
       }else
           fiador=0;
       
       // cortando resultados
       String fiadoresPartes1[] = null;
       String fiadoresPartes2[] = null;
              
       if (fiador != 0)
       {
           if (fiador==1)
           {
               fiadoresPartes1 = fiadores[0].split("-");
           }
           else if (fiador==2)
           {
               fiadoresPartes1 = fiadores[0].split("-");
               fiadoresPartes2 = fiadores[1].split("-");
           }
       }
       
       qr = "select pre.idSolicitante,pre.cantidad, pre.plazo,pre.tasa_interes,pre.estado,t.tipoPago,d.descripcion_destino from prestamos as pre "
               + " inner join tipopago as t on t.idtipoPago = pre.tipo_pago inner join destinoprestamo as d  on "
               + "d.destinoprestamos = pre.destinoPrestamos where pre.codigoPrestamo='" + codigoPrestamo +"' and pre.estado='En proceso'";
       con.cerrarConexion();
       rs.close();
       con = new conexion();
       con.setRS(qr);
       rs = con.getRs();
       if (rs.next())
       {
           plazo = rs.getInt("plazo");
           tasa_interes = rs.getDouble("tasa_interes");
           cantidad = rs.getDouble("cantidad");
           idSolicitante = rs.getString("idSolicitante");
          %>
          

<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>  
<script language="javascript" src="<%=ruta%>/js/tags.js"></script> 
<script lang="javascript" src="<%=ruta%>/js/jquery.mask.js"></script>
<script lang="javascript" src="<%=ruta%>/js/selectAjax.js"></script>
<script lang="javascript" src="<%=ruta%>/js/paginador/funciones.js"></script>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script>
    /**
     * 
     * @param {String} id_caja id de la caja a evaluar
     * @param {type} tipo de campo a consultar: refinanciamiento, tasa_bcr, valor maximo etc
     * @returns {mensaje} valor de campo consultado
     */
    function verificacion(id_caja, tipo){
        var mensaje = "";
        if(id_caja != "" && tipo != ""){
            $.ajax({
               data: {tipo: tipo},
               type: "POST",
               url: "/arcamacW/consultasConfiguraciones"
              }
            ).done(function(respuesta_config){
                $("#respuesta_config").html(respuesta_config);
                mensaje = $("#mensaje_config").html(); //Obteniendo dato del servlet
            }).fail(function(){
                mensaje = 0;
            });
            return mensaje;
        }
    }
    /**
     * @param {type} event        evento en el cual se dispara la funcion
     * @param {String} consulta   input en el que se escribe el nombre del fiador
     * @param {String} hidden     input en el que se guarda el id del fiador
     */
    var div; //Div que contendrá el form a cargar
    var hidden; 
    function DialogoConBoton(event, consulta, hid){
            Dialog.header = "Nuevo fiador";
            Dialog.width  = "700px";
            hidden = hid;
            //Asignando los valores
            /**Estableciendo mascaras***/
            $("#consultas").val(consulta);
            div = $("#box").html();
            Dialog.content =  document.getElementById("caja").innerHTML;
            $("#caja").remove();
            Dialog.showDialog();
    }
    
    function fin(form, url, diverror)
    {
        var val = sumarize(form);
        if(val){
            /**Verificando si la tasa es menor a la maxima**/
            verificacion('interes',2);
            
            if($("#mensaje_config").html() >= $("#interes").val()){
                $("#btnfinalizar").attr("disabled",true);       //Deshabilitando el boton
                document.getElementById(form).action= url;document.getElementById(form).submit();
            }
            else{
                document.getElementById(diverror).innerHTML =" <div class=\"alert alert-warning fade in\"> "
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">X</button>"
                    + "El porcentaje de inter&eacute;s ingresado sobrepasa a la tasa m&aacute;xima establecida<br /></div>";
            }
        }
        else{
            document.getElementById(diverror).innerHTML =" <div class=\"alert alert-warning fade in\"> "
                    + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">X</button>"
                    + "Los datos ingresados contienen errores, por favor aseg&uacute;rese de ingresarlos en el formato correcto<br /></div>";
        }
    }//fin 
    function loadContent(form, nombre, apellido)
    {
        var val = sumarize(form);
        if(val){ //Validando los datos
            $("#btnnuevo_fiador").attr("disabled",true);
            var input  = $("#consultas").val();
            //Tomando los valores del formulario
            var id1             = $("#id").val();
            var nombreFiador1   = nombre;
            var apellidoFiador1 = apellido;
            var edadF1          = $("#edadF").val(); 
            var DUIF1           = $("#DUIF").val();
            var NITF1           = $("#NITF").val();
            var DomicilioF1     = $("#DomicilioF").val();
            var profesionF1     = $("#profesionF").val();
            var lugarTrabajoF1  = $("#lugarTrabajoF").val();
            var cargoF1         = $("#cargoF").val();
            var id_departamento1 = $("#id_departamento").val();
            var id_municipio1    = $("#id_municipio").val();
            var telefono_trabajo1 = $("#telefono_trabajo").val();
            var ext1 = $("#ext").val();
            var direccionParticular1 = $("#direccionParticular").val();
            var telResidenciaF1 = $("#telResidenciaF").val();
            var celularF1 = $("#celularF").val();
            //var lugar_fecha1 = $("#lugar_fecha").val();
            var salario1 = $("#salario").val();
            var deducciones1 = $("#deducciones").val();
            var liquidez1 = $("#liquidez").val();
            var url1 = $("#url").val();
            
            $("#respuesta").empty();
            $.post("../Mantenimientos/fiadores/agregarFiadorAction.jsp?type=con",
             {idSolicitante: id1, nombreFiador: nombreFiador1, apellidoFiador: apellidoFiador1, edadF: edadF1, DUIF:DUIF1, NITF: NITF1,DomicilioF: DomicilioF1, profesionF: profesionF1, lugarTrabajoF: lugarTrabajoF1,cargoF: cargoF1, telefono_trabajo: telefono_trabajo1, ext: ext1,direccionParticular: direccionParticular1, telResidenciaF: telResidenciaF1,
                 celularF: celularF1,id_departamento: id_departamento1, id_municipio: id_municipio1, salario: salario1, deducciones: deducciones1, liquidez: liquidez1, url: url1}, 
             function(respuesta){
                 $("#respuesta").html(respuesta); 
                 $("#" + input).val($("#nombre").html());
                 $("#" + hidden).val($("#id_newfiador").html());
             });
             $("#simpleDialogInstance").fadeOut("slow"); 
             $("#simpleDialogBackScreenInstance").fadeOut("fast");
             $("#box").append(div);
             //Eliminando simpledialog
             $("#simpleDialogInstance").remove(); 
             $("#simpleDialogBackScreenInstance").remove();
         }
         else{
            alert("error en los datos del fiador");
            document.getElementById("error").innerHTML =" <div class=\"alert alert-warning fade in\"> "+ 
                "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>" + "Los datos ingresados contienen errores, por favor aseg&uacute;rese de ingresarlos en el formato correcto<br /></div>";}
    }
    function buscar(id, caja, input, opc, codigoPrestamo)
    {
        $('#'+id).sugerir(id, caja, input, opc, codigoPrestamo);
    }
    /**Funciones de calculo de cuota**/
    /**
     * 
     * @param {String} url
     * @param {String} id
     * @param {double} cantidad
     * @param {double} plazo
     * @param {double} interes
     */
    function carga(url, id, can, p, i, nfst)
    {
        window._cant = can;
        window._plaz = p;
       
        if($("#cantidadMandar").val() != "" && $("#plazo").val() != ""){
            $.get(url,{cantidad: can, plazo: p, interes: i, type: "new"}, function(respuesta){
                $("#"+id).val(respuesta);
                $("#fiadores").fadeIn("slow");
                $("#fiador1").fadeIn();
                 var cantidad_minima = $("#cantidad_minima_fiadores").html();
                 if(parseFloat(can) >= parseFloat(cantidad_minima))
                    $("#fiador2").fadeIn("middle");
                 else
                     $("#fiador2").fadeOut("middle");
            });
        }
        else{
            alert("Los campos cantidad y plazo son obligatorios");
        }
    }
    
    function seleccionarFiador(idFiador, nombreFiador){
        if(window._nfst==1){
            $('#consulta1').val(nombreFiador);
            $('#fiador_1').val(idFiador);
        }
        
        if(window._nfst==2){
            $('#consulta2').val(nombreFiador);
            $('#fiador_2').val(idFiador);
        }
        
        $('#contenedor').css('display', 'none');
        $('#bcl').css('display', 'none');
    }
    
    function mostrarCuadroSeleccion(nf){
        window._nfst = nf;
        paginarFiadores('mostrarFiador.jsp', 'txtNombre','contenedor', <% if(request.getParameter("pag")!="") out.print(request.getParameter("pag")); else out.print(1);%>,'nuevoPrestamo.jsp','6',<% out.print(request.getParameter("idSolicitante")); %>, window._cant, window._plaz);
        $('#bcl').css('display', 'block');
        $('#contenedor').css('display', 'block');
    }
    
    function paginarFiadores(url,cajaPrincipal,id, pagR, BaseUrl,listado, idSolicitante, cantidad, plazo)
    {
    var elemento = document.getElementById(id);
    var valorP = document.getElementById(cajaPrincipal);
    var y = valorP.value;
    if ( y != "")
    {
        if (pagR == null)
            pagR = 1;
        var cpr = "";
        if($('#codigoPrestamo').val()!="") cpr = "&codigoPrestamo="+$('#codigoPrestamo').val();
        var url1 = url + "?criterio=" + y + "&pagR=" + pagR + "&s=1&idSolicitante="+idSolicitante+"&cantidadP="+cantidad+"&plazoP="+plazo+"&nfst="+window._nfst + cpr;
        
        //var tm2 =  BaseUrl + "?criterio=" + y + "&pagR=" + pagR;
        elemento.innerHTML = "<i class='fa fa-spinner fa-spin'></i>";
        peticion.open("GET", url1);
        peticion.onreadystatechange = function() 
        {
            if(peticion.readyState == 4)
                elemento.innerHTML = peticion.responseText;
        }
        peticion.send(null);
    }
    else{
     if (listado === "1")
     {
         peticion.open("GET", "prestamosActivos.jsp?t=1&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";   
     }
     
     if (listado === "2")
     {
         peticion.open("GET", "prestamosCancelados.jsp?t=2&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";   
     }
     
     if (listado === "3")
     {
         peticion.open("GET", "prestamosNoAprobados.jsp?t=3&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalDocumetnos, de reimpresion
     if (listado === "4")
     {
         peticion.open("GET", "listadoPrestamos.jsp?t=3&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalPago, principalPago
     if (listado === "5")
     {
         peticion.open("GET", "listadoPrestamos.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     // viene de principalFiadores, mantenimiento
     if (listado === "6")
     {
         
         peticion.open("GET", "mostrarFiadorMantenimiento.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalPrestamosEliminar
     if (listado === "7")
     {
         peticion.open("GET", "prestamosEliminar.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     if (listado === "8")
     {
         peticion.open("GET", "mostrarPagos.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
   }
 }
 
 function verificarMonto(){
    var deudaTotal = $('#deudaTotal').val();
    var cantidad   = $('#cantidadMandar').val();
    if(cantidad < deudaTotal){
        alert("La cantidad no cumple con los requerimientos");
        $('#cantidadMandar').val("");
        $('#cantidadMandar').focus();
    }
 }
    
    $(document).ready(function(){
        window._nfst = null;
        window._cant = 0;
        window._plaz = 0;
        $('#txtNombre').val(" ");
        
        if(<% out.print(request.getParameter("pag")); %>!="" && <% out.print(request.getParameter("pag")); %>!=null) { 
            document.getElementById('cantidadMandar').value = <% out.print(request.getParameter("cantidadP"));%>;
            document.getElementById('plazo').value = <% out.print(request.getParameter("plazoP"));%>;
            $('#contenedor').css('display', 'block');
            $('#bcl').css('display','block');
            window._nfst = <% out.print(request.getParameter("nfst"));%>;
            mostrarCuadroSeleccion(window._nfst);
            carga('actionAnalisisNuevo.jsp','cuotas',
            document.getElementById('cantidadMandar').value, document.getElementById('plazo').value, document.getElementById('interes').value, window._nfst);
        }
});

</script>
<style>
             body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
            .tabla {width:600px; margin-bottom: 20px; }
            .container { max-width:  1000px; margin: 0 auto;}
            .form-control, .input-group {width:200px;} 
            .container h2 {font-family: Century Gothic; }  
            .tabla  tfoot, tr, td{ padding: 8px; border-collapse: separate; border-spacing: 10px 10px 10px 0px;}
            legend{text-align: left;}
            .seleccion{font-style: italic; min-height: 40px;padding: 5px; background: #ffffc1;}
	    .seleccion a{display: inline-block; padding-left: 6px; color: red;}
            #busqueda .sugerencias{position: absolute; top: 35px;left: 15px; z-index: 100; width: 300px;}
            #busqueda .sugerencias ul{padding: 5px;list-style-type: none;border: 1px solid black;background: white;}
            #busqueda .sugerencias ul li{margin: 0;border-bottom: 1px dotted #ccc;background: white; padding: 5px; }
	    #busqueda .sugerencias ul li:hover{background: #fcf8e3; cursor: pointer;}
            .transparent {
	/* Required for IE 5, 6, 7 */
	/* ...or something to trigger hasLayout, like zoom: 1; */
	width: 100%; 
		
	/* Theoretically for IE 8 & 9 (more valid) */	
	/* ...but not required as filter works too */
	/* should come BEFORE filter */
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
	
	/* This works in IE 8 & 9 too */
	/* ... but also 5, 6, 7 */
	filter: alpha(opacity=50);
	
	/* Older than Firefox 0.9 */
	-moz-opacity:0.5;
	
	/* Safari 1.x (pre WebKit!) */
	-khtml-opacity: 0.5;
    
	/* Modern!
	/* Firefox 0.9+, Safari 2?, Chrome any?
	/* Opera 9+, IE 9+ */
	opacity: 0.5;
}
</style>

              
        <input type='hidden' name='txtNombre' id='txtNombre' class='form-control'
               placeholder="Fiador"  
              oninput="paginar('mostrarFiador.jsp', 'txtNombre','contenedor', 1,'nuevoPrestamo.jsp','6');" />
        
        <div id="contenedor" style="box-shadow: 2px 2px 2px #000; position: fixed; z-index: 100001; width: 600px; margin-left: 50%; left: -300px; top: 100px; border: solid 1px #d3d3d3; background: #fff; padding: 20px;display: none;">
            
        </div>
        <div id="bcl" class="transparent" style="display:none; width:100%; height: 100%; background: #000;position:fixed;z-index: 100000; top: 0px;"></div>
        <div class="container" align="center">
            <h2>${tituloVista}</h2>
            <label>Por favor complete los siguientes campos </label>
            <form name="frmMovificarSolicitante" id="frmMovificarSolicitante" action="actionModificarPrestamo.jsp" method="POST">
            <fieldset>
                
                 <input type="hidden" value="idSolicitante" name="idSolicitante" />
                 <input type="hidden" id="consultas" />
                 <input type="hidden" id="deudaTotal" value="${deudaTotal}"/>
                 <input type="hidden" id="hidden" />
                 <input type="hidden" id="codigoPrestamo" value="${codigoPrestamo}"/>
                <div id="errorprestamo"></div>
               <table class="tabla">
                <tr>
                    <td>
                        <label>*Cantidad a solicitar (USD): </label>
                    </td>
                    <td>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                        <!--    <input class="form-control" type="text" name="cantidad" id="cantidad" placeholder="Cantidad" 
                                   onblur="verificacion('cantidad', 3);" 
                                   onchange="verificarMonto();" required autofocus />-->
                        
                         <input class="form-control" type="text" name="cantidad" id="cantidad" placeholder="Cantidad" 
                                   onblur="formatoCantidad(1);" 
                                   onchange="formatoCantidad(2);" required autofocus
                                   value="<%=cantidad%>"/>
                          <input type="hidden" id="cantidadMandar" name="cantidadMandar" />
                          
                        </div>
                        <span style="font-size: 10px; color: red;">${lblLimite}</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Plazo a pagar(meses): </label>
                    <td>
                        <input type="text" class="form-control" name="plazo" id="plazo"  pattern="^[0-9]{1,2}$" placeholder="Ej. 12" 
                          onblur="validarMeses();" value="<%=plazo%>" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Interes a aplicar: </label>
                    <td>
                       
                       <div class="input-group">
                          <!--Creando conexion !-->
                          <sql:query dataSource="jdbc/mysql" var="q1">
                              SELECT porcentaje_maximo_bcr FROM configuraciones
                          </sql:query>
                          <c:forEach var="f" items="${q1.rows}">
                              <span class="input-group-addon">%</span>
                          <input class="form-control" style="width: 150px;" type="text" onchange="carga('actionAnalisisNuevo.jsp','cuotas',document.getElementById('cantidadMandar').value, document.getElementById('plazo').value, document.getElementById('interes').value);" id="interes"  name="interes" value="<%=tasa_interes%>" required />
                          </c:forEach>
                        </div>
                    </td>
                    <td></td>
                    <td><input type="button" class="btn btn-info" value="Analizar" onclick="carga('actionAnalisisNuevo.jsp','cuotas',
                         document.getElementById('cantidadMandar').value, document.getElementById('plazo').value, document.getElementById('interes').value);" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>*Cuotas (USD): </label>
                    </td>
                    <td>
                        <div class="input-group">
                          <span class="input-group-addon">$</span>
                            <input class="form-control" type="text" name="cuotas" id="cuotas" placeholder="Valor de cuota" required readonly />
                        </div>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Tipo de pago: </label>
                    </td>
                    <td>
                        <select name="tipo_pago" class="form-control">
                            <option value="1">Planilla</option>
                            <option value="2">Efectivo</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Destino de pr&eacute;stamo: </label>
                    </td>
                    <td>
                        <select name="destinoPrestamos" class="form-control">
                            <option value="GP">Gastos personales</option>
                            <option value="GM">Gastos m&eacute;dicos</option>
                            <option value="CD">Consolidaci&oacute;n de deudas</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Estado de pr&eacute;stamo: </label>
                    </td>
                    <td>
                        <select name="estado" class="form-control" readonly>
                            <option value="En proceso">En proceso</option>
                        </select>
                    </td>
                </tr>
                <hr />
                <tr>
                    <td colspan="2">
                        <label>Campos marcados con * son obligatorios </label>
                    </td>
                </tr>
            </table>
         </fieldset>
               <br />
               <fieldset style="border: 2px #84a3b4 outset; width:600px; display: none;" id ='fiadores' >
                   <legend>Fiadores</legend>
                   <div class="row" id="fiador1" style="display:none;">
                        <div class="col-md-2">
                            <b>*Fiador 1:</b>
                        </div>
                        <div id="busqueda" class="col-md-5">    
                        <%
                        if (fiador == 0)
                        {
                            %>
                               <input id="consulta1" type="text" class="form-control" style="width: 250px;" name="consulta" autocomplete="off" onkeyup="buscar('consulta1', 'sug1', 'fiador_1','${opc}', '${codigoPrestamo}')"  required />
                            <%
                        }
                        %>
                        <%
                        if (fiador >= 1)
                        {
                            %>
                               <input id="consulta1" type="text" class="form-control" style="width: 250px;" name="consulta" autocomplete="off" onkeyup="buscar('consulta1', 'sug1', 'fiador_1','${opc}', '${codigoPrestamo}')"  value="<%=  fiadoresPartes1[1]%>" required />
                            <%
                        }
                        %>
                            <div class="sugerencias" id='sug1'></div>
                        <%
                        if (fiador == 0)
                        {
                            %>
                            <input type="hidden" id="fiador_1" name="fiador1"  />
                            <%
                        }
                        %>
                        <%
                        if (fiador >= 1)
                        {
                            %>
                            <input type="hidden" id="fiador_1" name="fiador1" value="<%=  fiadoresPartes1[0]%>"   />
                            <%
                        }
                        %>
                        </div>
                        <div class="col-md-2">
                            <a onclick="mostrarCuadroSeleccion(1)" style="cursor: pointer;">Listado</a>
                            <br/>
                            <a onclick="DialogoConBoton(event, 'consulta1', 'fiador_1');" style="cursor: pointer;"> <i class="fa fa-plus"></i> Nuevo</a>
                        </div>
                    </div>
                    <br />
                    <div class="row" id="fiador2" style="display:none;">
                        <div class="col-md-2">
                            <b>*Fiador 2:</b>
                        </div>
                        <div id="busqueda" class="col-md-5">   
                        <%
                        if (fiador == 0)
                        {
                            %>
                            <input id="consulta2" type="text" class="form-control" style="width: 250px;"  onkeyup="buscar('consulta2', 'sug2', 'fiador_2', '${opc}', '${codigoPrestamo}')"  autocomplete="off" />
                            <%
                        }
                        %>
                        <%
                        if (fiador == 2)
                        {
                            %>
                            <input id="consulta2" type="text" class="form-control" style="width: 250px;"  onkeyup="buscar('consulta2', 'sug2', 'fiador_2', '${opc}', '${codigoPrestamo}')"  value="<%=  fiadoresPartes2[1]%>" autocomplete="off" />
                            <%
                        }
                        %>
                        <div class="sugerencias" id='sug2'></div>
                        <%
                        if (fiador == 0)
                        {
                            %>
                            <input type="hidden" id="fiador_2" name="fiador2"  />
                            <%
                        }
                        %>
                        <%
                        if (fiador == 2)
                        {
                            %>
                                <input type="hidden" id="fiador_2" name="fiador2" value="<%=  fiadoresPartes2[0]%>" />
                            <%
                        }
                        %>
                        </div>
                        <div class="col-md-2">
                             <a onclick="mostrarCuadroSeleccion(2)" style="cursor: pointer;">Listado</a>
                             <br/>
                            <a onclick="DialogoConBoton(event, 'consulta2', 'fiador_2');" style="cursor: pointer;"> <i class="fa fa-plus"></i> Nuevo</a>
                        </div>
                    </div>
                                
                                
                    <br />
                    <sql:query dataSource="jdbc/mysql" var="q1">
                        SELECT cantidad_minima_fiadores,cantidad_maxima_prestamo FROM configuraciones
                    </sql:query>
                    <c:forEach var="f1" items="${q1.rows}">
                        Si el monto solicitado excede los ${f1.cantidad_minima_fiadores} (USD)  ser&aacute; necesario presentar dos fiadores.
                        <!-- Div que guardan la informacion de los parametros de configuracion !-->
                        <div id='cantidad_maxima_prestamo' style="display:none">${f1.cantidad_maxima_prestamo}</div>
                        <div id='cantidad_minima_fiadores' style="display:none">${f1.cantidad_minima_fiadores} </div>
                    </c:forEach>
                    
               </fieldset>
               <br />
               <table> 
                    <tr align="center">
                        <td colspan = "2">
                            
                            <input type="button" id="btnfinalizar" onclick="fin('frmMovificarSolicitante', 'actionModificarPrestamo.jsp?r=Sin%20Refinanciamiento&codigoPrestamo=<%=codigoPrestamo %>', 'errorprestamo')" class="btn btn-primary" value="Finalizar" />
                        </td>
                    </tr> 
               </table>
            </form> 
        <hr />
      </div>
      <div id="box" style="visibility: hidden;">
          <div class="container" id="caja">
            <fieldset>
            <legend>Nuevo Fiador</legend><br />
            <form class="form-signin" action="<%=ruta%>/Mantenimientos/fiadores/agregarFiadorAction.jsp?type=con" method="post" name="formulario2" id="formulario2">
            <div id="error">
            </div>
           <table class="tabla">
                <tr>
                    
                    <td><b>* Nombres:</b></td>
                    <input type="hidden" name="id" id="id" value="<%=idSolicitante %>" />
                    <input type="hidden" name="url"  id="url" value="<%=ruta%>/Mantenimientos/fiadores/agregarFiadorPaso2.jsp" />
                    <td><br /><input type="text" class="form-control requerido" size="40" placeholder="Nombre" id="nombreFiador"  name="nombreFiador" required onblur="cargarMascaras();validar('nombreFiador');" ></td>
                    <td><div id='divnombreFiador'> </div></td>
               </tr>
               <tr>
                    <td><b>* Apellido:</b></td>
                    <td><br /><input type="text" class="form-control requerido"  size="40" placeholder="Apellido" name="apellidoFiador" id="apellidoFiador" required onblur="validar('apellidoFiador');" ></td>
                    <td><div id='divapellidoFiador'> </div></td>
                </tr>

                <tr>
                    <td><b>* Edad:</b></td>
                    <td><br /><input type="text" size="40" class="form-control requerido" placeholder="Edad" name="edadF" id="edadF" pattern="^[0-9]{1,2}$" maxlenght="2" required onblur="validar('edadF');" ></td>
                    <td><div id='divedadF'> </div></td>
                </tr>
                <tr>
                    <td><b>* DUI:</b></td>
                    <td><br /><input type="text" size="40" class="form-control requerido" placeholder="DUI" name="DUIF" id="DUIF" size="10" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('DUIF');" /> </td>
                    <td><div id='divDUIF'> </div></td>

                </tr>
                <tr>
                    <td><b>* NIT:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="NIT" name="NITF" id="NITF" required pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" onblur="validar('NITF');" /></td>
                    <td><div id='divNITF'> </div></td>
               </tr>
               <tr>
                    <td><b>Domicilio:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Domicilio" name="DomicilioF" id="DomicilioF" onblur="validar('DomicilioF');" ></td>
                    <td><div id='divDomicilioF'> </div></td>
                </tr>

                <tr>
                    <td><b>Profesi&oacute;n:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Profesi&oacute;n" name="profesionF" id="profesionF" required onblur="validar('profesionF');" ></td>
                    <td><div id='divprofesionF'> </div></td>
                </tr>
                <tr>
                    <td><b>Lugar Trabajo</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Lugar Trabajo" name="lugarTrabajoF" id="lugarTrabajoF" onblur="validar('lugarTrabajoF');" ></td>
                    <td><div id='divlugarTrabajoF'> </div></td>
                </tr>


                <tr>
                    <td><b>* Cargo:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Cargo" name="cargoF" id="cargoF" required onblur="validar('cargoF');" ></td>
                    <td><div id='divcargoF'> </div></td>
               </tr>
               <tr>
                    <td><b>Tel&eacute;fono Trabajo:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Tel&eacute;fono" name="telefono_trabajo" pattern="^[0-9]{4}-[0-9]{4}$"  id="telefono_trabajo" onblur="validar('telefono_trabajo');" ></td>
                    <td><div id='divtelefono_trabajo'> </div></td>
                </tr>

                <tr>
                    <td><b>Extensi&oacute;n:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Extensi&oacute;n" name="ext" id="ext" pattern="^[0-9]{1,8}$" onblur="validar('ext');"></td>
                    <td><div id='divext'> </div></td>
                </tr>
                <tr>
                    <td><b>* Direcci&oacute;n Particular:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Direcci&oacute;n" name="direccionParticular" id="direccionParticular" required  onblur="validar('direccionParticular');"></td>
                    <td><div id='divdireccionParticular'> </div></td>
                </tr>
                <tr>
                <td><b>Departamento:</b></td>
                <td>
                    <select name="id_departamento" id="id_departamento" class="form-control" onchange="cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0)" style="width: 150px; display: inline;">
                            <sql:query dataSource="jdbc/mysql" var="depto">
                                SELECT id_departamento, departamento FROM departamento</sql:query>
                            <c:forEach var="depto_f" items="${depto.rows}">
                                <option value="${depto_f.id_departamento}">${depto_f.departamento}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div id='divid_depto'></div></td>
                </tr>
                <tr>
                    <td><b>Municipio:</b></td>
                    <td>
                        <select name="id_municipio" id="id_municipio" class="form-control" style="width: 150px; display: inline;"></select>
                    </td>
                    <td><div id='divid_municipio'></div></td>
                </tr>
                <tr>
                    <td><b>Tel&eacute;fono Residencia:</b></td>
                    <td><br /><input type="text" class="form-control" placeholder="Tel. Residencia" pattern="^[0-9]{4}-[0-9]{4}$" name="telResidenciaF" id="telResidenciaF" onblur="validar('telResidenciaF');"></td>
                    <td><div id='divtelResidenciaF'> </div></td>
                </tr>
                <tr>
                    <td><b>*Celular:</b></td>
                    <td><br /><input type="text" class="form-control requerido" placeholder="Celular" name="celularF" id="celularF" pattern="^[0-9]{4}-[0-9]{4}$" required onblur="validar('celularF');" ></td>
                    <td><div id='divcelularF'> </div></td>
                </tr>
             
                </table>
       <fieldset>
                <h3>Informaci&oacute;n financiera</h3>
                <table class='table'>
                    <tr>
                        <td><b>*Salario:</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='salario' id='salario' class='form-control requerido' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' placeholder="Salario" required onblur="validar('salario');" />
                            </div>
                        </td>
                        <td><div id='divsalario' class='infor'></div></td>
                        <td rowspan="5"><b>*Liquidez:</b></td>
                        <td>
                             <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='liquidez' id='liquidez' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' onblur="blurF();" placeholder="Liquidez" />
                            </div>
                        </td>
                        <td><div id='divliquidez' class='infor'></div></td>
                    </tr>
                    <tr>
                        <td><b>Descuentos (deducciones)</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='deducciones' id='deducciones' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' placeholder="Descuento" required readonly />
                            </div>
                        </td>
                    </tr>
                     <tr>
                <td colspan="6"><center><br />
                      <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                  if (estado.equals("1"))
                  {
                      out.print("<div class=\"alert alert-success\">Fiador Agregado Exitosamente!</div>");
                  }
                  if (estado.equals("2"))
                  {
                      out.print("<div class=\"alert alert-danger\">Lo Sentimos ha ocurrido un error!</div>");
                  }
              }
              
              %>
            <br />
                <input type="button" class="btn btn-info" value="Agregar Fiador" id="btnnuevo_fiador" onclick="loadContent('formulario2', document.getElementById('nombreFiador').value, document.getElementById('apellidoFiador').value);" />
                </center></td>
                </tr>

           </table>

            <br /><br />
            </form>
            </fieldset>
        </div> <!-- /container -->
      </div>

      <div id='respuesta' style="display: none;"></div>
      <div id='respuesta_config' style="display: none"></div>
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          <%
           
       }
       else
       {
           response.sendRedirect("/arcamacW/prestamos/principalPrestamosMostrar.jsp?t=3&erno=16");
       }
   }
%>
   
<script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
<%@include file="../administracion/footer.jsp" %>
<script>
    $(document).ready(function(){
       $("#cantidadMandar").blur(function (){
           if(parseFloat($("#cantidadMandar").val()) > parseFloat($("#cantidad_maxima_prestamo").html())){
               alert("La cantidad no puede exceder a los $" + $("#cantidad_maxima_prestamo").html());
               var cantidad_maxima = $("#cantidad_maxima_prestamo").html();
               cantidad_maxima = parseFloat(cantidad_maxima).toFixed(2);
               $("#cantidadMandar").val(cantidad_maxima);
           }
       });
       /**Cargando los municipios*/
        cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0);
    });
    function cargarMascaras(){
       $("#DUIF").mask('00000000-0');
       $("#NITF").mask('0000-000000-000-0');
       $("#telResidenciaF").mask('0000-0000');
       $("#celularF").mask('0000-0000');
       $("#telefono_trabajo").mask('0000-0000');
    } //fin cargar mascaras
     function blurF(){
          if($("#salario").val() == "" || $("#liquidez").val() == "") 
              alert("Los campos Salario y liquidez son obligatorios");
          else 
          {
              var deduccion;
              $("#deducciones").val($("#salario").val() - $("#liquidez").val());
              deduccion = $("#deducciones").val();
              deduccion = parseFloat(deduccion).toFixed(2);
              
              $("#deducciones").val(deduccion);  //Redondeando numero
              if($("#deducciones").val() <= 0){
                  $("#deducciones").css("border-color","#a94442");}
              else
                  $("#deducciones").css("border-color", "#4b984a");
          }
    }
    
    function validarMeses()
    {
        var valor = $('#plazo').val();
        valor = valor.trim();
        var partes = valor.split(" ",2);
        if (partes.length > 1)
        {
            alert('En este campo solo se requiere el número de meses, ejemplo: 12');
            if (!isNaN(partes[0]))
                 document.getElementById('plazo').value = partes[0];
            else
                 document.getElementById('plazo').value = 12;
        }
        // llamar fucion
        carga('actionAnalisisNuevo.jsp','cuotas',
        document.getElementById('cantidadMandar').value, document.getElementById('plazo').value.trim(), document.getElementById('interes').value, window._nfst);
    }
    
    function  eliminarComa(cantidad)
    {
        var patron=",";
        //indicamos que queremos sustituir el patron por una cadena vacia
        cantidad=cantidad.replace(patron,'');
        return cantidad;
    }
    
    function formatoCantidad(tipo)
    {
         // copiando valor antes de modificarlo
        cantidad = eliminarComa(document.getElementById('cantidad').value);
        document.getElementById('cantidadMandar').value = cantidad;
        var partes = cantidad.split(".");
        // partes =1, no lleva decimales  , si partes=2 si lleva . decimales
            var cadena1 = '';
            entero = partes[0].split('');
                if(entero.length === 4 )
                {
                    for (var i=0; i < entero.length; i++) {
                        if (i===0)
                        {
                            cadena1 += entero[i] + ',';
                        }
                        else
                          cadena1 += entero[i];
                    }
                    if (partes.length === 1)
                        cadena1 += '.00';
                    else
                         cadena1 += '.' +(partes[1]+ "0");
                    
                document.getElementById('cantidad').value = cadena1;
                } else if (entero.length <= 3)
                {
                     for (var i=0; i < entero.length; i++) {
                       cadena1 += entero[i];
                     }
                    if (partes.length === 1)
                        cadena1 += '.00';
                    else
                        cadena1 += '.' +(partes[1]+ "0");
                    
                document.getElementById('cantidad').value = cadena1;
                }
                // verificando que no tenga 3 decimales ( 3 ceros a la derecha)
                cantidad =  document.getElementById('cantidad').value;
                partes = cantidad.split(".");
                partesDecimal = partes[1].split("");
                if (partesDecimal.length === 3)
                {   
                    partes2 = partes[1].split("");
                    document.getElementById('cantidad').value = partes[0]+"."+ partes2[0] + partes2[1];
                }
                
                if (tipo=== 1)
                    verificacion(document.getElementById('cantidadMandar').value, 3);
                else if (tipo === 2)
                    verificarMonto();
    }
</script>