<%-- 
    Document   : listadoSolicitante
    Created on : 06-21-2014, 09:15:33 PM
    Author     : jorge
--%>


<%@page import="javax.swing.JOptionPane"%>
<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>

<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
int RegistrosAMostrar=10;
int PaginasIntervalo=3;
int RegistrosAEmpezar=0;
int PagAct = 1;
int PagAct2 = 0;

String criterio = request.getParameter("criterio");
String opc      = request.getParameter("opc");         //Tipo de prestamo
String pagR     = request.getParameter("pagR");

  if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
  {
      RegistrosAEmpezar = 0;
      PagAct2 = Integer.parseInt(pagR);
  }
 
String pag = request.getParameter("pagina");
if (pag==null || pag==""){
RegistrosAEmpezar=0;
PagAct=2; // tendria q ser la 1
}else{
RegistrosAEmpezar=(Integer.valueOf(pag).intValue()-1)* RegistrosAMostrar;
PagAct=Integer.valueOf(pag).intValue();
}
%>

<%
conexion con = new conexion();
String qr="";
int tmp = RegistrosAMostrar * ( PagAct2-1);
if (tmp < 0)
tmp =0;

 if (criterio != null && criterio !="" ) // si criterio no es nulo, colocar en cuadro busqueda
  {
      //qr ="select * from pagos where codigoPrestamo like '%" + request.getParameter("criterio")+ "%' limit " + tmp + "," + RegistrosAMostrar;
        
        qr = "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion "
             + "FROM solicitante where estado_sistema='activo' AND  idSolicitante NOT IN (SELECT idSolicitante FROM prestamos WHERE estado='Activo' OR estado='En proceso' GROUP BY idSolicitante) AND "
             + "concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%" + criterio + "%' limit " + tmp + "," + RegistrosAMostrar;
              
  }
 else 
 {
    // qr ="select * from pagos  limit " + tmp+ "," + RegistrosAMostrar;
      qr  =  "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion "
             + "FROM solicitante where estado_sistema='activo' AND  idSolicitante NOT IN (SELECT idSolicitante FROM prestamos "
              + "WHERE estado='Activo' OR estado='En proceso' GROUP BY idSolicitante) limit " + tmp + "," + RegistrosAMostrar;
 }
    con.setRS(qr);
    ResultSet resultado = con.getRs();
    if (resultado.next())
    {
        out.println("<table class='tablita'>");
        out.println("<caption>Solicitantes Disponibles</caption>");
        out.println("<tr>");
        out.println("<th>Nombre</th>");
        out.println("<th>DUI</th>");
        out.println("<th>Edad</th>");
        out.println("<th>Profesi&oacute;n</th>");
        out.println("<th>Tel&eacute;fono</th>");
        out.println("<th>Estado</th>");
        out.println("<th colspan=\"2\">Opciones</th>");
        out.println("</tr>");
        out.println("<tbody>");
    }
    resultado.previous();
    int contador = 0;
    String idSolicitante ="";
while (resultado.next())
{
    contador++;
    idSolicitante = resultado.getString("idSolicitante");
%>
    <tr id="grid">
        <td id="formnuevo"><%= resultado.getString("solicitante") %></td>
        <td id="formnuevo"><%= resultado.getString("DUI") %></td>
        <td id="formnuevo"><%= resultado.getString("edad") %></td>
        <td id="formnuevo"><%= resultado.getString("profesion") %></td>
        <td id="formnuevo"><%= resultado.getString("celular") %></td>
        <td id="formnuevo"><%= resultado.getString("estado_sistema") %></td>
        <td id="formnuevo"><a href="nuevoPrestamo.jsp?idSolicitante=<%=idSolicitante%>" >Seleccionar</a></td>
    </tr>
<%
} 
if (contador == 0)
{ 
    out.println("<div class=\"noData\"><br />No hay datos que mostrar</div>");
}
%>
<%

 if (criterio != null || criterio !="" ) 
    //qr ="select count(*) as total from Usuarios  where concat(nombreUsuario,' ' , apellidoUsuario)  like '%"  + criterio + "%'";
     qr ="select count(*)  as total FROM solicitante where estado_sistema='activo' AND  idSolicitante NOT IN (SELECT idSolicitante FROM "
             + "prestamos WHERE estado='Activo' OR estado='En proceso' GROUP BY idSolicitante) AND "
             + "concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%" +criterio+ "%'";
 else 
    qr ="select count(*) as total FROM solicitante where estado_sistema='activo' AND  idSolicitante NOT IN (SELECT idSolicitante FROM "
            + "prestamos WHERE estado='Activo' OR estado='En proceso' GROUP BY idSolicitante)";
 
con.setRS(qr);
ResultSet rs = con.getRs();
rs.next();
String NroRegistros =  rs.getString("total");

int PagAnt=PagAct-1;
int PagSig=PagAct+1;

double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
if(Res>0){
PagUlt=Math.floor(PagUlt)+1;
}

out.println("</tbody>");
out.println("</table><br />");
out.println("<div style='width:300px; float:right'>");

    if(PagAct>(PaginasIntervalo+1)) {
    out.println("<a onclick=listaproductos('1'); class='paginador'><< Primero</a>");
    out.println("&nbsp;");
    }
    for (int i = (PagAct2-PaginasIntervalo) ; i <= (PagAct2-1) ; i ++) {
        if(i>=1) {
            out.println("<a href='prePrestamo.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"&act=1' class='paginador'>"+i+"</a>");
            out.println("&nbsp;");
        }
    }
    if (contador != 0){
    out.println("<span class='paginadoractivo'>"+PagAct2+"</span>");
    out.println("&nbsp;");
    }
    for ( int i = (PagAct2+1) ; i <= (PagAct2+PaginasIntervalo) ; i ++) {
        if(i<=PagUlt) {
        out.println("<a href='prePrestamo.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"&act=1' class='paginador'>"+i+"</a>");
        out.println("&nbsp;");
        }
    }
    if(PagAct<(PagUlt-PaginasIntervalo)) {
     out.println("<a href='prePrestamo.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ PagUlt+"&act=1' class='paginador'>Ultimo</a>");
    }
    out.println("</div>");
%>