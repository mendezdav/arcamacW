<%-- 
    Document   : cambiarEstado
    Created on : 18-ene-2014, 13:52:38
    Author     : Pochii
--%>
<%@page import="clases.generarTablaAmortizacion"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="clases.classPagos"%>
<%@page import="clases.pruebaPagos"%>
<%@page import="clases.ActualizacionPagos"%>
<%@page import="clases.generarTabla"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion" %>
<%
    /**
     * Estableciendo variables
     */
    //Tomando codigo
    String codigoPrestamo = request.getParameter("codigoPrestamo");
    int plazo = 0;
    double saldo = 0.0;
    String sinRefinanciamiento = "Sin Refinanciamiento";
    SimpleDateFormat formatoMutuo = new SimpleDateFormat("yyyy-MM-dd H:mm:ss"); //
    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd"); //
    String fechaAprobacionMutuo = formatoMutuo.format(new Date());
    String fechaAprobacion = formato.format(new Date());
    Calendar c = Calendar.getInstance();
    //Tomando ultimo pago
    conexion con  = new conexion();
    try
    {
        //Tomando los datos necesarios de prestamo
        ResultSet rs = null; 
        con.setRS("SELECT plazo,saldo, refinanciado FROM prestamos WHERE codigoPrestamo = '" + codigoPrestamo + "'");
        rs = con.getRs();
        //Validando
        rs.next();
        plazo = rs.getInt("plazo");
        saldo = rs.getDouble("saldo");
        String refinanciado = rs.getString("refinanciado");
        //JOptionPane.showMessageDialog(null,refinanciado);
        c.add(Calendar.MONTH, plazo);           //Adelantando el calendario para fecha de vencimiento
        /**
         * Actualizando la tabla prestamo
         */
        con.actualizar("UPDATE prestamos SET fechaAprobacion='" + fechaAprobacionMutuo + "', fechaVencimiento = '" + formato.format(c.getTime()) + "', estado='Activo' WHERE "
                        + "codigoPrestamo = '" + codigoPrestamo + "'");
        /**
         *  Llenando tabla de BD "tabla_amortizacion"
         */
        generarTablaAmortizacion tabla_amortizacion = new generarTablaAmortizacion(codigoPrestamo);
        tabla_amortizacion.cargarDatos("img");
        boolean estado_tablaAmortizacion = tabla_amortizacion.ingresarTabla();
        if (estado_tablaAmortizacion)
        { 
            //Regresando el calendario a fecha original
            c.add(Calendar.MONTH, -plazo);          
            /**
             * Llenando la tabla pagos
             */
            classPagos.nuevoPago(plazo, fechaAprobacion, saldo, codigoPrestamo, "1000-01-01"); 

            /***Actualizamos las filas del pago**/
            ActualizacionPagos actualizar = new ActualizacionPagos(codigoPrestamo);
            actualizar.actualizar(true);
            if (refinanciado.equals("Refinanciado"))
            {
                String qr = "select codigoPrestamo,saldo,refinanciado from prestamos where codigoPrestamo like '%" + codigoPrestamo.substring(0, 3)+ "%' order by codigoPrestamo ASC limit 1";
                con.setRS(qr);
                String codigoPrestamoAnt="";
                ResultSet rsTemp = con.getRs();
                if (rsTemp.next())
                {
                    codigoPrestamoAnt = rsTemp.getString("codigoPrestamo");
                    saldo = rsTemp.getDouble("saldo");
                    refinanciado = rsTemp.getString("refinanciado");
                }
                else 
                    response.sendRedirect("/arcamacW/prestamos/prestamosNoAprobados.jsp?erno=2");
                 
                // cancelar todas las cuotas 
                    double total_deuda = classPagos.total_deuda(codigoPrestamoAnt, saldo);
                    // obteniendo id del pago a realizar
                    con.setRS("select idPago from pagos where codigoPrestamo='"+ codigoPrestamoAnt+"' and estado!='Pagado' limit 1");
                    rsTemp = con.getRs();
                    String idPago="";
                    if (rsTemp.next())
                        idPago = rsTemp.getString("idPago");
                    else 
                        response.sendRedirect("/arcamacW/prestamos/prestamosNoAprobados.jsp?erno=2");
                    classPagos pago = new classPagos(idPago,total_deuda);
                    // modificado el 18 de enero
                    pago.insertarAbono(idPago,codigoPrestamoAnt,total_deuda);
            }
        }
        else 
        {
            response.sendRedirect("/arcamacW/prestamos/prestamosNoAprobados.jsp?erno=2");
        }
    }
    catch(Exception e){
        //response.sendRedirect("listadoPrestamo.jsp?erno=1");
    }
%>
s