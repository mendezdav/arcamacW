<%-- 
    Document   : prePrestamo
    Created on : 18-oct-2013, 0:22:38
    Author     : Pochii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<<%--%@include  file="../administracion/ValSession.jsp" %>--%>
<%@include file="../administracion/header.jsp" %>
 <c:choose>
    <c:when test="${empty param.opc}">
        <c:redirect url="panelPrestamos.jsp">
            <c:param name="erno" value="2" />
        </c:redirect>
    </c:when>
    <c:when test="${!empty param.opc}">
        <c:set var="opc" scope="session" value="${param.opc}" />
    </c:when>
</c:choose>
 <style>
    body {padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
    .table {width:600px;alignment-baseline:central;}
    .container{max-width:  1200px;margin: 0 auto;}
    .container h2{font-family: Century Gothic;}
</style>
        <%
            String activar ;
            if (request.getParameter("act") != null && request.getParameter("act") !="")
            { activar = request.getParameter("act");
              if (activar.equals("1"))
              {
                  %>
                  <body onLoad="mostrardiv();">
                  <%
              }
            }
        %>
     <div class="container">
         <div id="divOpciones" style="width: 500px; border: solid 1px #d3d3d3; padding: 20px; border-radius: 5px; text-align: center; margin: 0 auto;">
            <h3>Para crear un nuevo préstamo utilizar un</h3>
            <br/>
            <br/>
            <a class="btn btn-primary" onclick="mostrardiv();">Solicitante existente</a>
            <a class="btn btn-primary" href="../solicitantes/nuevoSolicitanteStep1.jsp?type=pre">Nuevo solicitante </a>
         </div>
        <hr />
     </div> <!-- /container -->
     <!--                                                                 -->
     <div class="container"  id="divsolicitantes" style="visibility: hidden;">
        <%
        String pagR = request.getParameter("pag");
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('listadoSolicitante.jsp', 'txtNombre','busqueda',<%=pagR%>,'prePrestamo.jsp');" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
       <fieldset>
           <h3>Buscar Solicitante</h3>
        <%
            String criterio  = request.getParameter("criterio");
            if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="Nombre del solicitante"  
                                onkeyup="paginar('listadoSolicitante.jsp', 'txtNombre','busqueda',<%=pagR%>,'prePrestamo.jsp');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            {
                %>
                 <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="Nombre del solicitante"  
                                onkeyup="paginar('listadoSolicitante.jsp', 'txtNombre','busqueda',<%=pagR%>,'prePrestamo.jsp');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
                
                <%
            }
                %>
    </fieldset>
</div>
    
<%@include file="../administracion/footer.jsp" %>
<script lang="javascript" src="<%=ruta%>/js/selectAjax.js"></script>
<script>
   $(document).ready(function(){
      //Enviamos el tipo pre de prestamo
      //$('#txtNombre').val(" ");
      $('#txtNombre').keyup();
       paginar('listadoSolicitante.jsp', 'txtNombre','busqueda',<%=pagR%>,'prePrestamo.jsp');
    });
    function mostrardiv(){
        document.getElementById("divsolicitantes").style.visibility = "visible";
        document.getElementById("divOpciones").style.display = "none";
    }
</script>