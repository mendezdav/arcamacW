<%@page import="java.sql.*"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    String json_response = "";
   
    String codigoPrestamo = request.getParameter("codigoPrestamo");

    try{
        String query = "SELECT count(*) as cnt FROM pagos WHERE codigoPrestamo='"+codigoPrestamo+"' AND estado!='Pagado' AND fechaEstablecida < CURDATE()";
        conexion con = new conexion();
        con.setRS(query);
        ResultSet rs = con.getRs();
        int lim = 0;
        while(rs.next()){
            lim = rs.getInt("cnt") + 1;
        }
        
        query = "SELECT (capital - pago_capital) as capital, (intereses - pago_interes) as interes, (interes_moratorio - pago_interes_moratorio) as interes_moratorio, (iva_interes + iva_mora - pago_iva_interes - pago_iva_mora) as iva FROM pagos WHERE codigoPrestamo='"+codigoPrestamo+"' AND estado!='Pagado' ORDER BY idPago ASC LIMIT "+lim;
        con.setRS(query);
        rs = con.getRs();
        double capital = 0;
        double interes = 0;
        double interes_moratorio = 0;
        double iva = 0;
        
        json_response += "[";
        while(rs.next()){
            capital += rs.getDouble("capital");
            interes += rs.getDouble("interes");
            interes_moratorio += rs.getDouble("interes_moratorio");
            iva += rs.getDouble("iva");
        }
        
        query = "SELECT saldo FROM prestamos WHERE codigoPrestamo='"+codigoPrestamo+"'";
        con.setRS(query);
        rs = con.getRs();
        while(rs.next()){
            capital = rs.getDouble("saldo");
        }
        json_response += "{";
        json_response += "\"capital\":"+capital+",";
        json_response += "\"interes\":"+interes+",";
        json_response += "\"interes_moratorio\":"+interes_moratorio+",";
        json_response += "\"iva\":"+iva+"";
        json_response += "},";
        
        json_response += "{}]";
        con.cerrarConexion();
    }catch(Exception s){
        json_response = "{}";
    }
 
    out.write(json_response);
%>
