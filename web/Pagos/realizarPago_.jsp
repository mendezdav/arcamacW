<%-- 
    Document   : realizarPago
    Created on : 01-10-2014, 10:22:00 AM
    Author     : Jorge Luis
--%>

 <%@page import="java.util.TreeMap"%>
<jsp:useBean class="clases.nuevoPrestamo" id="prestamo" scope="request" />
 
<%@page import="clases.nuevoPrestamo,Conexion.conexion"%>
<%@page import="clases.RealizarPago"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>


<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*,clases.diferenciaDias" %>
<%@page  session="true" language="java" %>

<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
 

<%@include file="../administracion/header.jsp" %>
<script type="text/javascript" src="../js/selectAjax.js"></script>
<style media="all" type="text/css" >
    @import url("../css/screen.css");
</style>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  750px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
    
    <script language="JavaScript" >
        function llamar(ruta)
        {
            window.open(ruta + '/ReporteFactura','','resizable=yes,scrollbars=yes,height=400,width=800');
            window.history.back(-1);

        }
    </script>
  </head>
  <div class="container">
        <center>
          <h3>Realizar Pago</h3><p />
        </center>
        <br /><br />
       
        <%
            /**
            * DECLARACION DE VARIABLES
            **/
            String id="",query="";
            ResultSet rs = null;
            Statement s = null;
            conexion con = null;
            id =  request.getParameter("idPago");                                                 //ID del pago a realizar
            TreeMap<String,Double> mapa = new TreeMap<String,Double>();
            
            String fechaActual="",codigoPrestamo="", fechaEstablecida="",estado="",comprobante="", 
            fechaPago=""; 
            
            double diasRetraso = 0.0,tasa_interes=0.0,saldo=0.0, saldoIdeal = 0.0, cuota=0.0,total=0.0, 
                    intereses=0.0,capital=0.0,interes_moratorio=0.0,pago_interes=0.0,
                    pago_capital=0.0,pago_interes_moratorio=0.0,pagoIva=0.0,pago_capitalAux=0.0;
            int mora = 0, bandera = 1;            //Variable bandera, si es 1 hay abono a capital. Si es cero, el abono a capital sera la diferenciade saldo
            double dias_aux=0.0, pago_al_dia=0.0; //Monto necesario para poner al dia el pago 
            
            diferenciaDias diasF = new diferenciaDias();
            
            /**Capturando fecha actual***/
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            fechaActual = formato.format(new Date());
            
            // sacando datos del objeto request.
            // si el id del pago esta definido, guardar en variable
            if (request.getParameter("idPago") != "")
            {   
                query="select pre.saldo,p.idPago,p.estado,p.comprobante,p.fechaEstablecida, "
                        + "p.fechaPago,p.monto,p.pago_interes, p.pago_capital,p.capital,"
                        + " p.pago_interes_moratorio,p.interes_moratorio,p.intereses,pre.interes,pre.cuotas,pre.codigoPrestamo,"
                        + "pre.tasa_interes,pre.plazo, p.mora from pagos as p inner join prestamos "
                        + "as pre on pre.codigoPrestamo = p.codigoPrestamo where p.idPago=" + id + ";";
                
                con = new conexion();
                con.setRS(query);
                rs = con.getRs();
               
                // hay pagos con dicho idPago
                if (rs.next())
                {
                  // obtener datos 
                    comprobante       = rs.getString("comprobante");
                    codigoPrestamo    = rs.getString("codigoPrestamo");
                    tasa_interes      = rs.getDouble("tasa_interes");
                    saldo             = rs.getDouble("saldo");
                    estado            = rs.getString("estado");
                    
                    fechaEstablecida  = rs.getString("fechaEstablecida");
                    intereses         = rs.getDouble("intereses");
                    capital           = rs.getDouble("capital");
                    interes_moratorio = rs.getDouble("interes_moratorio");
                    pago_interes      = rs.getDouble("pago_interes");
                    pago_capital      = rs.getDouble("pago_capital");
                    pago_interes_moratorio = rs.getDouble("pago_interes_moratorio");
                    fechaPago         = rs.getString("fechaPago");
                    cuota             = rs.getDouble("cuotas");
                    mora              = rs.getInt("mora"); 
                    //saldoIdeal        = rs.getDouble("saldoIdeal");
                    
                    if(mora == 1){
                        if (estado.equals("Pendiente"))
                        {
                            diasRetraso = diasF.obtenerDiasRetraso(fechaActual,fechaEstablecida);
                        }else if(estado.equals("Abonado")){
                            diasRetraso = diasF.obtenerDiasRetraso(fechaActual, fechaPago);
                        }
                    }
                } //fin if  rs
                else
                {
                       /***ERROR NO HAY REGISTROS***/
                }
                rs.close();                                 //Cerrando rs
                con.cerrarConexion();                       //Cerrando conexion
%>      

<table class="table">
                            <tr>
                                <td>C&oacute;digo:</td>
                                <td><%= codigoPrestamo%></td>
                            </tr>
                            
                            <tr>
                                <td>Comprobante:</td>
                                <td><%= comprobante%></td>
                            </tr>
                            
                            <tr>
                                <td>Estado:</td>
                                <td><%= estado%></td>
                            </tr>
                            
                            <tr>
                                <td>Saldo Actual:</td>
                                <td><%= saldo%></td>
                            </tr>
                            
                            
                            <tr>
                                <td>Cuota:</td>
                                <td>$ <%= cuota%></td>
                            </tr>
                            
                            
                            
                            <% if (estado.equals("Abonado"))
                                {
                                %>
                                
                                <tr>
                                    <td>Fecha establecida de pago:</td>
                                    <td><%=fechaEstablecida%></td>
                                </tr>
                            
                                <tr>
                                    <td>Fecha Ultimo pago:</td>
                                    <td><%=fechaPago%></td> 
                                </tr>
                                <%
                                }
                                else 
                                {
                                %>
                                <tr>
                                    <td>Fecha establecida de pago:</td>
                                    <td><%=fechaEstablecida%></td>
                                </tr>
                                <%
                                }
                            %>
                            
              <%                         
                    pagoIva = (intereses - (intereses/1.13));                   //Calculando IVA de los intereses, tanto moratorios como normales 
                    pagoIva += (interes_moratorio - (interes_moratorio/1.13)); 
                   
                    if (estado.equals("Pendiente"))                             // verificar el estado del pago , es pendiente o abonado?
                    { 
                        pago_capitalAux = capital;                              //Para el caso de un pago pendiente
              %>
                             <tr>
                                 <td><b>Intereses:</b></td>
                                <td>$ <%=diferenciaDias.redondear(intereses+interes_moratorio) %></td>   
                             </tr>
                            
                            
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;Intereses Moratorios (<%= (int)diasRetraso%> Dias):</td>
                                <td>$ <%=diferenciaDias.redondear(interes_moratorio) %></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;Intereses:</td>
                                <td>$ <%=intereses%></td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;Iva Interes:</td>
                                <td>$ <%=diferenciaDias.redondear(pagoIva)%></td>
                            </tr> 
                            <tr>
                                <td>Capital:</td>
                                <td>$ <%=diferenciaDias.redondear(capital)%></td> 
                            </tr>
                            
                            <tr>
                                <td><b>Detalles</b></td>
                                <td></td>
                            </tr>
                <% 
                        total = capital + intereses + interes_moratorio;             
                    }
                    else // el pago fue abonado, anteriormente
                    {
                      
                        %>
                      
                         <tr>
                             <td><b>Detalles Ultimo Pago</b></td>
                             <td></td>
                         </tr>
                         
                         <tr>
                             <td></td>
                             <td><b>Cantidad Solicitada</b></td>
                             <td><b>Cantidad Abonada</b></td>
                         </tr>
                         
                         
                         <tr>
                             <td>Interes Moratorio e:</td>
                             <td>$ <%=diferenciaDias.redondear(interes_moratorio)%></td>
                             <td>$ <%=diferenciaDias.redondear(pago_interes_moratorio)%></td>
                         </tr>
                         
                         <tr>
                             <td>Interes con IVA:</td>
                             <td>$ <%=diferenciaDias.redondear(intereses)%></td>
                             <td>$ <%=diferenciaDias.redondear(pago_interes)%></td>
                         </tr>
                         
                         <tr>
                             <td>Capital:</td>
                             <td>$ <%=diferenciaDias.redondear(capital)%></td>
                             <td>$ <%=diferenciaDias.redondear(pago_capital)%></td>
                         </tr>
                         
                         
                         <tr>
                             <td><b>Detalles Pago </b></td>
                             <td></td>
                             <td></td>
                         </tr>
                         
                         <%
                          /**Estableciendo primeras propiedades del mapa***/
                          // calculo de nuevos cargos a realizar en un pago abonado
                          // anteriormente
                          pago_interes_moratorio = (interes_moratorio  - pago_interes_moratorio);
                          pago_interes = (intereses - pago_interes);
                          pago_capitalAux = pago_capital;

                          pago_capital = (capital-pago_capital); 
                          // sacando iva al interes moratorio
                    %>
                         <tr>
                             <td>Interes Moratorio (<%= (int)diasRetraso%> Dias):</td>
                             <td>$ <%=diferenciaDias.redondear(pago_interes_moratorio) %></td>
                         </tr>
                         
                         <tr>
                             <td>Interes con IVA:</td>
                                        
                             <td>$ <%=diferenciaDias.redondear(pago_interes) %></td>
                         </tr>
                         
                         <tr>
                             <td>Capital:</td>
                             <td>
                                 <div id="capital">
                                    $ <%=Math.rint(pago_capital*100)/100%>
                                 </div>
                             </td>
                       
                         <% total = diferenciaDias.redondear(pago_interes_moratorio + pago_interes + pago_capital); %>
                       
                        <%
                    }    
            }
           
            /***
            * ESTABLECIENDO PROPIEDADES DEL MAPA
            ***/
            mapa.put("id",  Double.parseDouble(id));                       // id del pago a abonar
            mapa.put("pay_interes_moratorio", pago_interes_moratorio);     //Cuanto se ha abonado de interes moratorio
            mapa.put("pay_interes", pago_interes);                         //Cuanto se ha abonado de interes normal
            mapa.put("pay_capital", pago_capital);                         //Cuanto se ha abonado de capital
            mapa.put("ideal_interes_pagoIVA", intereses);                  //Cuanto se debe pagar de intereses
            mapa.put("ideal_capital_cantidad", capital);                   //Cuanto se debe pagar de capital
            mapa.put("ideal_interes_moratorio", interes_moratorio);        //Cuanto se debe pagar de mora
            mapa.put("saldo", saldo);    
            mapa.put("cuota", cuota);                                      //Cuota asignada al pago
       
             SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
             SessionActual.setAttribute("estado",estado);
             SessionActual.setAttribute("fechaActual", fechaActual);
                                                                                          
             // guardando mapa en session
             SessionActual.setAttribute("mapa", mapa);
         %>
                </tr>
             <tr>
                  <td>Nuevo saldo:</td>
                  <td>$ <%= saldo-pago_capitalAux - pago_al_dia%></td>
            </tr>
        
              </table>      
               <div id="cantidad">
               <form action="finalizarPago.jsp" method="post">
                    <center>
                        <table class="table">
                        <tr><td><label>Cantidad: $</label></td>
                            <td> <input  type="text" name="cantidad" value="<%= Math.rint((total+pago_al_dia)*100)/100 %>" size="8" /></td> 
                            <td><input type="hidden" name="idPago" value="" /></td>
                        </tr>
                        
                        <tr>
                            <td colspan="2"><center><br />
                                <input type="submit" class="btn btn-info"  value="Realizar Pago" >
                                <input type="button" class="btn btn-info" onclick="enviarPanel();" value="Cancelar" >
                                </center>
                            </td>
                        </tr>
                    </table> 
                    </center>
                    <br />
                    
                 </form>
            </div>
                    </div>

   <script language="javascript">
     
      function mostrar()
      {
         $("#cantidad" ).fadeOut( "slow", function() {
    // Animation complete.
        });
      }
   </script>
      
  </div>
<%@include file="../administracion/footer.jsp" %>

