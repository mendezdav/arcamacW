<%-- 
    Document   : listado
    Created on : 05-30-2014, 03:11:29 PM
    Author     : jorge
--%>

<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../js/paginador/funciones.js"></script>

<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%

int RegistrosAMostrar=10;
int PaginasIntervalo=3;
int RegistrosAEmpezar=0;
int PagAct = 1;
int PagAct2 = 0;

String criterio = request.getParameter("criterio");
String pagR = request.getParameter("pagR");
  if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
  {
      RegistrosAEmpezar = 0;
      PagAct2 = Integer.parseInt(pagR);
  }
 
String pag = request.getParameter("pagina");
if (pag==null || pag==""){
RegistrosAEmpezar=0;
PagAct=2; // tendria q ser la 1
}else{
RegistrosAEmpezar=(Integer.valueOf(pag).intValue()-1)* RegistrosAMostrar;
PagAct=Integer.valueOf(pag).intValue();
}
%>

<%

conexion con = new conexion();
String qr="";
int tmp = RegistrosAMostrar * ( PagAct2-1);
String codigoPrestamo = request.getParameter("codigoPrestamo");

     //qr ="select * from pagos where codigoPrestamo like '%" + request.getParameter("criterio")+ "%' limit " + tmp + "," + RegistrosAMostrar;
     qr = "select idPago,codigoPrestamo,comprobante,fechaEstablecida,estado,"
             + "monto from pagos where codigoPrestamo='" + codigoPrestamo + "' limit " + tmp + "," + RegistrosAMostrar;

con.setRS(qr);
ResultSet resultado = con.getRs();
if (resultado.next())
{
    out.println("<table class='tablita'>");
    out.println("<caption>Pagos</caption>");
    out.println("<tr>");
    out.println("<th>Codigo Prestamo</th>");
    out.println("<th>Comprobante</th>");
    out.println("<th>Fecha Vencimiento</th>");
    out.println("<th>Estado</th>");
    out.println("<th>Opciones</th>");
   out.println("</tr>");
    out.println("<tbody>");
}
resultado.previous();
int contador = 0;
while (resultado.next())
{
contador++;
%>
<tr id="grid">
<td id="formnuevo"><%= resultado.getString("codigoPrestamo") %></td>
<td id="formnuevo"><%= resultado.getString("comprobante") %></td>
<td id="formnuevo"><%= resultado.getString("fechaEstablecida") %></td>
<td id="formnuevo"><%= resultado.getString("estado") %></td>
<td id="formnuevo"><a href="CuotasDetalles.jsp?codigoPrestamo=<%=resultado.getString("codigoPrestamo")%>" >Seleccionar</a></td>
</tr>

<%

} 
if (contador == 0)
{ 
    out.println("<div class=\"noData\"><br />No hay datos que mostrar</div>");
}
%>
<%

 if (criterio != null || criterio !="" ) 
  {
      //qr = "select count(*) as total from pagos where codigoPrestamo like '%" + request.getParameter("criterio") +"%' ";
      qr = "select count(*) as total from prestamos as p  inner join solicitante as s on s.idSolicitante  = p.idSolicitante "
              + "where  p.codigoPrestamo like  '%"+ criterio +"%' or concat(s.nombre1, ' ', s.nombre2,' ', s.apellido1,' ',s.apellido2) "
              + "like '%" + criterio + "%'";
  }
 else 
 {
    // qr = "select count(*) as total from pagos";
     qr = "select count(*) as total from prestamos as p  inner join solicitante as s on s.idSolicitante  = p.idSolicitante";
 }

con.setRS(qr);
ResultSet rs = con.getRs();
rs.next();
String NroRegistros =  rs.getString("total");

int PagAnt=PagAct-1;
int PagSig=PagAct+1;

double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
if(Res>0){
PagUlt=Math.floor(PagUlt)+1;
}

 

out.println("</tbody>");
out.println("</table><br />");
//principio del paginador
out.println("<div style='width:300px; float:right'>");

if(PagAct>(PaginasIntervalo+1)) {
out.println("<a onclick=listaproductos('1'); class='paginador'><< Primero</a>");
out.println("&nbsp;");
}
for ( int i = (PagAct2-PaginasIntervalo) ; i <= (PagAct2-1) ; i ++) {
if(i>=1) {

out.println("<a href='PrincipalPago.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"' class='paginador'>"+i+"</a>");
out.println("&nbsp;");
}
}
if (contador != 0){
out.println("<span class='paginadoractivo'>"+PagAct2+"</span>");
out.println("&nbsp;");}

for ( int i = (PagAct2+1) ; i <= (PagAct2+PaginasIntervalo) ; i ++) {
if(i<=PagUlt) {
out.println("<a href='PrincipalPago.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ i+"' class='paginador'>"+i+"</a>");
out.println("&nbsp;");
}
}
if(PagAct<(PagUlt-PaginasIntervalo)) {

 out.println("<a href='PrincipalPago.jsp?criterio=" +request.getParameter("criterio") + "&pag="+ PagUlt+"' class='paginador'>Ultimo</a>");
}
out.println("</div>");

%>