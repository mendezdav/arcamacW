<%-- 
    Document   : finalizarPago
    Created on : 01-10-2014, 10:11:08 PM
    Author     : Jorge Luis
--%>
<%@page import="clases.classPagosAnt"%>
<%@page import="clases.classPagos"%>
<%@page import="Conexion.conexion"%> 
<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>

<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@include file="../administracion/header.jsp" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<script type="text/javascript" src="../js/selectAjax.js"></script>
<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }  
        .container
        {
            max-width:  550px;
            margin: 0 auto;
            text-align: justify;
        }
    </style>
    
    <script language="JavaScript" >
        function llamar(ruta,codigoPrestamo)
        {
            window.open(ruta + '/ReporteFactura','','resizable=yes,scrollbars=yes,height=400,width=800');
            window.location.href = "/arcamacW/Pagos/CuotasDetalles.jsp?codigoPrestamo=" + codigoPrestamo;
        }
    </script>
    
  </head>
  <ol class="breadcrumb">
            <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
            <li><a href="<%=ruta%>/Pagos/PrincipalPago.jsp">Pagos</a></li>
            <li class="active">Listado de pr&eacute;stamos</li>
  </ol>
  <div class="container">
        <center>
          <h3>Pago Finalizado</h3>
          <br />
        </center>
        <br /><br />
        <%
            String idPago = request.getParameter("idPago");
            String codigoPrestamo = request.getParameter("codigoPrestamo");
            String tipoPago = request.getParameter("tipoPago");
            String fechaA = request.getParameter("fechaA");
            int mensaje = 0;
            Double monto = Double.parseDouble(request.getParameter("monto"));
            if(tipoPago.equals("1")){ 
                classPagos pago = new classPagos(idPago,monto);  
                pago.insertarAbono(idPago,codigoPrestamo, monto); 
                SessionActual.setAttribute("comprobante", pago.getComprobateAbono());
                SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
                mensaje = pago.mensaje;
                pageContext.setAttribute("mensaje", mensaje);
            }
            else{ 
                classPagosAnt pago = new classPagosAnt(idPago,monto,fechaA);  
                pago.insertarAbono(idPago, monto); 
                SessionActual.setAttribute("comprobante", pago.getComprobateAbono());
                SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
                mensaje = pago.mensaje;
                pageContext.setAttribute("mensaje", mensaje);
            }
            
        %>  
        
        <div class="alert alert-success fade in">
           <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
            <c:choose>
                <c:when test="${mensaje==1}">
                    Pago realizado correctamente
                </c:when>
                <c:when test="${mensaje==2}">
                        Pr&eacute;stamo finalizado
                </c:when>
            </c:choose>

       </div>
        
         <sql:query var="q1" dataSource="jdbc/mysql">
            SELECT pago_capital, pago_interes, pago_interes_moratorio, saldo, estado FROM pagos WHERE idPago = ${param.idPago};
        </sql:query>
        <h4>Detalle Pago:</h4><br /><br />
        
        <table class="table">
            <c:forEach var="info" items="${q1.rows}">
                <tr>
                    <td>INTERESES </td>
                    <td>
                        <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" pattern="$ ####.##" value="${info.pago_interes}" />
                    </td>
                </tr>
                <tr>
                    <td>CAPITAL </td>
                    <td>
                        <fmt:formatNumber type="number" maxFractionDigits="2"  minFractionDigits="2" pattern="$ ####.##" value="${info.pago_capital}" />
                    </td>
                </tr>
                <tr>
                    <td>MORA </td>
                    <td>
                        <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" pattern="$ ####.##" value="${info.pago_interes_moratorio}" />
                    </td>
                </tr>
            </c:forEach> 
            <tr>
                <td>
                    <input type="button" class="btn btn-info" value="Recibo" onclick="llamar('<%=ruta%>','<%=codigoPrestamo%>');" />
                </td>
                <td>
                    <a href="<%=ruta%>/prestamos/panelPrestamos.jsp" class="btn btn-info">
                        Regresar
                    </a>
                </td>
            </tr>
        </table>
  </div>
<%@include file="../administracion/footer.jsp" %>


