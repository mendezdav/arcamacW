<%-- 
    Document   : realizarPago
    Created on : 01-jun-2014, 21:43:37
    Author     : Pochii
--%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="clases.diferenciaDias"%>
<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<%@include file="../administracion/header.jsp" %>
<script src="<%=ruta%>/js/pagos.js"></script>
<style type="text/css">
 .content{background-position: center; margin: 100px 30px 20px 12%;} 
.tg  {border-collapse:collapse;border-spacing:0;border:none;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal; width: 150px;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.form-control{width: 200px;}
fieldset{margin: 30px 5px 30px 0px; border: 1px #006dcc solid; width:550px;padding-left: 15px;background-color: #ceeaea;}
legend{border: 1px #0099FF solid;padding: 5px 5px 5px 20px; width: 150px; border-radius: 5px; background-color: #fff;}
#detalle td{width: 200px; margin-left: 20px; text-align: justify;}
#detalle tr{height: 25px;}
</style>
<div class="content" ng-app="pagosApp" ng-controller="controladorPagos">
    
    <h2>Pago de cuotas</h2>
    <hr />
    <%
                diferenciaDias diasF = new diferenciaDias();
                String fechaActual = "", fechaEstablecida = "", fechaPago="";
                double diasRetraso = 0.0;
                /**Capturando fecha actual***/
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                if(request.getParameter("fechaA") != null)
                {
                   fechaActual = request.getParameter("fechaA").toString();
                   pageContext.setAttribute("tipoPago", 2);
                }else{
                   pageContext.setAttribute("tipoPago", 1);
                   fechaActual = formato.format(new Date());
                }
                pageContext.setAttribute("fechaA", fechaActual);
    %>
    <c:choose>
        <c:when test="${empty param.rule}">
            <sql:query dataSource="jdbc/mysql" var="q1">
                SELECT * FROM pagosview WHERE idPago = ${param.idPago} AND estado != "Pagado"
            </sql:query>
        </c:when>
        <c:otherwise>
            <sql:query dataSource="jdbc/mysql" var="q1">
                <sql:query dataSource="jdbc/mysql" var="count">
                   SELECT count(*) total FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago}) AND estado != "Pagado" 
                   AND mora = 1 ORDER BY idPago ASC
               </sql:query>
               <c:forEach var="info" items="${count.rows}">
                   <c:if test="${info.total > 0}">
                       SELECT SUM(iva_interes) as iva_interes, SUM(pago_iva_interes) as pago_iva_interes, SUM(iva_mora) as iva_mora, SUM(pago_iva_mora) as pago_iva_mora, idPago, codigoPrestamo,comprobante, SUM(monto) as monto, fechaPago, fechaEstablecida,SUM(intereses) as intereses,
                        SUM(capital) as capital, estado,SUM(pago_interes) as pago_interes,SUM(pago_capital) as pago_capital,SUM(interes_moratorio) as interes_moratorio,
                        SUM(pago_interes_moratorio) as pago_interes_moratorio,saldo,SUM(cuotas) as cuotas,mora,solicitante
                        FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago}) AND estado != "Pagado" 
                        AND mora = 1 GROUP BY codigoPrestamo ORDER BY idPago ASC
                   </c:if>
                  <c:if test="${info.total == 0}">
                       SELECT * FROM pagosview WHERE idPago = ${param.idPago} AND estado != "Pagado"
                   </c:if>
               </c:forEach>     
            </sql:query>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${empty param.rule}">
            <sql:query dataSource="jdbc/mysql" var="q2">
               SELECT * FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago})
               AND estado != "Pagado" AND mora = 1 
           </sql:query>
        </c:when>
        <c:otherwise>
            <sql:query dataSource="jdbc/mysql" var="q2">
                SELECT SUM(iva_interes) as iva_interes, SUM(pago_iva_interes) as pago_iva_interes, SUM(iva_mora) as iva_mora, SUM(pago_iva_mora) as pago_iva_mora,idPago, codigoPrestamo,comprobante, SUM(monto) as monto, fechaPago, fechaEstablecida,SUM(intereses) as intereses,
                SUM(capital) as capital, estado,SUM(pago_interes) as pago_interes,SUM(pago_capital) as pago_capital,SUM(interes_moratorio) as interes_moratorio,
                SUM(pago_interes_moratorio) as pago_interes_moratorio,saldo,SUM(cuotas) as cuotas,mora,solicitante
                FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago}) AND estado != "Pagado" 
                AND mora = 1 GROUP BY codigoPrestamo ORDER BY idPago ASC
           </sql:query>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${empty param.rule}">
           <sql:query dataSource="jdbc/mysql" var="q3">
                <c:if test="${param.ontime ==1}">
                    SELECT * FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago})
                    AND estado != "Pagado" AND mora = 1 ORDER BY idPago ASC LIMIT 1
                </c:if>
                <c:if test="${empty param.ontime}">
                    SELECT * FROM pagosview WHERE idPago = ${param.idPago} AND estado != "Pagado"
                </c:if>
            </sql:query>
        </c:when>
        <c:otherwise>
            <sql:query dataSource="jdbc/mysql" var="q3">
                <c:if test="${param.ontime ==1}">
                    SELECT SUM(iva_interes) as iva_interes, SUM(pago_iva_interes) as pago_iva_interes, SUM(iva_mora) as iva_mora, SUM(pago_iva_mora) as pago_iva_mora,idPago, codigoPrestamo,comprobante, SUM(monto) as monto, fechaPago, fechaEstablecida,SUM(intereses) as intereses,
                    SUM(capital) as capital, estado,SUM(pago_interes) as pago_interes,SUM(pago_capital) as pago_capital,SUM(interes_moratorio) as interes_moratorio,
                    SUM(pago_interes_moratorio) as pago_interes_moratorio,saldo,SUM(cuotas) as cuotas,mora,solicitante
                    FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago}) AND estado != "Pagado" 
                    AND mora = 1 GROUP BY codigoPrestamo ORDER BY idPago ASC 
                </c:if>
                <c:if test="${empty param.ontime}">
                    SELECT * FROM pagosview WHERE idPago = ${param.idPago} AND estado != "Pagado"
                </c:if>
            </sql:query>
            
        </c:otherwise>
    </c:choose>        
            
    
    <c:forEach var="info" items="${q3.rows}">
          <c:set var="fechaEstablecida" value="${info.fechaEstablecida}" />
          <c:set var="fechaPago" value="${info.fechaPago}" />
          <c:set var="estado" value="${info.estado}" />
          <c:set var="mora" value="${info.mora}" />
          <%
                fechaEstablecida = pageContext.getAttribute("fechaEstablecida").toString();
                fechaPago        = pageContext.getAttribute("fechaPago").toString();
                Boolean mora = Boolean.parseBoolean(pageContext.getAttribute("mora").toString()); 
                if(mora){
                    if (pageContext.getAttribute("estado").equals("Pendiente"))
                        {
                            diasRetraso = diasF.obtenerDiasRetraso(fechaActual,fechaEstablecida);  
                        }else if(pageContext.getAttribute("estado").equals("Abonado")){
                            diasRetraso = diasF.obtenerDiasRetraso(fechaActual, fechaPago); 
                        }
                }
          %>
          <c:set var="diasRetraso" value="<%=(int)diasRetraso%>" />
          <c:set var="codigoPrestamo" value="${info.codigoPrestamo}" />
          <c:set var="idPago" value="${param.idPago}" />
          <table class="tg">
            <tr>
              <td class="tg-031e"><strong>Solicitante:</strong></td>
              <td class="tg-031e" colspan="3">${info.solicitante}</td>
            </tr>
            <tr>
              <th class="tg-031e"><strong>Pr�stamo No.</strong></th>
              <th class="tg-031e" ng-init="codigoPrestamo='${info.codigoPrestamo}'">${info.codigoPrestamo}</th>
              <th class="tg-031e"><strong>Comprobante:</strong></th>
              <th class="tg-031e">${info.comprobante}</th>
              
            </tr>
            <tr>
              <td class="tg-031e"><strong>Estado:</strong></td>
              <td class="tg-031e">${info.estado}</td>
              <td class="tg-031e"><strong>Fecha Establecida:</strong></td>
              <td class="tg-031e">${info.fechaEstablecida}</td>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Saldo Actual:</strong></td>
              <td class="tg-031e">$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.saldo}" /></td>
              <td class="tg-031e" colspan="2" rowspan="2"></td>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Cuota:</strong></td>
              <td class="tg-031e">$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.cuotas}" /></td>
            </tr>
          </table>
            <br/>
            <span style="font-size: 12px;">Pagos pendientes: {{n_pendientes}}</span>
                |
            <span style="font-size: 12px;">Pagos abonados: {{n_abonados}}</span>
                |
            <span style="font-size: 12px;">Monto a aplicar a los siguientes pagos: {{n_monto|currency}}</span>    
      </c:forEach>
      <c:if test="${param.ontime ==1}">
          <c:forEach var="info" items="${q2.rows}">
              
          </c:forEach>
      </c:if>
      <c:if test="${empty param.ontime}"> 
        <c:forEach var="info" items="${q1.rows}">
          <fieldset>
              <legend>
                  DETALLE 
              </legend>
              <input id="ckBtn" type="checkbox" onclick="ponerAlDia();"/> Poner al d�a (Esta opci�n cargar� todos los pagos vencidos al total a cobrar)
              <table id="detalle">
                  <c:choose>
                      <c:when test="${info.estado == 'Abonado'}">
                          <tr>
                            <td></td>
                            <td><strong>Requerido</strong></td>
                            <td><strong>Abonado </strong></td>
                            <td><strong>Pago a realizar </strong></td>
                          </tr>
                      </c:when>
                      <c:when test="${info.estado == 'Pendiente'}">
                          <tr>
                            <td></td>
                            <td><strong>Cantidad requerida</strong></td>
                            <td><strong>Pago a realizar </strong></td>
                          </tr>
                      </c:when>
                  </c:choose>
                  
                  <tr>
                      <td><strong>Intereses ordinarios:</strong></td>
                      <td><span ng-init="intereses_ordinarios=${info.intereses}" ng-bind="intereses_ordinarios|currency"> <span/></td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              <span ng-init="a_intereses_ordinarios=${info.pago_interes}" ng-bind="a_intereses_ordinarios|currency"> <span/>
                          </td>
                      </c:if>
                      <td>{{p_intereses_ordinarios|currency}}</td>
                  </tr>
                   <tr>
                      <td><strong>IVA:</strong></td>
                      <td>
                          <span ng-init="iva=${info.iva_interes}" ng-bind="iva|currency"> <span/>
                      </td>  
                      <c:if test="${info.estado == 'Abonado'}"> 
                          <td>
                              <span ng-init="a_iva=${info.pago_iva_interes}" ng-bind="a_iva|currency"> <span/>        
                          </td>
                      </c:if>
                      <td>{{p_iva|currency}}</td>    
                  </tr>
                  <tr>
                      <td><strong>Mora (${diasRetraso} dias):</strong></td>
                      <td>
                          <span ng-init="mora=${info.interes_moratorio}" ng-bind="mora|currency"> <span/>
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              <span ng-init="a_mora=${info.pago_interes_moratorio}" ng-bind="a_mora|currency"> <span/>
                          </td>
                      </c:if>
                      <td>{{p_mora|currency}}</td>      
                  </tr>
                  <tr>
                      <td><strong>IVA:</strong></td>
                      <td>
                          <span ng-init="iva_mora=${info.iva_mora}" ng-bind="iva_mora|currency"> <span/>
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              <span ng-init="a_iva_mora=${info.pago_iva_mora}" ng-bind="a_iva_mora|currency"> <span/>
                          </td>
                      </c:if>
                      <td>{{p_iva_mora|currency}}</td>  
                  </tr>
                           <tr>
                      <td><strong>Abono a capital:</strong></td>
                      <c:choose>
                          <c:when test="${info.saldo >= info.capital}">
                              <c:set var="abonoCapital" value="${info.capital}" />
                          </c:when>
                          <c:when test="${info.saldo < info.capital}">
                              <c:set var="abonoCapital" value="${info.saldo}" />
                          </c:when>
                      </c:choose>
                      <td>
                          <span ng-init="capital=${abonoCapital}" ng-bind="capital|currency"> <span/>
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              <span ng-init="a_capital=${info.pago_capital}" ng-bind="a_capital|currency"> <span/>
                          </td>
                      </c:if>
                      <td>{{p_capital|currency}}</td>      
                  </tr>
                  <tr>
                      <%--Estableciendo totales --%>
                      <c:choose>
                        <c:when  test="${info.estado == 'Pendiente'}">
                            <c:set var="total" value="${info.intereses + abonoCapital + info.interes_moratorio + info.iva_mora + info.iva_interes}" />
                        </c:when>
                          <c:when test="${info.estado == 'Abonado'}">
                            <c:set var="total" value="${(info.intereses - info.pago_interes) +
                                                        (info.iva_mora - info.pago_iva_mora)+
                                                        (info.iva_interes - info.pago_iva_interes)+
                                                        (abonoCapital - info.pago_capital) + 
                                                        (info.interes_moratorio - info.pago_interes_moratorio)}" />
                          </c:when>
                      </c:choose>
                          
                      <td><strong>Total a pagar:</strong></td>
                      <td>
                          <span ng-init="total=${total}" ng-bind="total|currency"> <span/>
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td></td>
                      </c:if>
                      <td>{{p_total|currency}}</td> 
                  </tr>
                  <tr>
                      <td><strong>Nuevo saldo:</strong></td>
                      <td>
                          <c:choose> 
                            <c:when test="${(info.saldo - (abonoCapital - info.pago_capital)) >=0.0}">
                                <span ng-init="nuevo_saldo=${info.saldo - (abonoCapital - info.pago_capital)}" ng-bind="nuevo_saldo|currency"> <span/>
                            </c:when>
                            <c:when test="${(info.saldo - (abonoCapital - info.pago_capital)) < 0.0}">
                              <span ng-init="nuevo_saldo=0" ng-bind="nuevo_saldo|currency"> <span/>
                            </c:when>
                          </c:choose>
                          
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td></td>
                      </c:if>
                      <td>{{p_nuevo_saldo|currency}}</td> 
                  </tr>
                  <tr>
                      <td></td>
                      <td></td>
                      <td class="mora"></td>
                  </tr>
              </table>
          </fieldset>
          </c:forEach>
      </c:if>
          <%
                double total = Double.parseDouble(pageContext.getAttribute("total").toString());
                total = diferenciaDias.redondear(total);
                pageContext.setAttribute("total", total);
          %>
          <form action="finalizarPago.jsp" method="POST">
            <input type="hidden" name="codigoPrestamo" value="${codigoPrestamo}" />
            <input type="hidden" name="idPago" value="${idPago}" />
            <input type="hidden" name="tipoPago" value="${tipoPago}" />
            <input type="hidden" name="fechaA" value="${fechaA}}" />
            <input type="hidden" id="tgr" ng-click="obtenerPagosPendientes();"/>
            <div class="row">
                <div class="col-md-2"><strong>Cantidad: </strong></div>
                <div class="col-md-3">
                    <input type="text"  id="inpMonto" class="form-control" name="monto" ng-model="pago" ng-keyup="verificacionDePago();" ng-init="pago=${total}" required />
                
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="submit" value="Realizar pago" class="btn btn-primary" />
                </div>
            </div>
          </form>
</div>
<script type="text/javascript">
   function ponerAlDia(){
       if($('#ckBtn').is(":checked")){
           location.href = location.href + '&rule=acum'
       }else{
           location.href = location.href.split("&")[0];
       }
   }
   
   $(document).ready(function(){
       var uri = location.href;
       angular.element('#inpMonto').trigger('keyup');
       angular.element('#tgr').trigger('click');
       var uri_params = uri.split("?")[1];
       var params_parts = uri_params.split("&");
       for(var i = 0; i < params_parts.length; i++){
           var key = params_parts[i].split("=")[0];
           var val = params_parts[i].split("=")[1];
           
           if(key==="rule"&&val==="acum"){
               $('#ckBtn').attr("checked","checked");
           }
       }
   });
</script>
<%@include file="../administracion/footer.jsp" %>