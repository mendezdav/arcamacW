<%@page import="java.sql.*"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    String json_response = "";
    double montoUltimoPago = 0;
    String fechaUltimoPago = " - - ";
    String query = "";
    String codigoPrestamo = request.getParameter("codigoPrestamo");
    
    try{
        query = "SELECT idabono, monto, fechaPago FROM abonos WHERE codigoPrestamo='"+codigoPrestamo+"' order by idabono desc limit 1";
        conexion con = new conexion();
        con.setRS(query);
        ResultSet rs = con.getRs();
        
        while(rs.next()){
            fechaUltimoPago = rs.getDate("fechaPago").toString();
            montoUltimoPago = rs.getDouble("monto");
        }
        
        query = "SELECT fechaEmision,tipo_pago, cuotas, fechaVencimiento, CONCAT(nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2) as nombreSolicitante FROM prestamos JOIN solicitante ON solicitante.idSolicitante = prestamos.idSolicitante WHERE codigoPrestamo='"+codigoPrestamo+"'";
        con.setRS(query);
        rs = con.getRs();
        json_response += "[";
        
        while(rs.next()){
            json_response += "{";
            json_response += "\"fechaEmision\":\""+rs.getDate("fechaEmision")+"\",";
            json_response += "\"fechaVencimiento\":\""+rs.getDate("fechaVencimiento")+"\",";
            json_response += "\"fechaUltimoPago\":\""+fechaUltimoPago+"\",";
            json_response += "\"tipoPago\":"+rs.getInt("tipo_pago")+",";
            json_response += "\"cuota\":"+rs.getDouble("cuotas")+",";
            json_response += "\"montoUltimoPago\":"+montoUltimoPago+",";
            json_response += "\"nombreSolicitante\":\""+rs.getString("nombreSolicitante")+"\"";
            json_response += "},";
        }
        
        json_response += "{}]";
        
        con.cerrarConexion();
    }catch(Exception s){
        json_response = "{\"msg\":\""+s.getMessage()+"\"}";
    }
 
    out.write(json_response);
%>
