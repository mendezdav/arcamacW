<%-- 
    Document   : realizarPago
    Created on : 01-jun-2014, 21:43:37
    Author     : Pochii
--%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="clases.diferenciaDias"%>
<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 
<%@include file="../administracion/header.jsp" %>
<style type="text/css">
 .content{background-position: center; margin: 100px 30px 20px 12%;} 
.tg  {border-collapse:collapse;border-spacing:0;border:none;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal; width: 150px;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.form-control{width: 200px;}
fieldset{margin: 30px 5px 30px 0px; border: 1px #006dcc solid; width:550px;padding-left: 15px;background-color: #ceeaea;}
legend{border: 1px #0099FF solid;padding: 5px 5px 5px 20px; width: 150px; border-radius: 5px; background-color: #fff;}
#detalle td{width: 200px; margin-left: 20px; text-align: justify;}
#detalle tr{height: 25px;}
</style>
<div class="content">
    <h2>Pago de cuotas</h2>
    <hr />
    <%
                diferenciaDias diasF = new diferenciaDias();
                String fechaActual = "", fechaEstablecida = "", fechaPago="";
                
                double diasRetraso = 0.0;
                /**Capturando fecha actual***/
                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                fechaActual = formato.format(new Date());
    %>
    <sql:query dataSource="jdbc/mysql" var="q1">
        SELECT * FROM pagosview WHERE idPago = ${param.idPago} AND estado != "Pagado"
    </sql:query>
    <sql:query dataSource="jdbc/mysql" var="q2">
        SELECT * FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago})
        AND estado != "Pagado" AND mora = 1 
    </sql:query>
    <sql:query dataSource="jdbc/mysql" var="q3">
        <c:if test="${param.ontime ==1}">
             SELECT * FROM pagosview WHERE codigoPrestamo IN (SELECT codigoPrestamo FROM pagos WHERE idPago = ${param.idPago})
            AND estado != "Pagado" AND mora = 1 ORDER BY idPago ASC LIMIT 1
        </c:if>
        <c:if test="${empty param.ontime}">
            SELECT * FROM pagosview WHERE idPago = ${param.idPago} AND estado != "Pagado"
        </c:if>
    </sql:query>    
    <c:forEach var="info" items="${q3.rows}">
          <c:set var="fechaEstablecida" value="${info.fechaEstablecida}" />
          <c:set var="fechaPago" value="${info.fechaPago}" />
          <c:set var="estado" value="${info.estado}" />
          <c:set var="mora" value="${info.mora}" />
          <%
                fechaEstablecida = pageContext.getAttribute("fechaEstablecida").toString();
                fechaPago        = pageContext.getAttribute("fechaPago").toString();
                Boolean mora = Boolean.parseBoolean(pageContext.getAttribute("mora").toString()); 
                if(mora){
                    if (pageContext.getAttribute("estado").equals("Pendiente"))
                        {
                            diasRetraso = diasF.obtenerDiasRetraso(fechaActual,fechaEstablecida);  
                        }else if(pageContext.getAttribute("estado").equals("Abonado")){
                            diasRetraso = diasF.obtenerDiasRetraso(fechaActual, fechaPago); 
                        }
                }
          %>
          <c:set var="diasRetraso" value="<%=(int)diasRetraso%>" />
          <c:set var="codigoPrestamo" value="${info.codigoPrestamo}" />
          <c:set var="idPago" value="${param.idPago}" />
          <table class="tg">
            <tr>
              <td class="tg-031e"><strong>Solicitante:</strong></td>
              <td class="tg-031e" colspan="3">${info.solicitante}</td>
            </tr>
            <tr>
              <th class="tg-031e"><strong>Pr�stamo No.</strong></th>
              <th class="tg-031e">${info.codigoPrestamo}</th>
              <th class="tg-031e"><strong>Comprobante:</strong></th>
              <th class="tg-031e">${info.comprobante}</th>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Estado:</strong></td>
              <td class="tg-031e">${info.estado}</td>
              <td class="tg-031e"><strong>Fecha Establecida:</strong></td>
              <td class="tg-031e">${info.fechaEstablecida}</td>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Saldo Actual:</strong></td>
              <td class="tg-031e">$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.saldo}" /></td>
              <td class="tg-031e" colspan="2" rowspan="2"></td>
            </tr>
            <tr>
              <td class="tg-031e"><strong>Cuota:</strong></td>
              <td class="tg-031e">$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.cuotas}" /></td>
            </tr>
          </table>
      </c:forEach>
      <c:if test="${param.ontime ==1}">
          <c:forEach var="info" items="${q2.rows}">
              
          </c:forEach>
      </c:if>
            
      <c:if test="${empty param.ontime}"> 
        <c:forEach var="info" items="${q1.rows}">
          <fieldset>
              <legend>DETALLE</legend>
              <table id="detalle">
                  <c:choose>
                      <c:when test="${info.estado == 'Abonado'}">
                          <tr>
                            <td></td>
                            <td><strong>Cantidad requerida</strong></td>
                            <td><strong>Cantidad Abonada </strong></td>
                          </tr>
                      </c:when>
                      <c:when test="${info.estado == 'Pendiente'}">
                          <tr>
                            <td></td>
                            <td><strong>Cantidad requerida</strong></td>
                            <td></td>
                          </tr>
                      </c:when>
                  </c:choose>
                  
                  <tr>
                      <td><strong>Intereses ordinarios:</strong></td>
                      <td>$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.intereses/1.13}" /></td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.pago_interes/1.13}" /></td>
                      </c:if>
                  </tr>
                   <tr>
                      <td>IVA:</td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${(info.intereses /1.13)*0.13}" />
                      </td>  
                      <c:if test="${info.estado == 'Abonado'}"> 
                          <td>$ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${(info.pago_interes /1.13)*0.13}" /></td>
                      </c:if> 
                  </tr>
                  <tr>
                      <td><strong>Abono a capital:</strong></td>
                      <c:choose>
                          <c:when test="${info.saldo >= info.capital}">
                              <c:set var="abonoCapital" value="${info.capital}" />
                          </c:when>
                          <c:when test="${info.saldo < info.capital}">
                              <c:set var="abonoCapital" value="${info.saldo}" />
                          </c:when>
                      </c:choose>
                      <c:set var="ns" value="${info.saldo - (abonoCapital - info.pago_intereses)}" ></c:set>
                      <c:set var="total_pago" value="${abonoCapital+  info.intereses+  info.interes_moratorio+ ns}" />
                      <!-- obtener nuevo saldo en -->
                      <c:choose> 
                            <c:when test="${(info.saldo - (abonoCapital - info.pago_capital)) >=0.0}">
                                <c:set var="nuevo_saldo" value="${info.saldo - (abonoCapital - info.pago_capital)}" />
                            </c:when>
                            <c:when test="${(info.saldo - (abonoCapital - info.pago_capital)) < 0.0}">
                               <c:set var="nuevo_saldo" value="0.00" />
                            </c:when>
                      </c:choose>
                      
                      <c:if test="${total_pago < info.cuotas}">
                          <c:set var="abonoCapital"  value="${abonoCapital + nuevo_saldo}" />
                          <c:set var="nuevo_saldo" value="0.00" />
                          <c:set var="saldoMenor" value="1" />
                      </c:if>
                                      
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${abonoCapital}" />
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.pago_capital}" />
                          </td>
                      </c:if>
                  </tr>
                  <tr>
                      <td><strong>Mora (${diasRetraso} dias):</strong></td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.interes_moratorio/1.13}" />
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${info.pago_interes_moratorio/1.13}" />
                          </td>
                      </c:if>
                  </tr>
                  <tr>
                      <td><strong>IVA:</strong></td>
                      <td>
                          $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${(info.interes_moratorio /1.13)*0.13}" />
                      </td>
                      <c:if test="${info.estado == 'Abonado'}">
                          <td>
                              $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${(info.pago_interes_moratorio /1.13)*0.13}" />
                          </td>
                      </c:if>
                  </tr>
                  <tr>
                      <%--Estableciendo totales --%>
                      <c:choose>
                        <c:when  test="${info.estado == 'Pendiente'}">
                            <c:set var="total" value="${info.intereses + abonoCapital + info.interes_moratorio}" />
                        </c:when>
                          <c:when test="${info.estado == 'Abonado'}">
                            <c:set var="total" value="${(info.intereses - info.pago_interes) + 
                                                        (abonoCapital - info.pago_capital) + 
                                                        (info.interes_moratorio - info.pago_interes_moratorio)}" />
                          </c:when>
                      </c:choose>
                      
                      <c:if test="${info.saldo < info.cuotas}">
                           <c:set var="total" value="${total + (info.intereses) + info.interes_moratorio}" />
                      </c:if>
                      <td><strong>Total a pagar:</strong></td>
                      <td>
                          <c:if test="${total > info.cuotas}">
                              <c:set var="total" value="${info.cuotas + info.interes_moratorio}" />
                              <c:set var="total_interes_moratorio" value="1" />
                          </c:if>
                          <c:if test="${saldoMenor != 1}">
                            $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${total}" />
                          </c:if>
                          <c:if test="${saldoMenor == 1}">
                            $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${total - (info.intereses/1.13) - (info.intereses/1.13) * 0.13 }" />
                          </c:if>  
                          
                      </td>
                      <td></td>
                  </tr>
                  <tr>
                      <td><strong>Nuevo saldo:</strong></td>
                      <td>
                         $ <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"  pattern="####.##" value="${nuevo_saldo}" />
                          <c:if test="${ns > 0.0}">
                            
                             <c:if test="${total_pago > info.cuotas and info.estado != 'Abonado'}">
                                  <c:set var="total" value="${info.cuotas}" />
                             </c:if>
                             <c:if test="${total_pago < info.cuotas}">
                                  <c:set var="total" value="${total_pago}" />
                             </c:if>
                              
                              <c:if test="${info.estado == 'Abonado' and total_pago < info.cuotas }">
                                  <c:set var="total" value="${nuevo_saldo}" />
                             </c:if>
                              
                           </c:if>
                           <c:if test="${total_interes_moratorio == 1}">
                                <c:set var="total" value="${total + info.interes_moratorio}" />
                           </c:if>
                          <!-- en el caso que sea abonado-->
                      </td>
                      <td></td>
                  </tr>
                  <tr>
                      <td></td>
                      <td></td>
                      <td class="mora"></td>
                  </tr>
              </table>
          </fieldset>
          </c:forEach>
      </c:if>
          <%
                double total = Double.parseDouble(pageContext.getAttribute("total").toString());
                total = diferenciaDias.redondear(total);
                pageContext.setAttribute("total", total);
          %>
          <form action="finalizarPago.jsp" method="POST">
            <input type="hidden" name="codigoPrestamo" value="${codigoPrestamo}" />
            <input type="hidden" name="idPago" value="${idPago}" />
            <div class="row">
                <div class="col-md-2"><strong>Cantidad:</strong></div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="monto" value="${total}" required />
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <input type="submit" value="Realizar pago" class="btn btn-primary" />
                </div>
            </div>
          </form>
            
</div>
<%@include file="../administracion/footer.jsp" %>