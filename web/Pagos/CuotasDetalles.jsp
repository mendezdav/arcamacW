<%-- 
    Document   : CuotasDetalles
    Created on : 06-01-2014, 01:23:36 AM
    Author     : jorge
--%>

<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*,clases.diferenciaDias" %>
<%@page  session="true" language="java" %>
<%@page import="java.sql.ResultSet"%>
<%@page import="javax.swing.JOptionPane"%>
<%@include file="../administracion/header.jsp" %>
<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../js/paginador/funciones.js"></script>
<script type="text/javascript" src="../js/cobros.js"></script>


<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
       
    </style>
    <style type="text/css">
    #frmPago td {
        padding-top: 10px;
    }
    
    table{
        font-size: 12px;
    }
    
    .currency{
        text-align: right;
        padding-right: 10px;
    }
</style>
    <%
         String codigoPrestamo = request.getParameter("codigoPrestamo");
         conexion con  = new conexion();
         String query = "select idPago from pagos where codigoPrestamo='" + codigoPrestamo + "' and estado!='Pagado' limit 1";
         //JOptionPane.showMessageDialog(null, query);
         con.setRS(query);
         ResultSet rs = con.getRs();
         if (rs.next())
             SessionActual.setAttribute("PagoActivo", rs.getString("idPago"));
         else
             response.sendRedirect("PrincipalPago.jsp?erno=2");
     %>
  </head>
  <body>
         
    <div ng-app="cobrosApp" ng-controller="controladorCobros" ng-init="no_prestamo = '<% out.print(codigoPrestamo); %>'">
        <center>
            <h4 style="margin-top: -20px;">Registrar un nuevo pago</h4>
        </center>
        <%
            ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            codigoPrestamo = request.getParameter("codigoPrestamo");
            SessionActual = request.getSession();
            SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
            int  contador = 0;//Integer.parseInt(SessionActual.getAttribute("contador").toString());
            
            if (codigoPrestamo != "" && codigoPrestamo != null)
            {
                String qr ="select p.codigoPrestamo,concat(s.nombre1,' ', s.nombre2,' ',s.apellido1,' ',s.apellido2) as nombre,"
                        + " t.tipoPago from prestamos as p inner join solicitante as s on s.idSolicitante = p.idSolicitante " 
                        + "inner join tipopago as t on t.idtipopago = p.tipo_pago where p.codigoPrestamo='"+codigoPrestamo+"'";
                SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
                con = new conexion();
                con.setRS(qr);
                ResultSet datosGenerales = (ResultSet) con.getRs();
                if (datosGenerales.next())
                {
                                                
        %>
        <div style="text-align: right; width: 1000px;">
            <span style="margin-right: 500px;"><a href="reimpresion.jsp?codigoPrestamo=<%out.print(codigoPrestamo);%>">Reimpresi�n de recibos</a></span>
            <button ng-click="nuevoPago();">
                <i class="fa fa-file"></i>
                <br/>
                Nuevo
            </button>
            <button ng-click="realizarPago();">
                <i class="fa fa-save"></i>
                <br/>
                Realizar pago
            </button>
            <button ng-click="reload();">
                <i class="fa fa-eraser"></i>
                <br/>
                Cancelar
            </button>
        </div>
        <br/>
        <center>
        <!--<table>
            <tr>
                <td><b>C&oacute;digo Prestamo: </b><br /><br /></td>
                <td><%= datosGenerales.getString("codigoPrestamo") %><br /><br /></td>
            </tr>
            
            <tr>
                <td><b>Solicitante: </b><br /><br /></td>
                <td><%= datosGenerales.getString("nombre") %><br /><br /></td>
            </tr>
            
            <tr>
                <td><b>Tipo Pago: </b><br /><br /></td>
                <td><%= datosGenerales.getString("tipoPago") %><br /><br /></td>
            </tr>
            
        </table>-->
        <div style="background: #e3e3e3; padding: 30px; width: 900px;">
            <table style="border-collapse: collapse;">
                <tr>
                    <td>
                        No de comprobante: 
                    </td>
                    <td style="width: 355px;text-align: center;">
                        <strong>Cuota:</strong> {{cuota | currency}}
                    </td>
                    <td>No de pr�stamo: </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" ng-model="comprobante" disabled="" style="width: 150px;"/>
                    </td>
                    <td></td>
                    <td>
                        <input type="text" ng-model="no_prestamo" disabled=""/>
                    </td>
                </tr>
            </table>
            <table id="frmPago">
                <tr>
                    <td>Fecha de pago: </td>
                    <td>
                        <input type="text" ng-model="fecha_pago" disabled=""/>
                    </td>
                    <td style="width: 100px;"></td>
                    <td>Monto($): </td>
                    <td>
                        <input type="text" id="mnt" class="currency" ng-model="monto" disabled="" ng-change="validarMonto();"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        Recib� de: <input type="text" style="width: 100%;" disabled="" ng-model="nombre"/>
                    </td> 
                </tr>
                <tr>
                    <td>
                        Fecha de �ltimo pago: 
                    </td>
                    <td>
                        <input type="text" disabled="" ng-model="fecha_ultimo_pago"/>
                    </td>
                    <td></td>
                    <td>
                        Monto �ltimo pago: 
                    </td>
                    <td>
                        <input type="text" disabled="" class="currency" ng-model="monto_ultimo_pago | currency"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--La cantidad de:--> 
                    </td>
                    <td>
                        <!--<input type="text" />-->
                    </td>
                    <td></td>
                    <td style="background: #f3f3f3;">
                        Efectivo
                        <input type="radio" ng-model="tipo" value="2" disabled=""/>
                        Planilla
                        <input type="radio" ng-model="tipo" value="1" disabled=""/>
                    </td>
                    <td>

                    </td>
                </tr>
            </table>
            <br/>
            <table style="text-align: center; border: dotted 1px #c3c3c3;">
                <tr>
                    <td colspan="2"></td>
                    <td colspan="3"><strong>Intereses</strong></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Capital</td>
                    <td>Int. Normal</td>
                    <td>Mora</td>
                    <td>IVA</td>
                </tr>
                <tr>
                    <td>Saldo anterior: </td>
                    <td><input type="text" disabled="" class="currency" ng-model="saldo_anterior.capital | currency"  /></td>
                    <td><input type="text" disabled="" class="currency" ng-model="saldo_anterior.interes_normal | currency"/></td>
                    <td><input type="text" disabled="" class="currency" ng-model="saldo_anterior.mora | currency"/></td>
                    <td><input type="text" disabled="" class="currency" ng-model="saldo_anterior.iva | currency"/></td>
                </tr>
                <tr>
                    <td>Pago: </td>
                    <td><input type="text" disabled="" class="currency" ng-model="pago.capital | currency"/></td>
                    <td><input type="text" disabled="" class="currency" ng-model="pago.interes_normal | currency"/></td>
                    <td><input type="text" disabled="" class="currency" ng-model="pago.mora | currency"/></td>
                    <td><input type="text" disabled="" class="currency" ng-model="pago.iva | currency"/></td>
                </tr>
                <tr>
                    <td>Nuevo saldo: </td>
                    <td><input type="text" disabled="" class="currency" ng-model="nuevo_saldo.capital | currency"/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <br/>
            <table>
                <tr>
                    <td>Otorgamiento: </td>
                    <td>
                        <input type="text" ng-model="fecha_otorgamiento" disabled=""/>
                    </td>
                    <td style="width: 50px;"></td>
                    <td>Vencimiento: </td>
                    <td>
                        <input type="text" ng-model="fecha_vencimiento" disabled=""/>
                    </td>
                </tr>
            </table>
        </div>
        </center>
    </div>
                <%   } // fin if  hay datos
            } // fin if si no es nulo ni vacio
         %>
<%@include file="../administracion/footer.jsp" %>


