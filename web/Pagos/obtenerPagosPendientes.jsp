<%@page import="java.sql.*"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    String json_response = "";
   
    String codigoPrestamo = request.getParameter("codigoPrestamo");
    
    try{
        String query = "SELECT idPago,intereses,capital,interes_moratorio, pago_interes,pago_capital,pago_interes_moratorio FROM pagosView WHERE codigoPrestamo='"+codigoPrestamo+"' AND estado!='Pagado'";
        conexion con = new conexion();
        con.setRS(query);
        ResultSet rs = con.getRs();
        json_response += "[";
        while(rs.next()){
            json_response += "{";
            json_response += "\"idPago\":"+rs.getInt("idPago")+",";
            json_response += "\"intereses\":"+rs.getDouble("intereses")+",";
            json_response += "\"capital\":"+rs.getDouble("capital")+",";
            json_response += "\"interes_moratorio\":"+rs.getDouble("interes_moratorio")+",";
            json_response += "\"pago_interes\":"+rs.getDouble("pago_interes")+",";
            json_response += "\"pago_capital\":"+rs.getDouble("pago_capital")+",";
            json_response += "\"pago_interes_moratorio\":"+rs.getDouble("pago_interes_moratorio")+"";
            json_response += "},";
        }
        json_response += "{}]";
    }catch(Exception s){
        json_response = "{}";
    }
 
    out.write(json_response);
%>