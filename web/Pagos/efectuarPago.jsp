<%@page import="java.sql.*"%>
<%@page import="Conexion.conexion"%>
<%@page import="clases.classPagos"%>
<%@page import="clases.Pago"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    
    /**
     * NUEVO: algoritmo para realizar pagos
     */
    
    // Cadena para dar respuesta al cliente que ha solicitado la transacción
    String json_response = "";
    
    // Código del préstamo a procesar
    String codigoPrestamo = request.getParameter("codigoPrestamo");
    
    // Monto abonado por el solicitante
    double monto = Double.parseDouble(request.getParameter("montoAbonado"));
    
    // Fecha del abono
    String fechaA = request.getParameter("fechaA");
    
    // Variables de respuesta del servidor
    boolean success = true;
    String serverMsg = "";
    int noAbono = 0;
    int noPago[];
    
    // Variable auxiliar para almacenar las consultas
    String query = "";
    
    // Variable auxiliar para conexion con base de datos
    conexion con = null;
    con = new conexion();
    
    // Se extraen todos los pagos disponibles 
    // para el préstamo al cual se desea abonar
    try{
        // Se contabilizan todos los pagos que se encuentra en calidad de 'Abonado' o 'Pendiente' 
        query = "SELECT count(*) as cnt FROM pagos WHERE codigoPrestamo='"+codigoPrestamo+"' AND estado!='Pagado'";
        
        con.setRS(query);
        ResultSet rs = con.getRs();
        
        int lim = 0;
        while(rs.next()){
            lim = rs.getInt("cnt");
        }
        
        // Luego de contabilizar los pagos estos se extraen de la base de datos en orden ascendente
        query = "SELECT idPago FROM pagos WHERE codigoPrestamo='"+codigoPrestamo+"' AND estado!='Pagado' ORDER BY idPago ASC LIMIT "+lim;
        con.setRS(query);
        rs = con.getRs();
        
        // Se crea un arreglo de objetos de tipo Pago, con los cuales se gestionará la distribución del monto abonado
        Pago pagosDisponibles[] = new Pago[lim];
        noPago    = new int[lim];
        int count = 0;
        
        while(rs.next()){
            // Se inicializa cada uno de los objetos correspondientes a los pagos pendientes
            pagosDisponibles[count] = new Pago(rs.getInt("idPago"));
            noPago[count] = 0;
            count++;
        }
        
        // Se almacena el monto en una variable auxiliar para su distribución y así conservar el monto original del abono
        // sin alteraciones
        double montoAplicable = monto;
        
        // Variables que indican cuánto dinero del monto total fue distribuido para el pago de cada rubro
        double monto_a_iva = 0.0;
        double monto_a_mora = 0.0;
        double monto_a_interes = 0.0;
        double monto_a_capital = 0.0;
        
        // ******* Inicio del algoritmo de pago *******
        for(int i = 0; i < 4; i++){
            if(montoAplicable > 0){
                if(i==0){
                    // cancelamos primero el iva
                    for(int j = 0; j < lim;j++){
                        if(montoAplicable >= pagosDisponibles[j].getIvaPendiente()){
                            double pendiente = pagosDisponibles[j].getIvaPendiente();
                            pagosDisponibles[j].abonarIva(pendiente);
                            montoAplicable -= pendiente;
                            monto_a_iva += pendiente;
                        }else{
                            pagosDisponibles[j].abonarIva(montoAplicable);
                            monto_a_iva += montoAplicable;
                            montoAplicable = 0;
                        }
                    }
                }
                
                if(i==1){
                    // ahora cancelamos la mora
                    for(int j = 0; j < lim;j++){
                        if(montoAplicable >= pagosDisponibles[j].getInteresMoratorioPendiente()){
                            double pendiente = pagosDisponibles[j].getInteresMoratorioPendiente();
                            pagosDisponibles[j].abonarInteresMoratorio(pendiente);
                            montoAplicable -= pendiente;
                            monto_a_mora += pendiente;
                        }else{
                            pagosDisponibles[j].abonarInteresMoratorio(montoAplicable);
                            monto_a_mora += montoAplicable;
                            montoAplicable = 0;
                        }
                    }
                }
                
                if(i==2){
                    // ahora cancelamos el interes
                    for(int j = 0; j < lim;j++){
                        if(montoAplicable >= pagosDisponibles[j].getInteresPendiente()){
                            double pendiente = pagosDisponibles[j].getInteresPendiente();
                            pagosDisponibles[j].abonarInteres(pendiente);
                            montoAplicable -= pendiente;
                            monto_a_interes += pendiente;
                        }else{
                            pagosDisponibles[j].abonarInteres(montoAplicable);
                            monto_a_interes += montoAplicable;
                            montoAplicable = 0;
                        }
                    }
                }
                
                if(i==3){
                    // ahora cancelamos el capital
                    for(int j = 0; j < lim;j++){
                        if(montoAplicable >= pagosDisponibles[j].getCapitalPendiente()){
                            double pendiente = pagosDisponibles[j].getCapitalPendiente();
                            pagosDisponibles[j].abonarCapital(pendiente);
                            montoAplicable -= pendiente;
                            monto_a_capital += pendiente;
                        }else{
                            pagosDisponibles[j].abonarCapital(montoAplicable);
                            monto_a_capital += montoAplicable;
                            montoAplicable = 0;
                        }
                    }
                }
            }
        }
        // ******* FIN del algoritmo de pago *******
        
        // Una vez cada pago tiene sus cantidades distribuidas se procede a actualizar la información
        // en la base de datos
        for(int j = 0; j < lim;j++){
           pagosDisponibles[j].actualizar();
           if(pagosDisponibles[j].getAbonoRealizado() > 0){
               noPago[j] = pagosDisponibles[j].getIdPago();
           }
        }
       
        // Obtenemos el saldo actual del préstamo 
        double saldoPrestamo = 0.0;
       
        query = "SELECT saldo FROM prestamos WHERE codigoPrestamo='"+codigoPrestamo+"'";
        con.setRS(query);
        rs = con.getRs();

        while(rs.next()){
            saldoPrestamo = rs.getDouble("saldo");
        }
       
        // Se procede a actualizar el saldo del préstamo y el estado del préstamo
        if((saldoPrestamo - monto_a_capital) == 0){
            query = "UPDATE prestamos SET saldo = (saldo - "+monto_a_capital+"), estado='Pagado', fechaFinalizacion = CURDATE() WHERE codigoPrestamo='"+codigoPrestamo+"'";
        }else{
            query = "UPDATE prestamos SET saldo = (saldo - "+monto_a_capital+") WHERE codigoPrestamo='"+codigoPrestamo+"'";
        }
       
        con.actualizar(query);
      
        // Procedemos a obtener la fecha del último pago
        String qr = "select fechaPago from pagos where estado != 'Pendiente' AND codigoPrestamo='"+codigoPrestamo+"' order by idPago DESC limit 1"; 
        
        String fechapagoAnt = "1000-01-01";
        
        try{
            con.setRS(qr);
            rs = con.getRs();
            rs.next();
            fechapagoAnt = rs.getString("fechaPago");
        }
        catch (Exception e)
        {
            fechapagoAnt = "1000-01-01"; 
        }
      
        // Insertamos el abono
        query = "insert into abonos(monto,fechaPago,pago_iva,pago_interes,pago_capital,pago_interes_moratorio,fechaPagoAnt,saldoAnt,SaldoActual,codigoPrestamo)"
               + "values("+monto+",CURDATE(),"+(monto_a_iva)+","+(monto_a_interes)+","
               + ""+monto_a_capital+","+monto_a_mora+",'"+ fechapagoAnt+"'," + saldoPrestamo + "," + (saldoPrestamo - monto_a_capital) + ",'" +codigoPrestamo+ "');";
        
       
        
        con.actualizar(query);
        
        query = "SELECT MAX(idabono) as noAbono FROM abonos";
        // Obtenemos el número de abono realizado
        con.setRS(query);
        rs = con.getRs();
        while(rs.next()){
            noAbono = rs.getInt("noAbono");
        }
        
        
        for(int g = 0; g < lim; g++){
            if(noPago[g]!=0){
                query = "INSERT INTO abonos_a_pagos(idPago, idAbono) VALUES("+noPago[g]+","+noAbono+");"; 
                con.actualizar(query);
            }
        }
        
        // Cerramos la conexión
        con.cerrarConexion();
        
    }catch(Exception s){
        // Si ocurre algún error se notifica al cliente
        success = false;
        serverMsg = s.getMessage();
    }
  
    json_response = "{"
            + "\"success\":"+success+","
            + "\"serverMsg\":\""+serverMsg+"\","
            + "\"comprobante\":"+noAbono+""
            + "}";
    
    out.write(json_response);

%>