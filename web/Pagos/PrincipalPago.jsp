<%-- 
    Document   : PrincipalUsuario
    Created on : 10-20-2013, 04:18:06 PM
    Author     : darkshadow
--%>

<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../administracion/header.jsp" %>
<script type="text/javascript" src="../js/selectAjax.js"></script>
<script lang="javascript" src="../js/validacion.js"></script>
<script lang="javascript" src="../js/paginador/funciones.js"></script>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
  </head>
  <div class="container">
        <center>
            <h3>Pago de Cuotas</h3><p />
        </center>
        <br /><br />
        <%
        String criterio  = request.getParameter("criterio");
        String pagR = request.getParameter("pag");
        
        if (criterio == null || criterio.equals("") ) // si criterio no es nulo, colocar en cuadro busqueda
        {
        %>
        <body onload="listaproductoss('<%=criterio%>','<%=pagR%>',null,'5')">
        <%
        }
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('listadoPrestamos.jsp', 'txtNombre','busqueda',<%=pagR%>,'PrincipalPago.jsp','5');" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
       <fieldset>
       <legend>Buscar Prestamo</legend>
        <%
            // JOptionPane.showMessageDialog(null,pagR);
             if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="C&oacute;digo o Solicitante"  
                                oninput="paginar('listadoPrestamos.jsp', 'txtNombre','busqueda',1,'PrincipalPago.jsp','5');" />
             </div> 
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            { 
            %>
                 <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="C&oacute;digo o Solicitante"  
                                oninput="paginar('listadoPrestamos.jsp', 'txtNombre','contenedor', 1,'PrincipalPago.jsp','5');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="contenedor">
                    </div>
             </center>
             <%}%>
 </div>
<%@include file="../administracion/footer.jsp" %>


