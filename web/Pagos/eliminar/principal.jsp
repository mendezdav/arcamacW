<%-- 
    Document   : mostrar
    Created on : 05-30-2014, 05:11:40 PM
    Author     : jorge
--%>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>

<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../../js/selectAjax.js"></script>
<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
  </head>
 
  <div class="container">
  <%
       
        String pagR = request.getParameter("pag");
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('listado.jsp', 'txtNombre','busqueda',<%=pagR%>);" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
       <fieldset>
           <legend>Abonos por C&oacute;digo Pr&eacute;stamo:</legend>
        <%
            String criterio  = request.getParameter("criterio");
       // JOptionPane.showMessageDialog(null,pagR);
            if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="Buscar abonos por c&oacute;digo"  
                                oninput="paginar('listado.jsp', 'txtNombre','busqueda',1);" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            {
                %>
                 <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="Buscar abonos por c&oacute;digo"  
                                oninput="paginar('listado.jsp', 'txtNombre','busqueda', 1);" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
                
                <%
            }
                %>
    </fieldset>
  </div>
<%@include file="../../administracion/footer.jsp" %>
