<%-- 
    Document   : FechaPagoAnt
    Created on : 02-ago-2014, 15:45:44
    Author     : Pochii
--%>

<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../administracion/header.jsp" %>

<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<script type="text/javascript" src="../js/selectAjax.js"></script>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        #fecha{
            max-width: 200px;
            display: inline;
        }
         .container
         {max-width:  900px;
          margin: 0 auto;
          text-align: justify;     
         }
    </style>
  </head>
  <c:if test="${empty param.idPago}">
      <c:redirect url="PrincipalPago.jsp">
          <c:param name="erno" value="2" />
      </c:redirect>
  </c:if>
  <c:if test="${!empty param.idPago}">
      <c:set value="${param.idPago}" var="idPago" />
  </c:if>
  <div class="container">
      <center>
          <h1>Pre pago de pr&eacute;stamo</h1>
          <br />
          <form name='frmfecha' action='<%=ruta%>/ActualizarPagoAnteriores' method="post">
            <span>Por favor seleccione la fecha en que se realiz&oacute; el pago:</span>
            
            <input id='idPago' type='hidden' name='idPago' value='${idPago}' /> 
            <input id='fecha' type='date' name='fechaPagoAnterior' class='form-control' required /> 
            <br />
            <br />
            <input type="submit" value="Continuar" class='btn btn-primary' />
          </form>
      </center>
      
</div>
<%@include file="../administracion/footer.jsp" %>



