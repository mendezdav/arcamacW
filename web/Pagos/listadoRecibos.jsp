<%-- 
    Document   : listadoCuotaDetall
    Created on : 06-02-2014, 11:51:35 PM
    Author     : jorge
--%>
<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*,clases.diferenciaDias" %>
<%@page  session="true" language="java" %>
<%@page import="java.sql.ResultSet"%>
<%@page import="javax.swing.JOptionPane"%>

<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../js/paginador/funciones.js"></script>
    <script language="JavaScript" >
        function llamar(ruta, codigoPrestamo, comprobante)
        {
            window.open(ruta + '/ReporteFactura?codigoPrestamo='+codigoPrestamo+'&comprobante='+comprobante,'','resizable=yes,scrollbars=yes,height=400,width=800');
            window.history.back(-1);
        }
    </script>

        
        <h3>Pagos realizados</h3>
        <hr/>
        <br /><br />
        <%
            String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            String codigoPrestamo = request.getParameter("codigoPrestamo");
            HttpSession SessionActual = request.getSession();
            SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
            int  contador = 0;//Integer.parseInt(SessionActual.getAttribute("contador").toString());
            
            if (codigoPrestamo != "" && codigoPrestamo != null)
            {
                String qr ="select p.codigoPrestamo,concat(s.nombre1,' ', s.nombre2,' ',s.apellido1,' ',s.apellido2) as nombre,"
                        + " t.tipoPago from prestamos as p inner join solicitante as s on s.idSolicitante = p.idSolicitante " 
                        + "inner join tipopago as t on t.idtipopago = p.tipo_pago where p.codigoPrestamo='"+codigoPrestamo+"'";
                SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
                conexion con = new conexion();
                con.setRS(qr);
                ResultSet datosGenerales = (ResultSet) con.getRs();
                if (datosGenerales.next())
                {
                                                
        %>
        <center>
        <table>
            <tr>
                <td style="text-align: right;"><b>C&oacute;digo Pr�stamo: </b><br /><br /></td>
                <td><%= datosGenerales.getString("codigoPrestamo") %><br /><br /></td>
            </tr>
            
            <tr>
                <td style="text-align: right;"><b>Solicitante: </b><br /><br /></td>
                <td><%= datosGenerales.getString("nombre") %><br /><br /></td>
            </tr>
            
            <tr>
                <td style="text-align: right;"><b>Tipo Pago: </b><br /><br /></td>
                <td><%= datosGenerales.getString("tipoPago") %><br /><br /></td>
            </tr>
            
        </table>
        
        </center>
                
        <%   } // fin if  hay datos
            } // fin if si no es nulo ni vacio
         %>
 
   <!-- Cambios Paginacion  -->
     <%
   
     out.println("<br><br>");
     out.println("<div class='sinbusqueda'>");
     out.println("<table class='tablita'>");
     out.println("<caption>LISTADO DE PAGOS</caption>");
     out.println("<tr>");
     out.println("<th>Codigo Prestamo</th>");
     out.println("<th>Comprobante</th>");
     out.println("<th>Fecha de pago</th>");
     out.println("<th>Opciones</th>");
     out.println("</tr>");
     out.println("<tbody>");
     int RegistrosAMostrar=12;
     int PaginasIntervalo=3;
     int RegistrosAEmpezar=0;
     int PagAct = 1;
     String pag = request.getParameter("pagina");
     if (pag.equals("1"))
         contador=0;
     if (pag==null || pag==""){
        RegistrosAEmpezar=0;
        PagAct=1;
     }
     else{
        RegistrosAEmpezar=(Integer.valueOf(pag).intValue()-1)* RegistrosAMostrar;
        PagAct=Integer.valueOf(pag).intValue();
     }
     %>
     <%
     int tmp = RegistrosAMostrar * ( PagAct-1);
     String qr = "select codigoPrestamo,idabono as comprobante,fechaPago  "
             + "from abonos where codigoPrestamo='" + codigoPrestamo +"'  limit " + tmp + "," + RegistrosAMostrar;
     conexion con = new conexion();
     con.setRS(qr);
     ResultSet resultado2 = con.getRs();
     String estadoMora ="";
     int contadorPagados=0;
     while (resultado2.next())
     {
         
     %>
     <tr id="grid">
     <td id="formnuevo"><%=resultado2.getString("codigoPrestamo") %></td>
     <td id="formnuevo"><%=resultado2.getString("comprobante") %></td>
     <td id="formnuevo"><%=resultado2.getString("fechaPago") %></td>
     <td id="formnuevo">
         <%
             //SessionActual.setAttribute("codigoPrestamo", resultado2.getString("codigoPrestamo"));
             //SessionActual.setAttribute("comprobante", resultado2.getString("comprobante"));
             %>
            <a href="#" onclick="llamar('<%=ruta%>','<%out.print(resultado2.getString("codigoPrestamo"));%>','<%out.print(resultado2.getString("comprobante"));%>');" >Reimprimir</a>
             <%
         %></td>
     </tr>
   <%
     } %>
     <%
     qr = "select count(*) as total from abonos where codigoPrestamo='"+ codigoPrestamo+"'";
     con.setRS(qr);
     ResultSet rs = con.getRs();
     rs.next();
     String NroRegistros =  rs.getString("total");
     int PagAnt=PagAct-1;
     int PagSig=PagAct+1;
     double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
     int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
     if(Res>0){
     PagUlt=Math.floor(PagUlt)+1;
     }
     out.println("</tbody>");
     out.println("</table></div><br />");
     out.println("<div style='width:300px; float:right'>");
     if(PagAct>(PaginasIntervalo+1)) {
     out.println("<a onclick=listarecibos('1'); class='paginador'><< Primero</a>");
     out.println("&nbsp;");
     }
     for ( int i = (PagAct-PaginasIntervalo) ; i <= (PagAct-1) ; i ++) {
     if(i>=1) {
     out.println("<a onclick=\"listarecibos('"+i+"','"+ codigoPrestamo +"')\" class='paginador'>"+i+"</a>");
     out.println("&nbsp;");
     }
     }
     out.println("<span class='paginadoractivo'>"+PagAct+"</span>");
     out.println("&nbsp;");
     for ( int i = (PagAct+1) ; i <= (PagAct+PaginasIntervalo) ; i ++) {
     if(i<=PagUlt) {
     out.println("<a onclick=\"listarecibos('"+i+"','"+ codigoPrestamo+"')\" class='paginador'>"+i+"</a>");
     out.println("&nbsp;");
     }
     }
     if(PagAct<(PagUlt-PaginasIntervalo)) {
     out.println("<a onclick=\"listarecibos('"+PagUlt+"','"+ codigoPrestamo+"')\" class='paginador'>Ultimo >></a>");
     }
     out.println("</div>");
     //fin del paginador
     resultado2.close();
     con.cerrarConexion();
     %>
   <!-- fin cambios paginacion -->