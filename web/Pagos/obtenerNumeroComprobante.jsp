<%@page import="java.sql.*"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    String json_response = "";
   
    String codigoPrestamo = request.getParameter("codigoPrestamo");
    conexion con = null;
    try{
        con = new conexion();
        
        String query = "SELECT MAX(idabono) as noAbono FROM abonos";
        // Obtenemos el número de abono realizado
        con.setRS(query);
        ResultSet rs = con.getRs();
        int noAbono = 0;
        
        while(rs.next()){
            noAbono = rs.getInt("noAbono");
        }
        
        noAbono++;
        json_response += "";
        json_response += "{";
        json_response += "\"noAbono\":"+noAbono+"";
        json_response += "}";
        
        json_response += "";
        con.cerrarConexion();
        
    }catch(Exception e){
        json_response = "{}";
    }
    
    out.write(json_response);
%>