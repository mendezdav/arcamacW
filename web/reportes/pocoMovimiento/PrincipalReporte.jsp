<%-- 
    Document   : PrincipalReporte
    Created on : 07-16-2014, 11:09:31 PM
    Author     : jorge
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="clases.BeanReporteSinMovimiento"%>
<%@page import="clases.BeanReporteSinMovimiento"%>
<%@page import="clases.diferenciaDias"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script language="JavaScript" >
            function llamar(ruta)
            {
                window.location.href = ruta + "/reportes/panelReportes.jsp";
                window.open(ruta + '/reporteSinMovimiento','','resizable=yes,scrollbars=yes,height=400,width=800');
            }
        </script>
    </head>
<%
         String meses = request.getParameter("meses");
         int dias = Integer.parseInt(meses);
         dias = dias * 30;
         String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
         String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg");   
        
         HttpSession SessionActual = request.getSession();
         List<Object> reports = new LinkedList<Object>();
         ArrayList<String> abonosValidos = new ArrayList<String>();
         ArrayList<Integer> diasR = new ArrayList<Integer>();
         String qrPrestamos = "select codigoPrestamo,estado from prestamos where estado='Activo'";
         conexion con = new conexion();
         con.setRS(qrPrestamos);
         ResultSet rs = con.getRs();
         while(rs.next())
         {
             // obtener ultimo pago
             String qrPagos = "select idPago from pagos where codigoPrestamo='"+rs.getString("codigoPrestamo")+"' and estado !='Pendiente'\n" +
                              "order by idPago DESC limit 1";
             out.print(qrPagos);
             conexion con2  = new conexion();
             con2.setRS(qrPagos);
             ResultSet rs2 = con2.getRs();
             if (rs2.next()) // un iof ya que solo puede haber un registros
             {
                // teniendo el id del pago ultimo, obtenemos el ultimo abono
                 String qrAbonos = "select idabono,fechaPago from abonos where idPago=" + rs2.getInt("idPago") + " order by idabono DESC limit 1";
                 conexion con3 = new conexion();
                 con3.setRS(qrAbonos);
                 ResultSet rs3 = con3.getRs();
                 if (rs3.next())
                 {
                     // revisar los dias desde la ultima fecha de pago
                     String qrDateDiff = "select DATEDIFF('"+ diferenciaDias.fechaActual()+ "','" + rs3.getString("fechaPago")+ "') as dias";
                     conexion con4 = new conexion();
                     con4.setRS(qrDateDiff);
                     ResultSet rs4 =  con4.getRs();
                     if (rs4.next())
                     {
                         if (rs4.getInt("dias") >= dias)
                         {
                            diasR.add(rs4.getInt("dias"));
                            abonosValidos.add(rs3.getString("idabono"));
                         }
                         
                     }
                     
                 }
             }
             
         }
         
         /*
          * Ya teniendo los ultimos abonos con mas de X meses (dias), obtener los demas datos para llenar el reporte
          */
         int contador = 0;
         for (String abonoVal: abonosValidos)
         {
             BeanReporteSinMovimiento sinMovimiento = new BeanReporteSinMovimiento();
             String qr = "select a.idPago,p.codigoPrestamo,pre.cantidad,pre.codigoPrestamo,pre.saldo,pre.cuotas,\n" +
                         "pre.plazo,pre.interes, concat(s.nombre1, ' ', s.nombre2,' ',s.apellido1, ' ', s.apellido2) as nombre from abonos as a inner join pagos as p on p.idPago = a.idPago\n" +
                         "inner join prestamos as pre on pre.codigoPrestamo = p.codigoPrestamo inner join solicitante as s on\n" +
                         "s.idSolicitante = pre.idSolicitante where a.idabono = " + abonoVal;
             
             conexion con5 = new conexion();
             con5.setRS(qr);
             ResultSet rs5 = con5.getRs();
             if (rs5.next())
             {

                 sinMovimiento.setNumeroMeses(Integer.parseInt(meses));
                 sinMovimiento.setFechaActual(diferenciaDias.fechaActual());
                 sinMovimiento.setLogo(rutaImg);
                 sinMovimiento.setCodigo(rs5.getString("codigoPrestamo"));
                 sinMovimiento.setSolicitante(rs5.getString("nombre"));
                 sinMovimiento.setMonto(rs5.getDouble("cantidad"));
                 sinMovimiento.setCuota(rs5.getDouble("cuotas"));
                 sinMovimiento.setInteres(rs5.getDouble("interes"));
                 sinMovimiento.setSaldoActual(rs5.getDouble("saldo"));
                 sinMovimiento.setDiasRetraso(diasR.get(contador));
             }
             reports.add((Object) sinMovimiento);
             contador++;
         }  
         
         if (contador != 0)
         {
            SessionActual.setAttribute("reporte", reports);
         %>
         <body onload="llamar('<%=ruta%>');" >
         <%
          }
          
         else
             response.sendRedirect(ruta + "/reportes/panelReportes.jsp?erno=5");
         
 %>