<%-- 
    Document   : CuotasDetalles
    Created on : 01-09-2014, 01:23:36 PM
    Author     : jorge
--%>


<%@page import="clases.diferenciaDias"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>

<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
 

<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../js/selectAjax.js"></script>
<style media="all" type="text/css" >
    @import url("../../css/screen.css");
</style>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
  </head>
  <div class="container">
        <center>
          <h3>Detalles de la Cuenta</h3><p />
        </center>
        <br /><br />
        
        <% // sacar los datos necesarios
            
            // variables
            String cliente = "",codigo="",cuotas="";
            String fechaUltimoPago = "";
            String cuotasCanceladas="";
            Double montoInicial=0.0,montoActual=0.0;
            String fechaProximoPago ="";
            int diasI = 0;
            String cuotasF = "";
            // obtener idSolicitante
            String idSolicitante = request.getParameter("idSolicitante");
            Date fecha = new Date();
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            String fechaImpresion  = formato.format(fecha.getTime());
            
            String qr = "select concat(s.nombre1, ' ' , nombre2, ' ', apellido1, ' ', apellido2) as nombre,"
                    + " p.cantidad as saldoInicial, p.codigoPrestamo,p.saldo,p.plazo from solicitante as s "
                    + "inner join prestamos as p on p.idSolicitante = s.idSolicitante where p.estado='Activo' and"
                    + " s.idSolicitante='" +idSolicitante+ "';";
            
            // estableciendo conexion a BD
            conexion con = new conexion();
            con.setRS(qr);
            ResultSet resultado = (ResultSet) con.getRs();
            if (resultado.next())
            {
                cliente = resultado.getString("nombre");
                codigo = resultado.getString("codigoPrestamo");
                montoInicial = resultado.getDouble("saldoInicial");
                montoActual = resultado.getDouble("saldo");
                cuotas = resultado.getString("plazo");
                
                         // sacar ultima fecha de pago
            qr ="select * from pagos where estado='Pagado' and codigoPrestamo='" +codigo+ "' "
                        + "order by fechaPago DESC limit 1";
            con.setRS(qr);
            resultado = (ResultSet) con.getRs();
           
            if (resultado.next()) // si hay pagos con estado pagado!
            {
               fechaUltimoPago  =  resultado.getString("fechaPago");
            }
            else
            {
                fechaUltimoPago = "Aun no se reportan pagos";
            }
                      
            // obtener el numero de cuotas canceladas
             qr ="select count(*) as total from pagos where estado='Pagado' and codigoPrestamo='"+codigo+"'";
            con.setRS(qr);
            resultado = (ResultSet) con.getRs();
                        
            
            if(resultado.next())
                cuotasCanceladas = resultado.getString("total");
            else
                cuotasCanceladas = "0";
            
            
            // obtener dias en mora!
            qr ="select * from pagos where estado='Pendiente' and codigoPrestamo='"+codigo+"' limit 1";
            con.setRS(qr);
            resultado = (ResultSet) con.getRs();
            
            if (resultado.next())
            {
                fechaProximoPago = resultado.getString("fechaEstablecida");
            
            }
            else
                fechaProximoPago="Cancelado Totalmente";
         
          
            // instancia
            diferenciaDias diasF = new diferenciaDias();
            double dias = diasF.obtenerDiasRetraso(fechaImpresion,fechaProximoPago);
            cuotasF = cuotasCanceladas + "/" + cuotas;
            diasI = (int) dias;
            
            if (diasI <  0)
                diasI=0;
                
            }
            else // si no next no hay solicitante con prestamo activo
            {
                
            }
            
        %>
        
        <jsp:useBean id="DetalleCuenta" class="clases.beanDetalleCuenta" scope="session" />
        <jsp:setProperty name="DetalleCuenta" property="fecha_impresion" value="<%=fechaImpresion%>" />
        <jsp:setProperty name="DetalleCuenta" property="cliente" value="<%=cliente%>" />
        <jsp:setProperty name="DetalleCuenta" property="codigoPrestamo" value="<%=codigo%>" />                     
        <jsp:setProperty name="DetalleCuenta" property="montoOriginal" value="<%=montoInicial%>" />
        <jsp:setProperty name="DetalleCuenta" property="montoActual" value="<%=montoActual%>" />
        <jsp:setProperty name="DetalleCuenta" property="cuotas" value="<%=cuotasF%>" />
        <jsp:setProperty name="DetalleCuenta" property="ultimoPago" value="<%=fechaUltimoPago%>" />
        <jsp:setProperty name="DetalleCuenta" property="mora" value="<%=diasI %>" />
        <jsp:setProperty name="DetalleCuenta" property="proximoPago" value="<%=fechaProximoPago%>" />
        <%
        SessionActual.setAttribute("beanDetalleCuenta",DetalleCuenta);
        %>
                         
        <table>
            <tr>
                <td>Fecha: </td>
                <td><%= fechaImpresion%></td>
            </tr>
            
            <tr>
                <td>Cliente:</td>
                <td><%= cliente %></td>
            </tr>
            
            <tr>
                <td>C&oacute;digo Prestamo:</td>
                <td><%= codigo%></td>
            </tr>
           
            <tr>
                <td>Monto Inicial:</td>
                <td>$<%= montoInicial%></td>
            </tr>
            
            <tr>
                <td>Monto Actual:</td>
                <td>$<%= montoActual%></td>
            </tr>
            
            <tr>
                <td>Cuotas</td>
                <td>
                    <%= cuotasF %>
                </td>
            </tr>
            
            <tr>
                <td>Ultimo Pago:</td>
                <td><%= fechaUltimoPago %></td>
            </tr>
            
            <tr>
                <td>Dias en mora</td>
                <td>
                    <%=diasI %>
                </td>
            </tr>
            
            
        </table>

 <input type="button" class="btn btn-info" onclick="llamar('<%=ruta%>');" value="Imprimir " >
       
  </div>
   <script language="JavaScript" >
        function llamar(ruta)
        {
            window.open(ruta + '/ReporteDetalleCuenta','','resizable=yes,scrollbars=yes,height=400,width=800');
            window.history.back(-1);

        }
    </script>
 
 
<%@include file="../../administracion/footer.jsp" %>


