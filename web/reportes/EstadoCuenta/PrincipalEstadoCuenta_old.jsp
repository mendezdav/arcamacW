<%-- 
    Document   : PrincipalUsuario
    Created on : 10-20-2013, 04:18:06 PM
    Author     : darkshadow
--%>

<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>

<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../../js/selectAjax.js"></script>

<style type="text/css">
     body {
          padding-top: 60px;
        }
    .container
    {
                max-width:  1000px;
                margin: 0 auto;
                text-align: justify;
               
    }
    </style>
  </head>
  <div class="container">
        <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li><a href="<%=ruta%>/reportes/panelReportes.jsp">Panel de reportes</a></li>
                <li class="active">Estado de cuenta </li>
        </ol>
        <center>
          <h3>Estado Cuenta</h3><p />
        </center>
        <br /><br />
    
    <fieldset>
        <legend>C&oacute;digo del prestamos:</legend>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' placeholder="Buscar solicitante por prestamo"  onkeyup="buscarPrestamos('mostrarPrestamos.jsp?busqueda=c', 'txtNombre','busqueda');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
    </fieldset>
  
   <!-- Opcion 2, prestamo por solicitante -->     
    <fieldset>
        <legend>Solicitante:</legend>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre2' id='txtNombre2' class='form-control' placeholder="Buscar por solicitante"  onkeyup="buscarPrestamos('mostrarPrestamos.jsp?busqueda=s', 'txtNombre2','busqueda2');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda2">
                    </div>
             </center>
    </fieldset>     
        
       
  </div>
<%@include file="../../administracion/footer.jsp" %>


