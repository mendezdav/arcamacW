<%-- 
    Document   : mostrarFiadorMantenimiento
    Created on : 12-11-2013, 06:29:40 PM
    Author     : jorge
--%>

    <%@page import="javax.swing.JOptionPane"%>
    <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <style media="all" type="text/css" >
        @import url("../../css/screen.css");
    </style>
        <%
            String criterio,busqueda; 
            criterio = request.getParameter("criterio"); 
            busqueda = request.getParameter("busqueda");
           if(criterio != null) {
                if (busqueda.equals("c")){
          %>
        
          <sql:query var="consulta1" dataSource="jdbc/mysql" >
              select concat(s.nombre1,  ' ' , s.nombre2 , ' ' , s.apellido1 , ' ' , s.apellido2) as nombre,s.idSolicitante, s.DUI,s.tel_residencia
              from solicitante as s inner join prestamos as p on p.idSolicitante = s.idSolicitante
              where p.codigoPrestamo like  '%<%= criterio %>%'
          </sql:query>
              
          <display:table id="solicitantes" name="${consulta1.rows}" pagesize="10" export="true"  >
               <display:column title="Nombre Solicitante" property="nombre"  />
               <display:column title="DUI" property="DUI"  />
               <display:column title="Tel." property="tel_residencia" />
               <display:column  value="<i class='fa fa-edit'></i>Seleccionar" url="/reportes/EstadoCuenta/DetalleCuenta.jsp" paramId="idSolicitante" paramProperty="idSolicitante"  style="text-align:center;"/>
          </display:table>
              
        </ajax:displaytag>
              <%
                  } // fin busqueda por codigo
                
                if (busqueda.equals("s"))
                {
                    %>
              <sql:query var="consulta1" dataSource="jdbc/mysql" >
               select concat(s.nombre1,  ' ' , s.nombre2 , ' ' , s.apellido1 , ' ' , s.apellido2) as nombre,s.idSolicitante, s.DUI,s.tel_residencia
                from solicitante as s inner join prestamos as p on p.idSolicitante = s.idSolicitante
                where concat(s.nombre1,  ' ' , s.nombre2 , ' ' , s.apellido1 , ' ' , s.apellido2) like  '%<%= criterio %>%'
             </sql:query>
              
          <display:table id="solicitantes" name="${consulta1.rows}" pagesize="10" export="true"  >
               <display:column title="Nombre Solicitante" property="nombre"  />
               <display:column title="DUI" property="DUI"  />
               <display:column title="Tel." property="tel_residencia" />
               <display:column  value="<i class='fa fa-edit'></i>Seleccionar" url="/reportes/EstadoCuenta/DetalleCuenta.jsp" paramId="idSolicitante" paramProperty="idSolicitante"  style="text-align:center;"/>
          </display:table>
              
        </ajax:displaytag>
                    <%
                }
                }
                %>
       