<%-- 
    Document   : impresionDocumentacion
    Created on : 09-05-2014, 11:31:25 PM
    Author     : jorge

--%>
<%@page import="Conexion.conexion"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../../administracion/header.jsp" %>
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />

<%
    String codigoP = request.getParameter("codigoPrestamo");
    SessionActual.setAttribute("codigoReporte", codigoP);
    conexion con = new conexion();
    boolean estadoAprobado = true;
    String qr = "select estado from prestamos where codigoPrestamo='" + codigoP + "'";
    con.setRS(qr);
    ResultSet rs = con.getRs();
    if (rs.next())
    {
        if (rs.getString("estado").equals("En proceso"))
        {
            estadoAprobado=false;
        }
    }
    else 
    {
        response.sendRedirect("PrincipalDocumentos.jsp?erno=2");
    }
%>

        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 30px 15px;}
            .col-s a{height: 76px;}
            .col-small{padding-top: 20%; width: 150px;}
            .col-p{background-color: #7b34a5;}
            
       .wrapper{
                position: relative;
                float: left;
                left: 12%;
                width: 616px;
                margin-bottom: 4px;
                background-color: #ffffff
        }
        .left1{
           position: relative;
           float: left;
           left: 2px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left2{
           position: relative;
           float: left;
           left: 6px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left3{
           position: relative;
           float: left;
           left: 10px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left4{
           position: relative;
           float: left;
           left: 14px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }

        .left1:hover { background-color: gray;  }
        .left2:hover { background-color: gray;  }
        .left3:hover { background-color: gray;  }
        .left4:hover { background-color: gray;  }
</style>

<sql:query dataSource="jdbc/mysql" var="tipoPQuery">
    <c:if test="${!empty param.codigoPrestamo}">
        SELECT refinanciado FROM prestamos WHERE codigoPrestamo = '${param.codigoPrestamo}'
    </c:if>
    <c:if test="${empty param.codigoPrestamo}">
        SELECT refinanciado FROM prestamos WHERE codigoPrestamo = ' '
    </c:if>
</sql:query>
<c:if test="${empty param.codigoPrestamo}">
    <c:set var="estado" value="" />
</c:if>
<c:if test="${!empty param.codigoPrestamo}">
    <c:forEach var="info" items="${tipoPQuery.rows}">
        <c:set var="estado" value="${info.refinanciado}" />
    </c:forEach>
</c:if>             
        <input type="hidden" id="estado" value="${estado}" />
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Reportes</li>
           </ol>
                <h2>Impresi&oacute;n de Documentaci&oacute;n</h2><br /><br />
            Documentos hasta la fecha:     
            <input class="form-control requerido" style="width: 200px;" type="date" name="fecha" id="fecha" pattern="^[0-9]{8}-[0-9]{1}$" />
            <br/>
            <br/>
            <div class="wrapper">
	     <div class="left1" onclick="llamarReporte_re('<%=ruta %>');">
                <br />Resoluci&oacute;n del Pr&eacute;stamo <br />
             </div>
               
	     <div class="left2" onclick="llamarReporte('<%=ruta%>');">
                 <br />Solicitud de Pr&eacute;stamo
	     </div>
             <%
                 if(estadoAprobado)
                 {
                     %>
                     <div class="left3" onclick="llamarReporteMutuo('<%=ruta%>');">
                        <br />Generar Mutuo
                     </div>
                     <%
                 }
             %>
             
             <div class="left4" onclick="llamarTabla('<%=ruta%>');">
                 <br />Tabla Amortizaci&oacute;n
	     </div>
	   </div> 
    </div>
<%@include file="../../administracion/footer.jsp" %>
<script>
  function llamar(dir)
    {var win = window.open(dir, '_blank'); win.focus();}
 
  function llamarReporte_re(ruta)
  {
      
      if($('#estado').val()=="Refinanciado"){
          llamar('<%="../../prestamos/resolucionPrestamo.jsp?exito=in&codigo=" + request.getParameter("codigoPrestamo") +"&r=Refinanciado"%>'+'&fecha='+$('#fecha').val());
      }else{
          window.open(ruta + '/ReporteResolucionPrestamo_reimpresion','','resizable=yes,scrollbars=yes,height=400,width=800');
      }
  }  
    
  function llamarReporte(ruta)
  {
      window.open(ruta + '/reporteSolicitud_fiadores','','resizable=yes,scrollbars=yes,height=400,width=800');
  }
  
  function llamarReporteMutuo(ruta)
  {
     window.open(ruta + '/GenerarMutuo','','resizable=yes,scrollbars=yes,height=400,width=800');
  }
  function llamarTabla(ruta)
  {
     window.open(ruta + '/ReporteTablaAmortizacionIdel?tabla=s','','resizable=yes,scrollbars=yes,height=400,width=800');
  }
  
</script>