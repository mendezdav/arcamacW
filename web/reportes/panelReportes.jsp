<%-- 
    Document   : panelPrestamos
    Created on : 18-oct-2013, 0:21:06
    Author     : Pochii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<script language="javascript" src="../js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 30px 15px;}
            .col-s a{height: 76px;}
            .col-small{padding-top: 20%; width: 150px;}
            .col-p{background-color: #7b34a5;}
            
       .wrapper{
                position: relative;
                float: left;
                left: 10%;
                width: 616px;
                margin-bottom: 4px;
                background-color: #ffffff
        }
        .left1{
           position: relative;
           float: left;
           left: 2px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left2{
           position: relative;
           float: left;
           left: 6px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left3{
           position: relative;
           float: left;
           left: 10px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left4{
           position: relative;
           float: left;
           left: 14px;
           width: 150px;
           height: 150px;
           background-color: #03a0fa;
           text-align: center;
           color: white;
           padding-top: 6%;
        }
        .left1:hover { background-color: gray;  }
        .left2:hover { background-color: gray;  }
        .left3:hover { background-color: gray;  }
        .left4:hover { background-color: gray;  }

</style>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Reportes</li>
           </ol>
           <h2>Panel principal de reportes</h2><br /><br />
           <div class="wrapper">
	     <div class="left1" onclick="DialogoConBoton(event, 'Saldos Rojos','caja_mora','box_mora');">
	     <br />Reporte de Saldos <br />Rojos
             </div>
	     <div class="left2" onclick="llamar('EstadoCuenta/PrincipalEstadoCuenta.jsp');">
             <br />Estado de cuenta
	     </div>
               <div class="left3" onclick="DialogoConBoton(event, 'Reporte Poco Movimiento','caja_pocoMovimientos','box_pocoMovimientos');">
                 <i class="fa fa-spinner fa-2x"></i><br />Reporte de pr&eacute;stamos <br/>con poco movimiento
	     </div>
               <div class="left4" onclick="llamar('pagosRealizados/principalPagosRealizados.jsp');">
               <br />Pagos Realizados
             </div>
	   </div> 
           <div class="wrapper">
	     <div class="left1" onclick="llamar('pagoCuota.jsp');">
                 <i class="fa fa-money fa-2x"></i>
                       <br />Reporte de pagos <br />pr&eacute;stamos
	     </div>
             <div class="left2" onclick="llamar('<%=ruta%>/reportes/historialprestamos/principalhistorialprestamos.jsp');">
                 Historial de <br /> pr&eacute;stamos
	     </div>
               <div class="left3" onclick="llamar('pagoCuota.jsp');">
                 <i class="fa fa-repeat fa-2x"></i>
                       <br />Reporte de <br /> intereses cobrados
	     </div>
	     <div class="left4" onclick="llamar('<%=ruta%>/reportes/prestamosPlazo/principalPrestamosPlazo.jsp');">
                 Pr&eacute;stamos <br /> Opciones Generales
	     </div>
           </div>
             <div class="wrapper">
                <div class="left1" onclick="DialogoConBoton(event, 'Reporte BCR','caja_bcr','box_bcr');">
                    <br />Reporte BCR
                </div>
                <div class="left2" onclick="DialogoConBoton(event, 'Pr&eacute;stamos por plazo','caja_pplazo','box_pplazo');">
                    <i class="fa fa-clock-o fa-2x"></i><br /> Pr&eacute;stamos clasificados <br />por plazo
	        </div>
                <div class="left3" onclick="DialogoConBoton(event, 'Pr&eacute;stamos por plazo','caja_cierre','box_cierre');">
                    <i class="fa fa-money fa-2x"></i><br /> Cierre de <br />Saldos
	        </div>
             </div>
          </div> 
        <%@include file="cajas_reporte.jsp" %>
<%@include file="../administracion/footer.jsp" %>
<script>
    var div; //Div que contendrá el form a cargar
    function DialogoConBoton(event, header, caja, box){
            Dialog.header = header;
            Dialog.width  = "700px";
            //Asignando los valores
            //$("#consultas").val(consulta);
            //$("#hidden").val(hidden);
            div = $("#"+box).html();
            Dialog.content =  document.getElementById(caja).innerHTML;
            $("#" +caja).remove();
            Dialog.showDialog();
    }
    
    function llamar(dir)
    {window.location.href = dir;}
    
    
function fecha()
    {
    var estado = document.getElementById('completo').checked;
    if (estado == true)
    {
        //obteniendo fecha actual
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        }
        if(mm<10){
            mm='0'+mm
        } 
        var today = yyyy+'-'+mm+'-'+dd;
        document.getElementById('fechainicio').value = today;
        document.getElementById('fechafin').value = today;
    }
    else 
    {
        document.getElementById('fechaInicio').value = 'dd/mm/aaaa';
        document.getElementById('fechaFin').value = 'dd/mm/aaaa';
    }
    }
</script>