<%-- 
    Document   : principalPrestamos
    Created on : 01-17-2014, 03:49:02 PM
    Author     : Jorge Luis
--%>



<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>

<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../../js/selectAjax.js"></script>
<style media="all" type="text/css" >
    @import url("../../css/screen.css");
</style>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
  </head>
  <div class="container">
        <center>
          <h3>Listado Prestamos Vigentes</h3><p />
        
      <table>
            <form action="<%=ruta%>/ReportePrestamosVigentes" method="post"  target="_blank" >
            
                <tr>
                    <td>Fecha Inicio</td>
                    <td>Fecha Fin</td>
                </tr>
               
                 <tr>
                    <td>
                        <input type="date" name="fechaInicio" id="fechaInicio" required>
                    </td>
                    <td>
                        <input type="date" name="fechaFin" id="fechaInicio"  value="today" required>
                    </td>
                </tr>
                
                <tr><td colspan="2">
                <center><br /><br />
                    <input type="submit" class="btn btn-info" value="Ver Reporte" />
                </center>
                </td></tr>
                
            </form>
        </table>
        </center>
          
      
        <br /><br />
  </div>
      <script>
          
        //  $(document).ready( function() {
          //  $('#fechaFin').val(new Date().toDateInputValue());
           // });?
      
      </script>
  
<%@include file="../../administracion/footer.jsp" %>


