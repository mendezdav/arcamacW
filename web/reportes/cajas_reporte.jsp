 <div id="box_ppplazo" style="visibility:hidden;">
            <div class="container" id="caja_pplazo">
                <h2>Arcamac: </h2>
                <form action="<%=ruta%>/ReportePrestamosPorPlazo" method="POST" target="_blank">
                     <fieldset>
                                <legend>Reporte Prestamos clasificados por plazo</legend>
                                <table>
                                        <tr>
                                                <td>
                                                    <b>Seleccione el plazo: </b></td>
                                                <td>
                                                    <select name="meses">
                                                        <%
                                                            String me="";
                                                            for (int x= 0;x<36;x++)
                                                            {
                                                                if ((x+1) == 1)
                                                                    me = "Mes";
                                                                else
                                                                    me = "Meses";
                                                                out.print("<option value=" + (x+1)+ ">"+(x+1)+ " "+ me + "</option>");
                                                            }
                                                        %>
                                                    </select>
    
                                                </td>
                                               
                                        </tr>
                                        <tr>
                                            <td><br />
                                                <input class="btn btn-info" type="submit" value="Generar" />
                                            </td>
                                        </tr>
                                </table>
                     </fieldset>
                </form>
            </div> 
        </div>
                                                    
<!-- 
    saldos cierre
-->                                                    
 <div id="box_cierre" style="visibility:hidden;">
            <div class="container" id="caja_cierre">
                <h2> </h2>
                <form action="<%=ruta%>/ReporteSaldosCierre" method="POST" target="_blank">
                     <fieldset>
                                <legend>Cierre de Saldos</legend>
                           <div align="center">

                            <table class="tablita">
 <!--
                                <tr>
                                <td colspan="4">
                                    <input type="checkbox" name="completo" id="completo" value="completo" onclick="fecha();" >Sin importar fecha<br>
                               </td>
                            </tr>-->
                             <tr>
                                 <td>Inicio:</td>
                                 <td>
                                     <input type="date" id="fechainicio" name="fechainicio" 
                                     onblur="validar('fechainicio');" required />
                                 </td>
                                 <td><div id='divfechainicio' class='infor'></div></td>
                             </tr>
                             <tr>
                                 <td>Fin:</td>
                                 <td>
                                     <input type="date" id="fechafin" name="fechafin" 
                                     onblur="validar('fechafin');" required />
                                 </td>
                                 <td><div id='divfechafin' class='infor'></div></td>
                             </tr>
                             <tr><td colspan="3">
                                     <br /><center><input type="submit"  class="btn btn-primary" value="Generar" /></center>
                                 </td></tr>
                         </table>
                       </div>          
                     </fieldset>
                </form>
            </div> 
        </div>


         <div id="box_pocoMovimientos" style="visibility:hidden;">
            <div class="container" id="caja_pocoMovimientos">
                <h2>Arcamac: </h2>
                <form action="<%=ruta%>/reportes/pocoMovimiento/PrincipalReporte.jsp" method="POST">
                     <fieldset>
                                <legend>Reporte Prestamos Poco Movimiento</legend>
                                <table>
                                        <tr>
                                                <td>
                                                    <b>Seleccione n&uacute;mero mes sin movimiento: </b></td>
                                                <td>
                                                    <select name="meses">
                                                        <%
                                                            String M="";
                                                            for (int x= 0;x<24;x++)
                                                            {
                                                                if ((x+1) == 1)
                                                                    M = "Mes";
                                                                else
                                                                    M = "Meses";
                                                                out.print("<option value=" + (x+1)+ ">"+(x+1)+ " "+ M + "</option>");
                                                            }
                                                        %>
                                                    </select>
                                                    
                                                </td>
                                               
                                        </tr>
                                        <tr>
                                            <td><br />
                                                <input class="btn btn-info" type="submit" value="Generar" />
                                            </td>
                                        </tr>
                                </table>
                     </fieldset>
                </form>
            </div> 
        </div>  
                
        <div id="box_bcr" style="visibility:hidden;">
            <div class="container" id="caja_bcr">
                <h2>Arcamac: </h2>
                <form action="<%=ruta%>/reporteBCR" method="POST">
                     <fieldset>
                                <legend>Reporte banco central de reserva</legend>
                                <table>
                                        <tr>
                                                <td><b>Seleccione el mes a generar: </b></td>
                                                <td>
                                                    <input type="month" name="fechabcr" autofocus required />
                                                </td>
                                               
                                        </tr>
                                        <tr>
                                            <td>
                                                <input class="btn btn-info" type="submit" value="Generar" />
                                            </td>
                                        </tr>
                                </table>
                     </fieldset>
                </form>
            </div> 
        </div>
                     
        <div id="box_mora" style="visibility:hidden;">
            <div class="container" id="caja_mora">
                <h2>Pr&eacute;stamos con mora mayor a: </h2>
                <form action="<%=ruta%>/ReporteMora" method="POST">
                     <fieldset>
                                <legend>Meses de mora</legend>
                                <table>
                                        <tr>
                                                <td><b>Pr&eacute;stamos con mora mayor a: </b></td>
                                                <td>
                                                        <select name="no_meses" class="form-control">
                                                            <%
                                                                me="";
                                                                    for (int x= 0;x<36;x++)
                                                                    {
                                                                        if ((x+1) == 1)
                                                                            me = "Mes";
                                                                        else
                                                                            me = "Meses";
                                                                        out.print("<option value=" + (x+1)+ ">"+(x+1)+ " "+ me + "</option>");
                                                                    }
                                                            %>
                                                        </select>
                                                </td>
                                                <td> Meses</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input class="btn btn-info" type="submit" value="Generar" />
                                            </td>
                                        </tr>
                                </table>
                     </fieldset>
                </form>
            </div> 
        </div>