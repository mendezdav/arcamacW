<%-- 
    Document   : mostrarPrestamos
    Created on : 01-07-2014, 09:54:13 PM
    Author     : jorge
--%>


    <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <style media="all" type="text/css" >
        @import url("../../css/screen.css");
    </style>
        <%
            String criterio; 
            criterio = request.getParameter("criterio"); 
            if(criterio != null)
             {   
          %>
        
          <sql:query var="consulta1" dataSource="jdbc/mysql" >
              select p.codigoPrestamo,p.plazo,p.cuotas,p.cantidad, concat(s.nombre1, ' ', s.nombre2,' ', s.apellido1,' ',s.apellido2) as nombre from prestamos as p 
                inner join solicitante as s on s.idSolicitante  = p.idSolicitante;
          </sql:query>
              
          <display:table id="solicitnate" name="${consulta1.rows}" pagesize="10" export="true"  >
               <display:column title="Codigo" property="codigoPrestamo"  />
               <display:column title="Solicitante" property="nombre"  />
               <display:column title="Cantidad" property="cantidad" />
               <display:column title="Cuotas" property="cuotas" />
               <display:column title="Plazo" property="plazo" />
              
               <display:column  value="<i class='fa fa-edit'></i>Seleccionar" url="/Mantenimientos/Usuarios/ModificarUsuarios.jsp" paramId="codigoPrestamo" paramProperty="codigoPrestamo"  style="text-align:center;"/>
         
          </display:table>
              
        </ajax:displaytag>
              <%
                  }else // mostrar todos los solicitantes 
                
                    {
                %>
                      
        </ajax:displaytag>
                
         <%
                }
          %>