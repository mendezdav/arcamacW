<%-- 
    Document   : panelPrestamos
    Created on : 18-oct-2013, 0:21:06
    Author     : Pochii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<script language="javascript" src="<%=ruta%>/js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=ruta%>/css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 30px 15px;}
            .col-s a{height: 76px;}
            .col-small{padding-top: 20%; width: 150px;}
            .col-p{background-color: #7b34a5;}
        </style>
        
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Reportes</li>
           </ol>
           
                <h2>Panel principal de reportes</h2><br /><br />
            
             <div class="row">
               <div class="col-md-2 col-s">
                    <a class="btn btn-success" href="#" onclick="DialogoConBoton(event, 'Saldos Rojos','caja_mora','box_mora');"></a>
                </div>
                <div class="col-md-2 col-s">
                        <a class="btn btn-success col-p" href="EstadoCuenta/PrincipalEstadoCuenta.jsp"><br />Estado de cuenta</a>
                </div>
                
               <div class="col-md-2 col-s">
                    <a class="btn btn-success" href="#" onclick="DialogoConBoton(event, 'Reporte Poco Movimiento','caja_pocoMovimientos','box_pocoMovimientos');"><i class="fa fa-spinner fa-2x"></i>
                        <br />Reporte de pr&eacute;stamos <br/>con poco movimiento</a>
             
               </div>
                
               <div class="col-md-2 col-s">
                   <a class="btn btn-success col-s col-small col-p" href="pagosRealizados/principalPagosRealizados.jsp">Pagos Realizados</a>
               </div>
               
           </div>
           <div class="row">
               <div class="col-md-2 col-s">
                   <a class="btn btn-success" href="pagoCuota.jsp"><i class="fa fa-money fa-2x"></i>
                       <br />Reporte de pagos <br />pr&eacute;stamos</a>
               </div>
              
               
               <div class="col-md-2 col-s">
                   <a class="btn btn-success" href="pagoCuota.jsp"><i class="fa fa-repeat fa-2x"></i>
                       <br />Reporte de <br /> intereses cobrados</a>
               </div>
              
                <div class="col-md-2 col-s">
                    <a class="btn btn-success col-small" href="<%=ruta%>/reportes/prestamosPlazo/principalPrestamosPlazo.jsp"> 
                        Pr&eacute;stamos <br /> Opciones Generales</a>
                </div>
                <div class="col-md-2 col-s">
                    <a class="btn btn-success col-small" href="#" onclick="DialogoConBoton(event, 'Reporte BCR','caja_bcr','box_bcr');"> 
                        Reporte BCR</a>
                </div>
           </div>   
           <div class="row">
               <div class="col-md-2 col-s">
                   <a class="btn btn-success" href="#" onclick="DialogoConBoton(event, 'Pr&eacute;stamos por plazo','caja_pplazo','box_pplazo');"><i class="fa fa-clock-o fa-2x"></i>
                       <br /> Pr&eacute;stamos clasificados <br />por plazo</a>
               </div>
           </div>
        </div>
        <%@include file="cajas_reporte.jsp" %>
<%@include file="../administracion/footer.jsp" %>
<script>
         var div; //Div que contendrá el form a cargar
    function DialogoConBoton(event, header, caja, box){
            Dialog.header = header;
            Dialog.width  = "700px";
            //Asignando los valores
            //$("#consultas").val(consulta);
            //$("#hidden").val(hidden);
            div = $("#"+box).html();
            Dialog.content =  document.getElementById(caja).innerHTML;
            $("#" +caja).remove();
            Dialog.showDialog();
    }
    
</script>