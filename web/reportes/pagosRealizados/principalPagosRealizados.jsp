<%-- 
    Document   : principalPrestamosMostrar
    Created on : 10-15-2014, 03:35:42 PM
    Author     : Jorge Luis
--%>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<link rel="stylesheet" type="text/css" href="../../css/simple-dialog.min.css" media="screen" />
<script lang="javascript" src="../../js/selectAjax.js"></script>
<script lang="javascript" src="../../js/validacion.js"></script>
<script language="javascript" src="../../js/simple-dialog.min.js"></script>
<script lang="javascript" src="../../js/paginador/funciones.js"></script>
<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
    <link rel="stylesheet" type="text/css" href="../../css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:750px;
                align:center;
            }
            
            .simpleDialogBox{background:#fff;border:solid 1px #D3D3D3;bottom:0;box-shadow:1px 1px 4px #000;
                            height:400px;left:0;margin:auto;overflow:auto;padding:0;position:fixed;right:0;
                            top:0;width:600px;margin-top: 10%;}

             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                margin-left: 10%; 
            }
            .container h2
            {
                font-family: Century Gothic; 
            }
            legend{margin-left: 5%;}
            .spinner {
                position: fixed;
                top: 29.1%;
                left: 42.6%; padding: 10px 10px 10px 10px;
                margin-left: -50px; /* half width of the spinner gif */
                margin-top: -50px; /* half height of the spinner gif */
                text-align:center;
                z-index:1000000000;
                overflow: auto;
                opacity: 0.9;
                background-color: ghostwhite;
                width: 300px; /* width of the spinner gif */
                height: 202px; /*hight of the spinner gif +2px to fix IE8 issue */
            }
            .spinner p{text-align: justify; font-family: "Century Gothic"; margin-top: 40px; font-weight: bold;}
        </style>
  </head>
        <div class="container">
        <center>
            <h3>Pagos Realizados</h3><p />
        </center>
    <br /><br />
    <fieldset>
     <legend>Pagos Realizados entre fechas:</legend>
      <div class="input-group" style='width: 900px; float: left;'>
          <center>
         
            <form action="<%=ruta%>/PagosRealizados" method="post" >
                 <table>
                <tr>
                    <td><b>Fecha Inicio:</b></td>
                    <td>
                        <input type="date"  id="fechaInicio" name="fechaInicio" required>
                    </td>
                                        
                    <td><b>Fecha Fin:</b></td>
                    <td>
                        <input type="date" id="fechaFin" name="fechaFin" required>
                    </td>
                    
                </tr>
                <tr>
                    <td><b>Tipo Pago:</b></td>
                    <td>
                        <select name="tipopago">
                            <option value="Efectivo">Efectivo</option>
                            <option value="Planilla">Planilla</option>
                            <option value="Ambos">Ambos</option>
                        </select>
                    </td>
               </tr>
               <tr><td colspan="4">
                <center><br /><br />
                    <input type="hidden" name="completo" value="completo" />
                    <input type="submit" class="btn btn-info" value="Ver Reporte" />
                </center>
                </td></tr>
            
          </table> </form>  </center>           
      </div>
   </fieldset>  

                 
        <%
        String criterio  = request.getParameter("criterio");
        String pagR = request.getParameter("pag");
        
        if (criterio == null || criterio.equals("") ) // si criterio no es nulo, colocar en cuadro busqueda
        {
        %>
        <body onload="listaproductoss('<%=criterio%>','<%=pagR%>',null,'8')">
        <%
        }
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('mostrarPagos.jsp', 'txtNombre','busqueda',<%=pagR%>,'principalPagosRealizados.jsp','8');" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
       <fieldset>
       <legend>Buscar Prestamo</legend>
        <%
            // JOptionPane.showMessageDialog(null,pagR);
             if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="C&oacute;digo o Solicitante"  
                                oninput="paginar('mostrarPagos.jsp', 'txtNombre','busqueda',1,'principalPagosRealizados.jsp','8');" />
             </div> 
             <br /><br /> <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            { 
            %>
                 <div class="input-group" style='width: 450px; float: left;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="C&oacute;digo o Solicitante"  
                                oninput="paginar('mostrarPagos.jsp', 'txtNombre','contenedor', 1,'principalPagosRealizados.jsp','8');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="contenedor">
                    </div>
             </center>
                
            <% } %>
                    
          <div id="box_aprobarAnterior" style="z-index: 100000001; opacity: 0.99; 
              zoom: 1; width: 600px; height: 400px;visibility: hidden; visibility: hidden;">
            <div class="container" id="caja_aprobarAnterior">
                <form name="frmaprobarant" id="frmAprobarAnt" method='post' >
                    <input type="hidden" id="hiddenAnterior" name="codigoPrestamo" />
                    <label><b>Seleccione el rango de fecha a considerar</b></label>
                    <br />
                    <label>Fechas: </label>
                    <br />
                    <table class="tablita">
                       <tr>
                           <td colspan="4">
                               <input type="checkbox" name="completo" id="completo" value="completo" onclick="fecha();" >Sin importar fecha<br>
                           </td>
                       </tr>
                        <tr>
                            <td>Inicio:</td>
                            <td>
                                <input type="date" id="fechainicio" name="fechainicio" 
                                onblur="validar('fechainicio');" required />
                            </td>
                            <td><div id='divfechainicio' class='infor'></div></td>
                        </tr>
                        <tr>
                            <td>Fin:</td>
                            <td>
                                <input type="date" id="fechafin" name="fechafin" 
                                onblur="validar('fechafin');" required />
                            </td>
                            <td><div id='divfechafin' class='infor'></div></td>
                        </tr>
                        <tr><td colspan="3">
                                <br /><center><input type="button" onclick="redireccionar(2);" class="btn btn-primary" value="Generar" /></center>
                            </td></tr>
                    </table>
                    <br />
                </form>
            </div>
        </div>
       </fieldset>
      </div>

<%@include file="../../administracion/footer.jsp" %>
<script lang="javascript" src="../../js/selectAjax.js"></script>
<script lang="javascript" src="../../js/validacion.js"></script>

<script>
    $(function(){
       //Enviamos el parametro sol de Solicitante
       buscarUsuario('mostrarLista.jsp','buscar','contenedor', 'pre'); 
    });
    function cambiarfecha(){
        alert("Fecha: " + document.getElementById("fechaAprobacionss").value);
    }
    var div; //Div que contendr� el form a cargar
    function rechazarPrestamo(codigoPrestamo, tipo){
        var msj;
        if(tipo == 1)
            msj = "�Est� seguro/a que dese eliminar este pr�stamo?\nEsta operaci�n no se puede deshacer";
        else
            msj = "Esta seguro que desea rechazar la solicitud de prestamo\nEsta operacion no se puede deshacer";
        if(confirm(msj)){
            window.location.replace("actionDeletePrestamo.jsp?codigoPrestamo="+codigoPrestamo);
        }
    }
    function DialogoConBoton(event, header, caja, box, param,hidden){
            Dialog.header = header;
            Dialog.width  = "500px";
            Dialog.height = "300px";
            $("#"+hidden).val(param);
            div = $("#"+box).html();
            Dialog.content =  document.getElementById(caja).innerHTML;
            $("#" +caja).remove();
            Dialog.showDialog();
    }
  
    function redireccionar(tipo){
      /**MODIFICACION CON AJAX***/
      var ruta;
      var codigoPrestamo;
      var fechaAprobacion = "";
      if (tipo == 1) {
        ruta = "cambiarEstado.jsp";
        codigoPrestamo = $("#hidden").val();
      }
      else{ 
        var val = sumarize('frmAprobarAnt');
        if (val){
            ruta = "/arcamacW/PagosRealizados";
            codigoPrestamo = $("#hiddenAnterior").val();
            fechainicio = $("#fechainicio").val();
            fechafin = $("#fechafin").val();
        }
        else
            alert("datos incorrectos");
        }
            $("#spinner").show();
            $.ajax({
                  data: {codigoPrestamo: codigoPrestamo, fechaAprobacion: fechaAprobacion},
                  type: "POST", 
                  url: ruta
              }
            ).done(function(){
               //codigoPrestamo = $("#hidden").val();     
               //window.location.replace("principalPrestamosMostrar.jsp?t=tipo&exito=1");
               window.location.replace("/arcamacW/PagosRealizados?codigoPrestamo="+codigoPrestamo);
            }).fail(function(){
                window.location.replace("/arcamacW/PagosRealizados?busqueda=c&codigoPrestamo="+codigoPrestamo+ "&fechaInicio="+fechainicio+ "&fechaFin="+fechafin);
            });   
        }
      
function fecha()
    {
    var estado = document.getElementById('completo').checked;
    if (estado == true)
    {
        //obteniendo fecha actual
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        }
        if(mm<10){
            mm='0'+mm
        } 
        var today = yyyy+'-'+mm+'-'+dd;
        document.getElementById('fechainicio').value = today;
        document.getElementById('fechafin').value = today;
    }
    else 
    {
        document.getElementById('fechaInicio').value = 'dd/mm/aaaa';
        document.getElementById('fechaFin').value = 'dd/mm/aaaa';
    }
    }
 </script>     
