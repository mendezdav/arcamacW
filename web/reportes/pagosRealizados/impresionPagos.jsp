<%-- 
    Document   : impresionPagos
    Created on : 03-09-2014, 09:36:08 PM
    Author     : jorge
--%>

<%@page import="clases.beanPagoEfectuados"%>
<%@page import="java.util.LinkedList"%>
<%@page import="Conexion.conexion"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="clases.beanPagosRealizados"%>
<%@page import="java.util.Calendar"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
  String tipo = "";
    // almacenando variables
    if (request.getParameter("tipopago") != null)
        tipo = request.getParameter("tipopago");
    String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
 %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Impresi&oacute;n de pagos</title>
        <script language="JavaScript" >
            function llamar(ruta)
            {
                window.location.href = ruta + "/reportes/pagosRealizados/principalPagosRealizados.jsp";
                window.open(ruta + '/PagosRealizados','','resizable=yes,scrollbars=yes,height=400,width=800');
            }
        </script>
    </head>
    <%
         String busqueda="";
         String completo ="";
         
         if (request.getParameter("busqueda") != null)
             busqueda = request.getParameter("busqueda");
         if (request.getParameter("completo") != null)
             completo = request.getParameter("completo");
         //JOptionPane.showMessageDialog(null, request.getParameter("completo"));
         
         List<Object> reports = new LinkedList<Object>();
         // creando instancia conexion
         conexion con = new conexion();
         if (busqueda.equals("c"))
         {
             String codigoPrestamo = "";
             if (request.getParameter("codigoPrestamo") != null)
             codigoPrestamo = request.getParameter("codigoPrestamo");
         
             /**Llenando los datos generales del prestamo***/
             String queryview = "SELECT *, GROUP_CONCAT(nombreFiador SEPARATOR ';') as fiadores FROM "
                                + "viewdetalleprestamo GROUP BY codigoPrestamo WHERE codigoPrestamo = '" + codigoPrestamo + "'"; 
             con.setRS(queryview);
             ResultSet rs = con.getRs();
             if(rs.next()){
                 // llenando primeros datos
                 beanPagoEfectuados pagos = new beanPagoEfectuados();
                 pagos.setCodigoPrestamo(rs.getString("codigoPrestamo"));
                 pagos.setSolicitante(rs.getString("solicitante"));
                 pagos.setCantidad(rs.getDouble("cantidad"));
                 pagos.setFecha_aprobacion(rs.getString("fechaAprobacion"));
                 pagos.setTipo_pago(rs.getString("tipoPago"));
                 pagos.setFiador(rs.getString("fiadores"));
                 pagos.setDestino_prestamo(rs.getString("destinoPrestamo"));
                 pagos.setTasa_interes(rs.getDouble("tasaInteres"));
                 pagos.setCuota(rs.getDouble("cuota"));
                 pagos.setPlazo(rs.getInt("plazo"));
                 pagos.setEstadoPrestamo(rs.getString("estadoPrestamo"));
                 reports.add(pagos);
             }
             else
                 response.sendRedirect(ruta);
         }
         if(completo.equals("completo") )
         {
         
                String tipos = "";
                String fechaInicio ="";
                String fechaFin ="";
                String tipopago = "";
                String qr = "";
                String codigoPrestamo="";
                HttpSession SessionActual = request.getSession();
                fechaInicio = request.getParameter("fechaInicio");
                fechaFin = request.getParameter("fechaFin");

                if (fechaInicio.equals(fechaFin)) // todos los pagos!
                {
                    qr = "select * from viewabonos";
                }
                else 
                {
                    qr = "select * from viewabonos where fechaPago between '"+fechaInicio+"' and '"+fechaFin+"'";
                }

                if (!tipo.equals("Ambos"))
                   qr += " and tipoPago='" + tipo + "'";

                //JOptionPane.showMessageDialog(null, qr);
                con.setRS(qr);
                ResultSet rs = con.getRs();
                if (rs.next())
                {
                    rs.previous();
                    while(rs.next())
                    {
                       // llenando bean
                       beanPagoEfectuados bean  = new beanPagoEfectuados();
                       bean.setFechaInicio(fechaInicio);
                       bean.setFechaFin(fechaFin);
                       bean.setComprobante(rs.getString("comprobante"));
                       bean.setFechaPago(rs.getString("fechaPago"));
                       bean.setFechaEstablecida(rs.getString("fechaEstablecida"));
                       bean.setCodigoPrestamo(rs.getString("codigoPrestamo"));
                       bean.setSolicitante(rs.getString("solicitante"));
                       bean.setSaldo(rs.getDouble("saldoActual"));
                       bean.setSaldoActual(rs.getDouble("saldoAnterior"));
                       bean.setAbono(rs.getDouble("monto"));
                       bean.setInteresMoratorio(rs.getDouble("pago_interes_moratorio"));
                       bean.setInteresNormal(rs.getDouble("pago_interes"));
                       bean.setCapital(rs.getDouble("pago_capital"));
                       bean.setLogo(getServletContext().getRealPath("images/logoFactura.jpg"));
                       reports.add((Object) bean); 
                   }
                       SessionActual.setAttribute("reporte", reports);
                %>
                <body onload="llamar('<%=ruta%>');" >
                <%
                }
                   else
                   {
                    response.sendRedirect(ruta + "/reportes/pagosRealizados/principalPagosRealizados.jsp?erno=5");
                   }
                }
                %>
    </body>
</html>

   