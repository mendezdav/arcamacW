<%-- 
    Document   : impresionPrestamos
    Created on : 03-10-2014, 09:36:52 PM
    Author     : Jorge Luis
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="clases.BeanCreditosOtorgados"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.LinkedList"%>
<%@page import="Conexion.conexion"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reporte</title>
        <script language="JavaScript" >
            function llamar(ruta)
            {
                window.location.href = ruta + "/reportes/prestamosPlazo/principalPrestamosPlazo.jsp";
                window.open(ruta + '/ReportePrestamosPlazo','','resizable=yes,scrollbars=yes,height=400,width=800');
            }
        </script>
    </head>
         
        <%
         String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
         // tomando datos del request.
         String fechaInicio = request.getParameter("fechaInicio");
         String fechaFin = request.getParameter("fechaFin");
         // obteniendo dato del tipo de pago
         String tipopago = request.getParameter("tipopago");
         // estado del prestamos
         String estado = request.getParameter("estado");
         String completo = request.getParameter("completo");
         
        if (completo != null && completo != "")
            completo = "completo";
        else 
            completo = "rango";
        
        
          List<Object> reports = new LinkedList<Object>();
         // creando instancia conexion
         conexion con = new conexion();
         String rutaImg = getServletContext().getRealPath("images/logoFactura.jpg"); 
         
         String qr ="select concat(s.nombre1,' ', s.nombre2,' ', s.apellido1,' ', apellido2) as nombre,\n" +
                "p.codigoPrestamo,p.estado,p.plazo,p.cuotas,p.fechaVencimiento,p.tipo_pago,ifnull(p.fechaAprobacion, 'No establecida') as fechaAprobacion"
                 + ",p.cantidad,p.plazo,p.tasa_interes,p.saldo from prestamos as p inner join solicitante as s on p.idSolicitante = s.idSolicitante\n" +
                "inner join tipopago as t on t.idtipoPago = p.tipo_pago";
       
        if (completo.equals("rango"))
        {
            qr += " where p.fechaAprobacion between '"+fechaInicio+"' and '"+ fechaFin +"'";
       
            if (estado.equals("Activo"))
            qr += " and p.estado='Activo'";
        
            if (estado.equals("Pagado"))
                qr += " and p.estado='Pagado'";


            if (tipopago.equals("Planilla"))
                qr += " and p.tipo_pago=1";


            if (tipopago.equals("Efectivo"))
                qr += " and p.tipo_pago=2";
        }
        else  // completo sin importar fecha de inicio ni fin
        {
            if (estado.equals("Ambos") && tipopago.equals("Ambos")){ }
             else
                qr += " where ";
       
            if (estado.equals("Activo") && tipopago.equals("Planilla") )
                qr += " p.estado='Activo' and p.tipo_pago=1";
            
            if (estado.equals("Activo") && tipopago.equals("Efectivo") )
                qr += " p.estado='Activo' and p.tipo_pago=2";
        
            if (estado.equals("Pagado") && tipopago.equals("Planilla") )
                qr += " p.estado='Pagado'  and p.tipo_pago=1";
            
            if (estado.equals("Pagado") && tipopago.equals("Efectivo") )
                qr += " p.estado='Pagado' and p.tipo_pago=2";
            
            if (estado.equals("Ambos") && tipopago.equals("Planilla") )
                qr += " p.tipo_pago=1";
            
            if (estado.equals("Ambos") && tipopago.equals("Efectivo") )
                qr += " p.tipo_pago=2";
            
            if (tipopago.equals("Ambos") && estado.equals("Pagado") )
                qr += " p.estado='Pagado'";
            
            if (tipopago.equals("Ambos") && estado.equals("Activo") )
                qr += " p.estado='Activo'";
            
        }
        
         //instancia al javaBean
         // realizacion de consulta
         System.out.print(qr);
         con.setRS(qr);
         ResultSet resultado = (ResultSet) con.getRs();
         double saldo = 0.00;
         double cuotas = 0.00;
         
         // validar que tenga al menos un registro el reporte
         int contador=0;
         String codigoPrestamo="", qrAux="";
         ResultSet rsAux = null;
         conexion con2 = new conexion();
         while (resultado.next())
         {
             contador++;
             saldo = resultado.getDouble("saldo");
             BeanCreditosOtorgados creditos = new BeanCreditosOtorgados();
             // llenando datos
             codigoPrestamo = resultado.getString("codigoPrestamo");
             creditos.setCodigoPrestamo(codigoPrestamo);
             cuotas = resultado.getDouble("cuotas");
             cuotas = Math.rint(cuotas*100) / 100;
             creditos.setCuota(cuotas);
            
             
             if(fechaInicio.equals(fechaFin))
             {
                  creditos.setFechaFin(fechaFin);
                  creditos.setFechaInicio("---");
             }
             else 
             {
                  creditos.setFechaFin(fechaFin);
                  creditos.setFechaInicio(fechaInicio);
             }
             creditos.setFechaOtorgamiento(resultado.getString("fechaAprobacion"));
             creditos.setFechaVencimiento(resultado.getString("fechaVencimiento"));
             creditos.setInteresNormal(resultado.getDouble("tasa_interes"));
             creditos.setMontoPrestamo(resultado.getDouble("cantidad"));
             creditos.setPlazo(resultado.getInt("plazo"));
             creditos.setSolicitante(resultado.getString("nombre"));
             creditos.setEstado(resultado.getString("estado"));
             creditos.setPlazo(resultado.getInt("plazo"));
             creditos.setImg(rutaImg);
             // no queria hacer esto, pero fue necesario
             qrAux = "select diasInteres,diasInteresMora,iva,intereses,interes_moratorio "
                     + "from pagos where codigoPrestamo='"+ codigoPrestamo+"' "
                     + "and estado !='Pagado' limit 1";
             
             con2.setRS(qrAux);
             rsAux = con2.getRs();
             
             while(rsAux.next())
             {
                creditos.setDiasInteres(rsAux.getInt("diasInteres"));
                creditos.setDiasInteresMora(rsAux.getInt("diasInteresMora"));
                // total 
                creditos.setInteresNormal_total(rsAux.getDouble("intereses"));
                creditos.setInteressMora_total(rsAux.getDouble("interes_moratorio"));
                creditos.setIva(rsAux.getDouble("iva")); 
             }
             qrAux = "select count(*) as cancelados from pagos where codigoPrestamo='"+ codigoPrestamo+"' and estado ='Pagado'";
             con2.setRS(qrAux);
             rsAux = con2.getRs();
             while(rsAux.next())
             {
                creditos.setCuotasCancelados(rsAux.getInt("cancelados"));
             }
             if (saldo < 0.00)
                 creditos.setSaldoAfecha(0.00);
             else
                 creditos.setSaldoAfecha(resultado.getDouble("saldo"));
             reports.add((Object) creditos); 
         }
         if (contador != 0)
         {
             HttpSession SessionActual = request.getSession();
             SessionActual.setAttribute("reportesss", reports);
            // response.sendRedirect(ruta + "/ReportePrestamosPlazo");
             %>
               <body onload="llamar('<%=ruta%>');" >
             <%
         }
         else 
            response.sendRedirect(ruta + "/reportes/prestamosPlazo/principalPrestamosPlazo.jsp?erno=5");
       %>
</html>
 <script language="JavaScript" >
        function imprimir(ruta)
        {
            var x = ruta + '/ReportePrestamosPlazo';
            alert(x);
            window.open(x,resizable=yes,scrollbars=yes,height=400,width=800);
  //          window.history.back(-1);
        }
    </script>