<%-- 
    Document   : principalPrestamos
    Created on : 01-17-2014, 03:49:02 PM
    Author     : Jorge Luis
--%>

<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../../js/selectAjax.js"></script>
<style media="all" type="text/css" >
    @import url("../../css/screen.css");
</style>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
    
  </head>
  <div class="container">
        <center>
          <h3>Opciones Reporte Prestamos</h3><p />
        
      <table>
            <form action="impresionPrestamos.jsp" method="post" >
                <tr>
                    <td colspan="4">
                        <input type="checkbox" name="completo" id="completo" value="completo" onclick="fecha();" >Sin importar fecha<br>
                    </td>
                </tr>
                <tr>
                    <td><b>Fecha Inicio:</b></td>
                    <td>
                        <input type="date"  id="fechaInicio" name="fechaInicio" required>
                    </td>
                                        
                    <td><b>Fecha Fin:</b></td>
                    <td>
                        <input type="date" id="fechaFin" name="fechaFin"  value="today" required>
                    </td>
                    
                </tr>
                <tr>
                    <td><b>Tipo Pago:</b></td>
                    <td>
                        <select name="tipopago">
                            <option value="Efectivo">Efectivo</option>
                            <option value="Planilla">Planilla</option>
                            <option value="Ambos">Ambos</option>
                        </select>
                    </td>
                    
                     <td><b>Estado:</b></td>
                    <td>
                        <select name="estado">
                            <option value="Activo">Activo</option>
                            <option value="Pagado">Pagado</option>
                            <option value="Ambos">Ambos</option>
                        </select>
                    </td>
                </tr>
                
                <tr><td colspan="4">
                <center><br /><br />
                    <input type="submit" class="btn btn-info" value="Ver Reporte" />
                </center>
                </td></tr>
                
            </form>
        </table>
           
        </center>
          
      
        <br /><br />
  </div>
<script language="javascript">
  function fecha()
    {
    
    var estado = document.getElementById('completo').checked;
    if (estado == true)
    {
        //obteniendo fecha actual
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd
        }
        if(mm<10){
            mm='0'+mm
        } 
        var today = yyyy+'-'+mm+'-'+dd;
        document.getElementById('fechaInicio').value = today;
        document.getElementById('fechaFin').value = today;
    }
    else 
    {
        document.getElementById('fechaInicio').value = 'dd/mm/aaaa';
        document.getElementById('fechaFin').value = 'dd/mm/aaaa';
    }
    }
    </script>

<%@include file="../../administracion/footer.jsp" %>


