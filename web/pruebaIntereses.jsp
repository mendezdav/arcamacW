<%-- 
    Document   : pruebaIntereses
    Created on : 16-abr-2014, 21:31:29
    Author     : Pochii
--%>
<%@page import="clases.nuevoPrestamo, Conexion.conexion, clases.diferenciaDias, java.sql.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PRUEBA INTERESES</title>
    </head>
    <body>
        <h1>PAGOS</h1>
        <div class="container">
        <center>
          <h3>Pago de Cuotas</h3><p />
        </center>
        <br /><br />
        <%
            String codigoPrestamo = request.getParameter("codigoPrestamo");
            //SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
            int contador=0;
            if (codigoPrestamo != "" && codigoPrestamo != null)
            {
                String qr ="select p.codigoPrestamo,concat(s.nombre1,' ', s.nombre2,' ',s.apellido1,' ',s.apellido2) as nombre,"
                        + " t.tipoPago from prestamos as p inner join solicitante as s on s.idSolicitante = p.idSolicitante " 
                        + "inner join tipopago as t on t.idtipopago = p.tipo_pago where p.codigoPrestamo='"+codigoPrestamo+"';";
             //   SessionActual.setAttribute("codigoPrestamo", codigoPrestamo);
                conexion con = new conexion();
                con.setRS(qr);
                ResultSet datosGenerales = (ResultSet) con.getRs();
                if (datosGenerales.next())
                {                                      
        %>
        <center>
        <table>
            <tr>
                <td><b>C&oacute;digo Prestamo: </b><br /><br /></td>
                <td><%= datosGenerales.getString("codigoPrestamo") %><br /><br /></td>
            </tr>
            
            <tr>
                <td><b>Solicitante: </b><br /><br /></td>
                <td><%= datosGenerales.getString("nombre") %><br /><br /></td>
            </tr>
            
            <tr>
                <td><b>Tipo Pago: </b><br /><br /></td>
                <td><%= datosGenerales.getString("tipoPago") %><br /><br /></td>
            </tr>
        </table></center>
    <br /><br />
    <a href="ReporteTablaAmortizacion">Tabla Amortizacion Prueba</a>
    <h3>Pagos</h3>
    <br /><center>
        
          <sql:query var="consulta1" dataSource="jdbc/mysql" >
            select idPago,codigoPrestamo,comprobante,fechaEstablecida,estado,monto from pagos where codigoPrestamo='<%=codigoPrestamo%>';
          </sql:query>
              
          <display:table id="pagos" name="${consulta1.rows}" pagesize="24" export="true"  >
               <display:column title="C&oacute;digo" property="codigoPrestamo"  />
               <display:column title="Comprobante" property="comprobante"  />
               <display:column title="Fecha Vencimiento" property="fechaEstablecida" />
               <display:column title="Estado" property="estado" />
               <c:if test="${pagos.estado == 'Pagado'}">
                    <display:column  value="<i class='fa fa-edit'></i>Reimprimir" url="/Pagos/realizarPago.jsp" paramId="idPago" paramProperty="idPago"   style="text-align:center;"/>
               </c:if>
               
               <c:if test="${pagos.estado != 'Pagado'}">
                   <% contador++; 
                    if (contador <= 1){
                   %>
                    <display:column  value="<i class='fa fa-edit'></i>Pagar" url="/Pagos/realizarPago.jsp" paramId="idPago" paramProperty="idPago"  style="text-align:center;"/>
                    <% }      
                     %>
               </c:if>
          </display:table>
          
        </ajax:displaytag>
        </center>
      
        <%   } // fin if  hay datos
            } // fin if si no es nulo ni vacio
         %>
        </div>
    </body>
</html>
