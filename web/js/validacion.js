function validar(id)
    {
        var elemento = document.getElementById(id);
        var div      = document.getElementById("div"+id);
        if(elemento.checkValidity())
            div.innerHTML = "<i class='fa fa-check'></i>";
        else
            div.innerHTML = "<i class='fa fa-times'></i>";
    }
/**
 * 
 * @param {string} id_control            id del control que contiene los id de los departamentos
 * @param {string} id_control_respuesta  id del control donde se desea colocar la respuesta
 * @param {string} ruta                  Ruta del archivo   
 * @param {string} criterio              El criterio a utilizar en una clausula where
 * @returns {undefined}
 */
function cargar_municipio(id_control, id_control_respuesta, ruta, condicion){
    var id_departamento = $("#" + id_control).val();          //Tomando el id del prestamo
    $.ajax({
            data: {id_departamento: id_departamento, criterio: condicion},
            type: "POST", 
            url: ruta
        }
      ).done(function(respuesta){
         $("#"+id_control_respuesta).html(respuesta);	//Cargando resultado en div
      }).fail(function(){
          $("#"+id_control_respuesta).html("<p>Hubo un error</p>");
      });
}
    function sumarize(form)
    {
        var frm = document.getElementById(form);
        var val = true;
        for(i= 0; i < frm.elements.length; i++)
        {
            if(frm.elements[i].type == "text" || frm.elements[i].type == "textarea" || frm.elements[i].type == "date")
            {
                //Si no esta validado
                if(!frm.elements[i].checkValidity())
                {
                    frm.elements[i].title = "Por favor ingrese la informacion de este campo";
                    val = false;
                    break;
                }
            }
        }
        return val;
    }
/*----------Funcion para obtener la edad------------*/
    /**
     * @params id    campo que contiene la fecha de nacimiento 
     *         campo campo en el cual se mostrara el resultado
     */
    function calcular_edad(id, campo) {
        var fechaActual = new Date(); var diaActual = fechaActual.getDate();var mmActual = fechaActual.getMonth() + 1;
        var fecha = $("#"+id).val();var yyyyActual = fechaActual.getFullYear();FechaNac = fecha.split("-");var diaCumple = FechaNac[2];var mmCumple = FechaNac[1];var yyyyCumple = FechaNac[0];//retiramos el primer cero de la izquierda
        if (mmCumple.substr(0,1) == 0) {
        mmCumple= mmCumple.substring(1, 2);
        }
        //retiramos el primer cero de la izquierda
        if (diaCumple.substr(0, 1) == 0) {
        diaCumple = diaCumple.substring(1, 2);
        }
        var edad = yyyyActual - yyyyCumple;

        //validamos si el mes de cumpleaños es menor al actual
        //o si el mes de cumpleaños es igual al actual
        //y el dia actual es menor al del nacimiento
        //De ser asi, se resta un año
        if ((mmActual < mmCumple) || (mmActual == mmCumple && diaActual < diaCumple)) {
        edad--;
        }
        $("#"+campo).val(edad);
    };
//Funcion para actualizar el valor de un submit
function upload(form, ruta){
        document.getElementById(form).action = ruta;
        document.getElementById(form).submit();
    }
