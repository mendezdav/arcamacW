/* test */
var pagos = angular.module('pagosApp',[]);

pagos.controller('controladorPagos',function($scope, $http){
    $scope.pagosPendientes = [];
    $scope.n_pendientes = 0;
    $scope.n_abonados = 0;
    $scope.codigoPrestamo = "";
    $scope.pago = 0.0;
    $scope.intereses_ordinarios = 0.0;
    $scope.iva = 0.0;
    $scope.mora = 0.0;
    $scope.iva_mora = 0.0;
    $scope.capital = 0.0;
    $scope.total = 0.0;
    $scope.nuevo_saldo = 0.0;
    
    $scope.a_intereses_ordinarios = 0.0;
    $scope.a_iva = 0.0;
    $scope.a_mora = 0.0;
    $scope.a_iva_mora = 0.0;
    $scope.a_capital = 0.0;
    $scope.a_total = 0.0;
    $scope.a_nuevo_saldo = 0.0;
    $scope.n_monto = 0.0;
    
    $scope.p_intereses_ordinarios = 0.0;
    $scope.p_iva = 0.0;
    $scope.p_mora = 0.0;
    $scope.p_iva_mora = 0.0;
    $scope.p_capital = 0.0;
    $scope.p_total = 0.0;
    $scope.p_nuevo_saldo = 0.0;
    
    
    
    $scope.obtenerPagosPendientes = function(){
        var data = {};
        var url = "obtenerPagosPendientes.jsp?codigoPrestamo="+$scope.codigoPrestamo;
        
        $http.post(url,data).success(function(resp){
            var cont = 0;
            for(var i = 0; i < (resp.length-1);i++){
                $scope.pagosPendientes.push(resp[i]);
                cont++;
            }
            $scope.n_pendientes = cont;
        });
        
    };
    
    $scope.verificacionDePago = function(){
        if(isNaN($scope.pago)){
            $scope.pago = "";
            $scope.p_intereses_ordinarios = 0.0;
            $scope.p_iva = 0.0;
            $scope.p_mora = 0.0;
            $scope.p_iva_mora = 0.0;
            $scope.p_capital = 0.0;
            $scope.p_total = 0.0;
            $scope.p_nuevo_saldo = 0.0;
        }else{
            var monto = $scope.pago;
            var consumido = monto;
            
            if(monto >= ($scope.mora-$scope.a_mora)){
                $scope.p_mora = ($scope.mora-$scope.a_mora);
                monto -= ($scope.mora-$scope.a_mora);
            }else{
                $scope.p_mora = monto;
                monto = 0;
             }
            if(monto >= ($scope.iva_mora-$scope.a_iva_mora)){
                 $scope.p_iva_mora = ($scope.iva_mora-$scope.a_iva_mora);
                 monto -= ($scope.iva_mora-$scope.a_iva_mora);
            }else{
                 $scope.p_iva_mora = monto;
                 monto = 0;
            }
             
            if(monto >= ($scope.intereses_ordinarios - $scope.a_intereses_ordinarios)){
                $scope.p_intereses_ordinarios = ($scope.intereses_ordinarios - $scope.a_intereses_ordinarios);
                monto -= ($scope.intereses_ordinarios - $scope.a_intereses_ordinarios);
            }else{
                $scope.p_intereses_ordinarios = monto;
                monto = 0;
            }
            if(monto >= ($scope.iva - $scope.a_iva)){
                $scope.p_iva = ($scope.iva - $scope.a_iva);
                monto -= ($scope.iva - $scope.a_iva);
            }else{
                $scope.p_iva = monto;
                monto = 0;
            }
             if(monto >= ($scope.capital-$scope.a_capital)){
                 $scope.p_capital = ($scope.capital-$scope.a_capital);
                 monto -= ($scope.capital-$scope.a_capital);
             }else{
                 $scope.p_capital = monto;
                 monto = 0;
             }
             
             $scope.n_abonados = 1;
             consumido = consumido - monto;   
             $scope.p_total = consumido;
             $scope.p_nuevo_saldo = $scope.nuevo_saldo - $scope.p_capital + $scope.capital - $scope.a_capital;
             $scope.n_monto = monto;
             
             if(monto > 0){
                 var indice = 1;
                 while(monto > 0){
                    var interesPendiente = $scope.pagosPendientes[indice].intereses - $scope.pagosPendientes[indice].pago_interes; 
                    var interesMoratorioPendiente = $scope.pagosPendientes[indice].interes_moratorio - $scope.pagosPendientes[indice].pago_interes_moratorio;
                    var capitalPendiente = $scope.pagosPendientes[indice].capital - $scope.pagosPendientes[indice].pago_capital; 
                    
                    if(monto >= interesPendiente){
                        monto = monto - interesPendiente;
                    }else{
                        monto = 0;
                    }
                    
                    if(monto >= interesMoratorioPendiente){
                        monto = monto - interesMoratorioPendiente;
                    }else{
                        monto = 0;
                    }
                    
                    if(monto >= capitalPendiente){
                        monto = monto - capitalPendiente;
                    }else{
                        monto = 0;
                    }
                    
                    indice++;
                    $scope.n_abonados++;
                 }
             }
        }
    };
});
