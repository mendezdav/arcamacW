/* test */
var cobrosApp = angular.module('cobrosApp',[]);

cobrosApp.controller('controladorCobros',function($scope, $http){
    
    $scope.formatearFecha = function(dia, mes, anio){
        var d = dia;
        if(dia < 10){ d = "0"+dia; }
        var m = mes;
        if(m < 10){ m = "0"+mes; }
        return anio + "-" + m + "-" + d;
    };
    
    $scope.reload = function(){
        location.href = location.href;
    }
    
    $scope.nuevoPago = function(){
        $('#mnt').removeAttr('disabled');
        
        var uri = 'obtenerNumeroComprobante.jsp';
        
        var myData = { 
            codigoPrestamo: $scope.no_prestamo
        };
        
        $http.post(uri, myData, {
            headers : {
                "Content-Type" : 'application/x-www-form-urlencoded;charset=utf-8'
            },
            transformRequest : [function(data) {
                return angular.isObject(data)
                    ? jQuery.param(data)
                    : data;
            }]
        }).success(function(data, status, headers, config){
            $scope.comprobante = data.noAbono;
        });
    }
    
    $scope.realizarPago = function(){
        if($scope.monto <= 0){
            alert("*** ERROR AL PROCESAR EL PAGO*** \n\nNo se pueden procesar pagos por la cantidad de: \nCERO DOLARES");
        }else{
            if(confirm("Confirmacion:\n\n Desea proceder a efecutar el cobro?")){
                
                var informacionDePago = {
                    codigoPrestamo : $scope.no_prestamo,
                    montoAbonado : $scope.monto,
                    fechaA: $scope.fecha_pago
                };
                
                var uri = "efectuarPago.jsp";
                
                $http.post(uri, informacionDePago, {
                    headers : {
                        "Content-Type" : 'application/x-www-form-urlencoded;charset=utf-8'
                    },
                    transformRequest : [function(data) {
                        return angular.isObject(data)
                            ? jQuery.param(data)
                            : data;
                    }]
                }).success(function(data, status, headers, config){
                    
                    if(data.success){
                        alert("Abono realizado correctamente \n\n Comprobante no. "+data.comprobante);
                        location.href = location.href;
                    }else{
                        alert("Error: \n"+data.serverMsg);
                    }
                });
            }
        }
    }
    
    $scope.validarMonto = function(){
        if(isNaN($scope.monto)){
            $scope.monto = "0.00";
            $scope.pago.capital = 0;
            $scope.pago.interes_normal = 0;
            $scope.pago.mora = 0;
            $scope.pago.iva = 0;
        }else{
            if($scope.monto >= 0){
                
                var montoAplicable = $scope.monto;
                
                if(montoAplicable >= $scope.saldo_anterior.iva){
                    $scope.pago.iva = $scope.saldo_anterior.iva;
                    montoAplicable -= $scope.saldo_anterior.iva;
                }else{
                    $scope.pago.iva = montoAplicable;
                    montoAplicable = 0;
                }
                
                if(montoAplicable >= $scope.saldo_anterior.mora){
                    $scope.pago.mora = $scope.saldo_anterior.mora;
                    montoAplicable -= $scope.saldo_anterior.mora;
                }else{
                    $scope.pago.mora = montoAplicable;
                    montoAplicable = 0;
                }
                
                if(montoAplicable >= $scope.saldo_anterior.interes_normal){
                    $scope.pago.interes_normal = $scope.saldo_anterior.interes_normal;
                    montoAplicable -= $scope.saldo_anterior.interes_normal;
                }else{
                    $scope.pago.interes_normal = montoAplicable;
                    montoAplicable = 0;
                }
                
                if(montoAplicable >= $scope.saldo_anterior.capital){
                    $scope.monto -= (montoAplicable - $scope.saldo_anterior.capital);
                    $scope.pago.capital = $scope.saldo_anterior.capital;
                    $scope.nuevo_saldo.capital = 0;
                }else{
                    $scope.pago.capital = montoAplicable;
                    montoAplicable = 0;
                    $scope.nuevo_saldo.capital = $scope.saldo_anterior.capital - $scope.pago.capital;
                }
                
            }else{
                $scope.monto = "0.00";
                $scope.pago.capital = 0;
                $scope.pago.interes_normal = 0;
                $scope.pago.mora = 0;
                $scope.pago.iva = 0;
            }
        }
    }
    
    $scope.cargarDatosGeneralesPrestamo = function(){
        var uri = 'cargarDatosGeneralesPrestamo.jsp';
        
        var myData = { 
            codigoPrestamo: $scope.no_prestamo
        };
        
        $http.post(uri, myData, {
            headers : {
                "Content-Type" : 'application/x-www-form-urlencoded;charset=utf-8'
            },
            transformRequest : [function(data) {
                return angular.isObject(data)
                    ? jQuery.param(data)
                    : data;
            }]
        }).success(function(data, status, headers, config){
            data.splice(data.length - 1,1);
            $scope.nombre = data[0].nombreSolicitante;
            $scope.fecha_otorgamiento = data[0].fechaEmision;
            $scope.fecha_vencimiento = data[0].fechaVencimiento;
            $scope.tipo = data[0].tipoPago;
            $scope.fecha_ultimo_pago = data[0].fechaUltimoPago;
            $scope.monto_ultimo_pago = data[0].montoUltimoPago;
            $scope.cuota = data[0].cuota;
            
            //$scope.validarMonto();
        });
        
    };
    
    $scope.cargarDatosDePago = function(){
        var uri = 'cargarDatosDePago.jsp';
        
        var myData = { 
            codigoPrestamo: $scope.no_prestamo
        };
        
        $http.post(uri, myData, {
            headers : {
                "Content-Type" : 'application/x-www-form-urlencoded;charset=utf-8'
            },
            transformRequest : [function(data) {
                return angular.isObject(data)
                    ? jQuery.param(data)
                    : data;
            }]
        }).success(function(data, status, headers, config){
            data.splice(data.length - 1,1);
            $scope.saldo_anterior.capital = data[0].capital;
            $scope.saldo_anterior.interes_normal = data[0].interes;
            $scope.saldo_anterior.mora = data[0].interes_moratorio;
            $scope.saldo_anterior.iva = data[0].iva;
        });
    }
    
    $scope.comprobante = 0;
    $scope.fecha_pago = " - - ";
    $scope.fecha_otorgamiento = " - - ";
    $scope.fecha_vencimiento = " - - ";
    $scope.fecha_ultimo_pago = " - - ";
    $scope.monto_ultimo_pago = 0;
    $scope.nombre = "";
    $scope.monto = "0.00";
    $scope.no_prestamo = 0;
    $scope.tipo = 1;
    $scope.cuota = 0;
    
    var f = new Date();
    $scope.fecha_pago = $scope.formatearFecha(f.getDate(), (f.getMonth() +1) , f.getFullYear());
    
    $scope.saldo_anterior = {
        "capital" : 0,
        "interes_normal": 0,
        "mora":0,
        "iva":0
    };
    $scope.pago = {
        "capital" : 0,
        "interes_normal": 0,
        "mora":0,
        "iva":0
    };
    $scope.nuevo_saldo = {
        "capital" : 0
    };
    
    $scope.$watch('no_prestamo', function() {
        
        $scope.cargarDatosGeneralesPrestamo();
        $scope.cargarDatosDePago();
    });
    
});
