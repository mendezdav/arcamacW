
var peticion = false;
var  testPasado = false;

try {
  peticion = new XMLHttpRequest();
  } 
  catch (trymicrosoft) 
  {
    try {
        peticion = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (othermicrosoft) 
  {
        try {
            peticion = new ActiveXObject("Microsoft.XMLHTTP");
        } 
        catch (failed) 
        {
          peticion = false;
        }
  }
}

if (!peticion)
    alert("ERROR AL INICIALIZAR!");


function cargarCombo(url, comboPrincipal, id)
{
   
    var elemento = document.getElementById(id);    
    var valorDepende = document.getElementById(comboPrincipal);
    
    var x = valorDepende.value;
    
    
    var url1 = url + '?Id=' + x;
    elemento.innerHTML = '<img src="../img/5-0.gif" />';
    
    
    peticion.open("GET", url1);

    peticion.onreadystatechange = function() 
                                {
                                    if(peticion.readyState == 4)
                                    {
                                        elemento.innerHTML = peticion.responseText;
                                    }
        
                                 } //fin funcion
    
                                 peticion.send(null);
}
    
function buscarUsuario(url,cajaPrincipal,id, tipo)
{
    var elemento = document.getElementById(id);
    var valorP = document.getElementById(cajaPrincipal);
    var y = valorP.value;
        var url1 = url + "?criterio=" + y + "&tipo=" + tipo; 
        elemento.innerHTML = "<i class='fa fa-spinner fa-spin'></i>";
        peticion.open("GET", url1);
        peticion.onreadystatechange = function() 
                                {
                                    if(peticion.readyState == 4)
                                    {
                                        elemento.innerHTML = peticion.responseText;
                                    }
        
                                 } //fin funcion
                                 peticion.send(null);
}

function buscarFiador(url,cajaPrincipal,id)
{
    var elemento = document.getElementById(id);
    var valorP = document.getElementById(cajaPrincipal);
    var y = valorP.value;
    var url1 = url + "?criterio=" + y;
    peticion.open("GET", url1);
    peticion.onreadystatechange = function() 
                                {
                                    if(peticion.readyState == 4)
                                    {
                                        elemento.innerHTML = peticion.responseText;
                                    }
        
                                 } //fin funcion
                                 peticion.send(null);
}



function buscarPrestamos(url,cajaPrincipal,id, tipo)
{
    var elemento = document.getElementById(id);
    var valorP = document.getElementById(cajaPrincipal);
    
    var y = valorP.value;
    if ( y != "")
    {
        var url1 = url + "&criterio=" + y + "&tipo=" + tipo;
        elemento.innerHTML = "<i class='fa fa-spinner fa-spin'></i>";
        peticion.open("GET", url1);
        peticion.onreadystatechange = function() 
                                    {   if(peticion.readyState == 4)
                                        {
                                            elemento.innerHTML = peticion.responseText;
                                        }
                                     } //fin funcion
                                     peticion.send(null);
    }
    else 
    {
            elemento.innerHTML = "";
    }
}

function paginar(url,cajaPrincipal,id, pagR, BaseUrl,listado)
{
    
    var elemento = document.getElementById(id);
    var valorP = document.getElementById(cajaPrincipal);
    var y = valorP.value;
    if ( y != "")
    {
        if (pagR == null)
            pagR = 1;
        
        var url1 = url + "?criterio=" + y + "&pagR=" + pagR + "&s=1";
        //var tm2 =  BaseUrl + "?criterio=" + y + "&pagR=" + pagR;
        elemento.innerHTML = "<i class='fa fa-spinner fa-spin'></i>";
        peticion.open("GET", url1);
        peticion.onreadystatechange = function() 
        {
            if(peticion.readyState == 4)
                elemento.innerHTML = peticion.responseText;
        }
        peticion.send(null);
    }
    else{
     if (listado === "1")
     {
         peticion.open("GET", "prestamosActivos.jsp?t=1&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";   
     }
     
     if (listado === "2")
     {
         peticion.open("GET", "prestamosCancelados.jsp?t=2&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";   
     }
     
     if (listado === "3")
     {
         peticion.open("GET", "prestamosNoAprobados.jsp?t=3&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalDocumetnos, de reimpresion
     if (listado === "4")
     {
         peticion.open("GET", "listadoPrestamos.jsp?t=3&criterio=&pagR=");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalPago, principalPago
     if (listado === "5")
     {
         peticion.open("GET", "listadoPrestamos.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     // viene de principalFiadores, mantenimiento
     if (listado === "6")
     {
         
         peticion.open("GET", "mostrarFiadorMantenimiento.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     // viene de principalPrestamosEliminar
     if (listado === "7")
     {
         peticion.open("GET", "prestamosEliminar.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     if (listado === "8")
     {
         peticion.open("GET", "mostrarPagos.jsp");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
     
     if (listado === "10")
     {
         peticion.open("GET", "ListaSolicitantes.jsp?opc=noref");
         peticion.onreadystatechange = function() 
         {   if(peticion.readyState == 4)
                 elemento.innerHTML = peticion.responseText;
         }
         peticion.send(null);
     }
     else{
         elemento.innerHTML = "";
     }
   }
 }


