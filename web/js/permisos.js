var permisosApp = angular.module('permisosApp',[]);	

permisosApp.controller('permisosController', function($scope, $http){
	
        /* registro de los accesos disponibles para el usuario */
        $scope.permisos = [];
        
        /* usuario para el que se editan los permisos */
        $scope.usuario = "";
        
        /* carga el estado actual de los permisos del usuario */
	$scope.cargarPermisos = function(){
                var url = 'cargarPermisos.jsp';
		
                var data = { 
                    usuario: $scope.usuario
		};
                //alert(JSON.stringify(data));
		$http.post(url, data).
		success(function(data, status, headers, config) {
                        
                        data.splice(data.length - 1,1);
		    	$scope.permisos = data;
		    	
                });
	};

        /* concede el acceso a un recurso */
	$scope.concederAcceso = function(p_uri){
		if(confirm("Esta seguro que desea conceder este permiso?")){
			var uri = 'concederAcceso.jsp';
			
                        var myData = { 
                            usuario: $scope.usuario,
                            uri: p_uri
			};
                       
                        $http.post(uri, myData, {
                                    headers : {
                                        "Content-Type" : 'application/x-www-form-urlencoded;charset=utf-8'
                                    },
                                    transformRequest : [function(data) {
                                        return angular.isObject(data)
                                                ? jQuery.param(data)
                                                : data;
                                    }]
                                }).success(function(){
                                    $scope.cargarPermisos();
                                });

		}
	};

        /* cancela el acceso a un recurso */
	$scope.denegarAcceso = function(idAcceso){
		
		if(confirm("Esta seguro que desea cancelar este permiso?")){
			var uri = 'denegarAcceso.jsp';
			var myData = { 
                            idRec: idAcceso
			};
                        
			$http.post(uri, myData, {
                                    headers : {
                                        "Content-Type" : 'application/x-www-form-urlencoded;charset=utf-8'
                                    },
                                    transformRequest : [function(data) {
                                        return angular.isObject(data)
                                                ? jQuery.param(data)
                                                : data;
                                    }]
                                }).success(function(){
                                    $scope.cargarPermisos();
                                });
		}
	};
        
        $scope.$watch('usuario', function() {
            $scope.cargarPermisos();
        });


        // cargando los permisos
        //$scope.cargarPermisos();

});

