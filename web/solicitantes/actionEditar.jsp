<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.logging.Logger"%>
<%@page import="Conexion.conexion"%>
<%@page import="java.util.TreeMap"%>
<jsp:useBean id="solicitantes" class="clases.solicitantes" scope="request" />
        <jsp:setProperty name="solicitantes" property="idSolicitante" param="idSolicitante" />
        <jsp:setProperty name="solicitantes" property="nombre1" param="nombre1" />
        <jsp:setProperty name="solicitantes" property="nombre2" param="nombre2" />
        <jsp:setProperty name="solicitantes" property="apellido1" param="apellido1" />
        <jsp:setProperty name="solicitantes"  property="apellido2" param="apellido2" />
        <jsp:setProperty name="solicitantes" property="DUI" param="DUI" />
        <jsp:setProperty name="solicitantes" property="NIT" param="NIT" />
        <jsp:setProperty name="solicitantes" property="domicilio" param="domicilio" />
        <jsp:setProperty name="solicitantes" property="edad" param="edad" />
        <jsp:setProperty name="solicitantes" property="tel_residencia" param="tel_residencia" />
        <jsp:setProperty name="solicitantes" property="celular" param="celular" />
        <jsp:setProperty name="solicitantes" property="fecha_nacimiento" param="fecha_nacimiento" />
        <jsp:setProperty name="solicitantes" property="estado_sistema" param="estado_sistema" />
        <jsp:setProperty name="solicitantes" property="profesion" param="profesion" />
        <jsp:setProperty name="solicitantes" property="lugarTrabajo" param="lugarTrabajo" />
        <jsp:setProperty name="solicitantes" property="tiempo_servicio" param="tiempo_servicio" />
        <jsp:setProperty name="solicitantes" property="cargo" param="cargo" />
        <jsp:setProperty name="solicitantes" property="tel_trabajo" param="tel_trabajo" />
        <jsp:setProperty name="solicitantes" property="ext" param="ext" />
        <jsp:setProperty name="solicitantes" property="correo" param="correo" />
        <jsp:setProperty name="solicitantes" property="estado_civil" param="estado_civil" />
        <jsp:setProperty name="solicitantes" property="id_departamento" param="id_departamento" />
        <jsp:setProperty name="solicitantes" property="id_municipio" param="id_municipio" />
        <jsp:setProperty name="solicitantes" property="fecha_expedicion_DUI" param="fecha_expedicion_DUI" />
        <jsp:setProperty name="solicitantes" property="lugar_nacimiento" param="lugar_nacimiento" />
        <jsp:setProperty name="solicitantes" property="lugar_expedicion_DUI" param="lugar_expedicion_DUI" />
        
        <%--Informacion financiera --%>
        <jsp:setProperty name="solicitantes" property="ingresos" param="ingresos" />
        <jsp:setProperty name="solicitantes" property="descuentos" param="descuentos" />
        <jsp:setProperty name="solicitantes" property="liquidez"  param="liquidez" />
        <jsp:setProperty name="solicitantes" property="nombre_conyuge" param="nombre_conyuge" />
        <jsp:setProperty name="solicitantes" property="apellidos_conyuge" param="apellidos_conyuge" />
        <jsp:setProperty name="solicitantes" property="edad_conyuge" param="edad_conyuge" />
        <jsp:setProperty name="solicitantes" property="profesion_conyuge" param="profesion_conyuge" />
        <jsp:setProperty name="solicitantes" property="lugar_direccion_trabajo_conyuge" param="lugar_direccion_trabajo_conyuge" />
<%
    Logger logger = Logger.getLogger(this.getClass().getName());
    TreeMap<String, String>solicitantesPost = new TreeMap<String, String>();
    conexion con = new conexion();
    solicitantesPost = solicitantes.mapa(); //Creando mapa
    String idSolicitante = solicitantes.getIdSolicitante();
    
    try{
         String resultado = con.updateQuery(solicitantesPost, "solicitante", "idSolicitante=" + idSolicitante);
         response.sendRedirect("fichaSolicitante.jsp?idSolicitante=" + idSolicitante + "&" + resultado);
       }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, "Fallo debido a: " + e.getMessage());
        logger.severe("Existe un problema al editar solicitantes. Causa: " + e.getMessage()); 
    } 
%>