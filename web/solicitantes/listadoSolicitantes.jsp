<%-- 
    Document   : listadoSolicitantes
    Created on : 16-dic-2013, 14:23:35
    Author     : Pochii
--%>
<%-- 
    Document   : nuevoSolicitanteStep3
    Created on : 08-dic-2013, 16:24:46
    Author     : Pochii
--%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:750px;
                align:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                margin-left: 5%;
            }
            .container h2
            {
                font-family: Century Gothic; 
            }
            legend{margin-left: 10%;}
        </style>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li><a href="<%=ruta%>/solicitantes/panelSolicitantes.jsp">Solicitantes</a></li>
                <li class="active">Listado de solicitantes</li>
            </ol>
        </div>
        <div class="container">
            <div class="input-group" style='width: 300px; float: right;'>
                <span class="input-group-addon"><i class='fa fa-search'></i></span>
                 <input type='text' name='buscar' id='buscar' class='form-control' placeholder="Buscar"  onkeyup="buscarUsuario('mostrarLista.jsp', 'buscar','contenedor', 'sol');" />
            </div>
        </div>
            <legend>Lista de solicitantes </legend>
            <br />
            <div class="container" id="contenedor">
                
            </div>
        </ajax:displaytag>
<%@include file="../administracion/footer.jsp" %>
<script lang="javascript" src="<%=ruta%>/js/selectAjax.js"></script>
<script>
    $(function(){
       //Enviamos el parametro sol de Solicitante
       buscarUsuario('mostrarLista.jsp','buscar','contenedor', 'sol'); 
    });
</script>

