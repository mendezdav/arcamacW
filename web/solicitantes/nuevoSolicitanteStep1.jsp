<%-- 
    Document   : nuevoSolicitante
    Created on : 15-oct-2013, 11:41:48
    Author     : Pochii
--%>

<%--<%@include  file="../administracion/ValSession.jsp" %> --%>
<%@include file="../administracion/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="tipo" value="${param.type}" scope="session" />
<%--Validando si tipo es prestamo --%>
<c:if test="${param.type == 'pre'}"> 
    <c:if test="${empty sessionScope.opc}">
        <c:redirect url="../prestamos/panelPrestamos.jsp">
                <c:param name="erno" value="2" /> 
        </c:redirect>
    </c:if>
</c:if>
    <style>
         body{padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
        .table{width:800px;}
        .container {max-width:  1000px; margin: 0 auto;}
        .container h2 {font-family: Century Gothic; }
    </style>
        <ol class="breadcrumb">
            <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
            <li><a href="<%=ruta%>/solicitantes/panelSolicitantes.jsp">Solicitantes</a></li>
            <li class="active">Nuevo solicitante paso 1</li>
        </ol>
        <div class="container">
            <h4>Progreso: </h3>
            <div class="progress progress-striped active">
                <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                    <span style="text-align:center; color: #000;">0% finalizado</span>
                </div>
            </div>
            
            <h2>Paso 1: Informaci&oacute;n personal del solicitante</h2>
            <br/>
             <div id="error">
            </div>
            <fieldset>
               <form id="frmNuevoSolicitante" name="frmNuevoSolicitante"  action="nuevoSolicitanteStep2.jsp" method="POST">
               <table class="table">
                <tr>
                    <td><b>*Nombres:</b></td>
                    <td><input class="form-control requerido" type="text" name="nombre1" id="nombre1" size="40" placeholder="Primer Nombre" required autofocus onblur="validar('nombre1');" /> </td>
                    <td><div id='divnombre1' class='infor'> </div></td>
                    <td><input class="form-control" type="text" name="nombre2" id="nombre2" size="40" placeholder="Segundo Nombre" oninput="validar('nombre2');" /></td>
                    <td><div id='divnombre2'> </div></td>
              </tr>
              <tr>
                <td><b>Apellidos:</b></td>
                <td><input class="form-control requerido" type="text" name="apellido1" id="apellido1" size="40" placeholder="Primer Apellido" required onblur="validar('apellido1');"/></td>
                <td><div id='divapellido1' class='infor'> </div></td>
                <td> <input class="form-control" type="text" name="apellido2" id="apellido2" size="40" placeholder="Segundo Apellido" oninput="validar('apellido2');" /></td>
                <td><div id="divapellido2"> </div></td>
              </tr>
              <tr>
                    <td rowspan="2">
                        <label>*DUI: </label>
                    </td>
                    <td>
                        <input class="form-control requerido" type="text" name="DUI" id="DUI" size="10" placeholder="DUI" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('DUI');" />  
                    </td>
                    <td><div id='divDUI'></div></td>
                    <td><label>Fecha de expedici&oacute;n: </label></td>
                    <td>
                        <input class="form-control requerido" type="date" name="fecha_expedicion_DUI" id="fecha_expedicion_DUI" placeholder="DUI" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('fecha_expedicion_DUI');" />  
                    </td>
                    <td><div id='divfecha_expedicion_DUI'></div></td>
              </tr>
              <tr>
                    <td>
                        <textarea name="lugar_expedicion_DUI" id="lugar_expedicion_DUI" cols="7" rows="7" placeholder="Lugar de expedici&oacute;n" class="form-control requerido" required onblur="validar('lugar_expedicion_DUI');"></textarea> 
                    </td>
                    <td><div id='divlugar_expedicion_DUI'></div></td>
                </tr>
                <tr>
                    <td>
                        <label>*NIT: </label>
                    </td>
                    <td>
                        <input class="form-control requerido" type="text" name="NIT" id="NIT" size="20" placeholder="NIT" required pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" onblur="validar('NIT');" />  
                    </td>
                    <td><div id='divNIT'></div></td>
                </tr>
                <tr>
                    <td rowspan="2"><b>Tel&eacute;fonos: </b></td>
                    <td>
                        <input class="form-control" type="text" name="tel_residencia" id="tel_residencia" placeholder="Casa Ej. 2272-9185" pattern="^[0-9]{4}-[0-9]{4}$" onblur="validar('tel_residencia');"  />
                    </td>
                    <td><div id='divtel_residencia'></div></td>
                </tr>
                <tr>
                    <td>
                        <input class="form-control requerido" type="text" name="celular" id="celular"
                               pattern="^[0-9]{4}-[0-9]{4}$" placeholder="Celular Ej. 7923-6354" 
                               required onblur="validar('celular');" />
                    </td>
                    <td><div id='divcelular'></div></td>
                </tr>
                
                <tr>
                    <td><b>Correo Electr&oacute;nico: </b></td>
                    <td>
                         <input class="form-control" type="text" name="correo" id="correo"
                               pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email Ej. Usuario@hotmail.com" 
                               onblur="validar('correo');" />
                    </td>
                    <td><div id='divcorreo'></div></td>
                    <td><b>*Estado civil:</b>
                        <select name="estado_civil" class="form-control" style="width: 150px; display: inline;">
                            <sql:query dataSource="jdbc/mysql" var="e_civil">SELECT estado_civil FROM estado_civil</sql:query>
                            <c:forEach var="e_civil_f" items="${e_civil.rows}">
                                <option value="${e_civil_f.estado_civil}">${e_civil_f.estado_civil}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div id='divestado_civil'></div></td>
                </tr>
                
                <tr>
                    <td rowspan="2"><b>*Fecha de nacimiento:</b></td>
                    <td><input class="form-control requerido" type="date" name="fecha_nacimiento" id="fecha_nacimiento" required onblur="validar('fecha_nacimiento');calcular_edad('fecha_nacimiento','edad')" /></td>
                    <td><div id='divfecha_nacimiento'> </div></td>
                </tr>
                <tr>
                    <td><input class="form-control requerido" type="text" name="edad" id="edad" maxlength="2" placeholder="Edad"  pattern="^[0-9]{1,2}$" required onblur="validar('edad');" readonly /></td>
                    <td><div id='divedad'> </div></td>
                </tr>
                <tr>
                    <td><b>Lugar de nacimiento:</b></td>
                     <td>
                        <textarea name="lugar_nacimiento" id="lugar_nacimiento" cols="7" rows="7" placeholder="Lugar de nacimiento" class="form-control requerido" required onblur="validar('lugar_nacimiento');"></textarea>
                    </td>
                    <td><div id="divlugar_nacimiento"></div></td>
                </tr>
                <tr>
                    <td rowspan="2">
                        <label>*Domicilio: </label>
                    </td>
                    <td rowspan="2">
                        <textarea name="domicilio" id="domicilio" cols="7" rows="7" placeholder="Lugar de residencia" class="form-control requerido" required onblur="validar('domicilio');"></textarea>
                    </td>
                    <td><div id="divdomicilio"></div></td>
                    <td><b>*Departamento:</b>
                        <select name="id_departamento" id="id_departamento" class="form-control" onchange="cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0)" style="width: 150px; display: inline;">
                            <sql:query dataSource="jdbc/mysql" var="depto">
                                SELECT id_departamento, departamento FROM departamento</sql:query>
                            <c:forEach var="depto_f" items="${depto.rows}">
                                <option value="${depto_f.id_departamento}">${depto_f.departamento}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div id='divid_depto'></div></td>
                </tr>
                <tr>
                    <td></td>
                    <td><b>Municipio:</b>
                        <select name="id_municipio" id="id_municipio" class="form-control" style="width: 150px; display: inline;"></select>
                    </td>
                    <td><div id='divid_municipio'></div></td>
                </tr>
                <tr>
                    <td><b>Estado del solicitante:</b></td>
                    <td>
                        <select name="estado_sistema" class="form-control requerido">
                            <option value="Activo">Activo</option>
                            <option value="Inactivo">Inactivo </option>
                        </select>
                    </td>
                    <td><div id='divedad'> </div></td>
                </tr>
                <tr>
                    <td>
                        <input type="button" class="btn btn-info" onClick="fin('frmNuevoSolicitante', 'nuevoSolicitanteStep2.jsp?exito=1');" value="Continuar" />
                    </td>
                    <td>
                        <input type="button" id="btn_finalizar" class="btn btn-info" onClick="fin('frmNuevoSolicitante', 'actionGuardar.jsp?complete=1');" value="Finalizar" />
                    </td>
                </tr>

               </table>
                </form>
            </fieldset>
        <hr>
    </div> <!-- /container -->
    
<%@include file="../administracion/footer.jsp" %>
<script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
<script lang="javascript" src="<%=ruta%>/js/jquery.mask.js"></script>
<script language="javascript">
    function fin(form, url)
    {
        var val = sumarize(form);if(val){$("#btn_finalizar").attr("disabled", true);document.getElementById(form).action= url;document.getElementById(form).submit();}else{document.getElementById("error").innerHTML =" <div class=\"alert alert-warning fade in\"> "+ "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>" + "Los datos ingresados contienen errores, por favor aseg&uacute;rese de ingresarlos en el formato correcto<br /></div>";}
    }//fin 
    $(document).ready(function(){
        $("#DUI").mask('00000000-0');
        $("#NIT").mask('0000-000000-000-0');
        $("#tel_residencia").mask('0000-0000');
        $("#celular").mask('0000-0000');
        /**Cargando los municipios*/
        cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0);
    });
</script>