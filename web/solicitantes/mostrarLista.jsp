<%-- 
    Document   : mostrarLista
    Created on : 16-dic-2013, 16:46:29
    Author     : Pochii
--%>

<%@page import="javax.swing.JOptionPane"%>
<%-- 
    Document   : mostrarFiador
    Created on : 12-10-2013, 03:21:57 PM
    Author     : jorge
--%>
    <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <style media="all" type="text/css" >
        @import url("../css/displaytag.css");
    </style>
        <%
            /**
             * Declaracion de variable
             */
            String criterio; 
            String query, queryS;
            /**
             * url, es la ruta a la que redirigira la ultima columna
             * titulo, titulo que tendra la ultima columna en el displaytag
             */
            String url = "";
            String titulo = "";
            criterio = request.getParameter("criterio"); 
            //Variable para determinar cual enlace enviara el jsp
            String tipo = request.getParameter("tipo"); 
            if(tipo.equals(null)) out.println("La llamada a este archivo no es valida");
            
            /**
             * Si la llamada fue hecha por solicitante
             */
            if(tipo.equals("pre")){
                url = "/prestamos/nuevoPrestamo.jsp";
                titulo = "seleccionar";}
            else if(tipo.equals("sol")){
                url = "/solicitantes/fichaSolicitante.jsp";
                titulo = "Ver ficha";
            }
                
            if(criterio.equals(null) || criterio.equals("")){
              query  =  "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion "
                      + "FROM solicitante where estado_sistema='Activo' AND  idSolicitante NOT IN (SELECT idSolicitante FROM prestamos WHERE estado='Activo' OR estado='En proceso' GROUP BY idSolicitante)";
              
              queryS = "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion "
                      + "FROM solicitante where estado_sistema='Activo'";
            }
            else{
              query  = "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion "
                      + "FROM solicitante where estado_sistema='activo' AND  idSolicitante NOT IN (SELECT idSolicitante FROM prestamos WHERE estado='Activo' OR estado='En proceso' GROUP BY idSolicitante) AND "
                      + "concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%" + criterio + "%'";
              queryS = "select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion "
                      + "FROM solicitante where estado_sistema='Activo' AND "
                      + "concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%" + criterio + "%'";
            }
            
        %>
    <c:set var="title" value="<%=titulo%>" />
    <c:set var="url" value="<%=url%>" />
    <c:set var="tipo" value="<%=tipo%>" />
    <sql:query var="q1" dataSource="jdbc/mysql">
        <c:if test="${tipo=='sol'}">
           
                    <%=queryS%>
        </c:if>
        <c:if test="${tipo=='pre'}">
           
                    <%=query%>
        </c:if>
    </sql:query>
    
    <h4>Solicitantes activos en el sistema </h4>
    <ajax:displayTag id="ajax1">
     <display:table id="usuarios" name="${q1.rows}" pagesize="15" export="false" excludedParams="ajax" >  
            <display:column title="Solicitante" property="solicitante" sortable="true" />
            <display:column title="DUI" property="DUI" sortable="false" />
            <display:column title="Edad" property="edad" sortable="false" />
            <display:column title="Profesi&oacute;n" property="profesion" sortable="false" />
            <display:column title="Tel&eacute;fono" property="celular" sortable="false" />
            <display:column title="Estado" property="estado_sistema" sortable="false" />
            <display:column value="<i class='fa fa-edit'></i>${title}" url="${url}" paramId="idSolicitante" paramProperty="idSolicitante"  style="text-align:center;"/>
      </display:table>   
    </ajax:displaytag> 
    <c:if test="${tipo =='sol'}">
        <sql:query var="q2" dataSource="jdbc/mysql">
            select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI, edad, estado_sistema, celular, profesion  from solicitante where estado_sistema='Inactivo'
       </sql:query>
       <h4>Solicitantes inactivos</h4>
       <ajax:displayTag id="ajax2">
       <display:table  id="inactivos" name="${q2.rows}" pagesize="15" export="false">
            <display:column title="Solicitante" property="solicitante" sortable="true" />
            <display:column title="DUI" property="DUI" sortable="false" />
            <display:column title="Edad" property="edad" sortable="false" />
            <display:column title="Profesi&oacute;n" property="profesion" sortable="false" />
            <display:column title="Tel&eacute;fono" property="celular" sortable="false" />
            <display:column title="Estado" property="estado_sistema" sortable="false" />
            <display:column value="<i class='fa fa-edit'></i>Ver ficha" url="${url}" paramId="idSolicitante" paramProperty="idSolicitante"  style="text-align:center;"/>
      </display:table> 
       </ajax:displaytag>
    </c:if>
          
