<%-- 
    Document   : fichaSolicitante
    Created on : 16-mar-2014, 1:17:57
    Author     : Pochii
--%>

<%@page import="java.io.File"%>
<%@page import="clases.UploadFotos"%>
<%@include file="../administracion/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%String url = getServletContext().getRealPath("solicitantes\\images");%>
<jsp:useBean id="solicitantes" class="clases.solicitantes" scope="request" />
<style>
    body{padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
    .table{width:800px;}
    .container {max-width:  1000px; margin: 0 auto;}
    .container h2 {font-family: Century Gothic; }
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;border:none;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
    .tg .tg-s6z2{text-align:center}
    .tg .tg-e3zv{font-weight:bold}
    .tg .tg-hgcj{font-weight:bold;text-align:center}
    .tg .tg-34fq{font-weight:bold;text-align:right}
    /**Estilo div fotos**/
    .fotos{position: relative; width: 200px; height: 200px;}
    .foto_actual{width: 200px; height: 200px; position: absolute; left: 15px;top:0; }
    .nueva_foto{position: absolute;width: 200px; visibility: hidden; height: 200px; left: 15px; padding: 80px 30px 30px 20px; background-color: #c2ccd1; opacity: 0.8; z-index: 1000;}
    .nueva_foto input[type="file"]{max-width: 160px;}
    .fotos:hover .nueva_foto{visibility: visible;}
    
</style>
        <ol class="breadcrumb">
            <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
            <li><a href="<%=ruta%>/solicitantes/panelSolicitantes.jsp">Solicitantes</a></li>
            <li><a href="<%=ruta%>/solicitantes/listadoSolicitantes.jsp">Listado de solicitantes</a></li>
            <li class="active">Ficha de Solicitante</li>
        </ol>
   <%--Comprobando que esta definido el idSolicitante--%>
   <c:set var="idSolicitante" value='<%=request.getParameter("idSolicitante")%>' />   
   
   <c:if test="${(idSolicitante == NULL) || (idSolicitante=='')}">
        <c:redirect url="listadoSolicitantes.jsp">
            <c:param name="erno" value="2" /> 
        </c:redirect> 
   </c:if>
   <sql:query var="sQuery_" dataSource="jdbc/mysql">
      Select * FROM viewsolicitantes WHERE idSolicitante= ${idSolicitante}
   </sql:query>
   <div id="error">
   </div>
   <c:forEach var="sQuery" items="${sQuery_.rows}">
       
        <div class="container" id="ficha">
            <table class="tg">
              <tr>
                <th class="tg-hgcj" colspan="7">EDITAR SOLICITANTE</th>
              </tr>
              <tr>
                <td class="tg-s6z2" colspan="2" rowspan="4"><br> 
                   <div class="fotos">
                        <div class="foto_actual">
                        <%
                            String nombreImg = "thumbnail_" + request.getParameter("idSolicitante") ;
                            nombreImg = ruta + "/solicitantes/images/" + UploadFotos.getImageName(url, nombreImg, ruta);
                        %>
                        <img src="<%=nombreImg%>" width="200" height="200" /> 
                        </div>
                        <div class="nueva_foto">
                            <form id="frm_subir" method="POST" action="" enctype="multipart/form-data" >
                                <input type="hidden" name="rutared" value="/solicitantes/editarSolicitante.jsp?idSolicitante=${idSolicitante}" />
                                <input type="file" id="pic" name="pic" class="btn btn-info"  />
                            </form>
                        </div>
                   </div>
                </td> 
                <td class="tg-e3zv" colspan="5">Datos Personales</td> 
              </tr>
              <form name="frmEditar" id="frmEditar" method="POST">
              <tr>
                <input type="hidden" value="${idSolicitante}" name="idSolicitante" />
                <td class="tg-e3zv">Nombre:</td> 
                <td class="tg-031e"> 
                    <div id='divnombre1'></div><input type="text" value="${sQuery.nombre1}" name="nombre1" class="form-control requerido" id="nombre1" onblur="validar('nombre1');" required />
                     <br /> <input type="text" value="${sQuery.nombre2}" name="nombre2" class="form-control" /> 
                </td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Apellidos:</td>
                <td class="tg-031e" colspan="2">
                  <div id='divapellido1'></div> 
                  <input type="text" value="${sQuery.apellido1}" name="apellido1" class="form-control requerido" id="apellido1" onblur="validar('apellido1');" required /><br />
                
                <input type="text" value="${sQuery.apellido2}" name="apellido2" class="form-control" /></td>
              </tr>
              <tr>
                <td class="tg-e3zv">*Edad:</td>
                <td class="tg-031e">
                  <div id='divedad'></div> 
                  <input type="text" value="${sQuery.edad}" name="edad" id="edad" onblur="validar('edad');" class="form-control requerido" pattern="^[0-9]{1,2}$" required readonly /> a&ntilde;os</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; *Fecha de&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Nacimiento</td>
                <td class="tg-031e" colspan="2">
                  <div id='divfecha_nacimiento'></div>
                  <input type="date" value="${sQuery.fecha_nacimiento}" class="requerido" id="fecha_nacimiento" name="fecha_nacimiento" onblur="validar('fecha_nacimiento');calcular_edad('fecha_nacimiento','edad')" class="form-control" />
                </td>
              </tr>
              <tr>
                <td class="tg-e3zv">*DUI:</td>
                <td class="tg-031e">
                  <div id='divDUI'></div>
                  <input type="text" id='DUI' onblur="validar('DUI');" value="${sQuery.DUI}" class="form-control requerido" name="DUI" pattern="^[0-9]{8}-[0-9]{1}$" required /></td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; *NIT:</td>
                <td class="tg-031e" colspan="2">
                  <div id='divNIT'></div>
                  <input type="text" id='NIT' onblur="validar('NIT');" value="${sQuery.NIT}" class="form-control requerido" name="NIT" pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" required /></td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de expedici&oacute;n de DUI:</td>
                <td class="tg-031e" colspan="2"><div id="divlugar_expedicion_DUI"></div>
                   <div id="divlugar_expedicion_DUI"></div><input  style="max-width: 500px;" type="text" id="lugar_expedicion_DUI" class="form-control requerido" onblur="validar('lugar_expedicion_DUI');" name="lugar_expedicion_DUI" value="${sQuery.lugar_expedicion_DUI}" />
                </td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Fecha de expedici&oacute;n de DUI:</td>
                <td class="tg-031e" colspan="2">
                  <div id='divfecha_expedicion_DUI'></div> 
                  <input  style="max-width: 500px;" type="date" id="fecha_expedicion_DUI" class="form-control requerido" onblur="validar('fecha_expedicion_DUI');" name="fecha_expedicion_DUI" value="${sQuery.fecha_expedicion_DUI}" />
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Tel&eacute;fono de residencia:</td>
                <td class="tg-031e">
                  <div id="tel_residencia"></div>
                  <input type="text" value="${sQuery.tel_residencia}" id="tel_residencia" name="tel_residencia" class="form-control" pattern="^[0-9]{4}-[0-9]{4}$" oninput="validar('tel_residencia');" /></td>
                <td class="tg-34fq">*Celular:</td>
                <td class="tg-031e">
                  <div id='divcelular'></div>
                  <input type="text" class="form-control requerido" name="celular" id="celular" pattern="^[0-9]{4}-[0-9]{4}$" onblur="validar('celular');" class="form-control" value="${sQuery.celular}" /></td>
                <td class="tg-e3zv">*Estado:</td>
                <td class="tg-031e">
                    <select name="estado_sistema" id="estado_sistema" class="form-control" required>
                        <c:if test="${sQuery.estado_sistema == 'Activo'}"> 
                            <option value="Activo">Activo</option> 
                            <option value="Inactivo">Inactivo</option>
                        </c:if>
                        <c:if test="${sQuery.estado_sistema != 'Activo'}">
                            <option value="Inactivo">Inactivo</option>
                                <option value="Activo">Activo</option>
                        </c:if>
                    </select>
                </td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de nacimiento:</td>
                <td class="tg-031e" colspan="5">
                  <div id="divlugar_nacimiento"></div><input  style="max-width: 500px;" type="text" id="lugar_nacimiento" class="form-control requerido" onblur="validar('lugar_nacimiento');" name="lugar_nacimiento" value="${sQuery.lugar_nacimiento}" /></td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Correo Electr&oacute;nico:</td>
                <td class="tg-031e" colspan="5"><div id="divcorreo"></div>
                    <input  style="max-width: 500px;" type="text" id="correo" class="form-control" onblur="validar('correo');" 
                    name="correo"   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="${sQuery.correo}" /></td>
              </tr>
              
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Estado civil:</td>
                <td class="tg-031e" colspan="2"><div id="divcorreo"></div>
                    <select name="estado_civil" id="estado_civil" class="form-control" required>
                        <c:if test="${sQuery.estado_civil == 'Soltero/a'}"> 
                            <option value="Soltero/a">Soltero</option> 
                            <option value="Casado/a">Casado</option>
                        </c:if>
                        <c:if test="${sQuery.estado_civil != 'Soltero/a'}">
                            <option value="Casado/a">Casado</option>
                                <option value="Soltero/a">Soltero</option>
                        </c:if>
                    </select>
                </td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">*Domicilio:</td>
                <td class="tg-031e" colspan="5"><div id="divdomicilio"></div><input  style="max-width: 500px;" type="text" id="domicilio" class="form-control requerido" onblur="validar('domicilio');" name="domicilio" value="${sQuery.domicilio}" /></td>
              </tr>
              
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">*Departamento:</td>
                <td class="tg-031e" colspan="2"><div id="divid_departamento"></div>
                    <select name="id_departamento" id="id_departamento" class="form-control" onchange="cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0)" style="width: 150px; display: inline;">
                            <!--Cargando valor actual -->
                            <option value="${sQuery.id_departamento}">${sQuery.departamento}</option>
                            <sql:query dataSource="jdbc/mysql" var="depto">
                                SELECT id_departamento, departamento FROM departamento WHERE id_departamento != ${sQuery.id_departamento}</sql:query>
                            <c:forEach var="depto_f" items="${depto.rows}">
                                <option value="${depto_f.id_departamento}">${depto_f.departamento}</option>
                            </c:forEach>
                    </select>
                </td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;*Municipio:</td>
                <td class="tg-031e" colspan="2">
                  <div id='divid_municipio'></div> 
                  <c:set var="id_municipio" value="${sQuery.id_municipio}" />
                  <select name="id_municipio" id="id_municipio" class="form-control" style="width: 150px; display: inline;">
                      <!--Cargando valor actual -->
                      <option value="${sQuery.id_municipio}">${sQuery.municipio}</option>
                  </select>
              </tr>
              
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv" colspan="6">INFORMACI&Oacute;N LABORAL</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">*Profesi&oacute;n:</td>
                <td class="tg-031e" colspan="2"><div id="divprofesion"></div><input type="text" class="form-control requerido" onblur="validar('domicilio');" id="domicilio" name="profesion" value="${sQuery.profesion}" /></td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;*Cargo:</td>
                <td class="tg-031e" colspan="2">
                  <div id='divcargo'></div> 
                  <input type="text" class="form-control requerido" onblur="validar('cargo');" id="cargo" name="cargo" value="${sQuery.cargo}" /></td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">*Lugar de&nbsp;<br>Trabajo:<br></td>
                <td class="tg-031e" colspan="2">
                  <div id="divlugarTrabajo"></div><input type="text" class="form-control requerido" id="lugarTrabajo" name="lugarTrabajo" value="${sQuery.lugarTrabajo}" onblur="validar('lugarTrabajo');" /></td>
                <td class="tg-34fq">Extensi&oacute;n:</td>
                <td class="tg-031e" colspan="2"><input type="text" class="form-control" name="ext" value="${sQuery.ext}" /></td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Tel&eacute;fono de trabajo:</td>
                <td class="tg-031e" colspan="2"><input type="text" class="form-control" name="tel_trabajo" value="${sQuery.tel_trabajo}" pattern="^[0-9]{4}-[0-9]{4}$" /></td>
                <td class="tg-34fq">Tiempo de servicio:</td>
                <td class="tg-031e" colspan="2"><input style="width:50px" type="text" class="form-control" name="tiempo_servicio" value="${sQuery.tiempo_servicio}" />  a&ntilde;os</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv" colspan="6">INFORMACI&Oacute;N FINANCIERA</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">*Ingresos:</td>
                <td class="tg-031e">
                  <div id="divingresos"></div>
                  <div class="input-group">
                      <span class="input-group-addon">$</span>
                         <input type='text' name='ingresos' id='ingresos' class='form-control requerido' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' placeholder="Ingresos" value="${sQuery.ingresos}" required onblur="validar('ingresos');" />
                  </div>
                </td>
                <td class="tg-34fq">*Liquidez:</td>
                <td class="tg-031e">
                  <div id="divliquidez"></div>
                  <div class="input-group">
                      <span class="input-group-addon">$</span>
                        <input type='text' name='liquidez' id='liquidez' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' value="${sQuery.liquidez}" class='form-control requerido'onblur="validar('liquidez');" required/>
                  </div>
                </td>
                <td class="tg-34fq">Descuentos:</td>
                <td class="tg-031e">
                  <div id="divdescuentos"></div>
                  <div class="input-group">
                      <span class="input-group-addon">$</span>
                      <input type='text' name='descuentos' id='descuentos' value="${sQuery.descuentos}" pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' readonly />
                  </div>
                </td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv" colspan="6">INFORMACI&Oacute;N DE C&Oacute;NYUGE</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Nombre c&oacute;nyuge:</td>
                <td class="tg-031e" colspan="2"><input type="text" class="form-control" value="${sQuery.nombre_conyuge}" name="nombre_conyuge" /></td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Apellidos:</td>
                <td class="tg-031e" colspan="2"><input type="text" class="form-control" value="${sQuery.apellidos_conyuge}" name="apellidos_conyuge" /></td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Edad:</td>
                <td class="tg-031e" colspan="2"><input type="text" name="edad_conyuge" class="form-control" value="${sQuery.edad_conyuge}" /></td>
                <td class="tg-34fq">Profesi&oacute;n:</td>
                <td class="tg-031e" colspan="2"><input type="text" name="profesion_conyuge" class="form-control" value="${sQuery.profesion_conyuge}" /></td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de trabajo</td>
                <td class="tg-031e" colspan="2"><input type="text" name="lugar_direccion_trabajo_conyuge" class="form-control" value="${sQuery.lugar_direccion_trabajo_conyuge}" /></td>
                <td class="tg-031e"></td>
                <td class="tg-031e"></td>
                <td class="tg-031e"></td>
              </tr>
            </table>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-2">
                    <input type="button" value="Finalizar edici&oacute;n" class="btn btn-info" onclick="fin('frmEditar', 'actionEditar.jsp');" />
                </div>
            </div>  
        </form>
        </div>
        
        </c:forEach>
<%@include file="../administracion/footer.jsp" %>
<script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
<script lang="javascript" src="<%=ruta%>/js/jquery.mask.js"></script>
<script language="javascript">
    function fin(form, url)
    { 
      var val = sumarize(form);if(val){document.getElementById(form).action= url;document.getElementById(form).submit();}else{document.getElementById("error").innerHTML =" <div class=\"alert alert-warning fade in\"> "+ "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">X</button>" + "Los datos ingresados contienen errores, por favor aseg&uacute;rese de ingresarlos en el formato correcto<br /></div>";}
    }//fin 
    $(document).ready(function(){
       $("#liquidez").blur(function (){
          if($("#ingresos").val() == "" || $("#liquidez").val() == "") 
              alert("Los campos Ingresos y liquidez son obligatorios");
          else 
          {
              var deduccion;
              $("#descuentos").val($("#ingresos").val() - $("#liquidez").val());
              deduccion = $("#descuentos").val();
              deduccion = parseFloat(deduccion).toFixed(2);
              $("#descuentos").val(deduccion);
              if($("#descuentos").val() <= 0){
                  $("#liquidez").css("border-color","#a94442");}
              else
                  $("#liquidez").css("border-color", "#4b984a");
          }
       });
    });
    $(document).ready(function(){
        $("#pic").change(function(){
            upload("frm_subir", "<%=ruta%>/UploadFotos?tipo=edit&idSolicitante=${idSolicitante}");
        });
        /**Cargando mascaras**/
        $("#DUI").mask('00000000-0');
        $("#NIT").mask('0000-000000-000-0');
        $("#tel_residencia").mask('0000-0000');
        $("#celular").mask('0000-0000');
        /**Cargando los municipios**/
        cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp', ${id_municipio});
    });
</script>