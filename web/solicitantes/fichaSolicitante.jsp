<%-- 
    Document   : fichaSolicitante
    Created on : 16-mar-2014, 1:17:57
    Author     : Pochii
--%>

<%@page import="java.io.File"%>
<%@page import="clases.UploadFotos"%>
<%@include file="../administracion/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%String url = getServletContext().getRealPath("solicitantes\\images");%>
<jsp:useBean id="solicitantes" class="clases.solicitantes" scope="session" />
<style>
    body{padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */}
    .table{width:800px;}
    .container {max-width:  1000px; margin: 0 auto;}
    .container h2 {font-family: Century Gothic; }
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#999;border:none;}
    .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}
    .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}
    .tg .tg-s6z2{text-align:center}
    .tg .tg-e3zv{font-weight:bold}
    .tg .tg-hgcj{font-weight:bold;text-align:center}
    .tg .tg-34fq{font-weight:bold;text-align:right}
</style>
        <ol class="breadcrumb">
            <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
            <li><a href="<%=ruta%>/solicitantes/panelSolicitantes.jsp">Solicitantes</a></li>
            <li><a href="<%=ruta%>/solicitantes/listadoSolicitantes.jsp">Listado de solicitantes</a></li>
            <li class="active">Ficha de Solicitante</li>
        </ol>
   <%--Comprobando que esta definido el idSolicitante--%>
   <c:set var="idSolicitante" value='<%=request.getParameter("idSolicitante")%>' />   
   
   <c:if test="${(idSolicitante == NULL) || (idSolicitante=='')}">
        <c:redirect url="listadoSolicitantes.jsp">
            <c:param name="erno" value="2" /> 
        </c:redirect> 
   </c:if>
   <sql:query var="sQuery_" dataSource="jdbc/mysql">
      Select * FROM viewsolicitantes WHERE idSolicitante= ${idSolicitante}
   </sql:query>
        <%--Estableciendo las propiedades del bean --%>
   <c:forEach var="sQuery" items="${sQuery_.rows}">
        <jsp:setProperty name="solicitantes" property="idSolicitante" value="${sQuery.idSolicitante}" />
        <jsp:setProperty name="solicitantes" property="nombre1" value="${sQuery.nombre1}" />
        <jsp:setProperty name="solicitantes" property="nombre2" value="${sQuery.nombre2}" />
        <jsp:setProperty name="solicitantes" property="apellido1" value="${sQuery.apellido1}" />
        <jsp:setProperty name="solicitantes"  property="apellido2" value="${sQuery.apellido2}" />
        <jsp:setProperty name="solicitantes" property="DUI" value="${sQuery.DUI}" />
        <jsp:setProperty name="solicitantes" property="NIT" value="${sQuery.NIT}" />
        <jsp:setProperty name="solicitantes" property="domicilio" value="${sQuery.domicilio}" />
        <jsp:setProperty name="solicitantes" property="edad" value="${sQuery.edad}" />
        <jsp:setProperty name="solicitantes" property="tel_residencia" value="${sQuery.tel_residencia}" />
        <jsp:setProperty name="solicitantes" property="celular" value="${sQuery.celular}" />
        <jsp:setProperty name="solicitantes" property="fecha_nacimiento" value="${sQuery.fecha_nacimiento}" />
        <jsp:setProperty name="solicitantes" property="estado_sistema" value="${sQuery.estado_sistema}" />
        <jsp:setProperty name="solicitantes" property="profesion" value="${sQuery.profesion}" />
        <jsp:setProperty name="solicitantes" property="lugarTrabajo" value="${sQuery.lugarTrabajo}" />
        <jsp:setProperty name="solicitantes" property="tiempo_servicio" value="${sQuery.tiempo_servicio}" />
        <jsp:setProperty name="solicitantes" property="cargo" value="${sQuery.cargo}" />
        <jsp:setProperty name="solicitantes" property="tel_trabajo" value="${sQuery.tel_trabajo}" />
        <jsp:setProperty name="solicitantes" property="ext" value="${sQuery.ext}" />
        <jsp:setProperty name="solicitantes" property="correo" value="${sQuery.correo}" />
        
        <%--Informacion financiera --%>
        <jsp:setProperty name="solicitantes" property="ingresos" 	   value="${sQuery.ingresos}" />
        <jsp:setProperty name="solicitantes" property="descuentos"     value="${sQuery.descuentos}" />
        <jsp:setProperty name="solicitantes" property="liquidez"   	   value="${sQuery.liquidez}" />
        <jsp:setProperty name="solicitantes" property="nombre_conyuge" value="${sQuery.nombre_conyuge}" />
        <jsp:setProperty name="solicitantes" property="apellidos_conyuge" value="${sQuery.apellidos_conyuge}" />
        <jsp:setProperty name="solicitantes" property="edad_conyuge" value="${sQuery.edad_conyuge}" />
        <jsp:setProperty name="solicitantes" property="profesion_conyuge" value="${sQuery.profesion_conyuge}" />
        <jsp:setProperty name="solicitantes" property="lugar_direccion_trabajo_conyuge" value="${sQuery.lugar_direccion_trabajo_conyuge}" />
        <div class="container" id="ficha">
            <table class="tg">
              <tr>
                <th class="tg-hgcj" colspan="7">FICHA DE SOLICITANTE</th>
              </tr>
              <tr>
                <td class="tg-s6z2" colspan="2" rowspan="4"><br>
                    <%
                        String nombreImg = "thumbnail_" + request.getParameter("idSolicitante") ;
                        File f = new File(url + "\\" + nombreImg + ".jpg");
                        if(f.exists())
                            nombreImg = ruta + "/solicitantes/images/" + nombreImg + ".jpg";
                        else
                            nombreImg = ruta+ "/solicitantes/images/" + nombreImg + ".png"; 
                    %>
                    <img src="<%=nombreImg%>" width="200" height="200" /> 
                </td> 
                <td class="tg-e3zv" colspan="5">Datos Personales</td> 
              </tr>
              <tr>
                <td class="tg-e3zv">Nombre:</td> 
                <td class="tg-031e"> 
                    ${sQuery.nombre1} ${sQuery.nombre2} 
                </td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Apellidos:</td>
                <td class="tg-031e" colspan="2">${sQuery.apellido1} ${sQuery.apellido2}</td>
              </tr>
              <tr>
                <td class="tg-e3zv">Edad:</td>
                <td class="tg-031e">${sQuery.edad} a&ntilde;os</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fecha de&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Nacimiento</td>
                <td class="tg-031e" colspan="2">${sQuery.fecha_nacimiento}</td>
              </tr>
              <tr>
                <td class="tg-e3zv">DUI:</td>
                <td class="tg-031e">${sQuery.DUI}</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; NIT:</td>
                <td class="tg-031e" colspan="2">${sQuery.NIT}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de expedici&oacute;n de DUI:</td>
                <td class="tg-031e" colspan="2">${sQuery.lugar_expedicion_DUI}</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha de expedici&oacute;n de DUI:</td>
                <td class="tg-031e" colspan="2">${sQuery.fecha_expedicion_DUI}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Tel&eacute;fono de residencia:</td>
                <td class="tg-031e">${sQuery.tel_residencia}</td>
                <td class="tg-34fq">Celular:</td>
                <td class="tg-031e">${sQuery.celular}</td>
                <td class="tg-e3zv">Estado:</td>
                <td class="tg-031e">${sQuery.estado_sistema}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                
                <td class="tg-e3zv">Correo Electr&oacute;nico:</td>
                <td class="tg-031e">${sQuery.correo}</td>
                
                <td class="tg-e3zv">Domicilio:</td>
                <td colspan="3" class="tg-031e">${sQuery.domicilio}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Estado civil:</td>
                <td colspan="5" class="tg-031e">${sQuery.estado_civil}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de nacimiento:</td>
                <td class="tg-031e" colspan="5">${sQuery.lugar_nacimiento}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Departamento:</td>
                <td class="tg-031e" colspan="2">${sQuery.departamento}</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Municipio:</td>
                <td class="tg-031e" colspan="2">${sQuery.municipio}</tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv" colspan="6">INFORMACI&Oacute;N LABORAL</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Profesi&oacute;n:</td>
                <td class="tg-031e" colspan="2">${sQuery.profesion}</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Cargo:</td>
                <td class="tg-031e" colspan="2">${sQuery.cargo}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de&nbsp;<br>Trabajo:<br></td>
                <td class="tg-031e" colspan="2">${sQuery.lugarTrabajo}</td>
                <td class="tg-34fq">Extensi&oacute;n:</td>
                <td class="tg-031e" colspan="2">${sQuery.ext}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Tel&eacute;fono de trabajo:</td>
                <td class="tg-031e" colspan="2">${sQuery.tel_trabajo}</td>
                <td class="tg-34fq">Tiempo de servicio:</td>
                
                <td class="tg-031e" colspan="2">${sQuery.tiempo_servicio} a&ntilde;os</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv" colspan="6">INFORMACI&Oacute;N FINANCIERA</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Ingresos:</td>
                <td class="tg-031e">${sQuery.ingresos}</td>
                <td class="tg-34fq">Liquidez:</td>
                <td class="tg-031e">${sQuery.liquidez}</td>
                <td class="tg-34fq">Descuentos:</td>
                <td class="tg-031e">${sQuery.descuentos}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv" colspan="6">INFORMACI&Oacute;N DE C&Oacute;NYUGE</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Nombre c&oacute;nyuge:</td>
                <td class="tg-031e" colspan="2">${sQuery.nombre_conyuge}</td>
                <td class="tg-e3zv">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Apellidos:</td>
                <td class="tg-031e" colspan="2">${sQuery.apellidos_conyuge}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Edad:</td>
                <td class="tg-031e" colspan="2">${sQuery.edad_conyuge}</td>
                <td class="tg-34fq">Profesi&oacute;n:</td>
                <td class="tg-031e" colspan="2">${sQuery.profesion_conyuge}</td>
              </tr>
              <tr>
                <td class="tg-031e"></td>
                <td class="tg-e3zv">Lugar de trabajo</td>
                <td class="tg-031e" colspan="2">${sQuery.lugar_direccion_trabajo_conyuge}</td>
                <td class="tg-031e"></td>
                <td class="tg-031e"></td>
                <td class="tg-031e"></td>
              </tr>
            </table>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-2">
                    <a href="editarSolicitante.jsp?idSolicitante=${idSolicitante}" class="btn btn-info">Editar</a>
                </div>
                <div class="col-md-2">
                    <a href="<%=ruta%>/ReporteFicha?idSolicitante=${idSolicitante}" class="btn btn-primary">Imprimir</a>
                </div>
                <div class="col-md-2">
                    <!-- <a href="<%=ruta%>/prestamos/historialPrestamos.jsp?idSolicitante=${idSolicitante}" class="btn btn-info">Historial de pr&eacute;stamos</a> -->
                    <a href="<%=ruta%>/ReporteHistorialPrestamos?idSolicitante=${idSolicitante}" class="btn btn-info">Historial de pr&eacute;stamos</a>
                </div>
            </div>    
        </div>
        
        </c:forEach>
<%@include file="../administracion/footer.jsp" %>
