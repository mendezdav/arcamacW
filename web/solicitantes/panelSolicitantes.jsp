<%-- 
    Document   : panelSolicitantes
    Created on : 13-dic-2013, 15:36:15
    Author     : Pochii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../administracion/header.jsp" %>
<%--<%@include  file="../administracion/ValSession.jsp" %> --%>
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:600px;
                aling:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
            }
            .container h2
            {
                font-family: Century Gothic;
            }
            .col-s{margin: 10px 50px 50px 15px;}
        </style>
        <div class="container">
           <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li class="active">Solicitantes</li>
           </ol>
           <h2>Panel principal de Solicitantes</h2>
           <br />
           <div class="row">
               <div class="col-md-2 col-l">
                   <a class="btn btn-success" style="display:block; width: 150px;" href="nuevoSolicitanteStep1.jsp?type=sol">
                       <br/>
                       <i class="fa fa-file-text-o fa-3x"></i> 
                       <br/>
                       <br/>
                       Nuevo Solicitante
                   </a>
                </div>
                <div class="col-md-2 col-l">
                        <a class="btn btn-success" style="display:block; width: 150px;" href="listadoSolicitantes.jsp">
                            <br/>
                            <i class="fa fa-align-justify fa-3x"></i> 
                            <br/>
                            <br/>
                            Lista de solicitantes
                        </a>
                </div>
           </div>
        </div>
<%@include file="../administracion/footer.jsp" %>
<script>
    //Funcion para tomar la url de una pagina
    function getURLParams(){
       window.$GET = [];
       if(location.search){
           var params = decodeURIComponent(location.search.match(/[a-z_]\w*(?:=[^&]*)?/gi));
           if(params){
               var pm, i = 0;
               for(i=0;i<params.length; i++){
                   pm = params[i].split('=');
                   $GET[pm[0]] = pm[1] || '';
               }
           }
       }
       return $GET;
    }
    
    (function(){
        var div;
        var $GET = getURLParams();
        //Tomando mensajes de error
        if($GET['exito']!=null){
            div = document.getElementById("alertExito");
            div.style.visibility = visible;
            if($GET['exito'] == 'in')
            if($GET['exito'] == 'up')
            if($GET['exitp'] == 'del')
        }

        else if($GET['erno']!=null) {
            
        }
    })();
</script>