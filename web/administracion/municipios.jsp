<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
 <c:set var="ruta" value='<%=request.getScheme() +  "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()%>' /> 
 <c:choose>
     <c:when test="{empty param.id_departamento}">
         <c:redirect url="${ruta}/panelAdmin.jsp?erno=2" />
     </c:when>
     <c:when test="{empty param.criterio}">
         <c:redirect url="${ruta}/panelAdmin.jsp?erno=2" />
     </c:when>
</c:choose>
<c:set var="id_departamento" value="${param.id_departamento}" />
<c:set var="criterio" value="${param.criterio}" />
<!--Estableciendo la query--->
<sql:query var="q1" dataSource="jdbc/mysql">
    SELECT id_municipio, municipio FROM municipio WHERE id_departamento = ${id_departamento}
</sql:query>
<sql:query var="q2" dataSource="jdbc/mysql">
    SELECT id_municipio, municipio FROM municipio WHERE id_departamento = ${id_departamento} AND id_municipio != ${criterio}
</sql:query>
<sql:query var="q3" dataSource="jdbc/mysql">
            SELECT id_municipio, municipio FROM municipio WHERE id_municipio = ${criterio} 
</sql:query>
<c:choose>
    <c:when test="${criterio == 0}">
        <!-- En el caso que no sea necesario un where para los municipios -->
        <c:forEach var="q1_f" items="${q1.rows}">
            <option value="${q1_f.id_municipio}">${q1_f.municipio}</option>
        </c:forEach>
    </c:when>
    <c:when test="${criterio != 0}">
        <!--Estableciendo la query para el primer registro--->
        <c:forEach var="q3_f" items="${q3.rows}">
            <option value="${q3_f.id_municipio}">${q3_f.municipio}</option>
        </c:forEach>
        <c:forEach var="q2_f" items="${q2.rows}">
            <option value="${q2_f.id_municipio}">${q2_f.municipio}</option>
        </c:forEach>
    </c:when>
</c:choose>

