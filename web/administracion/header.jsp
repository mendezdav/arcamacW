<%@page import="java.sql.ResultSet,java.util.Map,clases.cumpleMes,javax.swing.JOptionPane"%> 
<%@page session="true" %>
<%
    String ruta = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
%>
<%! String nombreUsuario = ""; %>
<%! String apellidoUsuario = ""; %>
<%! String usuario = ""; %>
<%! String tipoUsuario = ""; %>
<%! ResultSet resultadoDia = null; %>
<%! ResultSet resultadoMes = null; %>
 <%
    // SessionActual ya existe!
  HttpSession SessionActual = request.getSession(false);
  
  try 
  { 
      usuario = (String) SessionActual.getAttribute("usuario");
      nombreUsuario = (String) SessionActual.getAttribute("nombreUsuario");
      apellidoUsuario = (String) SessionActual.getAttribute("apellidoUsuario");
      tipoUsuario = (String) SessionActual.getAttribute("tipoUsuario");
      
      if (usuario.equals(""))
      {
          //JOptionPane.showMessageDialog(null, "vacio");
      }
      if (usuario.equals(null))
      {
         // JOptionPane.showMessageDialog(null, "null");
      }
      
      if (nombreUsuario == "")
      {
          //JOptionPane.showMessageDialog(null, "vacio");
      }
      if (nombreUsuario == null)
      {
         // JOptionPane.showMessageDialog(null, "null");
      }
      
   }
  
  catch (Exception e)
  { response.sendRedirect(ruta +"/logueo.jsp?error=2");
 }  
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
    <!--<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""> !-->
    <title>Arcamac S.V</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<%=ruta %>/css/bootstrap.css" rel="stylesheet"></link>

    <!-- Add custom CSS here -->
    <link href="<%=ruta %>/css/sb-admin.css" rel="stylesheet"></link>
    <link rel="stylesheet" href="<%=ruta%>/font-awesome/css/font-awesome.min.css" />
    <script src="<%=ruta%>/js/jquery-1.10.2.js"></script>
    <script src="<%=ruta%>/js/angular.min.js"></script>
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<%=ruta%>/panelAdmin.jsp">Pr&eacute;stamos ARCAMAC</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            <li><a href="<%=ruta%>/Mantenimientos/fiadores/principalFiadores.jsp"><i class="fa fa-briefcase"></i> Fiadores</a></li>  
            <li><a href="<%=ruta%>/solicitantes/panelSolicitantes.jsp"><i class="fa fa-users"></i> Solicitantes</a></li>
            <li><a href="<%=ruta%>/prestamos/panelPrestamos.jsp"><i class="fa fa-th-list"></i> Pr&eacute;stamos</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Opciones r&aacute;pidas <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<%=ruta%>/Pagos/PrincipalPago.jsp">Pagos</a></li>
                <li><a href="<%=ruta%>/Pagos/eliminar/principal.jsp">Eliminar Pagos</a></li>
                <li><a href="<%=ruta%>/prestamos/prePrestamo.jsp?opc=noref">Nuevo pr&eacute;stamo</a></li>
                <li><a href="<%=ruta%>/prestamos/analisisNuevo.jsp">An&aacute;lisis de nuevos <br />pr&eacute;stamos</a></li>
                <li><a href="<%=ruta%>/reportes/documentos/PrincipalDocumentos.jsp">Imprimir Documentaci&oacute;n</a></li>
              </ul>
            </li>  
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Reportes <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Saldos rojos</a></li>
                <li><a href="<%= ruta%>/reportes/EstadoCuenta/principal.jsp">Estado de Cuenta</a></li>
                <!--  ultimas ediciones -->
                <li><a href="<%= ruta%>/reportes/prestamosPlazo/principalPrestamosPlazo.jsp">Reportes Pr&eacute;stamos</a></li>
                <li><a href="<%=ruta%>/reportes/panelReportes.jsp">Otros reportes</a></li>
              </ul>
            </li>
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Configuraci&oacute;n <b class="caret"></b></a>
              <ul class="dropdown-menu">
               <li><a href="<%=ruta%>/Mantenimientos/Usuarios/agregarUsuarios.jsp">Nuevo Usuario</a></li>
                <li><a href="<%=ruta%>/Mantenimientos/Usuarios/PrincipalUsuario.jsp">Administrar Usuarios</a></li>
                <li><a href="<%=ruta%>/administracion/respaldos.jsp">Copias de Seguridad</a></li>
                <li><a href="<%=ruta%>/administracion/restaurar.jsp">Restaurar backup</a></li>
                <li><a href="<%=ruta%>/administracion/refinanciamiento/principal_configuraciones.jsp">Otras Configuraciones</a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown messages-dropdown">
              <%@include file="lista_saldosrojos.jsp" %>
            </li>
            <!-- CUMPLEAÑEROS DEL DIA -->
              <li class="dropdown alerts-dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-calendar"></i>
			  Cumplea&ntilde;eros del Dia <span class="badge">
		<%
		 // variables controladoras
                    boolean dia,mes;
                    int totalDia, totalMes;
                    cumpleMes m = null;
                    cumpleMes m2 = null;
                    dia = mes = false; // es falso si no hay error!
                    totalDia = totalMes = 0;
                   
                    // capturando error de resultset en dias
                    try 
                    {
                         // resultset cumpleañeros dias!
                        m = new cumpleMes();
                        resultadoDia = (ResultSet) m.obtenerDias();
                        while (resultadoDia.next())
                        {
                            totalDia++;
                        }
                        // regresando al inicio
                        resultadoDia.first();
                    }
                    catch (Exception e)
                    {
                        dia = true;
                    }
                    
                    // capturando error de resulset en mes
                    try 
                    {  
                        m2 = new cumpleMes();
                        resultadoMes = (ResultSet) m2.obtenerMensaje();
                         while (resultadoMes.next())
                        {
                            totalMes++;
                        }
                        // regresando al inicio
                        resultadoMes.first();
                    }
                    catch (Exception e)
                    {
                        mes = true;
                    }
		    /* comenzando con la contruccion 
                     * del menu de cumpleañeros
                     */
                    
                    if (!dia)
                    { resultadoDia.next();}
                    if (!mes)
                    {
                        resultadoMes.next();
                        
                    }
                  
                    if (!dia)
                    {
                    %>
                    <!-- impresion del total de cumpleañeros del dia-->
                        <%= totalDia %>
                    
              </span> <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <!-- repeticion -->
                <%
                    resultadoDia.beforeFirst();
                    while (resultadoDia.next()){
                        
                %>
                 <li class="message-preview">
                  <a href="#">
                    <span class="avatar"><img src="<%= ruta%>/images/default.jpg"></span>
                    <span class="name">
                        <%= resultadoDia.getString("nombre") %>
                    </span>
                    <br />
                    <span class="message"><i class="fa fa-gift"></i>&iexcl;Celebra sus <%= m.getEdad() %> a&ntilde;os!</span>
                    <br />
                    <span class="time"></i>Tel&eacute;fono: <%=resultadoDia.getString("tel_residencia") %><span>
                  </a>
                 </li>
                 <% } %>
		<!-- repeticion -->
                    <%
                        
                    }else  // no hay cumpleañeros dias, hay en mes??
                    {%>
                    <%= totalDia %>
                    <%
                        %>
                         </span> <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                        <%
                    }
                    
                    %>
                    
                <li class="divider"></li>
                <%
                    if (totalMes != 0){
                 %>
		<li><a href="<%= ruta%>/ReporteCumpleanios">Cumplea&ntilde;eros del Mes <span class="badge"><%= totalMes %></span></a></li>
                <% }else{
                %>
                     <li><a href="#">Cumplea&ntilde;eros del Mes <span class="badge"><%= totalMes %></span></a></li>
               <% }%> 
               </ul>
            </li>
            <!-- FIN CUMPLEAÑEROS DEL MES -->
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                      <%
                         String  hora = new   java.util.Date().toLocaleString();
                         out.print(nombreUsuario + "  ");
                         out.print(hora.substring(11));
                      %>
                      <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<%=ruta%>/Mantenimientos/perfil/principal.jsp"><i class="fa fa-user"></i> Mi perfil</a></li>
                <li><a href="<%=ruta%>/Mantenimientos/Usuarios/PrincipalUsuario.jsp"><i class="fa fa-users"></i> Administrar Usuarios </a></li>
                <li><a href="<%=ruta%>/administracion/respaldos.jsp"><i class="fa fa-hdd-o"></i> Generar Backup</a></li>
                <li><a href="<%=ruta%>/administracion/restaurar.jsp"><i class="fa fa-hdd-o"></i> Resturar Backup</a></li>
                <li class="divider"></li>
                <li><a href="<%=ruta%>/administracion/CerrarSession.jsp"><i class="fa fa-power-off"></i> Cerrar Sesi&oacute;n</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
              <%
                  if (request.getParameter("erno") != null)
                  {
                      int error = Integer.parseInt(request.getParameter("erno"));
                      
                      switch (error)
                      {
                          case 1:
                              out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Error en la comunicaci&oacute;n con la base de datos. Alguno de los datos enviados fue err&oacute;neo</div>");
                          break;
                          
                          case 2:
                              out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Error. La p&aacute;gina a la que intent&oacute; acceder debe ser llamada desde un formulario</div>");
                          break;
                              
                          case 3:
                              out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Error. Existen datos err&oacute;neos o vac&iacute;os en el formulario, por favor verifique todos los campos</div>");
                          break;
                              
                          case 4:
                              out.print(" <div class=\"alert alert-warning fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "No hay datos que mostrar</div>");
                          break;
                              
                          case 5:
                              out.print(" <div class=\"alert alert-danger fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "No hay datos que mostrar</div>");
                          break;
                              
                          case 6:
                              out.print(" <div class=\"alert alert-danger\">Existe un conflicto con un registro en la base de datos</div>");
                          break;
                              
                          case 7:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Contrase&ntilde;a actual no coincide</div>");
                          break;
                              
                          case 8:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Contrase&ntilde;a nueva no coinciden</div>");
                          break;
                              
                          case 9:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Valor debe ser mayor a  $0.0</div>");
                          break;
                              
                          case 10:
                            out.print(" <div class=\"alert alert-warning fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "El pr&eacute;stamo posee al menos un pago realizado, por lo que no es posible eliminarlo</div>");
                          break;
                              
                         case 11:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Extensi&oacute;n del archivo inv&aacute;lida</div>");
                          break;
                             
                         case 12:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Error al subir el archivo</div>");
                         break;
                             
                         case 13:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Error al restaurar base de datos</div>");
                         break;
                             
                         case 14:
                            out.print(" <div class=\"alert alert-danger fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Fiador no se puede eliminar, existe prestamos activo o en proceso con dicho fiador</div>");
                         break;
                             
                         case 15:
                             out.print(" <div class=\"alert alert-danger fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "El socio seleccionado actualmente tiene un préstamo en proceso de aprobación</div>");
                         break;
                             
                         case 16:
                            out.print(" <div class=\"alert alert-warning fade in\">"
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "El préstamo no cumple con los requisitos para modificar la información</div>");
                         break;
                              
                      }
                  }
                  if(request.getParameter("exito")!=null)
                  {
                     // String exito = request.getParameter("exito");
                      if(request.getParameter("exito").equals("in"))
                              out.print("<div class=\"alert alert-success fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Registro insertado con &eacute;xito</div>");
                      if(request.getParameter("exito").equals("up"))
                              out.print("<div class=\"alert alert-success fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Registro actualizado con &eacute;xito</div>");
                      if(request.getParameter("exito").equals("del"))
                              out.print("<div class=\"alert alert-success fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Registro eliminado con &eacute;xito</div>");
                       if(request.getParameter("exito").equals("upl"))
                              out.print("<div class=\"alert alert-success fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Archivo subido con &eacute;xito</div>");
                      if(request.getParameter("exito").equals("1") || request.getParameter("exito").equals("2") || request.getParameter("exito").equals("3") )
                              out.print("<div class=\"alert alert-success fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Operaci&oacute;n exitosa</div>");
                      if(request.getParameter("exito").equals("RDB"))
                              out.print("<div class=\"alert alert-success fade in\"> "
                                      + "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>"
                                      + "Restauraci&oacute;n de base de datos exitosa</div>");
                      
                      }                     
         %>
<!-- FIN DEL HEADER -->

        
