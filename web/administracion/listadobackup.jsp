<%-- 
    Document   : listadobackup
    Created on : 06-07-2014, 04:53:49 AM
    Author     : jorge
--%>

<%@page import="clases.diferenciaDias"%>
<%@page import="java.io.File"%>
<%@page  import="java.util.Date" %>
<%@page  session="true" language="java" %>
<%@page import="javax.swing.JOptionPane"%>

<link rel="stylesheet" href="../css/estiloPaginacion.css" media="all">
<script type="text/javascript" src="../js/paginador/1.7.2.jquery.min.js"></script>
<script type="text/javascript" src="../js/paginador/funciones.js"></script>

  <!-- Cambios Paginacion  -->
     <%
   
     out.println("<br><br>");
     out.println("<div class='sinbusqueda'>");
     out.println("<table class='tablita'>");
     out.println("<caption>LISTA DE RESPALDOS DISPONIBLES</caption>");
     out.println("<tr>");
     out.println("<th>Nombre</th>");
     out.println("<th>Tama&ntilde;o</th>");
     out.println("<th>Opcion</th>");
     out.println("</tr>");
     out.println("<tbody>");
     int RegistrosAMostrar=12;
     int PaginasIntervalo=3;
     int RegistrosAEmpezar=0;
     int PagAct = 1;
     String pag = request.getParameter("pagina");
     if (pag==null || pag==""){
     RegistrosAEmpezar=0;
     PagAct=1;
     }
     else{
     RegistrosAEmpezar=(Integer.valueOf(pag).intValue()-1)* RegistrosAMostrar;
     PagAct=Integer.valueOf(pag).intValue();
     }%>
     <%
     int tmp = RegistrosAMostrar * ( PagAct-1);
     /// obtener los archivos
     String direccionFisica = application.getRealPath("/") + "respaldos/";
     String direccionListar = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/respaldos/";
     String[] listado = null; 
     File directorio = new File(direccionFisica); 
     out.print(direccionFisica);
     if(directorio.isDirectory()) 
     { 
        listado = directorio.list();
     }
     int valores = listado.length / (PagAct);
     int valores2 = valores -RegistrosAMostrar;
     if (valores2 <= 0)
         valores2 = 0;
     for (int x = valores-1; x >= valores2;x--)
     {
       if (listado[x].endsWith(".sql")){
        String descarga = direccionListar + listado[x];
        String sFichero = directorio + "/" + listado[x];
        File fichero = new File(sFichero);
        Double  tam = Double.valueOf(fichero.length()) / 1024.00; 
        tam = diferenciaDias.redondear(tam);
        System.out.print("backup: " + listado[x].toString());
         
     %>

     <tr id="grid">
     <td id="formnuevo"><%=listado[x] %></td>
     <td id="formnuevo"><%=tam %> KB</td>
     <td id="formnuevo"><a href='<%=descarga%>'> Descargar</a><br /></td>
     </tr>
     <%
     }}%>
     <%

     String NroRegistros =  String.valueOf(listado.length-1);

     int PagAnt=PagAct-1;
     int PagSig=PagAct+1;

     double PagUlt=Integer.valueOf(NroRegistros).intValue()/RegistrosAMostrar; //calculo cuantas paginas tendra mi paginacion
     int Res=Integer.valueOf(NroRegistros).intValue()%RegistrosAMostrar;
     if(Res>0){
     PagUlt=Math.floor(PagUlt)+1;
     }

     out.println("</tbody>");
     out.println("</table></div><br />");
     //principio del paginador
    
     out.println("<div style='width:300px; float:right'>");

     if(PagAct>(PaginasIntervalo+1)) {
     out.println("<a onclick=listabackup('1'); class='paginador'><< Primero</a>");
     out.println("&nbsp;");
     }
     for ( int i = (PagAct-PaginasIntervalo) ; i <= (PagAct-1) ; i ++) {
     if(i>=1) {
     out.println("<a onclick=\"listabackup('"+i+"')\" class='paginador'>"+i+"</a>");
     out.println("&nbsp;");
     }
     }
     out.println("<span class='paginadoractivo'>"+PagAct+"</span>");
     out.println("&nbsp;");
     for ( int i = (PagAct+1) ; i <= (PagAct+PaginasIntervalo) ; i ++) {
     if(i<=PagUlt) {
     out.println("<a onclick=\"listabackup('"+i+"')\" class='paginador'>"+i+"</a>");
     out.println("&nbsp;");
     }
     }
     if(PagAct<(PagUlt-PaginasIntervalo)) {
     out.println("<a onclick=\"listabackup('"+PagUlt+"')\" class='paginador'>Ultimo >></a>");
     }
     
     out.println("</div>");

     %>
   
   <!-- fin cambios paginacion -->