<%-- 
    Document   : actionRestaurar
    Created on : 12-07-2013, 11:31:20 AM
    Author     : jorge
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page  import="Conexion.conexion,java.io.BufferedReader,java.io.File,java.io.FileWriter,java.io.InputStreamReader" %>
<%@page  import="java.util.Calendar" %>

<%@ page import="org.apache.commons.fileupload.FileItem" %> 
<%@ page import="java.util.*" %> 
<%@ page import="org.apache.commons.fileupload.*" %> 
<%@ page import="org.apache.commons.fileupload.disk.*" %> 
<%@ page import="org.apache.commons.fileupload.servlet.*" %> 
<%@ page import="org.apache.commons.io.*" %> 
<%@ page import="java.io.*" %> 

<%
        String ubicacionArchivo = application.getRealPath("/")+ "respaldos/";
        String nombreArchivo = "";
        boolean tipo = false;
                // String direccion = application.getRealPath("/") + "respaldos/respaldo";
                //"/home/kaaf";
        DiskFileItemFactory factory = new DiskFileItemFactory();
        File file = null;
        factory.setSizeThreshold(1024); 
        factory.setRepository(new File(ubicacionArchivo));
        ServletFileUpload upload = new ServletFileUpload(factory);
        try
        {
            List<FileItem> partes = upload.parseRequest(request);

            for(FileItem item : partes)
            {
            file = new File( ubicacionArchivo, item.getName() );
            item.write(file);
            nombreArchivo = item.getName();
            }
            if (nombreArchivo.endsWith(".sql"))
            {
                tipo=true;
            }
           
        }
        catch(FileUploadException ex)
        {
           response.sendRedirect("restaurar.jsp?erno=12");
        }
 if (tipo){
        String q = ubicacionArchivo +  nombreArchivo;
        String[] restoreCmd = new String[]{"mysql ", "--user=" + "root", "--password=" + "123456",
                "-e", "source " + q};
        Process runtimeProcess;
        try {
 
            runtimeProcess = Runtime.getRuntime().exec(restoreCmd);
           /* int processComplete = runtimeProcess.waitFor();
 
            if (processComplete == 0) {
                System.out.println("Restored successfully!");
            } else {
                System.out.println("Could not restore the backup!");
            }*/
            response.sendRedirect("restaurar.jsp?exito=RDB");
        } catch (Exception ex) {
            ex.printStackTrace();
             response.sendRedirect("restaurar.jsp?erno=13");
        }
}
 else 
 {
     // borrar el archivo subido
     file.delete();
     response.sendRedirect("restaurar.jsp?erno=11");
 }
     
%>