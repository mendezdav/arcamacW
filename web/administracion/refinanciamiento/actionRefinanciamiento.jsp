<%-- 
    Document   : actionRefinanciamiento
    Created on : 07-15-2014, 10:19:49 PM
    Author     : jorge
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="Conexion.conexion"%>
<%
    /**Inicializando objeto logger**/
    Logger logger = Logger.getLogger(this.getClass().getName());
    int mensaje = 0;
    /**tomando parametros**/
    int tipo = Integer.parseInt(request.getParameter("tipo"));  //El tipo de valor a modificar: refinanciamiento, porcentaje, cantidad
    
    
    String valor = request.getParameter("valor");
    double valor_d = 0.00;
    try 
    {
        valor_d = Double.valueOf(valor);
    }
    catch(Exception e)
    {
        mensaje = 1; //Error de conversion
        logger.warning("Problema de conversion " + e.getMessage()); 
    }
    
    String query = "";
    switch(tipo){
        case 1: //Refinanciamiento
            query  = "update configuraciones set porcentaje_refinanciamiento=" + valor_d;
            break;
        case 2: //Tasa de interes maxima solicitada por el BCR
            query = "update configuraciones set porcentaje_maximo_bcr=" + valor_d;
            break;
        case 3: //Cantidad maxima a prestar
            query = "update configuraciones set cantidad_maxima_prestamo=" + valor_d;
            break;
        case 4: //Solicitud a partir de la cual se solicita mas de un fiador
            query = "update configuraciones set cantidad_minima_fiadores=" + valor_d;
            break;
        case 5:
            String id = request.getParameter("id");
            int id_d = 0;
            try 
            {
                id_d = Integer.valueOf(id);
                query = "update papeleria set costo = "+ valor_d + " where id = "+id_d;
            }
            catch(Exception e)
            {
                mensaje = 1; //Error de conversion
                logger.warning("Problema de conversion " + e.getMessage()); 
            }
            break;    
        default:
            response.sendRedirect("principal_configuraciones.jsp?erno=2");
            break;
    }
 
    conexion con = new conexion();
    
    boolean estado = con.actualizar(query);
    if (!estado){
        mensaje = 2; //Error al insertar
        logger.severe("Problema al actualizar tabla configuraciones");
    }
    else{
     if(mensaje == 0)  
         mensaje = 3; //Exito
    }
%>
 <div id="mensaje"><%=mensaje%></div>