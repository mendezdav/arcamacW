<%-- 
    Document   : principalRefinanciamiento
    Created on : 07-15-2014, 10:13:48 PM
    Author     : jorge
--%>


<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../../administracion/header.jsp" %>
<style>
    body{ padding: 30px 10px 20px 40px;}
    .container
    {
        max-width:  500px;
     //   margin: 0 auto;
        text-align: justify;
    }
    i{cursor: pointer; margin-left: 10px; }
    tr{margin-bottom: 20px;}
</style>
<script language="JavaScript">
    function envio()
    {
        document.formulario.submit();
    }
    /**
     * 
     * @param {string} id_caja id del textbox a modificar
     * @param {type} nuevo_valor Valor a modificar
     */
    function cambiarestado(id_caja, tipo){
        var url = "actionRefinanciamiento.jsp";
        var nuevo_valor = 0;
        if(id_caja != "" && tipo != ""){ //Validacion
            nuevo_valor = $("#" + id_caja).val();
            $.ajax({
               data:  {tipo: tipo, valor: nuevo_valor},
               type: "POST",
               url: url
              }
            ).done(function (respuesta){
                $("#respuesta").html(respuesta);
                var msj = $("#mensaje").html();
                if(msj == 1)
                    window.location.replace("principal_configuraciones.jsp?erno=1");
                if(msj == 2)
                    window.location.replace("principal_configuraciones.jsp?erno=2");
                if(msj == 3)
                    window.location.replace("principal_configuraciones.jsp?exito=1");
            }).fail(function(){
                window.location.replace("principal_configuraciones.jsp?erno=1");
            });
        }
    }
    
    function cambiarcosto(id_caja, tipo, id){
        var url = "actionRefinanciamiento.jsp";
        var nuevo_valor = 0;
        if(id_caja != "" && tipo != ""){ //Validacion
            nuevo_valor = $("#" + id_caja).val();
            $.ajax({
               data:  {tipo: tipo, valor: nuevo_valor, id: id},
               type: "POST",
               url: url
              }
            ).done(function (respuesta){
                $("#respuesta").html(respuesta);
                var msj = $("#mensaje").html();
                if(msj == 1)
                    window.location.replace("principal_configuraciones.jsp?erno=1");
                if(msj == 2)
                    window.location.replace("principal_configuraciones.jsp?erno=2");
                if(msj == 3)
                    window.location.replace("principal_configuraciones.jsp?exito=1");
            }).fail(function(){
                window.location.replace("principal_configuraciones.jsp?erno=1");
            });
        }
    }
</script>
<sql:query dataSource="jdbc/mysql" var="q1">
    SELECT porcentaje_refinanciamiento, cantidad_maxima_prestamo, cantidad_minima_fiadores, porcentaje_maximo_bcr FROM configuraciones
</sql:query>
<sql:query dataSource="jdbc/mysql" var="q2">
    SELECT id,de,a,costo FROM papeleria
</sql:query>
<div>
    <fieldset>
    <br />
    <h3>Configuraciones generales del sistema</h3><br />
    <c:forEach var="t1" items="${q1.rows}">
    <table>
        <tr>
            <td>Porcentaje m&iacute;nimo para refinanciamiento:</td>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">%</span>
                    <input class="form-control" type="text" name="porcentaje_refinanciamiento" id="porcentaje_refinanciamiento" 
                           value="${t1.porcentaje_refinanciamiento}" required autofocus />
                </div>
            </td>
            <td>
                <i class="fa fa-refresh fa-2x" onclick="cambiarestado('porcentaje_refinanciamiento', 1);"></i>
            </td>
        </tr>
        <tr>
            <td>Cantidad m&aacute;xima a prestar:</td>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input class="form-control" type="text" name="cantidad_maxima_prestamo" id="cantidad_maxima_prestamo" 
                           value="${t1.cantidad_maxima_prestamo}" required autofocus />
                </div>
            </td>
            <td>
                <i class="fa fa-refresh fa-2x" onclick="cambiarestado('cantidad_maxima_prestamo', 3);"></i>
            </td>
        </tr>
        <tr>
            <td>Cantidad a partir de la cual se solicita m&aacute;s de un fiador:</td>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input class="form-control" type="text" name="cantidad_minima_fiadores" id="cantidad_minima_fiadores" 
                           value="${t1.cantidad_minima_fiadores}" required autofocus />
                </div>
            </td>
           <td>
                <i class="fa fa-refresh fa-2x" onclick="cambiarestado('cantidad_minima_fiadores', 4);"></i>
            </td>
        </tr>
        <tr>
            <td>Tasa de inter&eacute;s m&aacute;xima aprobada por el BCR:</td>
            <td>
                <div class="input-group">
                    <span class="input-group-addon">%</span>
                    <input class="form-control" type="text" name="porcentaje_maximo_bcr" id="porcentaje_maximo_bcr" 
                           value="${t1.porcentaje_maximo_bcr}" required autofocus />
                </div>
            </td>
            <td>
                <i class="fa fa-refresh fa-2x" onclick="cambiarestado('porcentaje_maximo_bcr', 2);"></i>
            </td>
        </tr>
        
    </table>
    </c:forEach>
    <hr/>
    <h4>Costo de papelería por monto de préstamo</h4>
    <table>
        <c:forEach var="papeleria" items="${q2.rows}">
            <tr>
                <td style="padding-right: 30px;">De $${papeleria.de} a $${papeleria.a}</td>
                <td>
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input class="form-control" type="text" name="costo_${papeleria.id}" id="costo_${papeleria.id}" 
                               value="${papeleria.costo}" required />
                    </div>
                </td>
                <td>
                    <i class="fa fa-refresh fa-2x" onclick="cambiarcosto('costo_${papeleria.id}', 5, ${papeleria.id});"></i>
                </td>
            </tr>
        </c:forEach>
    </table>
    </fieldset>
</div>
<div id="respuesta" style="visibility: hidden;"></div>
<%@include file="../../administracion/footer.jsp" %>