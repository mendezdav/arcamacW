<%-- 
    Document   : principalRefinanciamiento
    Created on : 07-15-2014, 10:13:48 PM
    Author     : jorge
--%>


<%@page import="java.sql.ResultSet"%>
<%@page import="Conexion.conexion"%>
<%@include file="../../administracion/header.jsp" %>
<style>
    .container
    {
        max-width:  500px;
        margin: 0 auto;
        text-align: justify;
    }
</style>
<script language="JavaScript">
    function envio()
    {
        document.formulario.submit();
    }
</script>
<div class="container"><br />
    <center>
    <h2>Cambiar cantidad Refinanciamiento </h2>
    <br /><br /></center>
    <div align="center"><br /><br />
        
        <%
        // obtener valor actual
        conexion con = new conexion();
        String query = "select porcentaje_refinanciamiento from configuraciones";
        con.setRS(query);
        ResultSet rs = con.getRs();
        double porcentaje= 0.00;
        if (rs.next())
        {
            porcentaje = rs.getDouble("porcentaje_refinanciamiento");
        }
        con.cerrarConexion();
        
        %>
        <form action="actionRefinanciamiento.jsp" method="POST" name="formulario" id="formulario">
            
             <table class="table">
                <tr>
                    <td><b>Porcentaje actual:</b></td>
                    <td>
                         <div class="input-group">
                            <span class="input-group-addon">%</span>
                            <input class="form-control requerido" style="width: 100px;" type="text" name="porcentaje" id="porcentaje"
                               size="10"  value="<%= porcentaje%>" required autofocus />
                         </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <input type="button" class="btn btn-info" onclick="envio();" value="Guardar" >
                    </td>
                </tr>
               </table>

        </form>
    </div>
</div>
<%@include file="../../administracion/footer.jsp" %>