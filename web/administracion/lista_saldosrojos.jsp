<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<sql:query var="q1" dataSource="jdbc/mysql">
    SELECT count(*) as c FROM reportemora WHERE cuenta >=2;
</sql:query>
<sql:query var="q2" dataSource="jdbc/mysql">
    SELECT solicitante, codigoPrestamo,fecha, cuenta FROM reportemora WHERE cuenta >=2 LIMIT 5;
</sql:query>
<c:forEach var="info" items="${q1.rows}">
    <c:set var="numero" value="${info.c}" />
</c:forEach>
<c:forEach var="info_" items="${q2.rows}">
    <c:set var="solicitante" value="${info_.solicitante}" />
    <c:set var="codigoPrestamo" value="${info_.codigoPrestamo}" />
    <c:set var="fecha" value="${info_.fecha}" />
    <c:set var="cuenta" value="${info_.cuenta}" />
</c:forEach>
<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-warning"></i> Saldos Rojos <span class="badge">${numero}</span> <b class="caret"></b></a>
<ul class="dropdown-menu">
<c:choose>
    <c:when test="${numero == 0}">
        <li><span>No hay registros</span></li>
    </c:when>
    <c:when test="${numero > 0}">
                <li class="dropdown-header">${numero} Pr&eacute;stamos presentan mora de m&aacute;s de 2 meses</li>
                <li class="message-preview">
                  <a href="#">
                    <span class="name">
                        ${codigoPrestamo}
                    </span>
                    <span class="message">Pr&eacute;stamo realizado por: ${solicitante}</span>
                    <span class="time"><i class="fa fa-exclamation-circle"></i> Fecha establecida de pago:
                        <fmt:formatDate  value="${fecha}" pattern="dd-MM-yyyy" />
                    </span>
                    <span class="message">
                        Cuotas en mora: ${cuenta}
                    </span>  
                  </a>
                <li class="divider"></li>
                <li><a href="/arcamacW/ReporteMora?no_meses=2">Ver todos <span class="badge">${numero}</span></a></li>
    </c:when>
</c:choose>
</ul>
