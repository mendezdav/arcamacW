<%-- 
    Document   : EliminarFiadorAction
    Created on : 12-11-2013, 06:59:01 PM
    Author     : jorge
--%>
<%@page import="javax.swing.JOptionPane"%>
<%@page  language="java" session="true" %>
<%@page import="Conexion.conexion,java.sql.*" %>
<%
    // estableciendo conexion
    conexion con = new conexion();
    conexion conAux = new conexion();
    String qrPrestamo = "";
    ResultSet resultado = null;
    String estado = "";
    String codigoPrestamo  =  ""; 
    String qrInactivo = "";
    boolean prestamoActivo = false; // se asume que no hay prestamos activos
    // revisar si el fiador tiene algun prestamo
    // aunque esto siempre se cumplira con los nuevos cambios
    
    if (request.getParameter("idfiadores") != null)
    {
        // tomando id del fiador a intentar eliminar
        int idfiadores = Integer.parseInt(request.getParameter("idfiadores"));
        int totalFiadorPrestamos = -1;
        int totalPrestamosPagados = -1;
        // verificar si la totalidad de prestamos con dicho fiador, todos estan pagados
        /// total en prestamo_fiador
        String qrTotalFiadorPrestamo = "select count(*) as total from prestamo_fiador as pf inner join prestamos as p on p.codigoPrestamo = pf.id_prestamo where id_fiador=" + idfiadores;
        con.setRS(qrTotalFiadorPrestamo);
        resultado = con.getRs();
        try
        {
            resultado.next();
            totalFiadorPrestamos = resultado.getInt("total");
        }
        catch(Exception e1)
        {
            System.out.printf("Error qrTotal fiadores prestamos vacio");
        }
        // total de prestamos en estado "Pagado"
        String qrTotalPrestamosPagados = "select count(*) as total from prestamo_fiador as pf inner join prestamos as p on pf.id_prestamo = "
                + "p.codigoPrestamo where pf.id_fiador="+ idfiadores +" and p.estado='Pagado';";
        con.setRS(qrTotalPrestamosPagados);
        resultado = con.getRs();
        try
        {
            resultado.next();
            totalPrestamosPagados = resultado.getInt("total");
        }
        catch(Exception e1)
        {
            System.out.printf("Error qrTotal fiadores prestamos vacio");
        }
        // revisar si no existe un prestamos que contenga ese fiador, tomar en cuenta que el prestamo debe de estar diferente
        // a activo y en proceso!
        if (totalFiadorPrestamos != -1 && totalFiadorPrestamos > 0 && (totalFiadorPrestamos == totalPrestamosPagados))
        {
                qrPrestamo= "select pf.id_prestamo as codigoPrestamo,p.estado from prestamo_fiador as pf inner join prestamos as"
                         + " p on p.codigoPrestamo = pf.id_prestamo where id_fiador=" + idfiadores;
                con.setRS(qrPrestamo);
                resultado = (ResultSet) con.getRs();
                if (resultado.next())
                {
                    resultado.previous();
                    while(resultado.next())
                    {
                        // cambiar estadoPrestamo = 0, es decir a inactivo
                        codigoPrestamo  = resultado.getString("codigoPrestamo");
                        qrInactivo = "update prestamos set estadoPrestamo=0 where codigoPrestamo='" + codigoPrestamo + "'";
                        conAux.actualizar(qrInactivo);
                    }
                        //  A LEY SE QUE SON TODOS LOS PRESTAMOS
                        qrInactivo = "update fiadores set estadoFiador=0 where idfiadores='" + idfiadores + "'";
                        boolean estadoConsulta = conAux.actualizar(qrInactivo);
                        if (estadoConsulta)
                            response.sendRedirect("principalFiadores.jsp?exito=1");
                        else
                            response.sendRedirect("principalFiadores.jsp?erno=2");
                 }
            else
                response.sendRedirect("principalFiadores.jsp?erno=14");
            }
            else
                response.sendRedirect("principalFiadores.jsp?erno=14");
    }
%>
