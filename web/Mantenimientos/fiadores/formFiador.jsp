
<style>
     .tabla  tfoot, tr, td{padding: 8px; border-collapse: separate; border-spacing: 10px 10px 10px 0px;}
     .tabla{width:300px; margin-bottom: 20px; }
     .container{width: 500px; height: auto;}
</style>
<div class="container">
        <fieldset>
        <legend>Nuevo Fiador</legend><br />
        <form class="form-signin" action="agregarFiadorAction.jsp" method="post" name="formulario" id="formulario">
        
       <table class="tabla">
            <tr>
                <td><b>* Nombres:</b></td>
                <input type="hidden" name="id" value="<%=request.getParameter("idSolicitante") %>" />
                <td><br /><input type="text" class="form-control requerido" size="40" placeholder="Nombre" name="nombreFiador" id="nombreFiador" required onblur="validar('nombreFiador');" ></td>
                <td><div id='divnombreFiador'> </div></td>
           </tr>
           <tr>
                <td><b>* Apellido:</b></td>
                <td><br /><input type="text" class="form-control requerido"  size="40" placeholder="Apellido" name="apellidoFiador" id="apellidoFiador" required onblur="validar('apellidoFiador');" ></td>
                <td><div id='divapellidoFiador'> </div></td>
            </tr>
            
            <tr>
                <td><b>* Edad:</b></td>
                <td><br /><input type="number" min="18" max="85" size="40" class="form-control requerido" placeholder="Edad" name="edadF" id="edadF" required onblur="validar('edadF');" ></td>
                <td><div id='divedadF'> </div></td>
            </tr>
            <tr>
                <td><b>* DUI:</b></td>
                <td><br /><input type="text" size="40" class="form-control requerido" placeholder="DUI" name="DUIF" id="DUIF" size="10" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('DUIF');" /> </td>
                <td><div id='divDUIF'> </div></td>
        
            </tr>
          
            
            <tr>
                <td><b>* NIT:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="NIT" name="NITF" id="NITF" required pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" onblur="validar('NITF');" /></td>
                <td><div id='divNITF'> </div></td>
           </tr>
           <tr>
                <td><b>* Domicilio:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Domicilio" name="DomicilioF" id="DomicilioF" required onblur="validar('DomicilioF');" ></td>
                <td><div id='divDomicilioF'> </div></td>
            </tr>
           
            <tr>
                <td><b>* Profesi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Profesi&oacute;n" name="profesionF" id="profesionF" required onblur="validar('profesionF');" ></td>
                <td><div id='divprofesionF'> </div></td>
            </tr>
            <tr>
                <td><b>* Lugar Trabajo</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Lugar Trabajo" name="lugarTrabajoF" id="lugarTrabajoF" required onblur="validar('lugarTrabajoF');" ></td>
                <td><div id='divlugarTrabajoF'> </div></td>
            </tr>
            
            
            <tr>
                <td><b>* Cargo:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Cargo" name="cargoF" id="cargoF" required onblur="validar('cargoF');" ></td>
                <td><div id='divcargoF'> </div></td>
           </tr>
           <tr>
                <td><b>* Tel&eacute;fono Trabajo:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Tel&eacute;fono" name="telefono_trabajo"  id="telefono_trabajo" required onblur="validar('telefono_trabajo');" ></td>
                <td><div id='divtelefono_trabajo'> </div></td>
            </tr>
           
            <tr>
                <td><b>Extensi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Extensi&oacute;n" name="ext" id="ext" required ></td>
                <td><div id='divext'> </div></td>
            </tr>
            <tr>
                <td><b>* Direcci&oacute;n Particular:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Direcci&oacute;n" name="direccionParticular" id="direccionParticular" required  onblur="validar('direccionParticular');"></td>
                <td><div id='divdireccionParticular'> </div></td>
            </tr>
            
            <tr>
                <td><b>Tel&eacute;fono Residencia:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Tel. Residencia" name="telResidenciaF" id="telResidenciaF" required ></td>
                <td><div id='divtelResidenciaF'> </div></td>
            </tr>
            <tr>
                <td><b>Celular:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Celular" name="celularF" id="celularF" required ></td>
                <td><div id='divcelularF'> </div></td>
            </tr>
            
                       
            <tr>
                <td><b>* Lugar/Fecha</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="lugar_fecha" name="lugar_fecha" id="lugar_fecha" required onblur="validar('lugar_fecha');" ></td>
                <td><div id='divlugar_fecha'> </div></td>
            </tr>
           
            <tr>
                <td colspan="6"><center><br />
                      <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                  if (estado.equals("1"))
                  {
                      out.print("<div class=\"alert alert-success\">Fiador Agregado Exitosamente!</div>");
                  }
                  if (estado.equals("2"))
                  {
                      out.print("<div class=\"alert alert-danger\">Lo Sentimos ha ocurrido un error!</div>");
                  }
              }
              
              %>
            <br />
                      <input type="submit" class="btn btn-info" value="Agregar Fiador" />
            </center></td>
            </tr>
           
       </table>
                  
        <br /><br />
        </form>
        </fieldset>
    </div> <!-- /container -->