<%-- 
Document   : principalFIadores
Created on : 08-12-2014, 11:04:11 PM
Author     : jorge
--%>

<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../../js/selectAjax.js"></script>
<script language="javascript" src="../../js/simple-dialog.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../css/simple-dialog.min.css" media="screen" />
<script lang="javascript" src="../../js/selectAjax.js"></script>
<script lang="javascript" src="../../js/validacion.js"></script>
<script lang="javascript" src="../../js/paginador/funciones.js"></script>

<style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
      
             .container
            {
                max-width:  900px;
                margin: 0 auto;
                text-align: justify;
               
            }
    </style>
    <link rel="stylesheet" type="text/css" href="../css/simple-dialog.min.css" media="screen" />
        <style>
             body 
            {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
            .table
            {
                width:750px;
                align:center;
            }
             .container
            {
                max-width:  1000px;
                margin: 0 auto;
                margin-left: 10%; 
            }
            .container h2
            {
                font-family: Century Gothic; 
            }
            legend{margin-left: 5%;}
            .spinner {
                position: fixed;
                top: 29.1%;
                left: 42.6%; padding: 10px 10px 10px 10px;
                margin-left: -50px; /* half width of the spinner gif */
                margin-top: -50px; /* half height of the spinner gif */
                text-align:center;
                z-index:1000000000;
                overflow: auto;
                opacity: 0.9;
                background-color: ghostwhite;
                width: 300px; /* width of the spinner gif */
                height: 202px; /*hight of the spinner gif +2px to fix IE8 issue */
            }
            .spinner p{text-align: justify; font-family: "Century Gothic"; margin-top: 40px; font-weight: bold;}
        </style>
  </head>       
        <div class="container">
        
            <h3 style="margin-left: 50px;">Administrar Fiadores</h3><p />
        
        <br /><br />
        <%
        String criterio  = request.getParameter("criterio");
        String pagR = request.getParameter("pag");
        
        if (criterio == null || criterio.equals("") ) // si criterio no es nulo, colocar en cuadro busqueda
        {
        %>
        <body onload="listaproductoss('<%=criterio%>','<%=pagR%>',null,'6')">
        <%
        }
        if ( pagR != null && pagR !="" )
        {
        %>
    <body onload="paginar('mostrarFiadorMantenimiento.jsp', 'txtNombre','busqueda',<%=pagR%>,'principalFiadores.jsp','6');" >
    <%}else
        {
         %>
         <body>
         <%
        }
            %>
        <%
            // JOptionPane.showMessageDialog(null,pagR);
             if (criterio != null && criterio !="" && pagR != null && pagR !="" ) // si criterio no es nulo, colocar en cuadro busqueda
            {
        
            %>
            <div class="input-group" style='width: 450px; float: left; margin-left: 50px;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control' value='<%=criterio%>' 
                                placeholder="Fiador"  
                                oninput="paginar('mostrarFiadorMantenimiento.jsp', 'txtNombre','busqueda',1,'principalFiadores.jsp','6');" />
             </div> 
                                
             <br /><br />
             <center>
                    <div id="busqueda">
                    </div>
             </center>
             <% }
            else
            { 
            %>
                 <div class="input-group" style='width: 450px; float: left;margin-left: 50px;'>
                         <span class="input-group-addon"><i class='fa fa-search'></i></span>
                         <input type='text' name='txtNombre' id='txtNombre' class='form-control'
                                placeholder="Fiador"  
                                oninput="paginar('mostrarFiadorMantenimiento.jsp', 'txtNombre','contenedor', 1,'principalFiadores.jsp','6');" />
             </div>
             <br /><br /> <br /><br />
             <center>
                    <div id="contenedor">
                    </div>
             </center>
                
            <%}%>
 
<%@include file="../../administracion/footer.jsp" %>
