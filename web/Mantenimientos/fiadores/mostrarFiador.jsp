<%-- 
    Document   : mostrarFiador
    Created on : 12-10-2013, 03:21:57 PM
    Author     : jorge
--%>
    <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <style media="all" type="text/css" >
        @import url("../../css/screen.css");
    </style>
        <%
            String criterio; 
            
            criterio = request.getParameter("criterio"); 
                                      
            if(criterio != null)
             {   
          %>
    <sql:query var="q1" dataSource="jdbc/mysql">
            select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI  from solicitante  where 
            concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) like '%<%= criterio %>%'
    </sql:query>
        
    <ajax:displayTag id="displayTagFrame" ajaxFlag="displayAjax">
        
     <display:table  id="usuarios" name="${q1.rows}" pagesize="15" export="false">
            <display:column title="Solicitante" property="solicitante" url="/Mantenimientos/fiadores/verDatos_solicitante.jsp" paramId="id" paramProperty="idSolicitante"  sortable="false" />
            <display:column title="DUI" property="DUI" sortable="false" />
            <display:column  value="Seleccionar" url="/Mantenimientos/fiadores/agregarFiadorPaso2.jsp" paramId="idSolicitante" paramProperty="idSolicitante"  style="text-align:center;"/>
     </display:table> 
        
    </ajax:displaytag>
              <%
                  }else // mostrar todos los solicitantes 
                    {%>
                    
        <sql:query var="q1" dataSource="jdbc/mysql">
            select concat(nombre1,' ',nombre2, ' ', apellido1,' ', apellido2) as solicitante,idSolicitante,DUI  from solicitante
        </sql:query>
        
    <ajax:displayTag id="displayTagFrame" ajaxFlag="displayAjax">
        
     <display:table  id="usuarios" name="${q1.rows}" pagesize="15" export="false">
            <display:column title="Solicitante" property="solicitante" url="/Mantenimientos/fiadores/verDatos.jsp" paramId="idfiadores" paramProperty="idfiadores"  sortable="false" />
            <display:column title="DUI" property="DUI" sortable="false" />
            <display:column  value="Seleccionar" url="/Mantenimientos/fiadores/agregarFiadorPaso2.jsp" paramId="idSolicitante" paramProperty="idSolicitante"  style="text-align:center;"/>
      </display:table>           
    </ajax:displaytag>
                    <%
                
                    }
              %>