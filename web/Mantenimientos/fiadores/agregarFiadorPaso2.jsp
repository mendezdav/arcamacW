<%@page  import="java.util.Date" %>
<%@page import="Conexion.conexion, java.sql.*" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<style type="text/css">
     body {
          padding-top: 60px;
          padding-bottom: 60px;
         /* background-color: #eee;*/
        }
        .form-signin {
          max-width: 650px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        
        .container{max-width:  700px;margin: 0 auto; text-align: justify;}
        .container h2,h3 {font-family: Century Gothic; text-align: left; }
        legend{text-align: left;}
        .table{width:800px;}
        
    </style>
 
  <div class="container">
       
      <center>
        <h2 class="form-signin-heading">Nuevo Fiador</h2><br />
        <form class="form-signin" action="agregarFiadorAction.jsp?type=new" method="post" name="formulario" id="formulario">
        
       <table class="table">
            <tr>
                <td><b>* Nombres:</b></td>
                <input type="hidden" name="idSolicitante" value="<%=request.getParameter("idSolicitante") %>" /> 
                <input type="hidden" name="url" value="<%=ruta%>/Mantenimientos/fiadores/agregarFiadorPaso2.jsp" />
                <td><br /><input type="text" class="form-control requerido" size="40" placeholder="Nombre" name="nombreFiador" id="nombreFiador" required onblur="validar('nombreFiador');" ></td>
                <td><div id='divnombreFiador'> </div></td>
           
                <td><b>* Apellido:</b></td>
                <td><br /><input type="text" class="form-control requerido"  size="40" placeholder="Apellido" name="apellidoFiador" id="apellidoFiador" required onblur="validar('apellidoFiador');" ></td>
                <td><div id='divapellidoFiador'> </div></td>
            </tr>
            
            <tr>
                <td><b>* Edad:</b></td>
                <td><br /><input type="number" min="18" max="85" size="40" class="form-control requerido" placeholder="Edad" name="edadF" id="edadF" required onblur="validar('edadF');" ></td>
                <td><div id='divedadF'> </div></td>
            
                <td><b>* DUI:</b></td>
                <td><br /><input type="text" size="40" class="form-control requerido" placeholder="DUI" name="DUIF" id="DUIF" size="10" pattern="^[0-9]{8}-[0-9]{1}$" required onblur="validar('DUIF');" /> </td>
                <td><div id='divDUIF'> </div></td>
            </tr>
            <tr>
                <td><b>* NIT:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="NIT" name="NITF" id="NITF" required pattern="^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}$" onblur="validar('NITF');" /></td>
                <td><div id='divNITF'> </div></td>
           
                <td><b>Domicilio:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Domicilio" name="DomicilioF" id="DomicilioF" onblur="validar('DomicilioF');" ></td>
                <td><div id='divDomicilioF'> </div></td>
            </tr>
           
            <tr>
                <td><b>Profesi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Profesi&oacute;n" name="profesionF" id="profesionF" onblur="validar('profesionF');" ></td>
                <td><div id='divprofesionF'> </div></td>
            
                <td><b>Lugar Trabajo</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Lugar Trabajo" name="lugarTrabajoF" id="lugarTrabajoF" onblur="validar('lugarTrabajoF');" ></td>
                <td><div id='divlugarTrabajoF'> </div></td>
            </tr>
            
            
            <tr>
                <td><b>* Cargo:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Cargo" name="cargoF" id="cargoF" required onblur="validar('cargoF');" ></td>
                <td><div id='divcargoF'> </div></td>
           
                <td><b>Tel&eacute;fono Trabajo:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Tel&eacute;fono" name="telefono_trabajo"  id="telefono_trabajo" onblur="validar('telefono_trabajo');" ></td>
                <td><div id='divtelefono_trabajo'> </div></td>
            </tr>
           
            <tr>
                <td><b>Extensi&oacute;n:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Extensi&oacute;n" name="ext" id="ext" ></td>
                <td><div id='divext'> </div></td>
            
                <td><b>* Direcci&oacute;n Particular:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Direcci&oacute;n" name="direccionParticular" id="direccionParticular" required  onblur="validar('direccionParticular');"></td>
                <td><div id='divdireccionParticular'> </div></td>
            </tr>
            <tr>
                <td><b>Departamento:</b></td>
                <td>
                    <select name="id_departamento" id="id_departamento" class="form-control" onchange="cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0)" style="width: 150px; display: inline;">
                            <sql:query dataSource="jdbc/mysql" var="depto">
                                SELECT id_departamento, departamento FROM departamento</sql:query>
                            <c:forEach var="depto_f" items="${depto.rows}">
                                <option value="${depto_f.id_departamento}">${depto_f.departamento}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td><div id='divid_depto'></div></td>
                </tr>
                <tr>
                    <td><b>Municipio:</b></td>
                    <td>
                        <select name="id_municipio" id="id_municipio" class="form-control" style="width: 150px; display: inline;"></select>
                    </td>
                    <td><div id='divid_municipio'></div></td>
                </tr>
            <tr>
                <td><b>Tel&eacute;fono Residencia:</b></td>
                <td><br /><input type="text" class="form-control" placeholder="Tel. Residencia" name="telResidenciaF" id="telResidenciaF"  ></td>
                <td><div id='divtelResidenciaF'> </div></td>
         
                <td><b>*Celular:</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="Celular" name="celularF" id="celularF" required ></td>
                <td><div id='divcelularF'> </div></td>
            </tr>
            
            <!---           
            <tr>
                <td><b>* Lugar/Fecha</b></td>
                <td><br /><input type="text" class="form-control requerido" placeholder="lugar_fecha" name="lugar_fecha" id="lugar_fecha" required onblur="validar('lugar_fecha');" ></td>
                <td><div id='divlugar_fecha'> </div></td> -->
            </tr>
       </table>
       <fieldset>
                <h3>Informaci&oacute;n financiera</h3>
                <table class='table'>
                    <tr>
                        <td><b>*Salario:</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='salario' id='salario' class='form-control requerido' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' placeholder="Salario" required onblur="validar('salario');" />
                            </div>
                        </td>
                        <td><div id='divsalario' class='infor'></div></td>
                        <td rowspan="5"><b>*Descuentos (deducciones):</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='deducciones' id='deducciones' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' placeholder="Descuento" required onblur="validar('deducciones');" />
                            </div>
                        </td>
                        <td><div id='divdeducciones' class='infor'></div></td>
                    </tr>
                    <tr>
                        <td><b>Liquidez</b></td>
                        <td>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                    <input type='text' name='liquidez' id='liquidez' pattern='^[0-9]{1,5}.?[0-9]{1,2}?$' class='form-control requerido' placeholder="Liquidez" readonly />
                            </div>
                        </td>
                    </tr>
                     <tr>
                <td colspan="6"><center><br />
                      <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                  if (estado.equals("1"))
                  {
                      out.print("<div class=\"alert alert-success\">Fiador Agregado Exitosamente!</div>");
                  }
                  if (estado.equals("2"))
                  {
                      out.print("<div class=\"alert alert-danger\">Lo Sentimos ha ocurrido un error!</div>");
                  }
              }
              
              %>
            <br />
                      <input type="submit" class="btn btn-info" value="Agregar Fiador" />
            </center></td>
            </tr>
                </table>
        <br />
        <br />
        </form>
     </center>   
    </div> <!-- /container -->
    <script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
<%@include file="../../administracion/footer.jsp" %>
<script language="javascript">
    $(function(){
       $("#deducciones").blur(function (){
          if($("#salario").val() == "" || $("#deducciones").val() == "")
              alert("Los campos Salario y deducciones son obligatorios");
          else
          {
              $("#liquidez").val($("#salario").val() - $("#deducciones").val());
              if($("#liquidez").val() <= 0){
                  $("#liquidez").css("border-color","#a94442");}
          }
       });
       /**Cargando los municipios*/
        cargar_municipio('id_departamento','id_municipio','<%=ruta%>/administracion/municipios.jsp',0);
    });
</script>