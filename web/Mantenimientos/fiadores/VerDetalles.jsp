<%-- 
    Document   : VerDetalles
    Created on : 06-25-2014, 05:15:56 AM
    Author     : jorge
--%>

<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="clases.diferenciaDias"%>
<!-- incluyendo taglib para jstl -->
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@taglib  prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib  prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="../../administracion/header.jsp" %>
<%
    diferenciaDias df = new diferenciaDias();
%>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;border: none;}
    .tg td{font-family:Calibri, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
    .tg th{font-family:Calibri, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
    .tg .tg-e3zv{font-weight:bold;}
    .tg .tg-p7ly{font-weight:bold;font-size:20px;text-align:center}
    .tg .tg-733k{font-family:"Calibri", "Corbel", sans-serif !important;}
    .container{max-width: 80%;margin:30px 0px 20px 0;margin-left: 30%;}
     body{padding-top: 30px;}
     .center
     {
         display: block;
         margin-left: auto ;
         margin-right: auto ;
     }
</style>
 <ol class="breadcrumb">
                <li><a href="<%=ruta%>/panelAdmin.jsp">Inicio</a></li>
                <li><a href="<%=ruta%>/Mantenimientos/fiadores/principalFiadores.jsp">Fiadores</a></li>
                
 </ol>
<div class="container">
    <!--Capturando parametro !-->
    <c:if test="${!empty param.idfiadores}">
        <c:set var="idFiadores" value="${param.idfiadores}" />
    </c:if>
    <!--Estableciendo consulta para fiador!-->
    <sql:query dataSource="jdbc/mysql" var="q1">
        select concat(nombreFiador, ' ' , apellidoFiador) as nombre, fiadores.* from fiadores WHERE idfiadores = "${idFiadores}"
    </sql:query>
    
    <c:forEach var="info" items="${q1.rows}">
        <c:set var="nombre" value="${info.nombre}" />
        <c:set var="domicilio" value="${info.DomicilioF}" />
        <c:if test="${empty domicilio}">
            <c:set var="domicilio" value="----------" />
        </c:if>
        <c:set var="apellido" value="${info.apellidoFiador}" />
        <c:if test="${empty apellido}">
            <c:set var="apellido" value="----------" />
        </c:if>
        <c:set var="profesion" value="${info.profesionF}" />
        <c:if test="${empty profesion}">
            <c:set var="profesion" value="----------" />
        </c:if>
        <c:set var="lugarTrabajo" value="${info.lugarTrabajoF}" />
        <c:if test="${empty lugarTrabajo}">
            <c:set var="lugarTrabajo" value="----------" />
        </c:if>
        <c:set var="cargo" value="${info.cargoF}" />
        <c:if test="${empty cargo}">
            <c:set var="cargo" value="----------" />
        </c:if>
        <c:set var="telefono_trabajo" value="${info.telefono_trabajo}" />
        <c:if test="${empty telefono_trabajo}">
            <c:set var="telefono_trabajo" value="----------" />
        </c:if>
        <c:set var="direccionParticular" value="${info.direccionParticular}" />
        <c:if test="${empty direccionParticular}">
            <c:set var="direccionParticular" value="----------" />
        </c:if>
        <c:set var="telResidenciaF" value="${info.telResidenciaF}" />
        <c:if test="${empty telResidenciaF}">
            <c:set var="telResidenciaF" value="----------" />
        </c:if>
        <c:set var="ext" value="${info.ext}" />
        <c:if test="${empty ext}">
            <c:set var="ext" value="----------" />
        </c:if>
        <c:set var="lugar_fecha" value="${info.lugar_fecha}" />
        <c:if test="${empty lugar_fecha}">
            <c:set var="lugar_fecha" value="----------" />
        </c:if>
        <c:set var="deducciones" value="${info.deducciones}" />
        <c:if test="${empty deducciones}">
            <c:set var="deducciones" value="----------" />
        </c:if>
        <c:set var="liquidez" value="${info.liquidez}" />
        <c:if test="${empty liquidez}">
            <c:set var="liquidez" value="----------" />
        </c:if>
 <div class="center">
    <table class="tg" >
        <tr>
          <th class="tg-p7ly" colspan="4">DETALLE DE FIADOR</th>
        </tr>
        <tr>
          <td class="tg-e3zv">Nombre:</td>
          <td class="tg-733k">${nombre}</td>
        </tr>
        
        <tr>
          <td class="tg-e3zv">Docimicilio:</td>
          <td class="tg-733k">${domicilio}</td>
        </tr>
        
        <tr>
          <td class="tg-e3zv">Telefono:</td>
          <td class="tg-733k">${telResidenciaF}</td>
          <td class="tg-e3zv">Celular:</td>
           <td class="tg-733k">${info.celularF}</td>
        </tr>
        
        <tr>
          <td class="tg-e3zv">DUI:</td>
          <td class="tg-733k">${info.DUIF}</td>
          <td class="tg-e3zv">NIT:</td>
          <td class="tg-031e">${info.NITF}</td>
        </tr>
        
        <tr>
          <td class="tg-e3zv">Profesi&oacute;n:</td>
          <td class="tg-733k">${profesion}</td>
        </tr>
        <tr>
          <td class="tg-e3zv">Lugar de Trabajo:</td>
          <td class="tg-733k">${lugarTrabajo}</td>
        </tr>
    
         <tr>
          <td class="tg-e3zv">Cargo:</td>
          <td class="tg-733k">${cargo}</td>
          <td class="tg-031e">Telefono Trabajo: </td>
          <td class="tg-031e">${telefono_trabajo}</td>
        </tr>
        
         <tr>
          <td class="tg-e3zv">Direccion Particular:</td>
          <td class="tg-733k">${direccionParticular}</td>
          <td class="tg-031e"></td>
          <td class="tg-031e"></td>
        </tr>
        
        <tr>
          <td class="tg-e3zv">Lugar y Fecha:</td>
          <td class="tg-733k">${lugar_fecha}</td>
          <td class="tg-e3zv"></td>
          <td class="tg-031e"></td>
        </tr>
        
        <tr>
          <td class="tg-e3zv">Salario:</td>
          <td class="tg-733k">$ ${info.salario}</td>
          <td class="tg-e3zv">Deducciones:</td>
          <td class="tg-031e">$ ${deducciones}</td>
        </tr>
        
         <tr>
          <td class="tg-e3zv">Liquidez:</td>
          <td class="tg-733k">$ ${liquidez}</td>
          <td class="tg-e3zv"></td>
          <td class="tg-031e"></td>
        </tr>
        
   </table>
          </div>
    </c:forEach>
   <!-- <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-4">
            <a class="btn btn-info" onclick="">
                Imprimir
            </a>
        </div>
    </div>-->
</div>
<%@include file="../../administracion/footer.jsp" %>