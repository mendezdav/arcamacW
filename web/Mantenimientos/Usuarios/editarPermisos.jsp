<%@page import="Conexion.conexion, java.sql.*,java.util.Date" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
<script type="text/javascript" src="../../js/permisos.js"></script>
<div class="container" ng-app="permisosApp" ng-controller="permisosController" style="margin: 100px;">
    <%
        try{
            int idUsuario = Integer.parseInt(request.getParameter("idUsuario"));
            String usuario = "";
            // verificar si el usuario existe
            String query = "SELECT usuario FROM usuarios WHERE idUsuarios="+idUsuario;
            conexion con = new conexion();
            con.setRS(query);
            ResultSet resultado = con.getRs();
            if(!resultado.next()){
                out.print("<h3 style='margin-top: 100px;margin-left: 450px;margin-bottom: 200px;'>El usuario no existe!</h3>");
            }else{
                usuario = resultado.getString("usuario");
                %>
                <input type="hidden" ng-model="usuario" ng-init="usuario='<% out.print(usuario); %>'"/>
                <h3>Editar permisos del usuario: {{usuario}}</h3>
                <p><br/></p>
                <table class="table" style="width: 600px;">
                    <tr>
                        <thead>
                            <th>
                                M�dulo
                            </th>
                            <th>
                                Permiso
                            </th>
                            <th>
                                Acceso
                            </th>
                        </thead>
                    </tr>
                    <tr ng-repeat="permiso in permisos">
                        <td>{{permiso.modulo}}</td>
                        <td>{{permiso.uri}}</td>
                        <td>
                            <button class="button" ng-click="concederAcceso(permiso.uri);" ng-show="{{permiso.acceso}}==null">Conceder acceso</button>
                            <button class="button" ng-click="denegarAcceso(permiso.acceso)" ng-show="{{permiso.acceso}}!=null">Denegar acceso</button>
                        </td>
                    </tr>
                </table>
                <%
            }
        }catch(Exception e){
            out.print("<h3 style='margin-top: 100px;margin-left: 450px;margin-bottom: 200px;'>El usuario no existe!</h3>");
        }
    %>
</div>
<%@include file="../../administracion/footer.jsp" %>