<%@page import="java.sql.*"%>
<%@page import="Conexion.conexion"%>
<%@page contentType="application/json" pageEncoding="UTF-8"%>
<%
    String json_response = "";
   
    String usuario = request.getParameter("usuario");
    //System.out.print(usuario);
    try{
        
        String query = "SELECT vistas.uri as uri, vistas.modulo as modulo, vistas.id as id, acceso_vistas.id as acceso FROM acceso_vistas INNER JOIN vistas ON vistas.uri = acceso_vistas.uri WHERE usuario='"+usuario+"'";
        conexion con = new conexion();
        con.setRS(query);
        ResultSet rs = con.getRs();
        json_response += "[";
        while(rs.next()){
            json_response += "{";
            json_response += "\"uri\":\""+rs.getString("uri")+"\",";
            json_response += "\"id\":"+rs.getInt("id")+",";
            json_response += "\"modulo\":\""+rs.getString("modulo")+"\",";
            json_response += "\"acceso\":\""+rs.getString("acceso")+"\"";
            json_response += "},";
        }
        
        query = "SELECT vistas.uri as uri, vistas.modulo as modulo, vistas.id as id, acceso_vistas.id as acceso FROM vistas LEFT JOIN acceso_vistas on acceso_vistas.uri = vistas.uri WHERE vistas.uri NOT IN (SELECT vistas.uri as uri FROM acceso_vistas INNER JOIN vistas ON vistas.uri = acceso_vistas.uri WHERE usuario='"+usuario+"')";
        con.setRS(query);
        rs = con.getRs();
        
        while(rs.next()){
            json_response += "{";
            json_response += "\"uri\":\""+rs.getString("uri")+"\",";
            json_response += "\"id\":"+rs.getInt("id")+",";
            json_response += "\"modulo\":\""+rs.getString("modulo")+"\",";
            json_response += "\"acceso\":\""+rs.getString("acceso")+"\"";
            json_response += "},";
        }
        
        json_response += "{}]";
    }catch(Exception s){
        json_response = "{}";
    }
 
    out.write(json_response);
%>
