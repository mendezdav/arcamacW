<%-- 
    Document   : ModificarUsuarios
    Created on : 10-21-2013, 11:40:13 AM
    Author     : jorge
--%>

<%@page import="Conexion.conexion, java.sql.*,java.util.Date" %>
<%@page  session="true" language="java" %>
<%@include file="../../administracion/header.jsp" %>
       <style type="text/css">
     body {
          padding-top: 40px;
          padding-bottom: 40px;
         /* background-color: #eee;*/
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }
        
        .container
        {
            max-width:  500px;
            margin: 0 auto;
            text-align: justify;
        }
      
    </style>
      <script lang="javascript" src="<%=ruta%>/js/validacion.js"></script>
 <%! String idUsuarios = ""; %>
 <%! ResultSet resultado = null; %>
 
 
        <%
            // estableciendo conexion
            conexion con = new conexion();
            String qr ="";
            int perfil = 0; // sirve para ver de donde a venido la peticion
            if (request.getParameter("idUsuarios") != null) // viene del mantenimiento
            {
                qr ="select * from Usuarios where idUsuarios='"+request.getParameter("idUsuarios")+"'";
                perfil = 0;
            }
            else { // viene del perfil
                String usuario = SessionActual.getAttribute("usuario").toString();
                qr = "select * from Usuarios where usuario='"+usuario+"'";
                perfil = 1;
                // obteniendo valor del idUsuarios
            }
            
            SessionActual.setAttribute("perfil", perfil);
            
            con.setRS(qr);
            resultado = (ResultSet) con.getRs();
            resultado.next();
       
        %>
  <div class="container">

        <form class="form-signin" action="ModificarUsuarioAction.jsp" method="post">
        <center>
        <h2 class="form-signin-heading">Modificar Usuario</h2><br /></center>
        <table>
           <tr>
               <td><b>ID Usuario:</b></td>
               <td> <input type="text" class="form-control" placeholder="id" name="id" value="<%=resultado.getString("idUsuarios") %>" autofocus ReadOnly></td>
           </tr>
           
           <tr>
               <td><b>Nombres:</b></td>
               <td> <input type="text" class="form-control requerido" placeholder="Nombres" name="nombre" id="nombre" autofocus value="<%=resultado.getString("nombreUsuario") %>" required onblur="validar('nombre');" ></td>
               <td><div id='divnombre'> </div></td>
           </tr>
           
           <tr>
               <td><b>Apellidos:</b></td>
               <td><input type="text" class="form-control requerido" placeholder="Apellidos" name="apellido" id="apellido" autofocus value="<%=resultado.getString("apellidoUsuario") %>" required onblur="validar('apellido');" ></td>
               <td><div id='divapellido'> </div></td>
           </tr>
           
           <tr>
               <td><b>Usuario:</b></td>
               <td><input type="text" class="form-control requerido" placeholder="Usuario" name="usuario" id="usuario" autofocus value="<%=resultado.getString("usuario") %>" required onblur="validar('usuario');" ></td>
               <td><div id='divusuario'> </div></td>
           </tr>
           
           <tr>
               <td><b>Email:</b></td>
               <td><input type="email" class="form-control requerido" placeholder="Email" name="email" id="email" autofocus value="<%=resultado.getString("email") %>" required onblur="validar('email');" ></td>
               <td><div id='divemail'> </div></td>
           </tr>
           
           <tr>
               <td><b>Tipo Usuario:</b></td>
               <td><br/> <center>
                      <%
              // si el tipoUsuario = Adminsitrador puede cambiar!!
              if (SessionActual.getAttribute("tipoUsuario").equals("Administrador"))
              {
           %>
        <select name="tipousuario">
            <%
                String id = resultado.getString("idUsuarios");
             // llenando el combobox para que salga de primero el tipo de usuario actual
              con.setRS("select t.tipoUsuario, u.idUsuarios from tipoUsuario as t inner join usuarios "
                      + "as u on u.tipoUsuario = t.tipoUsuario where u.idUsuarios =" + id );
              ResultSet resultado = (ResultSet)con.getRs();
              while (resultado.next())
              {
              %>
              <option value="<%=resultado.getString("tipoUsuario") %>"><%=resultado.getString("tipoUsuario") %></option>
              <%
              }
                            
              //llenando el combo con los tipo de usuarios que NO es el usuario actualmente
              con.setRS("select t.tipoUsuario, u.idUsuarios from tipoUsuario as t inner join usuarios "
                      + "as u on u.tipoUsuario <> t.tipoUsuario where u.idUsuarios =" + id );
              ResultSet resultado2 = (ResultSet)con.getRs();
              while (resultado2.next())
              {
              %>
              <option value="<%=resultado2.getString("tipoUsuario") %>"><%=resultado2.getString("tipoUsuario") %></option>
              <%
              }
              
            
            %>
            </select><p /><br /></center>
            <% 
               } // fin del if si es administrador
                else {
                      %>
                      <input type="text" class="form-control" placeholder="tipoUsuario" name="tipousuario" value="<%=resultado.getString("tipoUsuario") %>" autofocus ReadOnly>
                      <%
                 }
            %>
               </td>
           </tr>
           <% 
            if (request.getParameter("idUsuarios") != null) // viene del mantenimiento
            {
                
           %>
           <tr>
               <td></td>
               <td><a href="editarPermisos.jsp?idUsuario=<%out.print(request.getParameter("idUsuarios"));%>"><i class="fa fa-lock"></i> Editar permisos de usuario</a></td>
           </tr>
           <%    
            }
           %>   
           <tr>
               <td colspan="2"><center>
                   <br /><br />
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Modificar</button>
               </center></td>
           </tr>
        </table>
        
          <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                  if (estado.equals("1"))
                  {
                      out.print("<font color=\"red\">Usuario modificado Exitosamente!</font>");
                  }
                  if (estado.equals("2"))
                  {
                      out.print("<font color=\"red\">Lo Sentimos ha ocurrido un error!</font>");
                  }
              }
              
           %>
        
 </div>
<%@include file="../../administracion/footer.jsp" %>