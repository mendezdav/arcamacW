<%-- 
    Document   : principal
    Created on : 12-07-2013, 08:33:44 PM
    Author     : jorge
--%>



<%@page import="java.text.SimpleDateFormat,java.util.Date" session="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../administracion/header.jsp" %>

<style>
           
             .container
            {
                max-width: 500px;
                margin: 0 auto;
                text-align: justify;
               
            }
            
          
        </style>
<div class="container">
      
    <div align="center"><br /><br />
        <h2>Perfil Usuario <b> <%=usuario %></b></h2><br />
        
    <div class="panel panel-default">
        <div class="panel-body">
          <table>
            <tr>
                <td><b>Conectado como:</b></td><td><%= usuario %></td>
            </tr>
            
            <tr>
                <td><b>Tipo Usuario:</b></td><td><%= tipoUsuario %></td>
            </tr>
            
            <tr>
                <td><b>Nombre:</b></td>
                <td><%= nombreUsuario + " " + apellidoUsuario %>
                </td>
            </tr>
            
            
            
            <tr>
                <td><b>Hora Sesi&oacute;n:</b></td><td>
                    <%
                    Date inicio = new Date(SessionActual.getCreationTime());
                    long duracion = SessionActual.getLastAccessedTime() - SessionActual.getCreationTime() ;
                    Date duracionDate = new Date(duracion);
                    SimpleDateFormat dt = new SimpleDateFormat("hh:mm:ss");
                    out.print(dt.format(inicio)); 
                    %></td>
            </tr>
            
            <tr>
                <td><b>Duraci&oacute;n Sesi&oacute;n:</b></td><td>
                    <%
                    if (duracionDate.getHours()-18 == 0 )
                    { out.print(duracionDate.getMinutes() + " Minutos, " + duracionDate.getSeconds() + " segundos");
                    }
                    else 
                    { out.print(duracionDate.getHours()-18 + " Horas, " + duracionDate.getMinutes() + " Minutos, " + duracionDate.getSeconds() + " segundos");
                    }
                   
                    %>
                </td>
            </tr>
         
            </table>
         <div>
      </div>
    </div>
  </div>
 
<form name="formP" id="formP" method="post" >
     <input type="button" class="btn btn-info" onClick="enviar(1,'formP','<%=ruta%>');" value="Cambiar Datos"  />
     &nbsp;&nbsp;&nbsp;&nbsp;
     <input type="button" class="btn btn-info" onClick="enviar(2,'formP','<%=ruta%>');" value="Cambiar Contrase&ntilde;a"  />
</form>        
 <br /><br /><br />
    <%
              
              if (request.getParameter("estado") != null)
              {
                  String estado = request.getParameter("estado");
                 if (estado.equals("1"))
                 {  out.print(" <div class=\"alert alert-success\">Usuario modificado exitosamente</div>");
                 }
                 if (estado.equals("2"))
                 {  out.print("<div class=\"alert alert-danger\">Usuario no pudo ser modificado</div>");
                 }
              }
              
           %>
 </div>
 <script language="javascript">
     
  function enviar(tipo,form,ruta)
    {
  
        if(tipo == 1)
            {
                document.getElementById(form).action   = ruta + "/Mantenimientos/Usuarios/ModificarUsuarios.jsp";
                document.getElementById(form).submit();
            }
            else
            {
               document.getElementById(form).action   = ruta + "/Mantenimientos/perfil/cambioPass.jsp";
                document.getElementById(form).submit();
            }
        
       
    }
    </script>
<%@include file="../../administracion/footer.jsp" %>